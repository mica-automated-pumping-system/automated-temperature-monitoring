# Analysis Of The AC-Detect System

As of 2023.12.10.

The ac-detect system is really the "internal alarm" connector of the Minus80 Freezer. It does indicate power-on/power-fail, we think it also indicates temperature-fail.

We report the alarm via mqtt as:

* local to the zero-w: `local/commands/publisher/aio-expensive { "topic" : "local/events/minus80/internal-alarm", "value" :  True | False }`
    * to be change to the just the lifted message `local/events/minus80/internal-alarm {1|0}`
* which is re-published to AIO as `feeds/minus80.power {True|False}`
    * which is to be changed to `1|0` for easier graph display.

## Hardware

The internal-alarm is built as a SPDT relay, providing a NO, or a NC, for "ok" (common, NO, NC). Hooked to a GPIO pin, configured as Pull-Up, monitored by `agents/physical-button.py` per `config/agents/physical-button-action.json`. Should be documented in `minus80-hardware.png` and `minus80-system-structure.png`.

We hypothesize that the Internal Alarm is a relay: it is powered when the freezer is plugged in. It doesn't seem to be completely stable when in "no alarm" state (we do a debounce like treatment).

## Failsafe detect

Configured the GPIO as Pull-up, on NC==OK, GND->common, and treating HIGH as "failure", means we are closer to fail-safe:

* We report false-negatives ("fail") more likely than false-positives ("ok")
* We detect "disconnected" faults: wire disconnected/miswired == HIGH due to pull-up
* We detect "internal alarm relay-fail" faults: relay fail (open) == HIGH due to pull-up
* We detect "No Internal Alarm": closed NC==ok pulls signal to LOW.
* We detect "Internal Alarm": not(NC) == HIGH due to pull-up
* We only have to run a GND & Signal to the freezer, which is safer:
    * very low current on the Signal for pull-up (> ~0.3 mA), so accidental shorts are less of a concern
    * GND is safe to short to most external objects, 
    * whereas the +3vcc of the microprocessor would not be safe to short to an external gnd.
* We **can't** distinguish "Internal Alarm is ok" vs "signal and gnd shorted"

If we used pull-down and NO==OK:

* Disconnected wiring, relay fail, mis-wiring, would read as OK (LOW)
* We'd have to run a 3V (vcc) wire.

### AIO alerting

* A "recovery" message should be sent by the "notification" deadman.
* I don't think we have a recovery sent by the "alarm" action.
* a deadman should detect operational failures, which might otherwise prevent a "fail" message.
    * e.g. zero-w internal failures, wifi failures, building/campus power failures, internet infrastructure failures
* A sequence of power-failures could prevent the Minus80-system batteries from charging, and thus no message might be sent on a "second" power fail. The deadman notification would catch this, but possibly only after many hours (rate-limit setting at AIO)


### Redundancy (internet)

The system provides 2 mostly independant internet connections to AIO:

* wifi -> MICA campus -> internet -> AIO
* LTE (cell network) -> internet -> AIO

Probably, the 2 connections share only part of the internet backbone. At least part of the internet pathway near AIO will be the same.

## Reported as inverted GPIO: "Is it ok?"

HIGH is fail, LOW is ok. So we invert in the message payload.

## Ambiquity of Alarm

* The "Internal Alarm" is actually 2 statuses: Power Fail, and Temperature Fail.
    * You have to look at the actual temperature values "minus80-minus80" to tell the difference. Which means you may have to wait several minutes.
* A "fail" my indicate a broken wire/connection rather than true Internal Alarm.
    * You could tell after 15+ minutes of monitoring the temperature ("minus80/minus80") 
    * Or after a few minutes of monitoring wifi/wan/lte-failover status.
* During cool-down of the freezer (from a long power-off), you should see a fail alarm (due to temperature-fail via the Internal Alarm)

## Ambiquity of No-Alarm

* A gnd-signal short would be read as No-Alarm. Overriding actual Internal Alarm.
* A relay-failed to closed would prevent Alarm indication.

Both of these may be tested by unplugging the freezer briefly (poll interval is about 1 minute).

## AIO Configuration

* There is an "Action" for `feeds/minus80.power` for "!= True", to send email, with some rate-limit.
* There is a "Notification" on the topic to send email if "offline" for some period (deadman)
* The publishing from the zero-w has a rate-limit (1/minute) and some anti-flapping protection.
    * and should only send on state change
    * except for a periodic (keepalive) for the AIO deadman

## Remaining Failure modes

The Minus80 system has battery backup for operation for only a limited time (hours).

----

Bugs, defects, etc in the ZeroW hardware, and software.

----

We rely on AIO to report an explicit failure (mqtt message), or deadman (lack of keepalives). It can fail in numerous ways:

* Adafruit business failure.
* Common internet pathways near AIO (from MICA campus, and from LTE cell-network) can fail
* Whatever hosting for AIO can fail (power, sub-systems, etc.)
* The AIO IT system could break in many wonderful ways, including misconfiguration, bugs, etc.

----

Local catastrophes (weather, etc) could disable both MICA->internet and the local LTE-cell system.

