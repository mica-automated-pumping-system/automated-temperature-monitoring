# Lab Automation System

An MQTT based system for building lab-automation.

See also the other projects in [https://gitlab.com/mica-automated-pumping-system](https://gitlab.com/mica-automated-pumping-system) .

## Minus80 alarm

High reliability monitor for a minus-80C freezer (with some supplementary environmental monitoring). The goal is to alert a person, on their cell phone, when the temperature becomes too warm, or the power fails.

Composed of:

See [the BOM](documentation/BOM.ods)

1. **`zerow-minus80/`**, A main microcontroller, multi-process OS

  1. Raspberry Pi Zero W, Raspbian OS
  1. with several sensors,
     1. a cyrogenic thermometer
     1. room temp/humidity (bme680 break-out)
     1. freezer internal alarm pin
  1. several processes for:
     1. reading the sensors
     2. sending values via mqtt to several brokers, including cloud servers
     3. a local mqtt broker
     4. wifi/wan connectivity detection
     5. a failover channel "boron" for mqtt independant of wifi/your-ISP
     6. Reverse SSH tunnel for remote connection
  1. [https://gitlab.com/mica-automated-pumping-system/automated-temperature-monitoring](https://gitlab.com/mica-automated-pumping-system/automated-temperature-monitoring), a python/linux system for mqtt agent
     1. re-used framework elements 
2. **`boron-mqtt-client/`**, A second microcontroller with LTE cell-service for an independent tcp/ip channel

    See `boron-mqtt-client/README.md`

  1. Particle Boron 404x
  1. Provides only an mqtt publish service 
     1. Serial port interface
     1. Future: should also provide a LTE/wan connectivity-detection status
  1. Uses `alarm-framework/` files
3. Battery power backup (UPS like)

4. Optional [USB-C power-detect breakout](https://gitlab.com/LabworkFlows/usb-c-power-detect-bob). If the USB port on the PB1000 (the exposed USB Micro-B port on the system for power) does not track the AC-power from the wall ("mains"), you need some way of detecting if the mains went out. For example, if you had an external powerbank w/passthrough, put this "shim" inline with the powerbank input, and wire it to one of the screw-block inputs. And, modify the configuration files....

### Installation

see the [Pi INSTALL](zerow-minus80/INSTALL.md) document.

### Testing

#### Doesn't require working boron or sensors

Can be done on a non-PI machine.

the local mqtt broker has to be running

    tools/mqtt-broker-local&
    agents/treepublisher.py --expose-boron& # noisy

* `tools/pytest tests/test_publisher.py` # slow

*kill off the mqtt-broker-local if you started it for these tests (see `log/mosquitto.pid`)'

### Minus80 Agents

#### 1. agents/all_services.py 

     all_services.py [-h] [--mqtt-config MQTT_CONFIG] [--mqtt-config-help]
                       [--topics] [--debug-show-incoming]

    Run all the services in agents/all_services/*.py (disable with suffix .disable)
    



##### Listening: localhost:1884

| | |
| --- | --- |
| local/get/services/minus80          | `` Repeats advertisement: local/devices/$device/$name {{name, topic, description...}}`` |
|  | ``                                     Listens on the several topics local/devices[/$device[/$name]]`` |
| local/get/services/minus80/all      | `` Immediately publish current values.`` |
|  | ``                                     They may be Null if not ready.`` |

##### Publishing:

| | |
| --- | --- |
| local/events/minus80/minus80        | `` freezer, celsius`` |
| local/events/minus80/room/humidity  | `` ambient, %`` |
| local/events/minus80/room/temp      | `` ambient, celsius`` |
#### 1. agents/boron_service.py 

     boron_service.py [-h] [--use-stdin] [--dev-service]
                        [--mqtt-config MQTT_CONFIG] [--mqtt-config-help]
                        [--topics] [--debug-show-incoming]

    Run all the services in agents/all_services/*.py (disable with suffix .disable)
    



##### Listening: localhost:1884

| | |
| --- | --- |
| local/commands/minus80/boron-lte-aio/aio-failover/#  | `` Republish local/commands/boron-lte/aio-failover/$user/feeds/$group.$feed`` |
|  | ``                                                      -> io.adafruit.com mqtt topic $user/feeds/$group.$feed`` |
|  | ``                                                      with verbatim payload (needs to be one line)`` |
|  | ``                                                      via Boron-lte`` |
|  | ``                                                      See BoronService.publish() above`` |
| local/get/minus80/boron-lte-aio                      | `` Repeats advertisement: local/devices/$device/$name {{name, topic, description...}}`` |
|  | ``                                                      Listens on the several topics local/devices[/$device[/$name]]`` |
| local/get/minus80/boron-lte-aio/all                  | `` Immediately publish current values.`` |
|  | ``                                                      They may be Null if not ready.`` |

##### Publishing:

| | |
| --- | --- |
| local/events/minus80/boron-lte-aio/connected         | `` BoronService (to AIO) ready, boolean`` |
| minus80.boron-lte-keepalive                          | `` AIO topic keepalive, prefixed with $user/feeds/, sent every 14400, over boron-lte channel`` |
#### 1. agents/network_status.py 

     network_status.py [-h] [--no-ping] [--mqtt-config MQTT_CONFIG]
                         [--mqtt-config-help] [--topics]
                         [--debug-show-incoming]

    Run all the services in agents/network_status/*.py (disable with suffix .disable)
    



##### Listening: localhost:1884

| | |
| --- | --- |
| local/get/minus80/network-status                | `` Repeats advertisement: local/devices/$device/$name {{name, topic, description...}}`` |
|  | ``                                                 Listens on the several topics local/devices[/$device[/$name]]`` |
| local/get/minus80/network-status/all            | `` Immediately publish current values.`` |
|  | ``                                                 They may be Null if not ready.`` |

##### Publishing:

| | |
| --- | --- |
| local/events/minus80/network-status/wan         | `` WAN connectivity 1&#124;0`` |
| local/events/minus80/network-status/wifi        | `` wifi-connected 1&#124;0`` |
| local/events/minus80/network-status/wifi-radio  | `` wifi-enabled 1&#124;0`` |
#### 1. agents/physical-button-action.py 

     physical-button-action.py [-h] [--mqtt-config MQTT_CONFIG]
                                 [--mqtt-config-help] [--topics]
                                 [--debug-show-incoming]

    This agent maps digital-pin transistions (e.g. switches) (on gpio-pins) to actions.
    When the pin-transition happens, we publish the topic with a payload.
    We use edge-detect (GPIO.add_event_detect), but we might miss events
        and we are still polling at 0.1 seconds
    See the configuration and defaults in `Example`.

    **This is all disabled till I figure out a better behavior:**
    Add:
        local/commands/ui/buttoner/add/<pin> { "topic" : "a topic", payload={...} }
        local/commands/ui/buttoner/add/<pin>/<topic...> {payload}
        If you omit the payload, we will send {} FIXME?
        You can have more than 1 topic:payload on a pin.
            but, a duplicate topic:payload is only sent once.
    Remove
        local/commands/ui/buttoner/remove/<pin> { "topic" : "a topic", "payload" : {...} }
        local/commands/ui/buttoner/remove/<pin>/<topic> {payload}
        local/commands/ui/buttoner/remove/<pin> { "topic" : "a topic", "all":True} # removes all payloads for the topic
    List
        local/get/ui/buttoner/pins -> local/events/ui/buttoner/pins [ {pin:x, topic:x},...]
        local/get/ui/buttoner/<pin> -> "" or "topic"

    The assignments of pin->topic are saved, and loaded automatically on startup.
    to state/agents/physical-button-action.json
    **End disabled**

    config/agents/physical-button-action.json
    Example:
        {
            "#" : "You can put in comments using '#' as the key",
            # Each pin gets one pin-setup, and possibly many messages
            "18" : { # physical pin (`GPIO.BOARD`), not "GPIOn" (BCM)
                "#" : "You can put in comments using '#' as the key",
                "pull" : 0, # -1 for pull-down, 0 for none, 1 for pull-up. Default is 1: pull-up.

                "#" : "these parameters are in order of application:

                # Send on which edge-crossing. BEFORE invert
                # Note that for `0` we have to read the pin-value after the edge-detect,
                #   so it might not be what the edge-detect saw.
                # For -1|1 (fall|rise), we know the value, so don't have to read, 
                #   so it will be what edge-detect saw.
                "event" : 0, # -1 for falling, 0 for both, 1 for rising. Default -1: falling

                # send the last-value again, it it has been `keepalive` seconds since the last send
                # Nb: forces an initial read, and publish, of the gpio pin when we start
                # Nb: will be rate-limited
                "keepalive" : 60.0, # default is none: don't do keepalive

                # if rate-limit exceeded, will ignore transistions for `flapping` seconds, and then send current value
                # default is `null`: no rate-limit.
                "rate_limit" : { "max" : 1, "secs" : 10, "flapping" : 60 }, # `flapping` defaults to 10*secs

                # ignore transitions to the opposite of `dir` for `duration` seconds after a transition to `dir`
                # i.e. keep `dir` for at least `duration`. useful for oscillating values that go flatline on "off"
                # so, will not send a message if "during latch duration"!
                # nb: this also suppresses changes during the latch-duration
                #   e.g. given latching.dir=1, events 10101...latch-duration...10 
                #   will send a single True for the first 5 events,  then nothing, then a True for the last 2 events
                #   because the latch-duration started on the first `1`
                # you might want to combine with a rate-limit
                # `dir` is BEFORE invert'ing
                "latch" : { "dir" : 1, "duration" : 0.1 },  # null is default: no latching. dir=0|1

                # send the opposite of the read value. True for typical "closed switch shorts to ground, with pull-up"
                "invert" : false, # default is `false`: no invert

                "as-boolean" : false, # true will force values to `true`/`false`, default values are `1`/`0`

                # will substitute the pin value for $value, or {value:}
                "messages" : [ # default is no-messages. which seems useless.
                    { 
                        "topic": "local/devices/publisher/aio-expensive", 
                        "payload" : "$value" # none->button_value, $value gets substituted, {"value":x} x->button_value
                    }
                ]
            }
        }

    We use GPIO.BOARD numbering. So, it's the actual pin-number not the "gpio" number.

    example debug values
    env DEBUG=2 NOGPIO=1 GPIOVALUES=13:1,1,0,+0.5,0,0,1,0,0.3,0,0.3,0 agents/physical-button-action.py
    



##### Listening: localhost:1884

| | |
| --- | --- |
| local/get/ui/buttoner           | `` Repeats advertisement: local/devices/$device/$name {{name, topic, description...}}`` |
|  | ``                                 Listens on the several topics local/devices[/$device[/$name]]`` |
| local/get/ui/buttoner/+         | `` .../pins -> dict of pin:config`` |
|  | ``                                 .../all -> read and publish each pin value`` |
|  | ``                                 .../$pinnumber -> that pin's config`` |

##### Publishing:

| | |
| --- | --- |
| local/events/#                  | `` # Pin configuration from config/agents/physical-button-action.json`` |
| local/events/pin/usb-power      | `` <13> -> local/events/pin/usb-power = None`` |
| local/events/ui/buttoner/<pin>  | `` Boolean for the pin when it changes`` |
#### 1. agents/treepublisher.py 

     treepublisher.py [-h] [--expose-boron] [--mqtt-config MQTT_CONFIG]
                        [--mqtt-config-help] [--topics]
                        [--debug-show-incoming]



##### Listening: localhost:1884

| | |
| --- | --- |
| local/commands/tree/$tree                     | `` A (re-)publish `tree`.`` |
|  | ``                                               Takes payload={ "topic" : $topic, "payload" : $payload },`` |
|  | ``                                               Re-publishes: $topic : $payload',`` |
|  | ``                                               On several mqtt connections (w/failover),`` |
|  | ``                                               May transform the $topic per connection (e.g. aio).`` |
| local/commands/tree/aio-cheap                 | `` Cheap parallel publisher including aio w/queuing:`` |
|  | ``                                               &#124;&#124;(`` |
|  | ``                                               &#124;1stOf(`` |
|  | ``                                               ?4/10 secs(`` |
|  | ``                                               # Transform topic:per config/aio-topic-mapping.json, annotations 'has aio:' below`` |
|  | ``                                               Then: $resultTopic -> $aiousername/$resultTopic`` |
|  | ``                                               Then:`` |
|  | ``                                               OnMqtt(mqtt-aio) PublisherMQTTByConfig`` |
|  | ``                                               )`` |
|  | ``                                               ?Dumy prints debug PublisherTestOk`` |
|  | ``                                               )`` |
|  | ``                                               &#124;OnMqtt(mqtt-lan) PublisherMQTTByConfig`` |
|  | ``                                               &#124;Dumy prints debug PublisherTestOk`` |
|  | ``                                               )`` |
| local/commands/tree/aio-expensive             | `` Failover from cheap to expensive (incl. queueing):`` |
|  | ``                                               &#124;&#124;(`` |
|  | ``                                               &#124;1stOf(`` |
|  | ``                                               ?if(`not-use-boron?`) then branch is satisfied (usu. `not pred`)`` |
|  | ``                                               ?# Transform topic:per config/aio-topic-mapping.json, annotations 'has aio:' below`` |
|  | ``                                               Then: $resultTopic -> $aiousername/$resultTopic`` |
|  | ``                                               Then:`` |
|  | ``                                               Republish on the boron-lte, via the local/commands/minus80/boron-lte-aio/aio-failover -> agents/boron_service.py`` |
|  | ``                                               Assumes aio-mapping has happened, prefixes the service-topic, embeds the topic/payload`` |
|  | ``                                               ?Dumy prints debug PublisherTestOk`` |
|  | ``                                               )`` |
|  | ``                                               &#124;4/10 secs(`` |
|  | ``                                               # Transform topic:per config/aio-topic-mapping.json, annotations 'has aio:' below`` |
|  | ``                                               Then: $resultTopic -> $aiousername/$resultTopic`` |
|  | ``                                               Then:`` |
|  | ``                                               OnMqtt(mqtt-aio) PublisherMQTTByConfig`` |
|  | ``                                               )`` |
|  | ``                                               &#124;OnMqtt(mqtt-lan) PublisherMQTTByConfig`` |
|  | ``                                               &#124;Dumy prints debug PublisherTestOk`` |
|  | ``                                               )`` |
| local/commands/tree/lan                       | `` LAN publisher w/queing:`` |
|  | ``                                               &#124;&#124;(`` |
|  | ``                                               &#124;1stOf(`` |
|  | ``                                               ?OnMqtt(mqtt-lan) PublisherMQTTByConfig`` |
|  | ``                                               ?Dumy prints debug PublisherTestOk`` |
|  | ``                                               )`` |
|  | ``                                               &#124;Dumy prints debug PublisherTestOk`` |
|  | ``                                               )`` |
| local/commands/tree/local                     | `` local publishing:`` |
|  | ``                                               &#124;&#124;(`` |
|  | ``                                               &#124;Dumy prints debug PublisherTestOk`` |
|  | ``                                               )`` |
| local/events/$device/$blah                    | `` Topics in config/agents/treepublisher.json are republished on a $tree,`` |
|  | ``                                               Noted below as 're-publish to tree $x'`` |
|  | ``                                               Topics in config/aio-topic-mapping.json can be renamed for AIO,`` |
|  | ``                                               Noted below as 'has aio:...'`` |
| local/events/minus80/boron-lte-aio/connected  | `` captures the value to use as a guard for choosing wan vs boron`` |
| local/events/minus80/boron-lte-aio/connected  | `` re-publish to tree aio-cheap`` |
|  | ``                                               has aio: {'topic': 'feeds/minus80.boron-lte', 'payload': {'for': 'boron-lte', 'value': None}}`` |
| local/events/minus80/minus80                  | `` re-publish to tree aio-expensive`` |
|  | ``                                               has aio: feeds/minus80.minus80`` |
| local/events/minus80/network-status/wan       | `` re-publish to tree aio-expensive`` |
|  | ``                                               has aio: feeds/minus80.wan`` |
| local/events/minus80/network-status/wifi      | `` re-publish to tree aio-expensive`` |
|  | ``                                               has aio: feeds/minus80.wifi`` |
| local/events/minus80/room/humidity            | `` re-publish to tree aio-cheap`` |
|  | ``                                               has aio: feeds/minus80.humidity`` |
| local/events/minus80/room/temp                | `` re-publish to tree aio-expensive`` |
|  | ``                                               has aio: feeds/minus80.roomtemp`` |
| local/events/pin/usb-power                    | `` re-publish to tree aio-expensive`` |
|  | ``                                               has aio: feeds/minus80.power`` |
| local/events/wancheck/wancheck1               | `` captures the value to use as a guard for choosing wan vs boron`` |
| local/get/publisher/tree-republisher          | `` Repeats advertisement: local/devices/$device/$name {{name, topic, description...}}`` |
|  | ``                                               Listens on the several topics local/devices[/$device[/$name]]`` |

##### Publishing:

| | |
| --- | --- |
#### 1. tools/agent-mqtt-action.py config/agents/mqtt-action-ssh-tunnel.json

     agent-mqtt-action.py [-h] --topic TOPIC --value VALUE
                            mqtt_config command [arguments ...]


     agent-mqtt-action.py [-h] [--json-help] json_config


     agent-mqtt-action.py [-h] [--mqtt-config MQTT_CONFIG]
                            [--mqtt-config-help] [--topics]
                            [--debug-show-incoming]

    on mqtt topic/value, run command(s).

    Meant to be used like something from agents/, but we require mqtt-config, topic, action arguments.
    So, not a standalone agents/*.

    Note the 2 invocation styles.
    



##### Listening: rhoover@io.adafruit.com:8883 (tls)

| | |
| --- | --- |
| rhoover/feeds/remote-signal  | `` On "8523", run ['/usr/bin/nohup', '/home/awgrover/dev/contract/ryanhoover/minus80/zerow-minus80/tools/ssh-tunnel', '-p', '8523']`` |

##### Publishing:

| | |
| --- | --- |

{::comment}END Minus80 Agents{:/comment}

## **`alarm-framework/`**, Shared files for Arduino/Boron mqtt/service framework

