"""
    @pytest.mark.parameterize('varname',..., [ (tuples-values...),... ]) # optional repeat test with tuple-values for varnames...
    @pytest.mark.$arb # flag for `pytest -m $arb`, only run those tests, `-m "not $arb"` skip tests
    def test_x(fixtures, or-varnames)  # magic prefix `test_`
        tmp_path # automagically gets a temp-dir, a fixture
        $fixture # run the @pytest.fixture $fixture, pass value as this var (recurse arg logic in fixtures!)
        )

    fixtures are "instantiated" once per test_x(), for it's whole tree-dependencies of fixtures
    @pytest.fixture(
        autouse=True # runs `x` for all tests that can see this, i.e. `x` not magically accessible
        scope="module" # once per module (file), not per test_x
            ="session", etc.
        # to repeat a request with different setups, put parms in the fixture:
        # i.e. the test is assertions for the (generic) environment (depends on), and the fixture is each environment
        params=[], # re-invokes the test for request in params, as thisfixture(request) w/request.param
    def x(...) 

    conftest.py
        any @pytest.fixture is visible to all test-files in same dir

"""
import pytest
import time,json

from lib.test_helpers import *

import mqtt_config_reader

class TestBoronService:

    @pytest.mark.parametrize(  "agent", ['boron_service.py'], indirect=True )
    @pytest.mark.behavior
    def test_boron_ready(self, mqtt_connection, mqtt_broker, agent ):
        """Behavior level: publishes a ready message"""
        messages = mqtt_subscribe( mqtt_connection, 'local/commands/publisher/#' )

        # evoke all values, including our target
        mqtt_connection.publish('local/get/minus80/boron-lte-aio/all',None)

        want_payload = {"topic": "local/events/minus80/boron-lte-aio/connected", "payload": True}

        # Actual mqtt response
        assert mqtt_message( messages, want_payload, timeout=2), f"Boron ready via mqtt: did not see {want_payload}"
