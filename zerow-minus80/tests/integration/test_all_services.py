# FIXME: refactor all_services is gone
"""
    @pytest.mark.parameterize('varname',..., [ (tuples-values...),... ]) # optional repeat test with tuple-values for varnames...
    @pytest.mark.$arb # flag for `pytest -m $arb`, only run those tests, `-m "not $arb"` skip tests
    def test_x(fixtures, or-varnames)  # magic prefix `test_`
        tmp_path # automagically gets a temp-dir, a fixture
        $fixture # run the @pytest.fixture $fixture, pass value as this var (recurse arg logic in fixtures!)
        )

    fixtures are "instantiated" once per test_x(), for it's whole tree-dependencies of fixtures
    @pytest.fixture(
        autouse=True # runs `x` for all tests that can see this, i.e. `x` not magically accessible
        scope="module" # once per module (file), not per test_x
            ="session", etc.
        # to repeat a request with different setups, put parms in the fixture:
        # i.e. the test is assertions for the (generic) environment (depends on), and the fixture is each environment
        params=[], # re-invokes the test for request in params, as thisfixture(request) w/request.param
    def x(...) 

    conftest.py
        any @pytest.fixture is visible to all test-files in same dir

"""
import pytest
import time,json

from lib.test_helpers import *

import mqtt_config_reader

class TestSensorService:

    @pytest.mark.integration
    @pytest.mark.parametrize(
        'agent,topic,range', 
        [ 
            ('all_services.py', 'minus80', [-100,40]), 
            ('all_services.py', 'roomtemp', [20,35]), 
            ('all_services.py', 'humidity', [10,90]) 
        ],
        indirect=['agent']
    )
    def test_sensor_ready(self, agent, topic, range, mqtt_connection):
        """Behavior level: publishes a ready message"""
        messages = mqtt_subscribe( mqtt_connection, 'local/commands/publisher/#' )

        # evoke all values, including our target
        mqtt_connection.publish('local/get/services/minus80/all',None)

        want_payload = {"topic": f"local/events/minus80/{topic}", "payload": True}

        low,high = range
        def within_range(message):
            # we expect same form as want_payload!
            #print(f"  ## within? range for {want_payload['topic']} ~ {range} :: {message}")
            if message.startswith('{'):
                message = json.loads(message)

            if isinstance(message,dict):
                if not ( 'topic' in message or message['topic'] == want_payload['topic'] ):
                    #print(f"    ## not our topic: {message['topic'] if 'topic' in message else f'{message.__class__.__name__} {message}'}")
                    return None # skip

                if 'payload' in message:
                    try:
                        v = float(message['payload'])
                        return True if (low <= v and high >= v) else v
                    except ValueError as e:
                        return message # fail
                else:
                    #print(f"## expected a dict with 'payload' for our range: {range}")
                    return message
            else:
                #print(f"    ## not our topic2: {message.__class__.__name__} {message}")
                return None # not ours

        # Actual mqtt response
        assert True==mqtt_message( messages, within_range, timeout=2),f"Sensor {topic} within {range}"
