# FIXME: use the topic dictionary
"""
"""

import pytest
import json

from lib.test_helpers import *

import os,sys
sys.path.append( os.path.dirname( __file__ ) + "/../lib" )
import mqtt_config_reader

class TestPower:
    @pytest.mark.manual
    @pytest.mark.parametrize("ensure_agent_still_running", ['agents/physical-button-action.py'], indirect=True)
    def test_power_change(self, ensure_agent_still_running, mqtt_connection):
        messages = mqtt_subscribe( mqtt_connection, 'local/commands/publisher/#' )

        # Collect current value

        # evoke all values, including our target
        mqtt_connection.publish('local/get/ui/buttoner/all',None)

        def is_our_topic(payload):
            payload = json.loads(payload)
            return payload if (
                isinstance(payload,dict)
                and payload.get('topic',None) == "local/events/pin/freezer-alarm"
            ) else None
        print("## wait for initial value (evoked)")
        msg =  mqtt_message( messages, is_our_topic, timeout=2)
        assert msg,"Saw initial value"

        want = not json.loads(msg)['payload']
        want_payload = {"topic": "local/events/pin/freezer-alarm", "payload": want}
        print(f"## was {want}")

        # Actual mqtt response
        sys.stderr.write(f"## Change the power-detect connection (was {not want}), 10 seconds...\n")
        assert mqtt_message( messages, want_payload, timeout=10),f"Power changed to {want} via mqtt"

