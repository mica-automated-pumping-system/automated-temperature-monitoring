import pytest
import re
import sys

import subprocess,time
from lib.test_helpers import *


class TestBoron:
    
    @pytest.fixture(scope='module')
    def running_boron_agent(self):
        """runs the agent,
            returns stdout (stderr is combined with it)
        """
        agent = "agents/boron_service.py"

        print(f"Staring {agent}") # -s doesn't seem to work
        process = subprocess.Popen(
            [ agent, '--use-stdin', '--mock-boron-ready' ],
            stdout=subprocess.PIPE, stderr=subprocess.STDOUT, stdin=subprocess.PIPE
        )
        time.sleep(1)
        assert None==process.poll(), f"Didn't start {agent}\n"+process.stdout.read().decode('utf-8') + process.stderr.read().decode('utf-8')
        print(f"Running {process.pid}")
        subprocess.run(['ps','w','-p',str(process.pid)])

        yield process.stdout

        print(f"Terminating {process.pid} {agent}...")
        process.terminate()

    def test_serial_publish(self, running_boron_agent, mqtt_connection):
        """Show that a mqtt message is sent to the boron on the serial port"""
        print("In Test")

        boron_out = running_boron_agent

        value = -80.9
        boron_topic = 'local/commands/minus80/boron-lte-aio/aio-failover'
        aio_topic = 'minus80.minus80' # feeds/ prefix is assumed
        mqtt_connection.publish( f"{boron_topic}/{aio_topic}", value )
        with Timeout(seconds=10):
            print("---boron")
            l=''
            while not re.search('Mock_ready!',l):
                l = boron_out.readline().decode('utf-8')
                #print(l)
            print(l)
            print(f'--- lookfor: $Pub${aio_topic} "{value}"')
            while True:
                l = boron_out.readline().decode('utf-8')
                #print(l)
                if l.startswith(f'$Pub${aio_topic} {value}'):
                    print(f"Saw {l}")
                    break
