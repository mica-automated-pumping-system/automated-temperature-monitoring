"""
"""
import pytest
import time,json,os
from random import randrange

from lib.test_helpers import *

# send message on one channel, get message on another

@pytest.mark.parametrize(  "agent", [ {'treepublisher.py': '--expose-boron'} ], indirect=True )
class TestPublisher:
    """testing agents/treepublisher.py"""

    @pytest.fixture(autouse=True,scope="class")
    def setup_rate_count(self):
        self.__class__._rate_count = 0

    def rate_limit_aio(self, ct=1, clear=False):
        # aio is rate limited
        # Call this BEFORE each publish that may go to aio
        # `clear`=True forces pause for ct seconds, and resets count
        n=4
        secs = 10
        self.__class__._rate_count += ct
        if self.__class__._rate_count > n or clear:
            print(f"## Rate limit hit, waiting @ {self.__class__._rate_count}/{secs}...")
            time.sleep(secs)
            self.__class__._rate_count = 0

    def republish_test(self, publish_connection, subscribe_connection, inout ):
        # `inout` is an entry from e.g. message_sequences_aio['tree_mapped:boron_serial']['treepublisher.py']
        # try: tools/pytest -rA  tests/lib/message_sequences.py

        pub_t,pub_p = next(iter(inout['in'].items()))
        sub_t,sub_p = next(iter(inout['out'].items()))
        return self._republish_test( publish_connection, subscribe_connection,
            pub_t, pub_p,
            sub_t, sub_p
        )

    def _republish_test(self, 
        publish_connection, 
        subscribe_connection, 
        publish_topic, 
        publish_payload, # publish payload (usu. subscribe==)
        subscribe_topic,
        subscribe_payload = None # assumes `publish_payload` override if NOT publish==subscribe
    ):
        if subscribe_payload == None:
            subscribe_payload == publish_payload

        print(f"Republish test: {publish_topic}:{publish_payload} -> {subscribe_topic}:{subscribe_payload}")

        messages = mqtt_subscribe( subscribe_connection, subscribe_topic)

        # send
        publish_connection.publish(
            publish_topic,
            json.dumps(publish_payload)
        )

        want_payload = subscribe_payload

        # Actual mqtt response
        # excessive timeout for aio roundtrip
        print(f"Asserting...")
        try:
            assert subscribe_payload == (actual:=mqtt_message( messages, want_payload, timeout=2)), f"Saw our value ({publish_payload['payload']}) at aio ({subscribe_topic})"
        except TimeoutError as e:
            # fixme: ._host is .host, same for ._port in newer paho
            #print(f"### .st {e.strerror} || {e}")
            #e.strerror += f", for topic '{subscribe_topic}' on {subscribe_connection._host}:{subscribe_connection._port}"
            print(e)
            raise TimeoutError(f"{e}, for topic '{subscribe_topic}' on {subscribe_connection._host}:{subscribe_connection._port}") from e
        finally:
            subscribe_connection.unsubscribe(subscribe_topic)
        return actual

    def if_for_expensive(xx):
        """Filter for aio_topic_mapping_short_names, only things for the ai-expensive tree (via treepublisher_topic_mapping)"""
        global _if_for_expensive
        print(f"## XX {xx}")
        if '_if_for_expensive' not in globals():
            want = [ k for k,v in treepublisher_topic_mapping().items() if v == 'aio-expensive' ]
            _if_for_expensive = lambda t : t in want
        return _if_for_expensive

    @pytest.mark.parametrize("aio_short", aio_topic_mapping_short_names(if_for_expensive) )
    @pytest.mark.behavior
    def test_aio_forwarding(self, agent, mqtt_connection, aio_connection, message_sequences_sensor_to_aio, aio_short): #, message_sequences_sensor_to_aio):
        # try tools/pytest --co tests/test_publisher.py # for names

        time.sleep(0.5)
        if aio_short == 'connected':
            pytest.skip("No good way to parametrically test the dict value")

        self.rate_limit_aio(2)

        # force where it goes (aio via wan)
        self.inform_preds(mqtt_connection, wan=True, boron=True )

        in_out = message_sequences_sensor_to_aio[ aio_short ]

        self.rate_limit_aio(1)
        self.republish_test(
            mqtt_connection, aio_connection,
            in_out
        )

    def inform_preds(self, mqtt_connection, wan, boron ):
        mqtt_connection.publish( 'local/events/minus80/network-status/wan', json.dumps(wan) )
        mqtt_connection.publish( 'local/events/minus80/boron-lte-aio/connected', json.dumps(boron) )

    # not system test
    def test_to_boron_if_it_should(self, mqtt_connection, agent, message_sequences_aio ):
        # send msg to treepublisher, see if it appears on local for boron-lte
        # we do the direct test on `--expose-boron`, then on the `aio-expensive` tree
        # We tell it wan=bad,boron=good first (could get race w/actual agents that check that)

        self.rate_limit_aio(2)
        self.inform_preds(mqtt_connection, wan=False, boron=True)

        self.rate_limit_aio(1)
        rez = self.republish_test(
            mqtt_connection, 
            mqtt_connection, 
            message_sequences_aio['boron_test:boron_serial']['treepublisher.py']
        )
        print(f"Saw {rez}")

        # Now test the expensive tree's logic, w/mapping

        # convince it to use boron
        self.inform_preds(mqtt_connection, wan=False, boron=True)

        rez = self.republish_test(
            mqtt_connection, 
            mqtt_connection, 
            message_sequences_aio['tree_mapped:boron_serial']['treepublisher.py']
        )
        print(f"Saw {rez}")

    @pytest.mark.parametrize(  "boron_ok", [ True, False ] )
    # not system test, but slow
    def test_to_aio_if_wan(self, mqtt_connection, agent, boron_ok, message_sequences_aio ):
        # send msg to treepublisher, see if it appears on local
        # We tell it wan=good,boron=good|bad first (could get race w/actual agents that check that)

        self.rate_limit_aio(clear=True) # ensure we are ready for the following 2

        # has wan, boron shouldn't matter
        # (this is republished to aio!, so rate-limits matter)
        self.inform_preds(mqtt_connection, wan=True, boron=boron_ok)

        # fixme? the rate-limit is painful/brittle
        # we have to wait for many seconds
        # and we have to get the `ct` argument right
        self.rate_limit_aio(2, clear=True)

        # should see aio (local)
        print(f"local --- wan true bok {boron_ok}")
        rez = self.republish_test(
            mqtt_connection, 
            mqtt_connection, 
            message_sequences_aio['expensive:io.adafruit.com']['treepublisher.py'],
        ) 
        print(f"Saw {rez}")

        # And, we shouldn't see a boron-lte message (because wan is true)

        self.inform_preds(mqtt_connection, wan=True, boron=boron_ok)

        self.rate_limit_aio(3, clear=True) # the count of above publish's

        with pytest.raises(TimeoutError): # Because wan=True, so never see the "via boron-lte" message
            print(f"## should timeout:")
            rez = self.republish_test(
                mqtt_connection, 
                mqtt_connection, 
                message_sequences_aio['expensive:boron_serial']['treepublisher.py'],
            )
            print(f"Saw, but shouldn't, boron-lte {rez}")

    # nb: if both boron & wan are marked as bad, it still tries to publish on local, which is fine

