# FIXME: use the topic dictionary
"""
"""

import pytest
import json

from lib.test_helpers import *

import os,sys
sys.path.append( os.path.dirname( __file__ ) + "/../lib" )
import mqtt_config_reader

class TestPower:
    @pytest.mark.behavior
    # hackish: hardcoded the pin value. sad
    # forced pin value is inverted by the physical pin setup, so 0 -> 1
    @pytest.mark.parametrize("agent", [ {'physical-button-action.py':{'env':{'GPIOVALUES':'13:0'}}} ], indirect=True)
    def test_power_good(self, mqtt_connection, agent ):
        """Behavior level: checks that power is good"""

        messages = mqtt_subscribe( mqtt_connection, 'local/events/pin/freezer-alarm' )

        # evoke all values, including our target
        mqtt_connection.publish('local/get/ui/buttoner/all',None)

        want_payload = "1"

        # Actual mqtt response
        assert mqtt_message( messages, want_payload, timeout=2),"Power True via mqtt"
