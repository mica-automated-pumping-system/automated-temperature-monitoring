"""
    def test_x(  # magic prefix `test_`
        tmp_path # automagically gets a temp-dir, a fixture
        $fixture # run the @pytest.fixture $fixture, pass value as this var (recurse arg logic in fixtures!)
        )

    fixtures are "instantiated" once per test_x(), for it's whole tree-dependencies of fixtures
    @pytest.fixture(
        autouse=True # runs `x` for all tests that can see this, i.e. `x` not magically accessible
        scope="module" # once per module (file), not per test_x
            ="session"
    def x(...) 

    conftest.py
        any @pytest.fixture is visible to all test-files in same dir

"""
import pytest
import serial
import os,re,time,signal,json
from datetime import datetime
import psutil

import paho.mqtt.client as paho
import ssl

import os,sys
sys.path.append( os.path.dirname( __file__ ) + "/../lib-pump-pi" )
sys.path.append( os.path.dirname( __file__ ) + "/../lib" )
import mqtt_config_reader

from lib.message_sequences import *

@pytest.fixture(scope="session")
def mqtt_config():
    connections = mqtt_config_reader.connection( 'config/mqtt-multi-config.py' )
    by_key = {}
    for i,config in enumerate(connections):
        by_key[i] = config
        by_key[ config['key'] ] = config
        del config['key']
    return by_key

@pytest.fixture(scope='session')
def aio_connection(mqtt_config):
    """ setup the subscription: we use the same config/mqtt-multi-config.py['cloud0'] as the agents """
    aio_config = mqtt_config['cloud0']
    c = mqtt_connect( aio_config )
    yield c
    c.disconnect()

def mqtt_connect( some_mqtt_config ):
    init_args = { k:some_mqtt_config[k] for k in ['client_id','clean_session','userdata','protocol','transport','reconnect_on_failure','manual_ack'] if k in some_mqtt_config }
    mqtt = paho.Client( callback_api_version = paho.CallbackAPIVersion.VERSION2, **init_args )

    up_args = { k:some_mqtt_config[k] for k in ['username','password'] if k in some_mqtt_config }
    if up_args:
        #print(f"userpass {up_args}")
        mqtt.username_pw_set( **up_args )

    if 'tls_params' in some_mqtt_config:
        #print(f"tls {some_mqtt_config['tls_params']}")
        tls_args = { k:getattr(some_mqtt_config['tls_params'],k) for k in some_mqtt_config['tls_params'].__dict__ }
        if not tls_args.get('cert_reqs',None):
            tls_args['cert_reqs'] =ssl.CERT_REQUIRED
        if not tls_args.get('tls_version',None):
            tls_args['tls_version'] = ssl.PROTOCOL_TLS_CLIENT
        #print(f"tls: {tls_args}")
        mqtt.tls_set( **tls_args )

    connect_args = { k:some_mqtt_config[k] for k in ['hostname','port','keepalive','clean_start'] if k in some_mqtt_config }
    connect_args['host'] = connect_args['hostname'] # required
    del connect_args['hostname']
    #print(f"ca: {connect_args}")

    rez = mqtt.connect( **connect_args )
    print(f"## MQTT to {some_mqtt_config.get('username',None)}@{some_mqtt_config['hostname']}{'<tls>' if 'tls_params' in some_mqtt_config else ''}:{some_mqtt_config.get('port','<default>')} -> {rez}")
    assert 0==rez,f"paho.connect gave error {rez}"
    
    return mqtt

def aio_subscribe(some_mqtt_config, topic ):
    """ setup the subscription """
    mqtt = mqtt_connect(some_mqtt_config)
    messages = []
    def subscriber(mqtt, userdata, message):
        # we just collect them, aio_messages reacts to them
        #print(f"## MQTT {message.topic}={message.payload}")
        messages.append(str(message.payload.decode("utf-8")))

    mqtt.on_message = subscriber
    mqtt.subscribe(topic, qos=1)
    mqtt.loop_start()
    return (mqtt_client,messages)

@pytest.fixture() # scope="session")
def mqtt_connection(mqtt_broker, mqtt_config ):
    """ setup the connection to the default local broker """
    c = mqtt_connect( mqtt_config[0] )
    yield c
    c.disconnect()

def mqtt_subscribe(mqtt_config, topic ):
    """ setup the subscription """
    mqtt = mqtt_connect( mqtt_config[0] )

    messages = []
    def subscriber(mqtt, userdata, message):
        # we just collect them, aio_messages reacts to them
        #print(f"## MQTT {message.topic}={message.payload}")
        messages.append(str(message.payload.decode("utf-8")))

    mqtt.on_message = subscriber
    mqtt.subscribe(topic, qos=1)
    mqtt.loop_start()
    return (mqtt,messages)

@pytest.fixture(scope="session")
def aio_userpass():
    return aio_userpass_data()

# Environment checks as fixtures

@pytest.fixture(scope='session')
def mqtt_broker():
    pid_file = f"log/mosquitto.pid" # for tools/mqtt-broker-local
    assert os.path.exists(pid_file), f"Need tools/mqtt-broker-local, don't see {pid_file}"

    with open(pid_file,'r') as fh:
        pid = int( fh.readline() )
    try:
        process_name = psutil.Process(pid).name()
    except psutil.NoSuchProcess:
        pytest.fail("Need toolls/mqtt-broker-local running")

    assert process_name=='mosquitto', "Expected mosquitto for {pid_file}"

@pytest.fixture()
def agent( request ): 
    """parameterize me: @pytest.mark.parametrize(  "agent", [
       '$agentname', ... # run the TEST for each agent
       [ '$agentname','$agentname', ] # require multiple agents for A test
       { '$agentname' : '--one-arg', ... } # require multiple agents for A test, provide one cli option
       { '$agentname' : [ '--one-arg', ...] , ... } # require multiple agents for A test, provide multiple cli args
       # args and env, and other fn-parameters for agent
       { '$agentname' : {
            'argv' : [ '--one-arg', ...], 
            'env' : { $var:$val, },
            'start_indication' : r' Start loop \\(last async\\) LocalListener', # wait for this in the agents stderr
            }, 
            ...  # require multiple agents for A test, provide multiple cli args
        }
     ]
     Make sure an agent(s) is running (with arguments)
        we don't check that the env vars are set as specified
     remember [ a, b, c ] parameterization runs the TEST 3 times
       but [ [a,b,c] ] passes 3 values as our .param
     name is the agent/$name part
    """

    print(f"--- request {request.param}")

    start_indication = r' Start loop \(last async\) LocalListener' # default stderr from agent.py, when loop starts

    name = request.param
    # normalize `name` to [ {name:[args]} ] form
    # normalize `name` to [ {name:{argv:args,env:vars]} ] form
    if isinstance(name, str): # 'name'
        #print(f"  --- entry single str {name}")
        name = {name:{'argv':[],'env':{}}}
    elif isinstance(name, list): # [ ... ]
        new_name = {}
        for a in name:
            # normalize .value of each to [ args... ]
            if isinstance(a,str): # [ 'name', ... ]
                #print(f"  --- entry str {a}")
                new_name[a] = {'argv':[],'env':{}}
            else:
                raise Exception(f"Expected `name` to be [ str, {{n:args,}}, ... ]: saw {a.__class__.__name__}::{a}, in {name}")
        name = new_name
    elif isinstance(name,dict): # { 'name' : ..., ... }
        for n,args in list(name.items()):
            # normalize args to [...], assume it is just str otherwise
            print(f"  --- entry {name}, {args}")
            if isinstance(args,dict):
                if 'start_indication' in args:
                    start_indication = args.pop('start_indication')
                name[n] = args # has argv,env in it
                print(f"  --- entry dict {name[n]}")
            elif isinstance(args,list):
                name[n] = {'argv':args,'env':{}}
                print(f"  --- entry list {name[n]}")
            elif isinstance(args,str):
                name[n] = {'argv':[args],'env':{}}
                print(f"  --- entry str {name[n]}")
            else:
                raise Exception(f"Expected {name} to each be {{arg:value,...}} OR [ arg,... ], saw {{ {n} : {args.__class__.__name__}::{args} }}")
    else:
       raise Exception(f"Expected `name` to be str, [str,...], {{name:args,...}}. saw {name.__class__.__name__}::{name}")

    print(f"--- `agent` wanted {name}")
    hits = {}
    almost = {}

    # find them all...
    for proc in psutil.process_iter():
        try:
            #print(f"Proc {proc.pid} {proc.name()} | {proc.cmdline()}") # name is cmd
            if proc.name().startswith('python'): # FIXME: factor this for all scripts
                args = proc.cmdline()
                #print(f"  --- ? {proc.pid} {proc.name()} {args}")
                for n,agent_args in name.items():
                    if f"agents/{n}" in args or n in args:
                        #print(f"   --- found {n}... as proc")
                        if len(set(agent_args) & set(args)) == len(agent_args):
                            hits[n] = agent_args # need to make sure we saw all by name
                            print(f"  --- found {proc.pid} {proc.name()} {args}")
                            #print(f"   --- found {n} {agent_args} as proc")
                        else:
                            almost[n] = args[2:]
        except psutil.ZombieProcess:
            pass # ignore
    if len(hits) == len(name):
        #print(f"  --- found all {name}")
        yield
    else:
        
        def launch_one(path, args, env):
            windication = f"waiting for stderr: {start_indication}" if start_indication else 'any stderr output'
            with Timeout(error_message=f"waiting for launch: {agent}, {windication}", seconds=5):
                with tempfile.NamedTemporaryFile('w', prefix='ensure_agent_still_running-', delete=False) as fh:
                    fh.write(f"# Started {agent}\n\n")

                    print(f"    --- Starting {path} {args} env {env}")

                    agent_proc = subprocess.Popen(
                        [path, *args],
                        env=env.update(os.environ),
                        #stdout=fh,
                        stderr=fh,
                        close_fds=True
                    )
                    #sys.stderr.write(f"> {agent_proc.poll()}");
                    # takes a bit to be ready to communicate
                    while agent_proc.poll() == None:
                        #sys.stderr.write(f"{fh.tell()}");
                        fh.flush()
                        # on any stdout -> implies it started
                        if fh.tell():
                            break;
                        time.sleep(0.1)

                    if start_indication:
                        # Wait for the indicator string in the process's output
                        # Weirdly, this works on the already open temp file
                        print(f"        saw stderr bytes: {fh.tell()}, so...")
                        with open(fh.name,'r') as monitor:
                            while True:
                                l = monitor.readline()
                                #if l != None and l != '':
                                #    sys.stderr.write("> ")
                                #    sys.stderr.write(l)
                                #    sys.stderr.flush()
                                if re.search(start_indication, l):
                                    sys.stdout.write("> ")
                                    sys.stdout.write(l)
                                    sys.stdout.flush()
                                    break

                    else:
                        # try to ensure it got to connect/subscribe
                        time.sleep(0.1)

                    if agent_proc.poll() != None:
                        pytest.fail(f"## Didn't run: {path}. See {fh.name}")

                    # Ok, appears to have started

                    fh.close()
                    os.unlink( fh.name )
                    print(f"    --- Started: {path}. See {fh.name}")
                    return agent_proc

        missing = { x:name[x] for x in name.keys() - hits.keys() }
        msg = f"Need to run the agent(s) for: {missing}, only saw currently running: { {**hits, **almost}}. maybe need args?"
        print(f"   --- {msg}")
        #print(f"## mi {missing.items()}")

        processes = [] # when we have an exception
        try:
            processes = [ launch_one(f'agents/{agent}', info.get('argv',[]), info.get('env',{})) for agent,info in missing.items() ]
            #print(f"## tried processes {processes}")
            yield
        finally:
            for proc in processes:
                if proc.poll() == None:
                    proc.terminate()
                    print(f"    --- killed {proc.args}")
                    sys.stdout.flush()
