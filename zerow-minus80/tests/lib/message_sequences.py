import pytest
from random import randrange
import os,sys

from lib.test_helpers import *

"""
These fixtures are meant as the single source of truth for mqtt-topics, for testing.
Don't use a mqtt-topic string in your tests, search for it here, and add as necessary.

Some fixtures return a sequence of messages,
so you can do a publish, and test the subscribe'd result to be as expected.

I.e. these fixtures are a single source of truth; no topic/payload repeating in your tests.

To show the values/sequences that we make, try:

    tools/pytest -rA  tests/lib/message_sequences.py

Message sequences
    declare with _inout_fixup:
      return _inout_fixup(
          $agent : { $topic : $payload }, # means "this $agent will send the $topic:$payload"
      )
    It will be fixed up to, which is what you'll get when you use the fixture. select by $agent for publishing/subscribing 
      { $agent : { "in" : None|$previous['out'], "out" : { $topic:$payload } }, ... }

You probably only need fixture `message_sequences_aio`,
then select the sequence from it (show the values as above).

"""

def _inout_fixup( msg_seq ):
    """change topic:payload to in:{topic:payload},out:$next"""
    new_msg_seq = {} # we construct a new sequence, so shouldn't mutate others

    #print(">>>")
    # change to 'out['
    agent_msg = list(msg_seq.items()) # stable order, we hope
    for msg_seq_i in range(0, len(msg_seq)):
        agent,msg = agent_msg[msg_seq_i]  # topic:payload or in:topic:payload,out:topic:payload
        if not(isinstance(msg,dict)) or 'out' not in msg:
            new_entry = { 
                "out" : msg, # we trust it is a single topic:payload dict
            }
            new_msg_seq[ agent ] = new_entry
        else:
            new_msg_seq[ agent ] = msg
        #print(f"entry[{msg_seq_i}] {agent}:{new_msg_seq[agent]}")

    #print("---")
    # add 'in'
    agent_msg = list(new_msg_seq.items()) # stable order, we hope
    for msg_seq_i in range(1, len(new_msg_seq)):
        # we work on [ msg_seq_i ]
        agent,msg = agent_msg[msg_seq_i-1]  # in:topic:payload,out:topic:payload
        next_agent,next_msg = agent_msg[ msg_seq_i ]
        if not(isinstance(next_msg,dict)) or 'in' not in next_msg:
            next_msg['in'] = msg['out']
        #print(f"entry[{msg_seq_i}] {next_agent}:{next_msg}")
        
    return new_msg_seq

@pytest.fixture()
def help_message_sequence():
    """For more info, see this file's docstring, and try: tools/pytest -rA  tests/lib/message_sequences.py"""

# x...() fixtures are meant to be used internal to this module

# topic lists, by purpose ("..s")
@pytest.fixture() # leave at test scope in case someone mocks the configs
def message_sequence_topics(aio_userpass):
    """The topics, single source of truth. See this file's docstring.
        See also message_sequences_sensor_to_aio()
    """
    topics = {
        'local' : 'local/events/minus80/minus80',
        'tree_expensive' : 'local/commands/tree/aio-expensive',
        'tree_boron_test' : 'local/commands/tree/boron-test',
        'aio' : f"{aio_userpass['IO_USERNAME']}/feeds/minus80.minus80", # FIXME: from config aio mapping
    }
    topics['aio_base'] =  os.path.basename( topics['aio'] )
    topics['boron_lte'] = f'local/commands/minus80/boron-lte-aio/aio-failover/{topics["aio_base"]}'
    topics['serial'] = f"$Pub${topics['aio_base']}"

    return topics

@pytest.fixture()
def message_sequence_temperature(base=-80):
    """Just the temperature, each test gets a different one (but all topic/payloads have the same, for a test)."""
    # will always have a fractional part, and no 0's in fractional part
    return float(f"{base}." + "".join( ( str(randrange(1,10)) for d in range(0,3) ) ))

# choices for each step ("or")

@pytest.fixture()
def xmessage_sequence_by_service(message_sequence_temperature, message_sequence_topics):
    """Choices for published topic:payload, single source of truth (internal use). See this file's docstring."""
    return {
        'all_services.py' : { message_sequence_topics['local'] : message_sequence_temperature },
        # not actually publishes this, but "internal" as if:
        'tree_for_boron' : { message_sequence_topics['boron_lte'] : message_sequence_temperature },
        'boron_service.py' : { message_sequence_topics['aio']: message_sequence_temperature }, # FIXME: from config aio mapping
        'boron_serial' : { message_sequence_topics['serial']: message_sequence_temperature },
        'io.adafruit.com' : { message_sequence_topics['aio']: message_sequence_temperature }, # FIXME: from config aio mapping
        'aio_base' : { message_sequence_topics['aio_base']: message_sequence_temperature },  # FIXME: from config aio mapping
    } 

@pytest.fixture()
def xmessage_sequence_treemapped_or_expensive_or_borontest(message_sequence_temperature, message_sequence_topics):
    """Choices for topic:payload, single source of truth (internal use). See this file's docstring."""

    wrapped = { "topic" : message_sequence_topics['local'], "payload" : message_sequence_temperature }
    return {
        'tree_mapped' : { message_sequence_topics['local'] : message_sequence_temperature },
        'expensive' : { message_sequence_topics['tree_expensive'] : wrapped },
        # aka, "exposed":
        'boron_test' : { message_sequence_topics['tree_boron_test'] : wrapped },
    }

@pytest.fixture()
def message_sequence_start(message_sequence_temperature, message_sequence_topics):
    """Sequence start, single source of truth (internal use). See this file's docstring."""
    return _inout_fixup(
        { 'all_services.py' : { message_sequence_topics['local'] : message_sequence_temperature } }
    )

@pytest.fixture()
def message_sequences_aio(message_sequence_temperature, message_sequence_topics, xmessage_sequence_treemapped_or_expensive_or_borontest, xmessage_sequence_by_service):
    """Possible sequences, named by how-it-uses-treepublisher + ultimate-destination. See this file's docstring.
    You probably only need this fixture."""
    # all-services,tree mapped|tree aio-expensive|boron-test,boron_serial|adafruit

    sequences = {}

    all_services = { 'all_services.py' : xmessage_sequence_by_service['all_services.py'] }
    tree_for_boron = { "treepublisher.py" : xmessage_sequence_by_service['tree_for_boron'] }
    boron_service = { 'boron_service.py' : xmessage_sequence_by_service['tree_for_boron'] }
    boron_serial = { 'boron_serial' : xmessage_sequence_by_service['boron_serial'] }
    boron_lte = { 'boron_lte' : xmessage_sequence_by_service['io.adafruit.com'] }
    io_adafruit_com = { 'io.adafruit.com' : xmessage_sequence_by_service['io.adafruit.com'] }

    sequences['tree_mapped:boron_serial'] = _inout_fixup(
        {
        **all_services,
        **tree_for_boron,
        **boron_service,
        **boron_serial,
        **boron_lte,
        **io_adafruit_com,
        }
    )
    sequences['tree_mapped:io.adafruit.com'] = _inout_fixup(
        {
        **all_services,
        "treepublisher.py" : xmessage_sequence_by_service['io.adafruit.com'],
        **io_adafruit_com,
        }
    )
    

    for tree_step in [ 'boron_test', 'expensive' ]:
        sequences[f'{tree_step}:boron_serial'] = _inout_fixup(
            {
            None : xmessage_sequence_treemapped_or_expensive_or_borontest[tree_step],
            **tree_for_boron,
            **boron_service,
            **boron_serial,
            **boron_lte,
            **io_adafruit_com,
            }
        )
        sequences[f'{tree_step}:io.adafruit.com'] = _inout_fixup(
            {
            None : xmessage_sequence_treemapped_or_expensive_or_borontest[tree_step],
            'treepublisher.py' : xmessage_sequence_by_service['io.adafruit.com'],
            # via the aio mqtt connection from treepublisher
            'io.adafruit.com' : xmessage_sequence_by_service['io.adafruit.com']
            }
        )
    return sequences


@pytest.fixture()
def message_sequences_sensor_to_aio(): # message_sequence_topics, message_sequences_aio):
    """By shortname of the topic, 'in' is what the sensor sends, 'out' is what a aio-feed subscribe sees.
        We get our topic/payloads from various fixtures.
        This is not a sequence, but a list of single entries.
        Cf.  message_sequences_aio_short_names() for parameterizing etc
    """

    def _make_inout(name, in_topicpayload, out_topicpayload ):
        in_out = {
            'in' : in_topicpayload,
            'out' : out_topicpayload
        }
        return { name : in_out }

    def unique_value(base):
        # adds 3 digits after decimal
        return float(f"{base}." + "".join( ( str(randrange(1,10)) for d in range(0,3) ) ))

    io = {}
    for local_topic, aio_topic in aio_topic_mapping().items():
        short = os.path.basename( local_topic ) # FIXME: duplicates aio_topic_mapping_short_names()
        #print(f"{short}: {local_topic} : { (v:=unique_value(1)) }, { aio_topic} : {v } ")
        if isinstance( aio_topic, dict):
            aio_topic = aio_topic['topic']
            # we discard the payload pattern!
        io.update( _make_inout( short, {local_topic : (v:=unique_value(1)) }, { aio_topic : v } ) )
    #print(f"## IO {io}")
    return io

    
if os.path.abspath(sys.argv[-1]) == os.path.abspath( __file__ ):
    # i.e. if we are the last argument
    # Show fixture values
    print("### DO SHOW")
    import json

    def test_show(
        message_sequence_topics,
        message_sequence_temperature,
        xmessage_sequence_by_service,
        xmessage_sequence_treemapped_or_expensive_or_borontest,
        message_sequences_aio,
        message_sequences_sensor_to_aio,
        ):
        # name:topic list, and values
        print("message_sequence_topics =", json.dumps(message_sequence_topics, sort_keys=True, indent=4))
        print("message_sequence_temperature =", message_sequence_temperature)

        # name:topic:payload choices ("or")
        print("xmessage_sequence_by_service =", json.dumps(xmessage_sequence_by_service, sort_keys=True, indent=4))
        print("xmessage_sequence_treemapped_or_expensive_or_borontest =", json.dumps(xmessage_sequence_treemapped_or_expensive_or_borontest, sort_keys=True, indent=4))

        def _deb_seq(name, seq,indent=0):
            """Pretty print a sequence, so you can see if `out` matches next `in`"""
            if name:
                print(f"{'    '*indent}{name} = ")

            indent+=1
            for agent,msg in seq.items():
                print( "    "*indent,msg['in'] if 'in' in msg else '<no-in: start>', "->", agent if agent else '<no-agent inject>', "->" )
                print( "    "*indent,msg['out'] )

        # sequences
        print("message_sequences_aio =")
        for choice,sequence in message_sequences_aio.items():
            _deb_seq(choice,sequence,1)

        print("message_sequences_sensor_to_aio =")
        for choice,sequence in message_sequences_sensor_to_aio.items():
            _deb_seq(None,{choice:sequence}) # not the best rendering
