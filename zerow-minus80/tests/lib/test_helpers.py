import pytest,psutil
import time,signal,json,os,re,subprocess,sys,tempfile
from numbers import Number

def mqtt_message( messages, pattern, timeout=5 ):
    """ 
        matches on pattern 
        consumes any messages seen since mqtt_subscribe, till match
        return False or the payload
        pattern:
            
    """
    started = time.monotonic()
    first_time = True

    def dict_contains( adict, parts ):
        # true if all parts match
        for want_key,want_value in parts.items():
            if not (want_key in adict and adict[want_key] == want_value):
                return False
        return True

    try:
        with Timeout(error_message=f"waiting for {pattern}", seconds=timeout):
            while(True):
                if messages: # actually, endless stream, so will timeout
                    msg = messages.pop(0)
                    #print(f"   test {msg} vs {pattern}")
                    if rez:=(callable(pattern) and pattern(msg)):
                        #print(f"## Test against message {msg.__class__.__name__} {msg} w/lambda {pattern}")
                        if rez != None: # None skips
                            #print("  hit: callable")
                            return rez
                        else:
                            print(f">>discard none: {msg} {msg.__class__.__name__}")
                    elif isinstance(pattern,Number) and float(msg) == pattern:
                        #print("  hit: float")
                        return float(msg)
                    elif isinstance(pattern,str) and msg.startswith(pattern):
                        #print("  hit: str")
                        return msg
                    elif isinstance(pattern, dict) and dict_contains( json.loads(msg), pattern):
                        #print("  hit: dict")
                        return msg
                    else:
                        print(f">>discard (!= {pattern}): {msg} {msg.__class__.__name__}")
                    sys.stdout.flush()

                time.sleep(0.1)
                if time.monotonic() - started > 0.5 and first_time:
                    print(f"## waiting for mqtt '{pattern}...'")
                    first_time=False
    except TimeoutError as e:
        print("## timeout")
        raise e

class Timeout:
    """ 
        try:
            with Timeout(seconds=6):
                stuff
        except TimeoutError as e:
            raise AssertionError("unexpected " + str(e))

    """
    def __init__(self, seconds=1, error_message='Timeout'):
        self.seconds = seconds
        self.error_message = error_message
    def handle_timeout(self, signum, frame):
        raise TimeoutError(self.error_message + f" in {self.seconds} secs")
    def __enter__(self):
        signal.signal(signal.SIGALRM, self.handle_timeout)
        signal.alarm(self.seconds)
    def __exit__(self, type, value, traceback):
        signal.alarm(0)
    
def mqtt_subscribe(mqtt, topic ):
    """ setup the subscription """
    messages = []
    def subscriber(mqtt, userdata, message):
        # we just collect them, aio_messages reacts to them
        #print(f"## MQTT {message.topic}={message.payload}")
        messages.append(str(message.payload.decode("utf-8")))

    mqtt.on_message = subscriber
    mqtt.subscribe(topic, qos=1)
    mqtt.loop_start()
    return messages

def aio_userpass_data():
    aio_key_file = '../zerow-minus80/config/aio.key'

    assert os.path.exists(aio_key_file),f"Need {aio_key_file}"

    rez={}
    with open(aio_key_file,'r') as fh:
        for line in fh:
            #export IO_USERNAME="awgrover"
            #export IO_KEY="8a296a6f43e04be6be78df29180a8562"
            if m := re.match(r'export ([^=]+)=(.+)', line):
                key,value = m.groups()
                value = re.sub('"','',value) # strip "
                rez[key] = value
    print(f"## IO user/pass {rez['IO_USERNAME']}")
    return rez

def treepublisher_topic_mapping():
    # local/... : feeds/...
    global _treepublisher_topic_mapping
    if '_treepublisher_topic_mapping' not in globals():
        with open('config/agents/treepublisher.json','r') as fh:
            _treepublisher_topic_mapping = json.load(fh)

        aio_userpass = aio_userpass_data()

        # username fixup
        for local_topic in list( _treepublisher_topic_mapping.keys() ):
            if local_topic.startswith('#'):
                del _treepublisher_topic_mapping[local_topic]
                continue
            if isinstance( (topic_payload:=_treepublisher_topic_mapping[local_topic]),dict):
                topic_payload['topic'] = f"{aio_userpass['IO_USERNAME']}/{aio_topic_payload['topic']}"
            else:
                _treepublisher_topic_mapping[local_topic] = f"{aio_userpass['IO_USERNAME']}/{_treepublisher_topic_mapping[local_topic]}"
    return _treepublisher_topic_mapping

def aio_topic_mapping():
    # local/... : feeds/...
    global _aio_topic_mapping_cache
    if '_aio_topic_mapping_cache' not in globals():
        with open('config/aio-topic-mapping.json','r') as fh:
            _aio_topic_mapping_cache = json.load(fh)

        aio_userpass = aio_userpass_data()

        # username fixup
        for local_topic in list( _aio_topic_mapping_cache.keys() ):
            if local_topic.startswith('#'):
                del _aio_topic_mapping_cache[local_topic]
                continue
            if isinstance( (aio_topic_payload:=_aio_topic_mapping_cache[local_topic]),dict):
                aio_topic_payload['topic'] = f"{aio_userpass['IO_USERNAME']}/{aio_topic_payload['topic']}"
            else:
                _aio_topic_mapping_cache[local_topic] = f"{aio_userpass['IO_USERNAME']}/{_aio_topic_mapping_cache[local_topic]}"
    return _aio_topic_mapping_cache

def aio_topic_mapping_short_names(pred=None):
    # just the short names of them, useful for parameterizing
    return [ os.path.basename(x) for x in aio_topic_mapping().keys() if pred and pred(x) ]
