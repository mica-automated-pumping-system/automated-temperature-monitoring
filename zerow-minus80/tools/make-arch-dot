#!/usr/bin/env perl
# Makes the ../documentation/*.png's for hardware and system-architecture

use strict; use warnings; no warnings 'uninitialized'; use 5.012; no if ($^V ge v5.18.0), warnings => 'experimental::smartmatch';
use Carp; $SIG{__DIE__} = sub { Carp::confess @_ };

use File::Basename qw(basename);
use Dot::DSL;

sub pydoc {
    my ($path) = @_;
    local @ARGV=$path;
    say "##pydoc ",$path;
    my @doc;
    while (<>) {
        if (/^(""")$/.../^(""("))$/) {
            if ($1) { next; }
            if ($2) { last; }
            push @doc, $_;
            say "> $_";
        }
    }
    return join("",@doc);
}

sub all_services {
    return map { [basename($_), pydoc($_)] } glob('agents/all_services/*.py');
}

graph( "Minus80 Hardware Level", 
    dot => '../documentation/minus80-hardware.dot',
    png => '../documentation/minus80-hardware.png',

    labelloc=>'t', # top label
    sub {
        my $pi = node("Pi ZeroW",label=>"\\N\nRaspian Linux");

        my $boron = node("Boron 404x",label=>"\\N\nArduino w/LTE");
        edge($pi, $boron, headlabel=>'config/\nping/\nsend' );
        #edge($boron, $pi, taillabel=>'ack');

        my $lte = node("LTE Cell Network", shape=>'oval');
        edge($boron, $lte, label=>'mqtt');

        my $sensors = node("Sensors", label=>"\\N\nI2C,SPI,GPIO", shape => 'tripleoctagon');
        edge( $sensors, $pi);

        edge( node("Ambient"), $sensors, label=>'temp\nhumidity' );

        my $wifi = node("WiFi", shape=>'oval');
        edge($pi, $wifi, label=>"mqtt");

        my $aio = node("io.adafruit.com", shape=>'oval');
        edge($lte,$aio);
        edge($wifi,$aio);
        edge($wifi, node("Amps Pi"));
        my $ups = node("Powerbank UPS");
        edge($ups,$boron);
        edge($ups,$pi);

        my $wall = node("Wall Socket");
        edge($wall, $ups);

        my $freezer = node("-80C Freezer");
        edge($freezer, $sensors, label=>"probe,\ninternal\nalarm");
        edge($wall, $freezer);
    });

graph(
    "Minus80 System Structure", 
    label=>"Minus80 System Structure\nPi ZeroW Agents\ncommunication via mqtt",
    dot => '../documentation/minus80-system-structure.dot',
    png => '../documentation/minus80-system-structure.png',

    labelloc=>'t', # top label
    sub {
        node("config/mqtt-multi-config.py", 
            label=>"\\N\naka mqtt-multi-config.pi.py\nSeveral mqtt broker configs: local, lan, aio,...\nUsed by all agents",
            shape=>'cylinder'
        );

        my $all_services = node("all_services.py",
            label => "\\N\nReads sensors, publishes values\nMessages are to local/commands/publisher/*\nwith nested \$actual-topic, \$actual-message"
        );

        my $aio;

        graph("sensors",
            sub {
                # the sensor plugins

                for my $info (all_services() ) {
                    my ($plugin,$doc) = @$info;
                    edge(
                        node($plugin, shape=>'component', label=>"\\N\n$doc"),
                        $all_services,
                        arrowhead => 'tee'
                    )
                }
                
            }
        );

        my $publisher = node("treepublisher.py", label=>"\\N\nChooses based on incoming message\nlocal/commands/publisher/*\nSends the nested message");

        my ($pub_mqtt, $pub_pump, $pub_boron);
        my $boron_service;

        graph("network", sub{
            graph("local", sub{
                $pub_mqtt = node("local mqtt broker", label=>"\\N\nmosquitto");
                #edge(node("config/mqtt-local-config.py", shape=>'cylinder'), $pub_mqtt, arrowhead => 'tee');
                edge($pub_mqtt, node("config/mqtt-local-config.py", shape=>'cylinder'), arrowhead => 'tee');
            });

            graph("LAN", sub{
                $pub_pump = node("Pump Pi", label=>"\\N\nLAN", shape=>'oval');
            });

            graph("Boron/LTE", sub {
              my $doc = pydoc('agents/boron_service.py');
              $boron_service = node('boron_service.py', label=>"\\N\n$doc");

              $pub_boron = node("Boron LTE", shape=>'oval');
              });

            graph("WAN", sub{
                $aio = node("io.adafruit.com mqtt", shape=>'oval');
            });
            edge($pub_boron, $aio, label=>'via LTE');
        });

        # network edges (outside of `graph` so $publisher stays outside)
        edge($publisher, $aio, label=>'via wifi\ncampus wan');
        edge("boron_service.py", $pub_boron, label=>'mqtt via serial');
        edge($publisher,$pub_mqtt);
        edge($publisher, $pub_pump, label=>'via wifi');
        edge($publisher, $all_services, label=>'mqtt via boron');
        edge($publisher, $boron_service, label=>'via boron_service.py');
        edge($all_services,$publisher, label=>"mqtt");

        my $button;
        graph("GPIO", sub{
            $button = node("physical-button-action.py", 
                label=>"\\N\nMonitors Wall Power\nMessages like all_services.py"
            );
            my $button_config = node("config/agents/physical-button-action.json", shape=>'cylinder', 
                label=>"\\N\nGPIO behavior -> mqtt message\ne.g. wall-power"
            );
            edge($button_config,$button, label=>'initial', arrowhead => 'tee');
        });
        edge($button, $publisher, label=>'mqtt');

        # ssh-tunnel
        my $ssh_tunnel = node([
            "config/agents/mqtt-action-ssh-tunnel.json",
            "ssh-tunnel",
            "mqtt: io.adafruit.com",
            "topic: \$tunnel-acct/feeds/remote-signal",
            "value: 8523",
            "sets up ssh-reverse-tunnel"
        ]);
        graph("Actions", sub {
            my $action = node("tools/agent-mqtt-action.py", label=>"\\N\nEach config runs an instance", peripheries=>2);
            edge($ssh_tunnel, $action, arrowhead => 'tee');
            edge($aio, $action, label=>"mqtt via wifi");
        });
    }
);
