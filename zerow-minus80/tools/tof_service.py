# time-of-flight example

# blinka!
import board
import busio
import time,sys,os
from adafruit_bus_device import i2c_device
import asyncio
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib" )
from service import ServiceBase
from simple_debug import debug

print(f"Path {sys.path}")
import adafruit_vl53l4cd

class TOFService(ServiceBase):
    # really a singleton

    async def run(self):
        # Try to create an I2C device
        i2c = busio.I2C(board.SCL, board.SDA)
        print("I2C instantiated!")

        start = time.monotonic()
        #print("I2C lock wait...")
        while not i2c.try_lock():
            pass
        try:
            print(f"took {time.monotonic()-start:2.2}")
            print("scan ", [f"0x{x:x}" for x in i2c.scan()] )

        finally:
            i2c.unlock()

        try:
            vl53 = adafruit_vl53l4cd.VL53L4CD(i2c)
            print("Device",vl53)
            print(f"took {time.monotonic()-start:2.2}")
            # optional
            vl53.inter_measurement = 0
            vl53.timing_budget = 200

            print("VL53L4CD Simple Test.")
            print("--------------------")
            model_id, module_type = vl53.model_info
            print("Model ID: 0x{:0X}".format(model_id))
            print("Module Type: 0x{:0X}".format(module_type))
            print("Timing Budget: {}".format(vl53.timing_budget))
            print("Inter-Measurement: {}".format(vl53.inter_measurement))
            print("--------------------")

            vl53.start_ranging()

            while(True):

                while not vl53.data_ready:
                    await asyncio.sleep(0)
                vl53.clear_interrupt()
                self.value = vl53.distance
                
                self.ready_now.set()

                #print(vl53.distance)
                await asyncio.sleep(0.25)

        finally:  # unlock the i2c bus when ctrl-c'ing out of the loop
            print("exit")
            try:
                i2c.unlock()
            except ValueError as e:
                if str(e) != 'Not locked':
                    raise e

if __name__ == '__main__':
    async def main():
        me = TOFService()
        asyncio.create_task( me.run() )

        while True:
            await me.ready_now.wait()
            me.ready_now.clear()
            debug(f"{me.__class__.__name__} {me.value}")
    asyncio.run( main() )

