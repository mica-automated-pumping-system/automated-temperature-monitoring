import os,asyncio,json
from simple_debug import debug

class ServiceBase:
    """
    Base for services: __init__ w/config file, .value, .ready_now.wait(), .run

    someservice = SomeService()
    .create_task( someservice.run() )
    some loop:
        if someservice.ready_now.wait():
            if someservice.fault:
                tell_somebody( someservice.fault ) # rate-limit|only-on-change...
            else:
                someservice.ready_now.clear() # for things that support one-shot ready
                dosomething( someservice.value )

    Make easier to test/dev by adding

    if __name__ == '__main__':
        async def main():
            me = SomeService()
            asyncio.create_task( me.run() )

            while True:
                await me.ready_now.wait()
                me.ready_now.clear()
                debug(f"{me.__class__.__name__} {me.value}")
        asyncio.run( main() )
        
    """

    DefaultConfigDir = 'config/service'

    def __init__(self, config_dir = None):
        """
            Base property setup, config->property setup.

            copy your init params to your self.x params
            then super().__init__() to override from config
            then your setup ...
        """
        if config_dir == None:
            config_dir = self.__class__.DefaultConfigDir

        self.value = None
        self.ready_now = asyncio.Event()
        self.fault = None # change to a message on fault

        config_file = f"{config_dir}/{self.__class__.__name__}.json"
        if os.path.exists(config_file):
            debug(f"Config for service {self.__class__.__name__}: {config_file}")
            with open(config_file,'r') as fh:
                config = json.load(fh)
            for k,v in config.items():
                if k.startswith('#'):
                    continue # comment
                if k not in dir(self):
                    raise Exception(f"Can't set a property from the config, no extant property (init it first in __init__), for: {self.__class__.__name__}.{k}={v} from {config_file}")
                setattr(self, k, v)
        else:
            debug(f"NO Config for service {self.__class__.__name__}: {config_file}")

    #async def run(self):

