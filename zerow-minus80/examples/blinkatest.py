#!/usr/bin/env -S sh -c 'exec "$(dirname $(realpath $0))/../.venv/bin/python3" "$0" "$@"'

import board
import busio
import time
from adafruit_bus_device import i2c_device

# Try to create an I2C device
i2c = busio.I2C(board.SCL, board.SDA)
print("I2C instantiated!")

print("I2C lock wait...")
start = time.monotonic()
#while not i2c.try_lock():
#    pass
#print(f"took {time.monotonic()-start:2.2}")

try:
    device = i2c_device.I2CDevice(i2c, 0x29)
    print("Device",device)
    print(f"took {time.monotonic()-start:2.2}")

finally:  # unlock the i2c bus when ctrl-c'ing out of the loop
    print("exit")
    i2c.unlock()

