#!/usr/bin/env -S sh -c 'exec "$(dirname $(realpath $0))/../.venv/bin/python3" "$0" "$@"'
import board
import busio
import time,sys,os
from adafruit_bus_device import i2c_device

sys.path.append( os.path.dirname( sys.argv[0] ) + "/lib-adafruit" )
print(f"Path {sys.path}")
import adafruit_vl53l4cd

# Try to create an I2C device
i2c = busio.I2C(board.SCL, board.SDA)
print("I2C instantiated!")

start = time.monotonic()
#print("I2C lock wait...")
while not i2c.try_lock():
    pass
try:
    print(f"took {time.monotonic()-start:2.2}")
    print("scan ", [f"0x{x:x}" for x in i2c.scan()] )

finally:
    i2c.unlock()

"""
device = i2c_device.I2CDevice(i2c, 0x29)

buffer = bytearray(3)

with device:
    for reg,val in {0xc0:0xee, 0xc1:0xaa, 0xc2:0x10}.items():
        device.write(buffer, end=1)
        device.readinto(buffer, end=1)
        print(f"[0x{reg:x}]= 0x{buffer[0]:x} should 0x{val:x}")
    
"""

try:
    vl53 = adafruit_vl53l4cd.VL53L4CD(i2c)
    print("Device",vl53)
    print(f"took {time.monotonic()-start:2.2}")
    # optional
    vl53.inter_measurement = 0
    vl53.timing_budget = 200

    print("VL53L4CD Simple Test.")
    print("--------------------")
    model_id, module_type = vl53.model_info
    print("Model ID: 0x{:0X}".format(model_id))
    print("Module Type: 0x{:0X}".format(module_type))
    print("Timing Budget: {}".format(vl53.timing_budget))
    print("Inter-Measurement: {}".format(vl53.inter_measurement))
    print("--------------------")

    vl53.start_ranging()

    while(True):

        while not vl53.data_ready:
            pass
        vl53.clear_interrupt()
        print(vl53.distance)
        time.sleep(0.25)

finally:  # unlock the i2c bus when ctrl-c'ing out of the loop
    print("exit")
    try:
        i2c.unlock()
    except ValueError as e:
        if str(e) != 'Not locked':
            raise e

