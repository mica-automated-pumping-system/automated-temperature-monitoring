    pi: **sudo apt install screen vim**
    git config --global pull.rebase false
    (export CFLAGS=-fcommon; python3.11 -m pip install -t lib-pip  RPi.GPIO) # untill fixed after 0.7.1...

Building and installing the software is fairly complex:

* Ansible for installing the rest of the stuff, and for building the os-image
* Packer.io for building the os-image
* Tools for uploading to the Proton Boron 404x
* Tools for compiling code for the Proton Boron 404x
* Python3.11 and libraries
* Site configuration files
* Build os-image

# Install Tools, Packages

This step installs the tools needed to develop, and build the os-image. It uses Ansible and a python3.11 virtual-env.

## Get our code

    git clone git:gitlab.com:automated-media-pumping-system/automated-temperature-monitoring.git
    cd automated-temperature-monitoring

## dev install

Requires sudo:

    if you don't have python3.11 installed, and sudo apt install python3.11 fails
        sudo add-apt-repository ppa:deadsnakes/ppa
    build/install

Note that there is a build/install` in automated-temperature-monitoring/zerow-minus80` for just installing the tools needed for the Pi and development for the Pi.

## dev update python/node packages

To update dev environment for the pi context

    cd zerow-minus80
    build/install --update
    
# Build micro-sd for PI

## Create the config files

The site config has a few values, and the directory for secrets. I've been putting it in config/os/$installationnamei.pi-image-maker:
        # .py vars methods for
        # tools/packer-layers.py --config $thisfile build/create-pi-image

        architecture = '64' # for pi-zero2, or '32' for pi-zero
        config_hostname = '$somehostname'
        config_tz = 'America/New_York'
        config_dir = 'config/os/$installationname'

Files in the config_dir:

        config/os/$installationname
            aio.key.secret
                # api username/key for io.adafruit.com
                # for normal data
                # looks like:
                export IO_USERNAME="awgrover"
                export IO_KEY="8a296a6f43e04be6be78df29180a8562"
            aio.ssh_tunnel_account.key.secret
                # same, but for the ssh-tunnel trigger feed
                # can be the same info
            login.secret
                # password for console login for user pi
            ssh-authorized_keys
                # copied to ~/.ssh/authorized_keys on the pi
            wifi-*.json.secret
                # wifi configs for /etc/NetworkManager/system-connections
                # tools wifi-from-config can help make them

## Get the base pi-os image:

* Download the .xz from https://www.raspberrypi.com/software/operating-systems/
    * you need the 32-bit version for the zero w
    * or 64-bit for zero 2 w
    * "with recommended"
* The .xz name is hardcoded in `build/create-pi-image`, at the top.

## Build .img's

    # it will ask for a sudo password, possibly multiple times every 15 minutes...
    # that's annoying

    tools/packer-layers.py --config config/os/$installationname.pi-image-maker build/create-pi-image

Build .img's using the current git branch

    env USEDEVBRANCH=1 tools/packer-layers.py --config config/os/$installationname.pi-image-maker build/create-pi-image

### Sparseness, i.e. size

All the .img's can take quite a bit of room.

Built images should be "sparse". Nb.: `cp` will unsparse them unless you use `--sparse=always`. `rsynce` can also re-sparse them with `--sparse` argument. I saw a savings of 20% to 30% when using sparse.

## Update .img's

Later, build new .img's with upgraded app-code, os-packages, python-packages, node-packages.

    env START=upgrade FORCE=1 tools/packer-layers.py build/create-pi-image

# Developing the os-image making

If you need to update the image making, or debug a problem.

Uses `tools/packer_layers.py build/create-pi-image` to create "layers" of .img's in `~/os-images` and `./os_images`.

This

* creates parameter files for each layer, and a `.makefile` for the final image.
* runs the makefile

Edit the recipe in `build/create-pi-image`. See the help `tools/packer_layers.py --help`.

That system uses packer.io to deal with .img files, and Ansible to modify them.

Packer and Ansible files that might be useful across projects are in `../pi-image-maker/tools/packer_layers/`.
You can shadow **all** of those by making a `./tools/packer_layers`
Files that are specific to this app are in `build/`. 

## Debugging/Developing Image Making

If a layer in the recipe crashes (e.g. typos, extra newline, whatever), you can edit and retry just that layer.
Or, if you are trying to add/change a layer.

Occasionally, you'll get a permissions error for something in os_images, because the process runs as root. You may have to: `chmod $USER:$USER $that_dir/*`.

On failure, the crashed .img is deleted by default, so to leave it for forensics:

**Use** `env USEDEVBRANCH=1 START=$name KEEPFAIL=1 FORCE=1 tools/packer_layers.py build/create-pi-image`

Where `$name` is from the layer name. e.g. `blah/blah:$name.img`. Also, it's the name in `with layer_name($name)`. So, this skips layers up to `$name`, and starts there. `tools/packer_layers.py build/create-pi-image --list` will also list all the .img's.

* `USEDEVBRANCH=1` uses the currently checked out branch for the .img. Otherwise, you'll get the `main` branch, which won't have any code changes that you are developing.
* `KEEPFAIL=1` preserves the crashed .img for forensics. 
* `FORCE=1` forces re-make from the `START` layer (which wouldn't be remade because it still exists)
* You can also limit to just 1 layer by adding `LIMIT=1`, to debug 1 layer at a time.

But, that means you have to delete that last .img before making again.

NB.: If you edit the recipe (build/create-pi-image), you have to rerun `... tools/packer_layers.py build/create-pi-image`.

NB.: If you edit any files that are part of the app, you'll have to commit and push, or they won't get "checked out". The imaging building files, .yml and stuff in os_images, don't need to be pushed since they are used on the "controller" side, no the pi (obviously, commit and push them eventually to capture them!).


You don't have to rebuild the makefile each time (if the recipe `create-pi-image` doesn't change), so you can:

**Use** env KEEPFAIL=1 make -f $makefile`

* where `$makefile` is from the output of the tools/packer_layers.py run.

**On failure**, you can examine the image, and even run stuff in emulation (with chroot).

`tools/in-img os-images/blah:$name.img` # sudo password ask

You'll be root, in bash, chrooted, and emulation enabled.

**Remember** to exit from that shell so it can clean up loopbacks etc. Watch out for a final sudo password requirement on exit.

**When debugged**, you should regenerate the .makefile with the plain invocation:

`env FORCE=1 tools/packer-layers.py build/create-pi-image`

Since this is using `make`, you may have to delete .img files to force remaking. The system tries to be pretty smart, remaking if parameter files change, etc. But, it can't always get it right.

# Burn image to sd-card

* Find the .img, typically 

        os_images/*:PRODUCTION.img
        # OR
        ls -tr os_images/*.img

* Insert the sd-card (e.g. usb-adapter, sd-card adapter, etc.)
* burn: this tool just wraps `dd` with some guessing and confirmation: 

        tools/sd-card-img out os_images/$the-dot-img

Copy speed is limited by the port (USB3 can be 10x faster than USB2, USBC can be faster), the SD-card (an older card maxed out at 22 MB/s), etc.

* Should be unmounted upon success

There's also a "demo mode" img, that will produce synthetic data, and can simulate a freezer temperature fail (to -70C):

`tools/sd-card-img out os_images/2024-03-15-raspios-bookworm-armhf-lite:DEMO.img`

## Boot the pi

* power the pi off
* install the sd-card
* power the pi on
  * The pi's led should blink
  * the first boot can take a while, it reboots at least once
* The most reliable way of connecting to the pi is with the serial-ttl cable. The pi is configured to put a login tty on the uart0 ("console").
* If you configured the wifi correctly, the pi should appear on mDNS (e.g. avahi, `avahi-browse ...`). FIXME: service type

## Checking/forensics on sd-card

This is also useful if the device fails to boot, ssh or wifi fails, etc. You can examine the systemd logs with mount/chroot.

* Remount the sd-card (re-insert)
* find the $mountpoint: ls `/dev/*`, or `mount` or ls /media/....., or something
* Mount it (some linux's auto mount to /media/....)
* `cd $mountpoint` and explore
* If you have the qemu arm emulator stuff installed, you can run stuff: `sudo chroot $mountpoint` and execute stuff
  * Though I think you want to setup path maybe, and mount the boot partition... FIXME: add this whole functionality to tools/in-img

# Adafruit IO dashboard setup

FIXME: Do I have the captured config & tool?

# Freezer probe

FIXME: copy from below

# OBSOLETE

    nmcli --fields autoconnect-priority,name connection
    # list current connection
    nmcli dev
    # add connection or above
    ssidname=auger wifipassword=vockiatedeckyijuichue
    nmcli connection add con-name $ssidname type wifi ifname wlan0 ipv4.method auto connection.autoconnect yes wifi-sec.key-mgmt wpa-psk wifi.ssid $ssidname wifi-sec.psk $wifipassword
    # set one of them to higher-priority
    nmcli connection modify "mylaptop" connection.autoconnect-priority 2
    # disconnect (may  need to do twice)
    nmcli conn down id connectionname $ssidname # if ssidname== the "con-name"
    nmcli conn reload
    # auto-connect from add'd connections (doesn't work)
    nmcli device set wlan0 autoconnect yes
    # connect from an add'd
    nmcli conn up $ssidname # if ssidname== the "con-name"


# adafruit.io setup

You need:

* username/password for the io.adafruit.com account
* an api key for the same account

Create `config/aio.key`:

* login to your io.adafruit.com
* click on the api-key (as of this writing: yellow circle w/key-image in upper right)
* copy the `linux-shell` section, like:

```
    export IO_USERNAME="YourIOAdafruitUserName"
    export IO_KEY="8a296a6f43e04be6be78df29180a8562"
```

* paste into and save `config/aio.key`
* we do not want that in git!

Currently using AIO as a cloud IOT central point, with email/txt alarms, and graphs/views.

Identify the sensor/state feeds:

    agents/treepublisher.py --topics | grep feeds/ | sort -u

In AIO under the username (see `config/aio.key`):

* to `Feeds`, add the group, `minus80`
* to the group `New` each of the feeds above
* outside of the group, add the feed `feeds/remote-signal` 

## Alarms and Notifications

I suggest setting up the following:

### "Actions"

1. If [minus80].minus80 is greater than or equal to "-73" then email me the minus80 value. Limit 1/hour, notify on reset. Type: Reactive
1. If power is not equal to "1" then email me the power value. Limit every 15 minutes, notify on reset. Type: Reactive

### Notifications (deadman)

This gives a fallback to detect if communications has gone wrong, or the system has failed.

For these feeds, setup a "Notification" ("notification status" "On").

* minus80.boron-lte-keepalive : 1 day
* minus80.minus80 : 30 minutes
* minus80.power : 30 minutes

### Dashboard

I built a [dashboard](../documentation/io-adafruit-dashboard.png) for "minus80"

# Assembly

See [BOM](../documentation/BOM.ods).

## Make enclosure

## Break-out-boards

Using (breadboard)[../documentation/minus80-pizero_bb.png]/(schematic)[../documentation/minus80-pizero_schem.svg], use jumpers, wires, screw-blocks, usb-cables to connect things.

*should be step by step'ish*

## Freezer connections

The alarm-system's main sensor is the cryogenic temperature probe ("precision platinum RTD, PT100"). 

### Probe Assembly

See the [bread-board](../documentation/minus80-pizero.fzz)/[schematic](../documentation/minus80-pizero.fzz).

1. piece of aluminum "L" to "anchor" the
1. probe soldered to
    note the 2 wires soldered together at the probe
1. flat 3 wire to
1. screw block soldered into
1. MAX31865 break-out-board

The flat wire is nice for passing through the door seal.

### Internal Alarm

If your freezer has an accessible connection for the internal alarm (e.g. screw block), the alarm-system can monitor that.

It is [pre-configured](config/agents/physical-button-action.json) for: Normally-Closed (non-alarm state). This is preferred because it gives a fail-safe behavior: if the wire breaks or comes loose, it registers as an alarm.

If you only have Normally-Open, change the configuration:

    "13" : {
        "#" : "Freezer internal alarm: Closed is alarm, open is ok",
        "#" : "disconnected is not detected.",
        ...
        "pull" : 0,
        "invert" : false,
        ...
    }

# System setup
## non-git

    # ssh
    copy both cleftpi.pub, and amps.pi.awg.pub to pi's .ssh/authorizedkeys
    authorizedkey entry on ww4:
        restrict,no-agent-forwarding,no-pty,no-X11-forwarding,command="/bin/sh -c 'sleep 30'",port-forwarding,permitlisten="localhost:8522",
        permitlisten="localhost:8523",permitlisten="localhost:8524" ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAcbJf23vQkra41zzpVJbOGT6KM7xB3upoV75R+CG/Mu pi@amps-pi

    %pi mkdir ~/zerow-minus80 ~/boron-mqtt-client
    setup links per XXXXX
    cd zerow-minus80
    pihost=minus80-pi.local
    scp -r Makefile agents config lib lib-adafruit tests tools systemd.units $pihost:zerow-minus80
    scp -r ../boron-mqtt-client/Makefile ../boron-mqtt-client/tests $pihost:boron-mqtt-client

    %pi cd ~/zerow-minus80

    # repairs
    %pi chmod ug+x tools/pytest
    %pi ln -s `pwd`/tools/pytest /home/pi/.local/bin
    %pi find ~/zerow-minus80 ~/boron-mqtt-client -name '__pycache__' | xargs rm -rf
    %pi find ~/zerow-minus80 ~/boron-mqtt-client -name '.*.sw?' | xargs rm

    # config
    %pi make configure-clean
    %pi pushd config
    %pi rm aio.key mosquitto.conf mqtt-multi-config.py mqtt-local-config.py
    %pi popd
    %pi make configure
    
    # systemd setup
    %pi make systemd-setup
    %pi (cd systemd.units; make status)

    # tests
    %pi pushd boron-mqtt-client
    %pi make tests
    %pi popd

    %pi pushd zerow-minsu80
    %pi make tests
    %pi popd

# Development flow

## systemd unit restart

CAUTION: avoid stopping mqtt-action-ssh-tunnel.service or you may lose the connection:
    * Run standalone: nohup tools/agent-mqtt-action.py config/agents/mqtt-action-ssh-tunnel.json.alt &
    * connect to it: tools/ssh-tunnel -x -c -p 8524
    * manually stop/restart mqtt-action-ssh-tunnel.service

## files in `pumping-system` are part of another archive: a subtree

The files are shared from the (`pumping-system`)[https://gitlab.com/mica-automated-pumping-system/automated-temperature-monitoring.git] as "git subtree". `Subtree` graphs another archive into our local one (another remote) as a directory. Edits go in the current archive (including files in the sub-dir). You have to do an explicit merge from the `pumping-system` to get code from its archive. Likewise, committing to `pumping-system`.

The initial checkout is above: "# git for zerow (or other dev locations)".

Note that we are using sparse-checkout (though only as a convenience).

Adding subtrees in `pumping-system/`: If you need another sub-directory:

    git sparse-checkout add pumping/system/pump-pi/$something
    # Everybody has to do that. sad. FIXME: add as a hook?

We have to refer to a branch of the `pumping-system`...

* we need a certain "version" that is compatible with this minus80 version
* we need to merge to/from our submodule branch to/from the `pumping-system` project.

To edit/commit files in `pumping-system/`:

    # you can just edit and commit and push
    # but the are only in archive `minus80`
    # do the "to push new code" below to get them back into the "real" `pumping-system`

To merge in new code from `pumping-system`:
    
    # go to a real pumping-system and merge code into branch minus80
    # then, in this project, merge that branch in with
    git subtree pull --prefix=pumping-system --squash pumping-system minus80 --squash

    # manual if you don't have `git subtree`:
    # We get the pumping-system commits, then merge them
    git fetch pumping-system 
    git merge -s subtree --squash pumping-system/minus80 # does not commit
    ...
    git commit -m 'merged from pumping-system'

To push new code to the `pumping-system`:

    git subtree push -P pumping-system --annotate pumping-system pumping-system minus80

    # then you have to go to a "real" pumping-system and mergei branch `minus80` into main
