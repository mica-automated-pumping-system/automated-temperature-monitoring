wheel
aiomqtt ~= 2.1
paho-mqtt ~= 2.1
psutil ~= 5.9
pytest ~= 7.4
sdnotify ~= 0.3
pyserial ~= 3.5
Adafruit-Blinka ~= 8.30
adafruit-circuitpython-bme680 ~= 3.7
adafruit-circuitpython-max31865 ~= 2.2
