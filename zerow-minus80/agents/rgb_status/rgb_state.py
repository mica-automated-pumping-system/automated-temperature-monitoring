#!/usr/bin/env -S sh -c 'exec "$(dirname $(realpath $(dirname $0)))/../.venv/bin/python3" "$0" "$@"'

"""
ignore a message that is a "positive" direction
    but "below" us
    above, cut @ us
ignore a negative
    above us
    below, cut @ us
a negative always goes to it's state
    from everywhere
    but only downward

start: all off
Have power: no special overlay
local/events/pin/power:1

red-yellow-green-blue-violet

send a get for each "device"?
local/get/minus80/network-status/all

START
local/events/minus80/network-status/wifi-radio
    rf-off  red-flash
    rf-on   red-solid
poll nmcli?
    ssid-search red/yellow alt
    known-ssid yellow-flash
local/events/minus80/network-status/wifi
    wifi conn yellow-on
local/events/minus80/network-status/wan
    wan un-reachable alt w/blue
    wan reachable blue

local/events/freezer-alarm
    inverted
        overlay red-flash 
local/events/pin/power
    power-loss: overlay white-flash
local/events/pin/battery
    # nb: missing battery is considerd OK
    bat-low: overlay off-flash
        long would be nice

mqqt-wan-start  blue/green alt
    systemd status
mqtt-wan-conn   green

ssh-listen  add flash-blue
ssh-conn longer blue-flash

single (solid)
A,B alternate (B can be OFF)
A,B,C alternate A/B, flash C between each
{ main: A   # solid, none=flash B
    second: B # alt
    overlay: { $name: C } # c after each, fast flash
"""
import os

USEGPIO = not os.environ.get('NOGPIO',False)

import sys
import asyncio
from copy import copy

try:
    import gpiozero
    from gpiozero import LED
    import board

    if 'SPI' not in dir(board): # proxy for "has gpio stuff"
        raise ModuleNotFoundError("No SPI")
except ModuleNotFoundError as e:
    import inspect
    #agent.HelpEpilog = f'\nGPIO disabled (not running on a pi?). See {__file__}:{inspect.currentframe().f_lineno - 3}\n{e}'
    USEGPIO=False

sys.path.append( os.path.dirname( sys.argv[0] ) + "/../../lib-pump-pi" )
from simple_debug import debug

# distinquishing object
class _Remove:
    pass
Remove = _Remove()

class RGBState:
    """The state-machine seems to be
            a linear progression towards highest functionality
            + some "overlay" states: e.g. fully-connected, but power is gone
        positive messages move up, negative down
        state = 
            curent state # index
            a main # for solid, or None=for flashing
            second # to alternate with main
            overlay list # fast-flash between main/second
    """
    
    class State:
        """helper for making a state
            message and payload are matched exactly,
            These are colors:
                main/second will replace the current state
                overlay will be added/subtracted to the overlay "list" of the current state
            Direction allows us to enforce the positive/negative movement rule.
                0 means it can happen anytime
            Start==True
                becomes the initial state
                can have multiple Overlays, but will only use the last "State"
        """
        def __init__(self, topic, payload, main=None, second=None, overlay=None,direction=0, start=False):
            if main==None and second==None and overlay==None:
                raise Exception(f"Need at least one of main, second, or overlay, for {topic} : {payload}")
            if direction==None:
                raise Exception(f"Need a direction, for {topic} : {payload}")
            self.topic=topic
            self.payload=payload
            self.main=main
            self.second=second
            if overlay == None:
                self.overlay = {}
            else:
                # need a "remove" signal
                self.overlay={topic : overlay} if overlay != Remove else {topic: None}
            self.direction=direction
            self.start = start

        def set(self, other):
            # take main, second, and direction
            self.topic = other.topic
            self.main = other.main
            self.second = other.second
            self.direction = other.direction # not important
            self.payload = other.payload # not important?
            self.update_overlay( other )

        def update_overlay(self, other):
            # add/remove the overlay
            if other.overlay != None:
                for o,v in other.overlay.items():
                    if v==None:
                        self.overlay.pop(o,None)
                    else:
                        self.overlay[o] = v
        def color_name(self, c):
            return next( (k for k,v in Colors.items() if v==c), c)

        def _description(self):
            ms = self.color_name(self.main) if self.main else ''
            ms += f"/{self.color_name(self.second)}" if self.second else ''
            return {
                'direction' : [ '-', 'O', '+' ][ self.direction + 1 ],
                'pattern' : ms,
                'overlay' : [ f"{self.color_name(c)}" for t,c in self.overlay.items() ],
            }

        def __str__(self):
            desc = self._description()
            return f"<State {desc['direction']} {self.topic}:{self.payload} {desc['pattern']}+ {desc['overlay']}>"

    def __init__(self):
        # need our current state
        self.state = self.__class__.State("",None,-1)

        # will be reset by .update if we have a start=True
        self.state_i = 0
        self.state.set( States[ self.state_i] )

        for s in States + Overlays:
            if s.start:
                debug(f"Start {s}",level=4)
                self.update(s.topic,s.payload)

        self.gpios = [] # lazy setup
    
    def __str__(self):
        return f"RGBState [{self.state_i}] {self.state}"
    
    def update(self, topic, value, force=False ):
        debug(f"> {topic}: {value}",level=4)

        # find it
        i,s = next( ([i,x] for i,x in enumerate(States) if x.topic==topic and x.payload==value), [0,None])
        if s:
            # if it is positive, allow if above us
            if s.direction == 1 and (i > self.state_i or force):
                self.state_i = i
                self.state.set( s )
                debug(f"-> {self}",level=2) 

            # if it is negative, allow if below us
            elif s.direction == -1 and (i < self.state_i or force):
                self.state_i = i
                self.state.set( s )
                debug(f"-> {self}",level=2) 

            else:
                debug(f"Update ignored dir {s.direction} from [{self.state_i}] to [{i}]",level=2)
        else:
            i,o = next( ([i,x] for i,x in enumerate(Overlays) if x.topic==topic and x.payload==value), [0,None])
            if o:
                   self.state.update_overlay(o) 
                   debug(f"OV-> {self}",level=2)
            else:
                debug(f"Not a state/overlay: {topic}: {value.__class__.__name__} {value}")

    async def run_rgb(self):
        """loop to update the rgb.
        update the state with .update()
        """

        if not self.gpios:
            if USEGPIO:
                led_object = LED
            else:
                class FakeLED:
                    def __init__(self,pin):
                        self.pin=pin
                        self._value = 0

                    @property
                    def value(self):
                        return self._value
                    @value.setter
                    def value(self,v):
                        self._value = v
                        debug(f"  {self.pin} -> {v}",level=4)

                led_object = FakeLED
            self.gpios = [ led_object(x) for x in [ RedPin, GreenPin, BluePin ] ]

        def to_gpio(color):
            if color==None:
                color = [0,0,0]
            return [[self.gpios[i],v] for i,v in enumerate(color)]
            
        last_pattern = None
        m_pins,s_pins,ov_pins = (None,None,None)

        def pattern_change():
            # update if pattern changes
            nonlocal last_pattern,m_pins,s_pins,ov_pins
            if last_pattern != str(self.state):
                # to the current state
                m_pins = to_gpio( self.state.main )
                # None means skip secondary: i.e. main is solid
                s_pins = to_gpio( self.state.second ) if self.state.second else None
                # overlays
                ov_pins = [ to_gpio(c) for c in self.state.overlay.values() ]
                debug(f"Saw pattern change {self.state}",level=4)
                last_pattern = str(self.state)

        last_rgb = None
        def set_rgb(pv):
            nonlocal last_rgb
            for p,v in pv:
                p.value = 0 if v==1 else 1
                #sys.stderr.write(f"{p.pin} -> {v}  ")
            #sys.stderr.write("\n")
            if last_rgb != pv:
                debug(f"  {[v for x,v in pv]}",level=4)
                last_rgb = pv

        overlay_i = 0
        ov_duration = 0.2
        async def do_overlay():
            nonlocal overlay_i,pattern_state,ov_duration
            # sequence through the overlay pattern
            if overlay_i < len(ov_pins):

                set_rgb(ov_pins[overlay_i])
                await asyncio.sleep( ov_duration )

                overlay_i += 1
                #debug(f"{time.monotonic()} ov -> {overlay_i}",level=2)

            # move ahead
            else:
                if ov_pins:
                    debug("ov done",level=4)
                overlay_i = 0
                pattern_state += 1

        pattern_state = 0
        last_pattern_state = pattern_state
        ms_duration = 1.5
        while True:
            # pick up any async changes

            pattern_change()
            match pattern_state:
                case 0: # start main
                    pattern_state += 1
                case 1: # main
                    # main is always set, which might be the OFF for a flash pattern
                    set_rgb(m_pins)
                    await asyncio.sleep(ms_duration)
                    pattern_state += 1
                case 2: # overlay m->s
                    await do_overlay()
                case 3: # secondary
                    if not s_pins:
                        pattern_state += 2
                    else:
                        set_rgb(s_pins)
                        await asyncio.sleep(ms_duration)
                        pattern_state += 1
                case 4: # overlay s->m
                    await do_overlay()  # updates pattern_state
                case 5: # to beginning
                    pattern_state = 0

            if last_pattern_state != pattern_state:
                debug(f"State {last_pattern_state} -> {pattern_state}",level=4)
                last_pattern_state = pattern_state

        debug(f"Exited Run!")

    def topics(self):
        t={}
        def add_topic(s):
            desc = s._description()
            ov = f"+ {desc['overlay']}" if desc['overlay'] else ''
            if t.get(s.topic,None):
                t[s.topic]['description'] += f" ; {s.payload} -> {desc['pattern']}{ov}"
            else:
                t[s.topic] = {
                    "description" : f"{s.payload} -> {desc['pattern']}{ov}",
                    "payload" : s.payload
                }
            #debug(f"T: {self} as {t[s.topic]}")
        for i,s in enumerate(States):
            add_topic(s)
        for i,s in enumerate(Overlays):
            add_topic(s)
        return t

    def say_help(self):
        for i,s in enumerate(States):
            print(f"[{i}] {s}")
        for i,s in enumerate(Overlays):
            print(f"[{i}] {s}")

    def cleanup(self):
            for pin in self.gpios:
                pin.off()
                pin.close()

def PosState(topic, payload, main=None, second=None, overlay=None, start=False):
    return RGBState.State(topic, payload, main=main, second=second, overlay=overlay, direction=1, start=start)
def NegState(topic, payload, main=None, second=None, overlay=None, start=False):
    return RGBState.State(topic, payload, main=main, second=second, overlay=overlay, direction=-1, start=start)
def State(topic, payload, main=None, second=None, overlay=None, start=False):
    return RGBState.State(topic, payload, main=main, second=second, overlay=overlay, direction=0, start=start)

## FIXME: move to config

# led pins
RedPin = 'BOARD36'
GreenPin = 'BOARD38'
BluePin = 'BOARD40'

## hack in between
Colors = { "Red":[1,0,0], "Green":[0,1,0], "Blue":[0,0,1], "Yellow":[1,1,0], "Cyan":[0,1,1], "Purple":[1,0,1], "White":[1,1,1], "Black":[0,0,0] }
for k,v in Colors.items(): globals()[k]=v

# FIXME: a /local/get/* should be deducible for each local/events/*, to evoke a repeat message
States = [
    NegState('local/events/minus80/network-status/wifi-radio',0, second=Red, start=True),
    PosState('local/events/minus80/network-status/wifi-radio',1, main=Red),
    # ssid-searching would go here: yellow
    NegState('local/events/minus80/network-status/wifi',0, main=Red, second=Yellow),
    PosState('local/events/minus80/network-status/wifi',1, main=Yellow),
    NegState('local/events/minus80/network-status/wan',0, main=Yellow,second=Blue), 
    PosState('local/events/minus80/network-status/wan',1, main=Blue), 
    # mqtt-wan-conn goes here: green
]
Overlays = [
    # no "progress" rule.
    # We can have several overlays active at once, so we use the topic as a tag
    # `Remove` would remove the overlay
    State('local/events/freezer-alarm',1, overlay= Red ), # note inverted
    State('local/events/freezer-alarm',0, overlay= Remove ), # note inverted
    State('local/events/pin/power',0, overlay= White),
    State('local/events/pin/power',1, overlay= Remove),
    State('local/events/pin/battery',0, overlay= Blue),
    State('local/events/pin/battery',1, overlay= Remove),
    State('local/events/minus80/boron-lte-aio/connected',1, overlay= Remove),
    State('local/events/minus80/boron-lte-aio/connected',0, overlay= Black, start=True),
    # ssh-conn goes here
]

# self dev/debug
if __name__ == '__main__':
    import sys,re,time

    # --- --topics > messages/state output
    # --- [--start msg] msg msg ... | < msg lines
    #   msg is: topic:value
    #   --start msg # forces initial state

    async def main():
        status = RGBState()
        if sys.argv[1] == '--topics':
            status.say_help()
            exit(1)

        def eat(msg, run=True):
            topic,value = msg.split(':',2)
            if re.match(r'\d+$', value):
                value = int(value)
            status.update( topic, value, force=not run )

        while len(sys.argv)>1 and sys.argv[1] == '--start':
            sys.argv.pop(1)
            smsg = sys.argv.pop(1)
            debug(f"Start w/ {smsg}")
            eat( smsg, run=False )
            
        debug(f"Start: {status}")

        asyncio.create_task( status.run_rgb() )

        duration = 8
        for msg in sys.argv[1:]:
            eat(msg)
            await asyncio.sleep(duration)
        if not sys.stdin.isatty():
            for l in sys.stdin:
                eat(l.rstrip())
                await asyncio.sleep(duration)
    asyncio.run(main())
