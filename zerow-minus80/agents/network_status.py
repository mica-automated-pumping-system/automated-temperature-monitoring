#!/usr/bin/env -S sh -c 'exec "$(dirname $(realpath $0))/../.venv/bin/python3" "$0" "$@"'

import asyncio
import sys,os,json,re,time
import paho.mqtt as paho # some constants|--help'
import aiomqtt as mqtt # for errors
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib" )
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib-pump-pi" )
from simple_debug import debug
import agent

sys.path.append( os.path.dirname( sys.argv[0] ) + "/network_status" )
# fixme: would like to do this automatically, but the instantiation of each object is different
from wan_check_service import WANCheckService
#from wifi_check_service import WIFICheckService

class NetworkStatusAgent(agent.Agent):
    """
    Run all the services in agents/network_status/*.py (disable with suffix .disable)
    """

    mqtt_config = {
        "device" : "minus80", # advertise's topic = local/devices/$device/$name
        "name" : "network-status",
    }
    publisher_topic = 'local/commands/publisher/' # + tree-name

    def __init__(self):
        super().__init__()
        self.all_publish = [] # pairs of [full-topic, lambda-value) # to publish for /get/all
        self.service_config_dir = os.path.splitext( self.config_file() )[0]
        debug(f"Service configs: {self.service_config_dir}")
        os.makedirs( self.service_config_dir, exist_ok=True )

    def argparse_add_arguments(self, parser ):
        # add your argparse arguments, override mqtt_config, etc
        parser.set_defaults(mqtt_config='config/mqtt-multi-config.py') # override

        parser.add_argument( '--no-ping', help="disable pinging (dev)", action='store_true', default=False)

    # FIXME: add some topic to get the service list

    async def handle_publish_values(self, message):
        """
        Immediately publish current values.
        They may be Null if not ready.
        """
        #debug(f"republish {self.all_publish}")
        for topic,payload_fn in self.all_publish:
            #debug(f"Republish {payload_fn()}")
            self.queue_a_message( topic, payload_fn() )

    def topics(self):
        # DRY up some topics
        self.topic_wifi_radio = self.topic_for_event('wifi-radio')
        self.topic_wifi = self.topic_for_event('wifi')
        self.topic_wan = self.topic_for_event('wan')

        return super().topics( {

            # listen
            self.topic_for_get('all') : { "handler": self.handle_publish_values },

            # publish:
            self.topic_wifi_radio: { "description": "wifi-enabled 1|0" },
            self.topic_wifi: { "description": "wifi-connected 1|0" },
            self.topic_wan : { "description": "WAN connectivity 1|0" },
        })

    async def publish_value_when_ready(self, obj, *topic_value, gate=None):
        # FIXME: factor and share with all_services.py
        """
        Publish the `topic` : `value()` 
        When obj.ready_now.wait() finishes (a asyncio.Event(), .set() )
        *topic_value is a sequence of: topic, value_fn
            Thus, a single `ready_now` on the object works for multiple topic-value publish
        And then gate() is tested (if != None)
        Those topic/value pairs are saved in a list for `self.topic_for_get('all')`: `local/get/services/minus80/all`
        """

        async def _if_ready_now( obj, awaitable, *args, **kwargs ):
            """
                Calls awaitable(*args,**kwargs) when object update's (`ready`)
                The gate` is checked at that time, so: when ready, and then gate'd, the publish
                E.g. publish w/ publisher()
            """

            while True:
                await obj.ready_now.wait()
                obj.ready_now.clear()
                await awaitable(*args,**kwargs)

        async def _gated_multi_publisher(*topic_value, gate=None):
                """ Wrapper for publishing via _if_ready_now, with last chance pred-gate """
                if gate==None or gate():
                    # sequential publish, could do in parallel, but we don't have to worry about captures this way
                    alternator = iter(topic_value)
                    for topic,value in zip(alternator,alternator): # by 2's
                        v = value()
                        debug(f"Publish {topic} -> {v}", level=4)
                        await self.mqtt_client.publish( topic, json.dumps( v ) )
                else:
                    debug(f"not-gate'd _publisher() {topic_value}",level=2)

        # save topic/value pairs for `self.topic_for_get('all')`: `local/get/services/minus80/all`
        alternator = iter(topic_value)
        for topic,value in zip(alternator,alternator): # by 2's
            self.all_publish.append( [ topic, value ])

        # test obj.ready_now, then _gated_multi_publisher( *topic_value, gate=gate)
        await _if_ready_now( obj, _gated_multi_publisher, *topic_value, gate=gate)

        debug(f"##PubWhenReady: {topic} via {obj.__class__.__name__}")

    async def loop(self, first_time):
        # required.
        # have to busy wait if you want to do nothing
        debug(f"LOOP...", level=(0 if first_time else 2))

        # start the services with delay-between, which will roughly cause publishing to be spread out
        max_per_window = 10
        window_secs = 60
        between = window_secs/max_per_window if not os.environ.get('NODELAY',False) else 0

        async with asyncio.TaskGroup() as tg:

            if not self.parsed_args.no_ping:
                wan_check = WANCheckService( config_dir = self.service_config_dir )
                tg.create_task( wan_check.run(), name='wan_check.run' )
                tg.create_task( 
                    self.publish_value_when_ready( wan_check, 
                        self.topic_wifi_radio, (lambda: 1 if wan_check.wifi_radio else 0),
                        self.topic_wifi, (lambda: 1 if wan_check.wifi else 0),
                        self.topic_wan, (lambda: 1 if wan_check.wan else 0),
                    ),
                    name='publish_value_when_ready wan'
                )
                #await asyncio.sleep(between)

            # FIXME: add topics() like: local/get/tof/tof1/value and local/get/$me/values
            # which means dry'ing these services

            debug("bottom of create_tasks, all launched", level=(0 if first_time else 2))
            #if first_time:
            #    debug(f"DEBUG closing")
            #    await self.mqtt_client.disconnect()

        #while(True):
        #    await asyncio.sleep(1)
        debug("FELL off bottom of LOOP: tasks died", level=(0 if first_time else 2))


    async def cleanup(self, e):
        await super().cleanup(e)

        print(f"do your cleanup, like GPIO.cleanup(), because we are exiting, because {e}")


# RUN
# note that Agent.run() does argparse for '-h|--help' in the base .run()
NetworkStatusAgent( ).async_run() # internally does asyncio.run( agent.run() )
