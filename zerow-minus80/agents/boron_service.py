#!/usr/bin/env -S sh -c 'exec "$(dirname $(realpath $0))/../.venv/bin/python3" "$0" "$@"'

import asyncio
import sys,os,json,re,time,traceback
import serial
from datetime import datetime
import paho.mqtt as paho # some constants|--help'
import aiomqtt as mqtt # for errors
import sdnotify
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib" )
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib-pump-pi" )
from simple_debug import debug
import agent

"""
BoronService maintains communication with the boron-lte over serial,
provides the standard service .ready_now event.
provides .publish( topic, message)
"""

import concurrent.futures

from service import ServiceBase
from aio_key import aio_key

class Recognizer:
    """
    We don't parse, just recognize simple token responses.
    
    something = Recognizer("Bob\n", lambda: ... )
    rez = something.consume( 'a' ) 
    # decide based on result
    something.reset()
    """

    def __init__(self, name, token, action=None):
        self._action = action
        self.name = name
        self.token = token
        self.reset()

    def reset(self):
        self.pos = 0

    def action(self):
        """override for more complex"""
        if self._action:
            #debug(f"ACT for {self.name}:{self.token} -> {self._action}")
            self._action()
        else:
            debug(f"No action for {self.token}")

    def consume(self, achar):
        """
            single character of type `str`, use somebyte.decode(encoding='ascii')
            returns -1==fail, 0=continue,1==hit!
        """
        if self.token[self.pos] == achar:
            self.pos += 1
            if self.pos == len(self.token):
                self.action()
                return 1 # hit
            else:
                return 0 # continue
        else:
            return -1 # fail

    def __str__(self):
        return f"<Recognizer {self.name}: '{self.token if self.token != None else ''}'>"
    def __repr__(self):
        return str(self)

class RecognizerOneOf(Recognizer):
    """
    Give a list of recognizers, pass .consume to each,
    dropping fails from consideration
    till all fail or one hits
    NB: shortest will hit first

    do .reset() to restart)
    """
    
    def __init__(self, name, *recognizers):
        self.recognizers = recognizers
        super().__init__(name,None) # `token` not used

    def reset(self):
        super().reset()
        self.candidates = self.recognizers # full list
        for recog in self.candidates:
            recog.reset()

    def consume(self, achar):
        next_candidates = []
        for recog in self.candidates:
            rez = recog.consume(achar)
            if rez==0:
                #debug(f"try {self}:{recog}")
                next_candidates.append(recog) # continue
            elif rez==1:
                #debug(f"OneOf! {self}:{recog}")
                return 1
            elif rez==-1:
                #debug(f"Drop {self}:{recog}")
                pass # drop from candidates
        self.candidates = next_candidates
        if not self.candidates:
            #debug(f"All failed {self}")
            return -1
        else:
            #debug(f"(continue {self}")
            return 0

class BoronService(ServiceBase):
    """
    We do `.ready_now.set()` whenever status changes,
    So, limited only by the pinginterval
    """

    Ping = "\n#\n"
    UserPass = "%Sup%{user} {password}\n"
    UserPassResponse = "%Sup%=\n"
    UserPassTimeout = 1 # <1 for serial response
    Publish = "$Pub${topic} {payload}\n"
    PublishACK = "$Pub$=\n"
    PublishNACK = "$Pub$!\n"

    def __init__(self, port=None,outport=None, username=None, password=None, interval=30, config_dir=None, mock_ready=False):
        """
        will default to /dev/ttyUSB0|1 (usb-ttl-adapter)
        will default to user/pass = aio_key()
        `interval` is the Ping check interval (deadman), and the read-port-timeout
        `mock_ready` causes us to pretend that the statemachine made it to: confirmed_user_pass. i.e. ready
            and pretends ping has never timed out
        """
        self.interval = interval
        self.inport = port
        self.outport = outport
        self.username = username
        self.password = password
        if self.username == None:
            self.username = aio_key()['IO_USERNAME']
        if self.password == None:
            self.password = aio_key()['IO_KEY']
        self.mock_ready = mock_ready

        self._set_value_first = True # once flag # setup
        super().__init__(config_dir=config_dir)

        if self.inport == None:
            serial_port = next( (x for x in ('/dev/ttyUSB0','/dev/ttyUSB1') if os.path.exists(x) ), None)
            debug(f"## BORON {serial_port} as boron", level=2)
            self.inport = serial.Serial(
                    port=serial_port, # a usb "serial-ttl" adapter to boron rx/tx
                    baudrate=115200,
                    parity=serial.PARITY_NONE,
                    stopbits=serial.STOPBITS_ONE,
                    bytesize=serial.EIGHTBITS,
                    #timeout= # boron service will set a timeout
            )


        if self.inport == None:
            raise Exception("Need an inport at least")

        # anything with .read(1), and .write(str)
        # if outport=none, use inport.write
        if self.outport==None:
            self.outport = self.inport
            if 'timeout' not in dir(self.outport):
                raise Exception(f"When port==outport, we need to .timeout on a read to ping, and {self.outport.__class__.__name__} doesn't do .timeout")

        if self.username == None or self.password == None:
            debug(f"WARNING: no user/pass for mqtt for {self.__class__.__name__}")

        self.saw_deadman = True # is flag for recovering
        self._saw_ping = asyncio.Event()
        self._saw_user_pass = asyncio.Event()
        self._wait_for_user_pass = asyncio.Event()
        self.confirmed_user_pass = False if not self.mock_ready else True
        self.ready_now.set() # cause initial state to be sent (which is "fail" unless mock_ready)

        # Everything ends with \n
        debug(f"Boron setup ping with {self.saw_ping}",level=2)
        self.recognizers = RecognizerOneOf('boron responses',
            Recognizer('ping', "#\n", self.saw_ping),
            Recognizer('userpass', self.__class__.UserPassResponse, self.saw_user_pass),
            Recognizer('publish-ack', self.__class__.PublishACK, self.saw_publish_ack ),
            Recognizer('publish-Nack', self.__class__.PublishNACK, self.saw_publish_nack )
        )
        #debug(f"Boron responses: {self.recognizers}")

    def set_ready_if_changed(self, new_confirmed_user_pass):
        """If changed, set ready_now. always return the new_confirmed_user_pass"""
        if self.confirmed_user_pass != new_confirmed_user_pass:
            self.ready_now.set()
        return new_confirmed_user_pass

    def saw_ping(self):
        debug(f"Boron saw ping", level=3)
        self._saw_ping.set() # for deadman

    def saw_user_pass(self):
        debug(f"Boron is setup w/user-pass!")
        self._saw_user_pass.set()

    def saw_publish_ack(self):
        debug(f"Boron Publish ACK",level=2)
        self._saw_ping.set() # counts for deadman

    def saw_publish_nack(self):
        # FIXME: do we need to do anything? maybe retry connection
        debug(f"Boron Publish FAIL NACK")

    async def run(self):
        debug(f"looking for \\n...",level=2)

        try:
            async with asyncio.TaskGroup() as tg:
                tg.create_task( self.consumer() ) # _saw_ping & _saw_user_pass
                if self.inport!=self.outport and not self.mock_ready:
                    # when same object, has to be done by alternating (timeout) with read, see .consumer
                    tg.create_task( self.pinger() )
                else:
                    debug("Ping'ing during read loop, because inport==output")
                tg.create_task( self.deadman() )
                tg.create_task( self.wait_for_user_pass() )
                debug(f"Boron tasks launched",level=2)
            debug(f"Exited boron.run")
        except (asyncio.exceptions.CancelledError,ExceptionGroup,Exception) as e:
            # We NEVER see ^c or CancelledError here (see the .inport.read stuff)
            debug("BORON run exception...")
            debug(f"BORON run exception {e.__class__.__name__}: {e}")
            raise e

    async def deadman(self):
        if self.mock_ready:
            self.saw_deadman = True
            self.confirmed_user_pass
            debug("Mock_ready!")
            return

        while True:
            #debug(f"boron deadman wait...")
            start = time.monotonic()
            try:
                async with asyncio.timeout(self.interval * 1.1):
                    await self._saw_ping.wait()
                    self._saw_ping.clear()
                    if self.saw_deadman: # also ==True if self.__class__.UserPassResponse timesout
                        self._wait_for_user_pass.set()
                        outstr = self.__class__.UserPass.format( user=self.username, password=self.password )
                        debug(f"Boron out: '{outstr}'",level=4)
                        self.outport.write( outstr.encode() )
                    self.saw_deadman = False
                    #debug(f"Boron saw deadman ping")
            except TimeoutError as e:
                if not self.saw_deadman:
                    debug(f"Boron dead! in {start-time.monotonic()}")
                self.saw_deadman = True
                self.confirmed_user_pass = self.set_ready_if_changed(False)

    async def wait_for_user_pass(self):
        while True:
            await self._wait_for_user_pass.wait()
            self._wait_for_user_pass.clear()

            debug(f"xBoron waiting for {self.__class__.UserPassResponse}",level=4)
            async with asyncio.timeout(self.__class__.UserPassTimeout):
                try:
                    # we could be here when a ping time's out
                    # and the %Sup% exchange could finish, 
                    # triggering us w/o the above _wait_for_user_pass
                    # That seems ok
                    await self._saw_user_pass.wait()
                    self._saw_user_pass.clear()
                    self._wait_for_user_pass.clear() # see note above

                    self.confirmed_user_pass = self.set_ready_if_changed(True)
                except asyncio.exceptions.CancelledError as e:
                    debug(f"Boron timeout user-pass response")
                    self.confirmed_user_pass = self.set_ready_if_changed(False)
                    self.saw_deadman = True # well, retry ping->user-pass->confirm

    @property
    def value(self):
        return self.confirmed_user_pass

    @value.setter
    def value(self,x):
        if not self._set_value_first:
            raise Exception("Setting .value not allowed (except by __init__ once)")
        self._set_value_first = False

    async def pinger(self):
        debug(f"Pinger using {self.outport} every {self.interval}")
        while True:
            debug(f"Boron ping every {self.interval}",level=2)
            self.outport.write(self.__class__.Ping.encode())
            self.outport.flush()
            await asyncio.sleep(self.interval)

    async def consumer(self):
        # also does ping for port=port
        debug(f"Boron Start serial reader... w/{self.inport}",level=2)

        if self.inport==self.outport:
            # have to do Ping when not doing .read, so timeout
            # fixme: we could do an async timeout wrapper around the self.inport.read
            self.inport.timeout = self.interval

            # initial ping
            self.outport.write(bytes(self.__class__.Ping,'utf-8'))
            self.outport.flush()

        till_eol = True # ignore till eof? we use that to synchronize the stream
        first_fail=True
        while True:
            achar = None

            # run the .read(1) in a thread to make it asyncio
            try:
                with concurrent.futures.ThreadPoolExecutor() as pool: # probably should be on outside of `while true`
                    try:
                        last_ping_time = time.monotonic()

                        # FIXME: BUG: this never can get cancelled (the .read hangs) if --use-stdin
                        # that also prevents recovery from mqtt connection failure
                        achar = await asyncio.get_event_loop().run_in_executor( pool, self.inport.read, 1 )
                        if len(achar)==0:
                            if time.monotonic()-last_ping_time > self.interval: # seconds
                                last_ping_time = time.monotonic()
                                # timeout -> ping
                                debug(f"Ping", level=2)
                                self.outport.write(bytes(self.__class__.Ping,'utf-8'))
                                self.outport.flush()
                            await asyncio.sleep(0.01)
                            continue
                    except (asyncio.exceptions.CancelledError) as e:
                        debug(f"BORON in_executor {e.__class__.__name__}: {e}")
                        traceback.print_exception(e)

                        import threading

                        debug(f"### Cancel read")
                        if 'cancel_read' in dir(self.inport):
                            self.inport.cancel_read() # this does unblock the .inport.read!
                        else:
                            debug(f"### Can't cancel read on {self.inport.__class__.__name__}, ^C&return till I exit")
                        await asyncio.sleep(0)

                        #debug(f"#T1 {threading.enumerate()}")

                        raise e # doesn't get handled till .inport.read finishes
                    #debug(f"BORON as pool bottom")
            except (Exception,asyncio.exceptions.CancelledError,KeyboardInterrupt,ExceptionGroup) as e:
                # for a serial port, we do see the re-raised Cancelled
                # this is really just here for debugging the cancel
                debug(f"BORON in as-pool {e.__class__.__name__}: {e}")
                raise asyncio.exceptions.CancelledError # doesn't get handled...

            # achar is a bytes array if we read from serial
            if isinstance(achar,bytes):
                achar = achar.decode(encoding='utf-8')
                
            #print(f">{achar if ord(achar[0]) >= 32 and ord(achar[0]) <= 126 else ord(achar[0])} till-eol? {till_eol}")
            if achar[0] != '\n'[0] and (ord(achar[0]) < 32 or ord(achar[0]) > 126):
                #print(f"!{achar if ord(achar[0]) >= 32 and ord(achar[0]) <= 126 else ord(achar[0])}")
                till_eol = True # probably in garbage land
                continue # discard non-printables
            if till_eol:
                print(f"~{achar if ord(achar[0]) >= 32 and ord(achar[0]) <= 126 else ord(achar[0])}")
                if  achar[0] == '\n'[0]:
                    debug(f"will .consume next...",level=4)
                    till_eol = False
                    first_fail = True
                    # we eat the \n
                continue # resyncing

            rez = self.recognizers.consume(achar)
            if rez == -1:
                if first_fail:
                    first_fail=False
                    debug(f"All Fail '{achar}' {self.recognizers}")
                self.recognizers.reset()
                if achar[0] != '\n':
                    debug(f"Resync to eol {self.recognizers}")
                    till_eol = True # resync to eol
            elif rez == 0:
                pass
            elif rez == 1:
                #print(f"\nHit {self.recognizers}")
                first_fail = True
                self.recognizers.reset()

    def publish(self, topic, payload):
        # the prefix of the topic the main class was listening on should be already stripped,
        # giving: $user/feeds/$feed
        # FIXME: that seems annoying, you have know username to call us. maybe prefix $user/ for us. but limits traffic...
        # For paid io.adafruit, feeds will be dynamically created
        # For free io.adafruit, they may not be
        message = self.__class__.Publish.format(topic=topic, payload=payload) # "$Pub${topic} {payload}\n"
        debug(f"Boron Publish: {topic} -> {message}",level=2)
        if self.mock_ready:
            # NB: this is for pytest, so must be expected format: $Pub$minus80.minus80 -80.9
            # yes, redundant, but output.write() isn't reliable for --use-stdin
            sys.stderr.write( message ) 
        self.outport.write(message.encode()) # NB: does not reliably show on stdout if --use-stdin
        # should get '$Pub$=' or '$Pub$!' response

if __name__ == '__main__':
        """
        sys.argv.pop(0)
        recognizers = [ Recognizer('?'+x, x) for x in sys.argv ]
        print(recognizers)
        """

        """
        recog = recognizers[0]
        while True:
            sys.stdout.write("Test: ")
            sys.stdout.flush()
            s = sys.stdin.readline()
            for c in s:
                rez = recog.consume(c)
                sys.stdout.write(c)
                if rez == -1:
                    print(f"\nFail '{c}' {recog}")
                    recog.reset()
                    break
                elif rez == 0:
                    pass
                elif rez == 1:
                    print(f"\nHit {recog}")
                    recog.reset()
                    break
        """
        """
        recog = RecognizerOneOf("test", *recognizers )
        while True:
            sys.stdout.write("Test: ")
            sys.stdout.flush()
            s = sys.stdin.readline()
            for c in s:
                rez = recog.consume(c)
                sys.stdout.write(c)
                if rez == -1:
                    print(f"\nFail '{c}' {recog}")
                    recog.reset()
                    break
                elif rez == 0:
                    pass
                elif rez == 1:
                    print(f"\nHit {recog}")
                    recog.reset()
                    break
        """
        async def testt():
            boron = BoronService(sys.stdin, sys.stdout, sys.argv[1], sys.argv[2])

            async def heartbeat():
                while True:
                    print(f"B: {boron.value}")
                    sys.stdout.flush()
                    await asyncio.sleep(5)
            async with asyncio.TaskGroup() as tg:
               tg.create_task( boron.run() ) 
               tg.create_task( heartbeat() )
        #asyncio.run( testt() )

# Just run the BoronService w/o the lib/agent. no mqtt, etc.
if len(sys.argv)>1 and sys.argv[1] == '--dev-service':
    async def main():
        cmd = sys.argv.pop(0)
        sys.argv.pop(0)
        sys.argv.insert(0,cmd)
        if len(sys.argv) > 1 and sys.argv[0] != None:
            # FIXME: this is borken, not sure how it ever worked
            debug("Using stdin/stdout as boron: you have to pretend to be boron\n")
            me = BoronService( sys.stdin, sys.stdout.buffer, config_dir= 'config/agents' )
        else: 
            me = BoronService( None,None, config_dir= 'config/agents' )
        asyncio.create_task( me.run() )
 
        while True:
            await me.ready_now.wait()
            me.ready_now.clear()
            debug(f"Update ok {me.__class__.__name__} {me.value}")
    asyncio.run( main() )
    exit(0)

class BoronServicesAgent(agent.Agent):
    """
    """

    mqtt_config = {
        "device" : "minus80", # advertise's topic = local/devices/$device/$name
        "name" : "boron-lte-aio",
    }

    # see our systemd.units/xxxx.py.service
    sdnotifier = sdnotify.SystemdNotifier()
    WatchdogSec = 5*60 # same value as in systemd.units/agents__all_services.py.service
    systemd_watchdog_period = WatchdogSec/5 # smaller than WatchdogSec, with tolerance for some other slow operation
    lte_keepalive_period = 4 * 60 * 60 # send a keepalive on the boron-lte channel

    def __init__(self):
        super().__init__()
        self.boron_check = None # gets set in loop
        self.boron_publish_topic = self.topic_for_command('aio-failover/#')
        self.all_publish = [] # pairs of [full-topic, lambda-value) # to publish for /get/all

    def argparse_add_arguments(self, parser ):
        # add your argparse arguments, override mqtt_config, etc
        parser.set_defaults(mqtt_config='config/mqtt-multi-config.py') # override

        parser.add_argument( '--use-stdin', help="Instead of /dev/serial0, for boron-communication ", action='store_true')
        parser.add_argument( '--mock-boron-ready', help="With --use-stdin, will act like we are ready with boron (skipping config/pinging)", action='store_true')
        # this doesn't get detected here: see the 
        #   if sys.argv[1] == '--dev-service':
        # above
        parser.add_argument( '--dev-service', help="Run just the BoronService class w/o lib/agent wrapper. 0 args=serial, 1 arg=stdin/stdout", action='store_true')

    async def handle_publish_values(self, message):
        """
        Immediately publish current values.
        They may be Null if not ready.
        """
        # FIXME: factor this functionality into agent.py (from other agents too)
        #debug(f"republish {self.all_publish}")
        for topic,payload_fn in self.all_publish:
            #debug(f"Republish {payload_fn()}")
            self.queue_a_message( topic, payload_fn() )

    async def handle_boron_publish(self, message):
        """Republish local/commands/boron-lte/aio-failover/$user/feeds/$group.$feed
        -> io.adafruit.com mqtt topic $user/feeds/$group.$feed
            with verbatim payload (needs to be one line)
        via Boron-lte
        See BoronService.publish() above
        """
        if self.boron_check:
            # FIXME extract sub-topic
            # local/commands/boron-lte/aio-failover/ $user/feeds/$x.$x

            # our subscribe-topic has trailing '#'
            # but we expect/extract that trailing part as the actual topic: $user/feeds/$x.$x
            actual_topic = message.topic[len( self.boron_publish_topic )-1: len(message.topic)] 

            # verbatim payload
            #debug(f"## BORON: {actual_topic} : {message.payload}")
            payload = message.payload
            if isinstance(payload,dict) or isinstance(payload,list):
                payload = json.dumps(payload)
            self.boron_check.publish(actual_topic, message.payload)

    def topics(self):
        # DRY up some topics
        self.topic_aio_failover = self.topic_for_event('connected')
        self.topic_lte_keepalive = "minus80.boron-lte-keepalive" # will get prefixed to $user/feeds/minus80.boron-lte-keepalive

        return super().topics( {

            # listen
            self.boron_publish_topic : { "handler": self.handle_boron_publish },
            self.topic_for_get('all') : { "handler": self.handle_publish_values },

            # publish:
            self.topic_aio_failover : { "description": "BoronService (to AIO) ready, 1|0" },
            self.topic_lte_keepalive : { "description": f"AIO topic keepalive, prefixed with $user/feeds/, sent every {self.__class__.lte_keepalive_period}, over boron-lte channel" },
        })

    async def publish_value_when_ready(self, obj, *topic_value, gate=None):
        """
        Publish the `topic` : `value()` 
        When obj.ready_now.wait() finishes (a asyncio.Event(), .set() )
        *topic_value is a sequence of: topic, value_fn
            Thus, a single `ready_now` on the object works for multiple topic-value publish
        And then gate() is tested (if != None)
        Those topic/value pairs are saved in a list for `self.topic_for_get('all')`: `local/get/services/minus80/all`
        """
        # FIXME: factor with network_status.py

        async def _if_ready_now( obj, awaitable, *args, **kwargs ):
            """
                Calls awaitable(*args,**kwargs) when object update's (`ready`)
                The gate` is checked at that time, so: when ready, and then gate'd, the publish
                E.g. publish w/ publisher()
            """

            while True:
                await obj.ready_now.wait()
                obj.ready_now.clear()
                debug(f"Saw Wait for {obj.__class__.__name__}.ready_now, will call {awaitable}")
                await awaitable(*args,**kwargs)

        async def _gated_multi_publisher(*topic_value, gate=None):
                """ Wrapper for publishing via _if_ready_now, with last chance pred-gate """
                if gate==None or gate():
                    # sequential publish, could do in parallel, but we don't have to worry about captures this way
                    alternator = iter(topic_value)
                    for topic,value in zip(alternator,alternator): # by 2's
                        await self.mqtt_client.publish( topic, json.dumps( value() ) )
                else:
                    debug(f"not-gate'd _publisher() {topic_value}",level=2)

        # save topic/value pairs for `self.topic_for_get('all')`: `local/get/services/minus80/all`
        alternator = iter(topic_value)
        for topic,value in zip(alternator,alternator): # by 2's
            self.all_publish.append( [ topic, value ])

        # test obj.ready_now, then _gated_multi_publisher( *topic_value, gate=gate)
        await _if_ready_now( obj, _gated_multi_publisher, *topic_value, gate=gate)

        debug(f"##PubWhenReady: {topic} via {obj.__class__.__name__}")

    async def systemd_watchdog_ping(self):
        if not os.environ.get('NOTIFY_SOCKET',None):
            debug("WARNING: Not systemd, so no watchdog recover")
        debug(f"Watchdog {self.__class__.WatchdogSec} sec, ping every {self.__class__.systemd_watchdog_period}")

        while(True):
            # alleged NOOP if not run by systemd
            debug(f"systemd watchdog...", level=2)
            self.__class__.sdnotifier.notify("WATCHDOG=1")
            await asyncio.sleep(self.__class__.systemd_watchdog_period)
        
    async def on_boron_status(self):
        # periodically publish status
        try:
            # catch and print error as last effort

            was = None
            was_ct = 0
            period = 30 
            while(True):
                payload = 1 if self.boron_check.value else 0
                debug(f"status every {period} {self.topic_aio_failover} {payload}", level=2)
                # FIXME: this is a pattern: low-rate if unchanged, etc
                if payload != was or was_ct >= 3:
                    self.queue_a_message(self.topic_aio_failover, payload )
                    was = payload
                    was_ct = 0
                else:
                    was_ct += 1

                await asyncio.sleep( period )
        except Exception as e:
            debug("Fail {e}")
            traceback.print_exception(e)

    async def on_boron_lte_keepalive(self):
        # periodically publish via lte as keepalive
        try:
            # catch and print error as last effort

            while(True):
                period = 10 # look for the topic to appear and boron-lte to be ready
                for topic,value in self.all_publish:
                    if topic == self.topic_aio_failover and self.boron_check.value:
                        # the topic has a value (was published), and we can publish on boron-lte
                        period = self.__class__.lte_keepalive_period # ok, period is longer if we publish

                        payload = value()
                        debug(f"LTE keepalive every {self.__class__.lte_keepalive_period} {json.dumps(payload)}")
                        self.boron_check.publish(self.topic_lte_keepalive, json.dumps(payload) )
                        break # found it

                await asyncio.sleep( period )
        except Exception as e:
            debug("Fail {e}")
            traceback.print_exception(e)

    async def loop(self, first_time):
        # required.
        # have to busy wait if you want to do nothing
        debug(f"LOOP...", level=(0 if first_time else 2))

        async with asyncio.TaskGroup() as tg:

            # The premise is that if the async world gets stuck, so will watchdog-ping
            tg.create_task( self.systemd_watchdog_ping(), name="systemd_watchdog_ping")

            # Boron
            port=[]
            serial_port=None
            mock_ready = False
            if self.parsed_args.use_stdin:
                #debug(f"## STDIN as boron emulator")
                serial_port = 'stdin'
                port = [ sys.stdin, sys.stdout.buffer ]
                mock_ready = self.parsed_args.mock_boron_ready
            else:
                if self.parsed_args.mock_boron_ready:
                    raise Exception("Can't use --mock-boron-ready with out --use-stdin")

                candidates = ('/dev/ttyUSB0','/dev/ttyUSB1')
                port_wait_interval = 0.2
                progress_interval_ct = 5 / port_wait_interval # every 5 seconds
                progress_interval_i = 0
                while not serial_port:
                    progress_interval_i += 1
                    serial_port = next( (x for x in candidates if os.path.exists(x) ), None)
                    if serial_port:
                        debug(f"## BORON {serial_port} as boron", level=(0 if first_time else 2))
                        port = [ None, None ]
                        break
                    if progress_interval_i % progress_interval_ct == 0:
                        if progress_interval_i == progress_interval_ct:
                            debug(f"Waiting for a serial port: {candidates}")
                        sys.stderr.write('.'); sys.stderr.flush()
                    await asyncio.sleep(port_wait_interval)

            # FIXME: publish NO PORT if none
            if not serial_port:
                debug(f"## BORON: no port /dev/ttyUSB*")
            # fixme: probably a copy of the mqtt_config ['cloud']?
            self.boron_check = BoronService(*port, config_dir= os.path.dirname( self.config_file() ), mock_ready=mock_ready )
            debug(f"## BORON {self.boron_check.inport} as boron", level=(0 if first_time else 2))

            tg.create_task( self.boron_check.run(), name='boron_check.run' )
            tg.create_task( 
                self.publish_value_when_ready( self.boron_check, self.topic_aio_failover, (lambda: 1 if self.boron_check.value else 0) ),
                name='publish_value_when_ready boron_check'
            )
            await asyncio.sleep(5)

            tg.create_task( self.on_boron_lte_keepalive() )
            tg.create_task( self.on_boron_status() )

            # FIXME: add topics() like: local/get/tof/tof1/value and local/get/$me/values
            # which means dry'ing these services

            debug("bottom of create_tasks, all launched", level=(0 if first_time else 2))
            #if first_time:
            #    debug(f"DEBUG closing")
            #    await self.mqtt_client.disconnect()

        #while(True):
        #    await asyncio.sleep(1)
        debug("FELL off bottom of LOOP: tasks died", level=(0 if first_time else 2))


    async def cleanup(self, e):
        await super().cleanup(e)

        print(f"do your cleanup, like GPIO.cleanup(), because we are exiting, because {e}")


# RUN
# note that Agent.run() does argparse for '-h|--help' in the base .run()
BoronServicesAgent( ).async_run() # internally does asyncio.run( agent.run() )
