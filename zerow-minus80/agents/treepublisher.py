#!/usr/bin/env -S sh -c 'exec "$(dirname $(realpath $0))/../.venv/bin/python3" "$0" "$@"'
# try --help
# see LocalListener as main class

import asyncio
import sys,os,json,re,datetime,typing,traceback
from copy import copy
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib" )
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib-pump-pi" )
from simple_debug import debug
import paho.mqtt as paho # some constants|--help'
import aiomqtt as mqtt # for errors
import agent
from rate_limiter import RateLimiter
from dynamic_scope import *

# Our job is dealing w/incoming, which is very noisy by default, turn it down
agent.AnnounceIncoming = 1

class RePublisher:
    """
    A service for re-publishing mqtt-messages.
    Listens on /local/commands/tree/$self-topic
    for a payload of { "topic" : $topic, "payload" : $payload }
    and re-publishes as $topic -> $payload
    on some publisher (like multiplexed-mqtt, or file, etc)
    """

    def __init__(self, mqttagent, description, publisher):
        self.description = description
        self.publisher = publisher
        self.self_topic = mqttagent.topic_for_command(publisher.name, with_device=False)

    def tree_description(self):
        return self.publisher.description() if 'description' in dir(self.publisher) else f"<{self.publisher.__class__.__name__}>"

    async def republish(self, message):
        if not isinstance(message.payload,dict):
            debug(f"WARNING: republish expects a payload of `dict`, (for {self.self_topic}) saw {message.payload}")
            return
        if not (topic:=message.payload.get('topic',None)):
            debug(f"WARNING: republish expects a payload with topic:$topic, (for {self.self_topic}) saw {message.payload}")
            return
        if topic == "":
            debug(f"WARNING: republish expects a topic to be >= 1 character, (for {self.self_topic}) saw {message.payload}")
            return
        if 'payload' not in message.payload:
            debug(f"WARNING: republish expects a payload with payload:$payload, (for {self.self_topic}) saw {message.payload}")
            return

        if not self.publisher:
            debug(f"Warning: republish had no publisher, (for {self.self_topic}) saw {message.payload}")
            return

        orig_topic=topic
        topic = str(topic)

        payload = message.payload['payload']
        debug(f"## REPUBLISH {self.self_topic}: {topic} = {payload}",level=2)
        if self.publisher:
            asyncio.create_task( self._publish(self.publisher, topic, payload) )
        await asyncio.sleep(0)

    async def _publish(self, publisher, topic, payload):
        """wrapper to catch certain errors
        """

        #try:
        await publisher.publish( topic, payload )
        #except asyncio.exceptions.CancelledError as e:

    def self_topic(self): # i.e. where we listen
        raise Exception("Implement me")


class PublisherBase : 
    # .name
    # .publish
    # print_tree(indent=0)

    def __init__(self, tree_name):
        self.name=tree_name

    async def publish(self, topic, message, depth=0):
        # can block
        raise Exception(f"{'' if depth==0 else '  '*depth}PUBLISH {self.__class__.__name__}:{self.name} {topic} = {message}")
        return True # must return success|failure

    async def run(self):
        # can block, should be called in a task-group etc
        raise Exception(f"{'' if depth==0 else '  '*depth}run Not Implemented {self.__class__.__name__}:{self.name}")
        return True # must return success|failure

class PublisherTransformTopic(PublisherBase):
    """
    Transforms the topic for another publisher.
    Takes the name & description of the other publisher
    Don't forget to separately do any .run() that `other` needs
    """
    def __init__(self, other, xform_fn):
        super().__init__( other.name + " w/xform" )
        self.other = other
        self.xform_fn = xform_fn

    async def publish(self, topic, message, depth=0):
        # can block
        topic = self.xform_fn(topic)
        debug(f"{'' if depth==0 else '  '*depth}PUBLISH {self.__class__.__name__}:{self.name} {topic} = {message}",level=2)
        rez = await self.other.publish( topic, message, depth+1 )
        #debug(f"{'' if depth==0 else '  '*depth}REZ {rez if rez!=None else '<none>':2} {self.__class__.__name__}:{self.name} from {self.other}:{self.other.name}")
        return rez

    def description(self):
        return self.other.description()

class PublisherTransform(PublisherBase):
    """
    Transforms the topic & possibly payload.
    Drops the message if not in the config mapping.
    Takes a mapping file or mapping dict:
        { 
            "$incoming topic" : "$outgoing topic", # simple topic map, payload stays the same
            "$intopic" : { "topic" : "$out topic", "payload" : { ...., "value" : null } }, # replace the `value`'s null
        }
    Don't forget to separately do any .run() that `other` needs

    """
    def __init__(self, other, mapping, map_description=None):
        super().__init__( other.name + " w/xform" )
        self.other = other
        self.map_description = map_description
        if isinstance(mapping, dict):
            self.mapping = mapping
        elif os.path.isfile(mapping):
            with open(mapping, 'r') as fh:
                self.mapping = json.load(fh)
        else:
            raise Exception(f"mapping wasn't a dict nor file: {mapping.__class__.__name__} {mapping}")

    def description(self):
        return "# Transform topic:\n" + "{\n"+"\n\t".join( ( f"{k} : {self.mapping[k]}" for k in sorted(self.mapping.keys()) ) ) +"\n}" + "\nThen:\n" + self.other.description()

    def check_topics_can_transform_or_die(self, test_topics):
        fails = []
        for a_test_topic in test_topics:
            info = self.mapping.get( a_test_topic, None )
            if not info:
                fails.append(a_test_topic)
        if fails:
            where=""
            if self.map_description:
                where += f"{self.map_description}"
            where += f"\n\t{self.mapping}" 
            raise Exception("We require these topics to have a mapping:\n\t" + ('\n\t'.join(fails))+ "\n\t#In "+where)

    def xform(self, topic, message):
        info = self.mapping.get( topic, None )
        debug(f"## xform {info}",level=4)
        if not info:
            debug(f"Warning: no transform (in {self.name}) in mapping for '{topic}', mapping={self.mapping}")
            return (None,None)

        if isinstance(info,str):
            # simple topic map
            return (info,message)

        elif isinstance(info,dict) and 'topic' in info and 'payload' in info and isinstance(info['payload'],dict):
            # topic:, payload: { ..., value:null }
            new_payload = copy(info['payload'])
            debug(f"from: {new_payload}", level=4)
            if 'value' in new_payload and new_payload['value'] == None:
                new_payload['value'] = message

            return info['topic'], new_payload
        else:
            raise Exception(f"mapping wasn't a dict with topic & payload (as dict): {mapping.__class__.__name__} {mapping}")
        
    async def publish(self, topic, message, depth=0):
        # can block
        new_topic, new_message = self.xform(topic, message)
        debug(f"{'' if depth==0 else '  '*depth}PUBLISH {self.__class__.__name__}:{self.name} {new_topic} = {new_message}",level=2)
        if topic:
            rez = await self.other.publish( new_topic, new_message, depth+1 )
            #debug(f"{'' if depth==0 else '  '*depth}REZ {rez if rez!=None else '<none>':2} {self.__class__.__name__}:{self.name} from {self.other}:{self.other.name}")
            return rez
        else: 
            debug(f"NO mapping")
            return True # not a failover situation

class AIOTransform(PublisherTransform):
    """We assume other is an aio config
    Don't forget to separately do any .run() that `other` needs
    """
    def __init__(self, other, mapping, aio_config, map_description=None):
        # The aio_config is used to get the username, but other config info in it is not used
        # (i.e. does not determine the mqtt connection)
        super().__init__(other,mapping, map_description)
        self.aio_config = aio_config

    def description(self):
        return (
            f"# Transform topic:{self.map_description or ''}" 
            + (
                "\n{\n"+"\n\t".join( ( f"{k} : {self.mapping[k]}" for k in sorted(self.mapping.keys()) ) ) +"\n}" 
                if not self.map_description
                else ''
              )
            + "\nThen: $resultTopic -> $aiousername/$resultTopic"
            + "\nThen:" 
            + "\n" + self.other.description()
        )

    def xform(self, topic, message):
        # standard topic mapping xform
        # and prefix with AIO username (uses unix username if no username in the mqtt (`aio_config`) config for debugging)
        new_topic,new_message = super().xform(topic,message)
        if new_topic:
            if not self.aio_config.get("username",None):
                debug(f"WARNING: using {self.__class__.__name__} with an aio_config ('cloud0') w/o a username: using env[USER]={os.environ.get('USER')}")
            new_topic = self.aio_config.get("username", os.environ.get('USER')) + "/" + new_topic
        return (new_topic, new_message)

class PublisherRateLimit(PublisherBase):
    """
    Rate limits to the underlying publisher.
    Note that discarding due to rate-limit is considered a successful publish!
    Don't forget to separately do any .run() that `other` needs
    """

    def __init__(self, other, max, secs, mqtt_announce=None, rate_limit_topic=None):
        super().__init__( other.name + f" w/limit {max}/{secs}secs" )
        self.other = other
        self.rate_limiter = RateLimiter(max,secs)
        self.mqtt_announce = mqtt_announce
        self.rate_limit_topic = rate_limit_topic

    def description(self):
        return f"{self.rate_limiter.max}/{self.rate_limiter.secs} secs(\n" + self.other.description() + "\n)"

    async def publish(self, topic, message, depth=0):
        if self.rate_limiter.allowed():
            debug(f"{'' if depth==0 else '  '*depth}PUBLISH {self.__class__.__name__}: (w/rl {self.rate_limiter.status()}) {self.name} {topic} = {message}",level=2)
            if int(os.getenv('DEBUG','0')) >= 4 and self.mqtt_announce:
                tp = [self.rate_limit_topic, { 'description': f"RATE_LIMIT: {self.name}", "sofar": self.rate_limiter.status(), 'topic': topic, 'payload':message } ]
                await self.mqtt_announce.publish(*tp, depth+1 )
            #debug(f"{'  '*(depth+1)}otherPUBLISH {self.other.__class__.__name__}:{self.other.name}...")
            return await self.other.publish( topic, message, depth+1 )
        else:
            debug(f"{'' if depth==0 else '  '*depth}NOPUBLISH {self.__class__.__name__}:{self.name} {topic} = {message}",level=2)
            if self.mqtt_announce:
                tp = [self.rate_limit_topic, { 'description': f"RATE_LIMIT: {self.name}", "LIMITED": self.rate_limiter.status(), 'topic': topic, 'payload':message } ]
                debug(f"{'' if depth==0 else '  '*depth} Publish rate-limit'ed {self.mqtt_announce.name}, {tp}")
                await self.mqtt_announce.publish(*tp, depth+1 )
            return True
        
class PublisherMQTT(PublisherBase):
    """
    Simple wrapper for a mqtt-client:
    Don't forget to implement `.run()` if appropriate
    
    """
    def __init__(self, name, mqtt_client, qos=1, retain=False):
        super().__init__( name) # {mqtt_client._client._host}:{mqtt_client._client._port}
        self.mqtt_client = mqtt_client
        self.qos = qos
        self.retain = retain

    def description(self):
        return f"OnMQTT({self.name})"

    async def publish(self, topic, message, depth=0):
        # must not block (i.e. no await)
        debug(f"{'' if depth==0 else '  '*depth}PUBLISH {self.__class__.__name__}:{self.name} {topic} = {message}",level=2)
        if self.mqtt_client and self.mqtt_client._client.is_connected():
            try:
                # succeeds or throws
                if isinstance(message,dict):
                    message = json.dumps(message)
                await self.mqtt_client.publish( topic, message, qos=self.qos, retain=self.retain )
                return True
            except mqtt.exceptions.MqttError as e:
                # FIXME: ratelimit w/"n-times", but for the __class__
                debug(f"Fail to publish {self.__class__.__name__}: {self.name} {e}. for\n\t{topic} = {message}")
                try:
                    if "The client is not currently connected" in str(e): # or "Operation timed out" in str(e):
                        # the retry-connect doesn't seem to see that it is closed, so force close to set the _state
                        await self.mqtt_client.disconnect()
                except mqtt.exceptions.MqttError as e:
                    if 'The client is not currently connected' not in str(e):
                        debug(f"ERROR: .disconnect() failed (assuming it did close)! {e}")
                debug(f"Connected? {self.mqtt_client._client.is_connected()}")
                return False
        else:
            debug(f"{'' if depth==0 else '  '*depth}NOPUBLISH {('' if self.mqtt_client._client.is_connected() else 'not-conn') if self.mqtt_client else 'no-client'} {self.__class__.__name__}:{self.name} {topic} = {message}",level=2)

        return False

class PublisherMQTTByConfig(PublisherMQTT):
    """
    An mqtt publisher, but by mqtt-config, and reconnects if you .run() somewhere
    """

    def __init__(self, name, mqtt_config): # FIXME: do we want a last-will?
        super().__init__(name, None) # will setup client if someone calls run()
        self.mqtt_config = mqtt_config

    mqtt_connect_with_retry = agent.Agent.mqtt_connect_with_retry
    resolve_mdns = agent.Agent.resolve_mdns

    def description(self):
        return f"OnMqtt({self.name}) {self.__class__.__name__}"

    async def run(self):
        """.run() as a task usually"""
        ct=0
        async for client,first_time in self.mqtt_connect_with_retry(**self.mqtt_config,will=None):
            # just need to capture
            self.mqtt_client = client 
            debug(f"Opened {self.name} {client._client._state} w/{self.mqtt_config}")
            check_interval=10
            announce_ct = (5*60)/check_interval
            announce_i = 0
            while client._client.is_connected():
                if announce_i % announce_ct == 0:
                    # to leave some info about when we were connected, and when it was failing
                    debug(f"state? {self.name} {client._client._state}")
                announce_i += 1
                await asyncio.sleep(check_interval)
            debug(f"Fell out of {self.__class__.__name__}.run()'s retry, again...")

class PublisherBoron(PublisherTransformTopic):
    """Via the boron lte, really we just need to change the topic """

    RepublishTopic = "local/commands/minus80/boron-lte-aio/aio-failover"

    def __init__(self,name, mqtt_client):
        super().__init__(
            mqtt_client,
            lambda topic: f"{self.__class__.RepublishTopic}/{topic}"
        )
        self.name = name

    def description(self):
        return (
            f"Republish on the boron-lte, via the {self.__class__.RepublishTopic} -> agents/boron_service.py\n"
            + "Assumes aio-mapping has happened, prefixes the service-topic, embeds the topic/payload"
        )
        
class PublishTreePred(PublisherBase):
    """Supply a lambda that gives T|F, which becomes the return value of the .publish"""
    def __init__(self, name, pred):
        super().__init__(name)
        self.pred = pred

    def description(self):
        return f"if(`{self.name}`) then branch is satisfied (usu. `not pred`)"

    async def publish(self, topic, message, depth=0):
        # must not block (i.e. no await)
        rez = self.pred()
        debug(f"{'' if depth==0 else '  '*depth}PUBLISH {self.__class__.__name__}:{self.name+': '} {rez}",level=2)
        return rez

class PublisherNull(PublisherBase):
    def description(self):
        return f"Null publishing {self.__class__.__name__} : {self.name}"

    async def publish(self, topic, message, depth=0):
        # must not block (i.e. no await)
        debug(f"{'' if depth==0 else '  '*depth}PUBLISH {self.__class__.__name__}:{self.name+': '} {topic} = {message}",level=4)
        return True

class PublisherTestOk(PublisherBase):
    def description(self):
        return f"Dumy prints debug {self.__class__.__name__}"

    async def publish(self, topic, message, depth=0):
        # must not block (i.e. no await)
        debug(f"{'' if depth==0 else '  '*depth}PUBLISH {self.__class__.__name__}:{self.name+': '} {topic} = {message}",level=2)
        return True

class PublisherTestFail(PublisherBase):
    async def publish(self, topic, message, depth=0):
        # must not block (i.e. no await)
        debug(f"{'' if depth==0 else '  '*depth}PUBLISH {self.__class__.__name__}:{self.name+': '} {topic} = {message}")
        return False

class PublishTreeParallel(PublisherBase):

    def __init__(self, tree_name, *trees):
        super().__init__(tree_name)
        self.trees = trees

    def description(self):
        desc = []
        for t in self.trees:
            desc.append( t.description() if 'description' in dir(t) else f"<{t.__class__.__name__}>" )
        return "||(\n\t|" + '\n\t|'.join(desc) + "\n)"

    async def publish(self, topic, message, depth=0):
        # fixme: queue to serialize...?
        debug(f"{'' if depth==0 else '  '*depth}PUBLISH {self.__class__.__name__}:{self.name+': '} {topic} = {message}",level=2)
        # FIXME: need a result of "did any work". so have to await
        tasks = []
        async with asyncio.TaskGroup() as tg:
            for tree in self.trees:
                debug(f"{'  '*(depth+1)}Queue a parallel {tree}:{tree.name}", level=2)
                tasks.append( tg.create_task( tree.publish( topic, message, depth+1), name=f'tree.publish {tree.name}') )
        
        # any true?
        rez = next(( True for x in tasks if x.result()==True ),False)

        debug(f"{'' if depth==0 else '  '*depth}Done w/{rez}: {self.__class__.__name__}:{self.name+': '} {topic} = {message}",level=2)
        return rez

class PublishTreeFailover(PublisherBase):
    def __init__(self, tree_name, *trees):
        super().__init__(tree_name)
        self.trees = trees

    def description(self):
        desc = []
        for t in self.trees:
            desc.append( t.description() if 'description' in dir(t) else f"<{t.__class__.__name__}>" )
        return "1stOf(\n?" + '\n\t?'.join(desc) + "\n)"

    async def publish(self, topic, message, depth=0):
        # fixme: queue to serialize...?
        debug(f"{'' if depth==0 else '  '*depth}PUBLISH {self.__class__.__name__}:{self.name+': ' } {topic} = {message}",level=2)
        debug(f"{'' if depth==0 else '  '*depth}   tree ct {self.trees}",level=2)
        rez = False
        for tree in self.trees:
            debug(f"{'' if depth==0 else '  '*(depth+1)}Await a failover publish {tree}:{tree.name}",level=2)
            rez = await tree.publish( topic, message, depth+1)
            if rez:
                break; # Done
            debug(f"{'' if depth==0 else '  '*(depth+1)}  failed (next) {tree}:{tree.name}", level=1)

        debug(f"{'' if depth==0 else '  '*depth}Done w/{rez}: {self.__class__.__name__}:{self.name+': '} {topic} = {message}",level=2)
        return rez

class LocalListener(agent.Agent):
    # "main" class
    """We forward a message to a tree of "publishers" with failover:
        `and` means do all
        `or` means failover
    The incoming topic is the tree to publish to. Try `--topics`.
    The incoming mqtt payload is like a node-red message:
        { "topic" : "actual/topic", "payload" : "..." }
    The ["payload"]:
        no "payload" key -> $incoming_[payload]
        { "payload" : null } -> { "payload" : $incoming_[payload] }
        { "payload" : { ..., "value":null} } -> { ..., "value" : $incoming_[payload] }
    Note that `qos` and `retain` may not be specified. The PublishTree sets that policy.
    
    We can rate-limit.
    Rates are in mqtt-config.

    We expect multiple mqtt-connection configs in the mqtt-config file (if there are multiple mqttpublishers).

    The topic->tree mapping is `config/agents/treepublisher.json`:
        {
        "#" : "topics to listen for, then republish on a failover/distribute tree",
        "#" : "Nb: treepublisher.py also listens to `local/commands/publisher/$tree`, see --topics",
        "#" : "nb: to specify the topic-transform for AIO, you'll need a entry in config/aio-topic-mapping.json",
        "#" : "Get (most) available topics in entire system from: tool/topics",
        "#" : "Get tree names from: agents/tree-publisher.py --help",
        "#$topic" : "$treename",
        "#$topic" : [ "$treename", "$treename" ], "#":"Not implemented yet...",

        "local/events/minus80/minus80" : "aio-expensive",
        "local/events/minus80/humidity" : "aio-cheap",
        }


    """
    # lifecycle is:
    # SomeAgent()
    # .argparse()
    # connect with `will` based on .description
    # taskgroup.create_task( .listener( ) )
    # taskgroup.create_task( .advertise() )
    # taskgroup.create_task( .loop() )
    # await .cleanup(exception) if we catch an exception
    #   always call super().cleanup(exception) if you override

    # the local mqtt
    mqtt_config = {
        "device" : "publisher",
        "tree-name" : "tree", # used by most topics
        "name" : "tree-publisher",
    }

    def __init__(self):
        super().__init__() # minimal setup
        self._topics = None # cached
        self._mqtt_to_reconnect = {} # name: {mqtt-config}. see loop()
        self.wan_check = True # assume it is working until someone tells us (i.e. maybe servcie-agent didn't run?)
        self.boron_check = False # assume it is not-working until someone tells us (i.e. maybe servcie-agent didn't run?)

        self.aio_map_file = 'config/aio-topic-mapping.json'
        if os.path.isfile(self.aio_map_file):
            with open(self.aio_map_file, 'r') as fh:
                try:
                    self.aio_mapping = json.load(fh)
                except json.decoder.JSONDecodeError as e:
                    sys.stderr.write(f"In {self.aio_map_file}\n")
                    raise e
            for t in list(self.aio_mapping.keys()):
                if t.startswith('#'):
                    del self.aio_mapping[t]
        else:
            raise Exception(f"mapping wasn't a file: {aio_mapping.__class__.__name__} {aio_mapping}")

        self.republishers = self.load_json_config() or {}
        for k in list(self.republishers.keys()):
            if k.startswith('#'):
                del self.republishers[k]
        debug(f"## repub {self.republishers}")

    def topics(self):
        if not self._topics:
            # once, cached

            # most of our topics are '.../tree' not .../tree-publisher, so pretend
            # kinda hackish
            with dynamic_scope( self.mqtt_config, 'name', self.mqtt_config['tree-name'] ):

                aio_map_description = f"per {self.aio_map_file}, annotations 'has aio:' below"

                # We have to do this (republisher-trees) here, so we can say what topics we listen to

                # you must NOT use mqtt_local w/o transforming the topic, otherwise infinite loop
                mqtt_local = PublisherMQTT('mqtt-lan', self.mqtt_client)

                rate_limit_topic = self.topic_for_event('rate-limit')

                # all the mqtt but self.mqtt_client must be reconnect-attempted
                mqtt_aio = PublisherRateLimit(
                    _aioxform:=AIOTransform( # uses config/aio-topic-mapping.json
                        _mqtt_aio:=PublisherMQTTByConfig('mqtt-aio', self.mqtt_config['connections']['cloud0'] ),
                        self.aio_mapping, # standard aio topic transform + `$user/` prefix
                        # FIXME: probably the `connections` key shouldn't be hard-coded
                        self.mqtt_config['connections']['cloud0'], # only for the .username, or env['USER'] if none=debug
                        map_description=aio_map_description
                    ),
                    # adafruit is 30/min free. "plus" is 60/min
                    # a small time-window tends to limit the burst rate of a single service
                    # (but also any overlap of that burst with some other service)
                    # 
                    24, 60,
                    mqtt_announce=mqtt_local,
                    rate_limit_topic=rate_limit_topic
                )
                self._mqtt_to_reconnect['mqtt-aio'] = _mqtt_aio

                # fixme: we really should do the `check` by tree-name for any tree that as AIOTransfrom in it
                _aioxform.check_topics_can_transform_or_die( 
                    (topic for topic,tree in self.republishers.items() if tree in ['aio-expensive','aio-cheap'])
                )

                # publishing to boron requires a topic prefix
                boron_mqtt_publisher = PublisherBoron(
                    "boron-lte",
                    mqtt_local
                )

                mqtt_boron = AIOTransform( 
                    PublisherTransformTopic(
                        boron_mqtt_publisher, 
                        # we have to strip the user/feeds/ prefix that AIOTransform includes
                        (lambda t: re.sub(r'^\w+/feeds/', '', t)) 
                    ),
                    self.aio_mapping, # standard aio topic transform + `$user/` prefix
                    self.mqtt_config['connections']['cloud0'], # only for the .username, or env['USER'] if none=debug
                    map_description=aio_map_description
                )

                mqtt_queued = PublisherTestOk('queued-ok')
                if self.mqtt_config['connections'].get('lan',None):
                    mqtt_lan = PublisherMQTTByConfig('mqtt-lan', self.mqtt_config['connections']['lan'] )
                    self._mqtt_to_reconnect['lan'] = mqtt_lan
                else:
                    debug(f"No ['lan'] in the mqtt_config")
                    mqtt_lan = PublisherNull('No lan')

                mqtt_log = PublisherTestOk('log-ok') # FIXME?

                def say_use_boron():
                    debug(f"Check use boron: wan_check {self.wan_check} boron_check {self.boron_check}. not use: {not( not self.wan_check and self.boron_check ) }")
                    return None

                # wan_ok or boron_fail
                use_wan = PublishTreePred('not-use-boron?',
                    (lambda : not( not self.wan_check and self.boron_check ) ) # for a failover-tree, use(d) wan if !boron || wan_check)
                )

                self.trees = []

                self.trees.append(
                    RePublisher(self, "Failover from cheap to expensive (incl. queueing)",
                        PublishTreeParallel("aio-expensive", # <- take this as name
                            PublishTreeFailover("aio-failover",
                                use_wan, # "sense" is backwards: stop at first True
                                mqtt_boron,
                                mqtt_queued
                            ),

                            # note that we try wan-aio (and lan) even if wan-check fails, why not
                            # we could do a wifi-check, meh?
                            mqtt_aio,
                            mqtt_lan,

                            # logging:
                            mqtt_log,
                        )
                    )
                )
                self.trees.append(
                    RePublisher(self, "Cheap parallel publisher including aio w/queuing",
                        PublishTreeParallel("aio-cheap", # <- take this as name
                            PublishTreeFailover("aio-failover",
                                mqtt_aio,
                                mqtt_queued
                            ),
                            mqtt_lan,
                            mqtt_log,
                        )
                    )
                )
                self.trees.append(
                    RePublisher(self, "LAN publisher w/queing",
                        PublishTreeParallel("lan", # <- take this as name
                            PublishTreeFailover("aio-failover",
                                mqtt_lan,
                                mqtt_queued
                            ),
                            mqtt_log,
                        )
                    )
                )
                self.trees.append(
                    RePublisher(self, "local publishing",
                        PublishTreeParallel("local", # <- take this as name
                            # DO NOT actually publish to the local mqtt, because then we'll republish in a loop again!
                            mqtt_log,
                        )
                    )
                )

                # for testing
                if self.parsed_args.expose_boron:
                    self.trees.append(
                        RePublisher(self, "boron-test tree",
                            PublishTreeParallel("boron-test", # <- take this as name
                                mqtt_boron,
                            )
                        )
                    )

                # special topics that we are interested in for behavior
                self._topics = {
                    self.topic_for_event('minus80/network-status/wan', with_device=False, with_id=False) : { "handler" : self.handle_wancheck },
                    self.topic_for_event('minus80/boron-lte-aio/connected', with_device=False, with_id=False) : { "handler" : self.handle_boroncheck },
                }

                # republish topics 
                # handle republishing on tree's topics
                self._topics[ self.topic_for_event(postfix='#',with_device=False, with_id=False) ] = {
                    "handler":None,  # in "listening help list, but no actual listener"
                    "description" : (f"Topics in {self.config_file()} are republished on a $tree,\n"
                        + "Noted below as 're-publish to tree $x'\n"
                        + f"Topics in {self.aio_map_file} can be renamed for AIO,\n"
                        + "Noted below as 'has aio:...'"
                    )
                }
                # need a map from tree-name -> tree-republisher
                tree_handler_map = {}
                for republisher in self.trees:
                    tree_handler_map[ republisher.publisher.name ] = republisher
                for topic,tree in self.republishers.items():
                    if has_aio_mapping := self.aio_mapping.get( topic ):
                        has_aio_mapping = f"\nhas aio: {has_aio_mapping}"
                    else:
                        has_aio_mapping = ''

                    #debug(f"## topic->tree {topic}->{tree}<{tree_handler_map[tree].self_topic}>")
                    republish_handle = {
                        # we can cheat a bit and make a synthetic .../$tree message to handle:
                        "handler" : (lambda msg: 
                            # local-topic -> tree -> tree-publisher
                            # don't try to capture topic or tree!
                            (aaa:=tree_handler_map[ self.republishers[msg.topic] ]).republish( mqtt.Message( aaa.self_topic, {"topic":msg.topic,"payload":msg.payload}, qos=None,retain=False,mid=None,properties=None ) )
                        ),
                        "description" : f"re-publish to tree: '{tree}'{has_aio_mapping}"
                    }
                    # some topics are special, and we need to have multiple handlers
                    self._topics[ topic ] = [ self._topics[ topic ], republish_handle ] if topic in self._topics else republish_handle

                # doc for trees
                self._topics[ self.topic_for_command('$tree', with_device=False) ] = { 
                    "handler":None,  # in "listening help list, but no actual listener"
                    "description" : """A (re-)publish `tree`.
                        Takes payload={ "topic" : $topic, "payload" : $payload },
                        Re-publishes: $topic : $payload',
                        On several mqtt connections (w/failover),
                        May transform the $topic per connection.
                        e.g. for aio, noted below as 'OnMqtt(mqtt-aio)'"""
                    }


                self._topics[ rate_limit_topic ] = { "description" : "Warning message when aio rate-limit is triggered (DEBUG=4 sends message every time rate is tested)" }

                for republisher in self.trees:
                    self._topics[ republisher.self_topic ] = { 
                        "handler" : republisher.republish ,
                        "description" : republisher.description + ":\n" + republisher.tree_description()
                    }

        return super().topics(self._topics)

    def argparse_add_arguments(self, parser ):
        parser.description += f"\n    Trees currently mentioned in {self.config_file()} (try --topics for valid trees & descriptions):\n        "
        parser.description += '\n        '.join(sorted(set(self.republishers.values())))

        parser.set_defaults(mqtt_config='config/mqtt-multi-config.py') # override
        parser.add_argument('--expose-boron', help="expose the boron as a tree for debug/test", action='store_true')

    async def handle_wancheck(self, message):
        """captures the value to use as a guard for choosing wan vs boron"""
        self.wan_check = message.payload
        # might be bare 0|1|true|false|null
        if isinstance(self.wan_check,str):
            try:
                self.wan_check = not not json.loads( self.wan_check )
            except json.decoder.JSONDecodeError as e:
                debug(f"  bad json payload, expected 0|1|true|false {message.payload}\n\t{e}")
                return

        debug(F"wancheck {self.wan_check.__class__.__name__} {self.wan_check} as not-not->bool",level=2)

    async def handle_boroncheck(self, message):
        """captures the value to use as a guard for choosing wan vs boron"""
        self.boron_check = message.payload
        debug(F"boroncheck {self.boron_check.__class__.__name__} {self.boron_check}",level=4)
        # might be bare 0|1|true|false|null
        if isinstance(self.boron_check,str):
            try:
                self.boron_check = not not json.loads( self.boron_check )
            except json.decoder.JSONDecodeError as e:
                debug(f"  bad json payload, expected 0|1|true|false {message.payload}\n\t{e}")
                return

    async def loop(self, first_time):
        try:
            async with asyncio.TaskGroup() as tg:

                debug("Start mqtt-reconnectors")
                for name,reconnector in self._mqtt_to_reconnect.items():
                    debug(f"  start {name}")
                    tg.create_task( reconnector.run(), name=f'reconnector.run() {name}' )
                    debug(f"  started {name}")

            debug("Fell out of loop's taskgroup... exiting?")
            while(True):
                await asyncio.sleep(10)

        except Exception as e:
            await self.exit(1, e)
            return

    async def cleanup(self, e):

        # cleanup the aio-side
        #FIXME
        #if self.other_side:
        #    await self.other_side.exit(1, None)

        await super().cleanup(e)

        #print(f"do your cleanup, like GPIO.cleanup(), because we are exiting because {e}")


# RUN
# note that Agent.run() does argparse for '-h|--help' in the base .run()
LocalListener( ).async_run() # internally does asyncio.run( agent.run() )
