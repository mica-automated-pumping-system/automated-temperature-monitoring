#!/usr/bin/env -S sh -c 'exec "$(dirname $(realpath $(dirname $0)))/.venv/bin/python3" "$0" "$@"'

import asyncio
import sys,os,json
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib" )
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib-pump-pi" )
from simple_debug import debug
import paho.mqtt as paho # some constants|--help'
import agent

sys.path.append( os.path.dirname( sys.argv[0] ) + "/rgb_status" )
from rgb_state import RGBState

USEGPIO = not os.environ.get('NOGPIO',False)
agent.AnnounceIncoming = 2

try:
    import gpiozero
    from gpiozero import Button
    import board

    if 'SPI' not in dir(board): # proxy for "has gpio stuff"
        raise ModuleNotFoundError("No SPI")
except ModuleNotFoundError as e:
    import inspect
    agent.HelpEpilog = f'\nGPIO disabled (not running on a pi?). See {__file__}:{inspect.currentframe().f_lineno - 3}\n{e}'
    USEGPIO=False

import agent

class RGBStatus(agent.Agent):
    """
    This agent updates an RGB to reflect status.
    The topics/RGB are in rgb_status/rgb_state.py

    List
        local/get/ui/buttoner/pins -> local/events/ui/buttoner/pins [ {pin:x, topic:x},...]
        local/get/ui/buttoner/<pin> -> "" or "topic"

    """
    mqtt_config = {
        "device" : "ui",
        "name" : "rgb_status",

        "connection" : { # paho mqtt args
            "hostname" : "localhost",
            "port" : 1884,
            # client_id
            # tls...
            "clean_session" : True, # if a short enough down-time, resync to the commands
            "keepalive" : 8, # trigger Will
            "clean_start" : paho.client.MQTT_CLEAN_START_FIRST_ONLY,
        }
    }

    def __init__(self):
        super().__init__() # minimal setup

        self.rgb_state = RGBState()

    async def argparse(self):
        # if you need to handle/look at any argparse
        await super().argparse()

    def topic_for_list(self):
        return self.topic_for_get('pins')

    def topics(self):
        # FIXME: Getting called multiple times? cache it
        debug("Setup Topics")

        listeners = {}
        for topic,info in self.rgb_state.topics().items():
            listeners[ topic ] = { 
                "handler" : self.handle_state_change,
                "description" : info['description'] 
            }

        return super().topics( {
            # listen
            self.topic_for_get('+') : { 
                "handler" : self.handle_local_get_pins
            },
            **listeners
            # publish:
        })

    # each handler
    async def handle_state_change(self, message):
        # message.topic is a str, message.payload is json-decoded {...}, [...], or str (absence of payload is {})
        message.payload = self.payload_more_convert( message.payload )
        self.rgb_state.update( message.topic, message.payload )

    async def handle_local_get_pins(self, message):
        """
        .../pins -> dict of pin:config
        .../all -> read and publish each pin value
        .../$pinnumber -> that pin's config
        """
        # message.topic is a str, message.payload is json-decoded {...}, [...], or str (absence of payload is {})
        base_len = len(self.topic_for_get().split('/') )
        tail = str(message.topic).split('/')
        tail = tail[-1]
        #debug(f"TAIL {tail}")

        if tail == 'pins':
            payload = { str(pin):assignment.to_dict() for pin,assignment in self.assignments.items() }
            await self.mqtt_client.publish( self.topic_for_event('pins'), json.dumps( payload ) )
        elif tail == 'all':
            debug(f"## Read/pub all...")
            for assignment in self.assignments.values():
                debug(f"## Read/pub {assignment}...")
                await assignment.publish_on_event(force=True)
        elif re.match(r'\d+$', tail ):
            pin = tail
            debug(f"GET pin {pin}")
            debug(f"assign: {self.assignments}")
            actions = self.assignments.get(pin,[])
            await self.mqtt_client.publish( self.topic_for_event(str(pin)), json.dumps( actions.to_dict() ) )
        else:
            debug(f"Bad get/<pin>: {message.topic}")

    async def loop(self, first_time):

        async with asyncio.TaskGroup() as tg:
            tg.create_task( self.rgb_state.run_rgb() )

            # FIXME: these topics should be in the config to correspond to the topics
            # AND we should keep trying if we haven't seen anything
            # AND they should be auto-deducible from the events we are looking for.
            await asyncio.sleep(0.25)
            self.queue_a_message('local/get/ui/buttoner/all','')
            self.queue_a_message('local/get/minus80/network-status/all','')
            self.queue_a_message('local/get/minus80/boron-lte-aio/all','')
                
        raise Exception("Should never get here")

    async def cleanup(self, e):
        await super().cleanup(e)
        self.rgb_state.cleanup()

    def argparse_add_arguments(self, parser ):
        parser.set_defaults(mqtt_config='config/mqtt-multi-config.py') # override

# RUN
# note that Agent.run() does argparse for '-h|--help' in the base .run()
RGBStatus( ).async_run() # internally does asyncio.run( agent.run() )

