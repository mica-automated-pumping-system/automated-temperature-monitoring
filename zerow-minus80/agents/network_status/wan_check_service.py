#!/usr/bin/env -S sh -c 'exec "$(dirname $(realpath $0))/../.venv/bin/python3" "$0" "$@"'
"""
Monitors internet availability (via wifi): wifi-radio, wifi-connection, wan-connectivity
Pings several servers.
"""

import time,sys,os
import subprocess,re
import asyncio

if __name__ == '__main__':
    libs_base = os.path.dirname( sys.argv[0] ) + '/../..'
else:
    libs_base = os.path.dirname( sys.argv[0] ) + '/..'

sys.path.append( f"{libs_base}/lib")
sys.path.append( f"{libs_base}/lib-pump-pi")
from simple_debug import debug
from service import ServiceBase
from every.every import Every

class WANCheckService(ServiceBase):
    # really a singleton

    # FIXME: should read a config
    wan_hosts = [
        # hostnames relay on dns, so use well-known ip preferrably
        '8.8.8.8', '8.8.4.4', # google dns
        '1.1.1.1','1.0.0.1', # cloudflare
        '208.67.222.222', # opendns
        'ftp.us.debian.org',
        '64.6.64.6', # verisign
    ]

    def __init__(self,
        interval_secs=10, # seconds, override with config/agents/all_services/$this.json "interval" : n
        config_dir=None, # default
    ):
        self.interval = interval_secs # the check-interval, publish on change or 10*interval
        self.publish_interval = Every( self.interval * 10 )
        self.interface = None # override in config, default is search iwconfig
        self.has_iwconfig = subprocess.run(['which','iwconfig'], stdout=subprocess.DEVNULL).returncode == 0

        # init to None so it will publish 1st time
        # we definitely set T|F each time
        self.wifi_radio = None
        self.wifi = None
        self.wan = None

        super().__init__(config_dir=config_dir)
        self.dynamic_interface = self.interface == None # otherwise, it was set in config

    async def check_iwconfig(self):
        # self.interface, self.wifi_radio, self.wifi = self.check_iwconfig()
        if not self.has_iwconfig:
            return (None, False, False)

        new_interface = None
        if self.dynamic_interface:
            new_interface = await self.find_interface()
            if new_interface != self.interface:
                self.interface = new_interface
                debug(f"New Wifi Interface: '{self.interface}'")

        wifi_radio = False
        if self.interface:
            proc = await asyncio.create_subprocess_exec( 'iwconfig', self.interface, stdout=asyncio.subprocess.PIPE)
            stdout, dumy = await proc.communicate()
            rez = stdout.decode(encoding='utf-8').split('\n')
            #debug(f"  iwconfig: {rez[0]}")
            # wlp0s20f3  IEEE 802.11  ESSID:"auger"  
            # OFF: wlp0s20f3  IEEE 802.11  ESSID:off/any  
            if m := re.search(r' ESSID:(.+)', rez[0]):
                debug(f"  essid: '{m.group(1)}'", level=3)
                if not m.group(1).startswith("off/any"): # trailing spaces
                    # but does not tell us if we are actually connected to the ap!
                    wifi_radio = True
            else:
                debug(f"WARNING: didn't match /ESSID:/ in: {rez[0]}")

        wifi = False
        if wifi_radio:
            # wlp0s20f3: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
            # NOT-Connect: wlp0s20f3: flags=4099<UP,BROADCAST,MULTICAST>  mtu 1500
            proc = await asyncio.create_subprocess_exec( 'ifconfig', self.interface, stdout=asyncio.subprocess.PIPE)
            stdout, dumy = await proc.communicate()
            rez = stdout.decode(encoding='utf-8').split('\n')
            debug(f"  ifconfig: {rez[0]}",level=3)
            wifi = not not re.search(r'\bRUNNING\b', rez[0])

        debug(f"  check: {(new_interface, wifi_radio, wifi)}", level=3)
        return (new_interface, wifi_radio, wifi)

    async def find_interface(self):
        # iwconfig to find a wifi interface
        if self.has_iwconfig:
            # we can look
            proc = await asyncio.create_subprocess_exec( 'iwconfig', stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.DEVNULL)
            stdout, dumy = await proc.communicate()
            rez = stdout.decode(encoding='utf-8').split('\n')
            debug(f"iwconfig: '{rez}'",level=2)

            for line in rez:
                # TO STDERR: enp36s0   no wireless extensions.
                # wlp0s20f3  anythingelse
                if len(line) == 0 or line[0] == ' ':
                    continue
                if "no wireless extensions" in line:
                    debug(f"  iface: {line}")
                    continue

                interface = line.split(' ',2)[0]
                #debug(f"  wireless: {interface}")
                return interface

            debug(f"## No wifi interface found")
            return None

    async def run(self):

        while(True):

            was = [ self.wifi_radio, self.wifi, self.wan ]

            self.wan = False # might get set below
            self.interface, self.wifi_radio, self.wifi = await self.check_iwconfig()

            if self.wifi:

                pingers = []
                #debug(f"START PING")
                start = time.monotonic()
                async with asyncio.TaskGroup() as tg:
                    for ip in self.__class__.wan_hosts:
                        cmd = f'/bin/ping -c 1 -W 2 -n -4 -I {self.interface} -q'.split(' ')
                        cmd.append(ip)
                        #debug(f"Pinged: {' '.join(cmd)}")

                        async def start_and_await(cmd):
                            # need a timeout for (possible) dns lookup
                            async with asyncio.timeout(2): # will CancelledError, which terminates the process w/ .returncode=None
                                pingers.append( await asyncio.create_subprocess_exec( *cmd, stdout=asyncio.subprocess.DEVNULL ) )
                                try:
                                    await pingers[-1].wait()
                                except asyncio.exceptions.CancelledError as e:
                                    #pingers[-1].terminate()
                                    pass

                        tg.create_task( start_and_await(cmd) )

                #debug(f"Took { time.monotonic() - start }")
                ok_ct = 0
                for ip,subp in zip(self.__class__.wan_hosts, pingers):

                    # None means it was time'd out
                    if subp.returncode == 0:
                        ok_ct += 1
                    else:
                        debug(f"PingFail {ip} {subp.returncode}")

                self.wan = ok_ct >= 2

            # only publish if changed, or interval*10
            if (self.publish_interval() or was != [ self.wifi_radio, self.wifi, self.wan ]):
                self.ready_now.set() # client should .clear() for one-shot

            await asyncio.sleep(self.interval)

if __name__ == '__main__':
    async def main():
        wancheck = WANCheckService( config_dir= 'config/agents/network_status' )
        asyncio.create_task( wancheck.run() )

        while True:
            await wancheck.ready_now.wait()
            wancheck.ready_now.clear()
            debug(f"CHECK radio {wancheck.wifi_radio} wifi {wancheck.wifi} wan {wancheck.wan}")
            debug(f"... {wancheck.interval} secs")

    asyncio.run( main() )
