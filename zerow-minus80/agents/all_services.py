#!/usr/bin/env -S sh -c 'exec "$(dirname $(realpath $0))/../.venv/bin/python3" "$0" "$@"'

import asyncio
import sys,os,json,re,time,traceback
import serial
from datetime import datetime
import paho.mqtt as paho # some constants|--help'
import aiomqtt as mqtt # for errors
import sdnotify
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib" )
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib-pump-pi" )
from simple_debug import debug
import agent

USEGPIO = not os.environ.get('NOGPIO',False)

try:
    import board, digitalio,busio # blinka, this will load on a non-pi, but stuff will be missing
    if 'SPI' not in dir(board): # proxy for "has gpio stuff"
        raise ModuleNotFoundError("No SPI")
except (ModuleNotFoundError,AttributeError) as e:
    # this is a proxy for "is a raspberry pi"
    # the exception list changes over time. sigh
    import inspect
    agent.HelpEpilog = f'\n# GPIO disabled (not running on a pi?). See above {__file__}:{inspect.currentframe().f_lineno}\n{e}'
    USEGPIO=False

sys.path.append( os.path.dirname( sys.argv[0] ) + "/all_services" )
# fixme: would like to do this automatically, but the instantiation of each object is different
if USEGPIO:
    from max31865_service import MAX31865Service
    from bme680_service import BMEService

# write your
class AllServicesAgent(agent.Agent):
    """
    Run all the services in agents/all_services/*.py (disable with suffix .disable)
    """

    mqtt_config = {
        "device" : "services", # advertise's topic = local/devices/$device/$name
        "name" : "minus80",
    }

    # see our systemd.units/agents__all_services.py.service
    sdnotifier = sdnotify.SystemdNotifier()
    WatchdogSec = 5*60 # same value as in systemd.units/agents__all_services.py.service
    systemd_watchdog_period = WatchdogSec/5 # smaller than WatchdogSec, with tolerance for some other slow operation

    def __init__(self):
        super().__init__()
        self.all_publish = [] # pairs of [full-topic, lambda-value) # to publish for /get/all

        self.service_config_dir = os.path.splitext(self.config_file())[0] # dirname, drop the .json
        agent.HelpEpilog = f"Configs in {self.service_config_dir}\n" + agent.HelpEpilog

        os.makedirs( self.service_config_dir, exist_ok=True )

    def argparse_add_arguments(self, parser ):
        # add your argparse arguments, override mqtt_config, etc
        parser.set_defaults(mqtt_config='config/mqtt-multi-config.py') # override

    # FIXME: add some topic to get the service list

    async def handle_publish_values(self, message):
        """
        Immediately publish current values.
        They may be Null if not ready.
        """
        #debug(f"republish {self.all_publish}")
        for topic,payload_fn in self.all_publish:
            #debug(f"Republish {payload_fn()}")
            self.queue_a_message( topic, payload_fn() )

    def topics(self):
        # DRY up some topics
        self.topic_roomtemp = self.topic_for_event('room/temp', with_device=False)
        self.topic_humidity = self.topic_for_event('room/humidity',with_device=False)
        self.topic_minus80 = self.topic_for_event('minus80', with_device=False)

        return super().topics( {

            # listen
            self.topic_for_get('all') : { "handler": self.handle_publish_values },

            # publish:
            # 1st two don't happen to have any /fault yet (FIXME: add i2c fail etc)
            self.topic_roomtemp: { "description": "ambient, celsius" },
            self.topic_humidity: { "description": "ambient, %" },
            self.topic_minus80 : { "description": "freezer, celsius" },
            (self.topic_minus80 + "/fault") : { "description": "freezer probe faults" },
        })

    async def publish_value_when_ready(self, obj, *topic_value, gate=None):
        """
        Publish the `topic` : `value()` 
        When obj.ready_now.wait() finishes (a asyncio.Event(), .set() )
        *topic_value is a sequence of: topic, value_fn
            Thus, a single `ready_now` on the object works for multiple topic-value publish
        And then gate() is tested (if != None)
        Those topic/value pairs are saved in a list for `self.topic_for_get('all')`: `local/get/services/minus80/all`
        """

        async def _if_ready_now( obj, awaitable, *args, **kwargs ):
            """
                Calls awaitable(*args,**kwargs) when object update's (`ready`)
                The gate` is checked at that time, so: when ready, and then gate'd, the publish
                E.g. publish w/ publisher()
            """

            while True:
                await obj.ready_now.wait()
                obj.ready_now.clear()
                await awaitable(*args,**kwargs)

        async def _gated_multi_publisher(*topic_value, gate=None):
                """ Wrapper for publishing via _if_ready_now, with last chance pred-gate """
                if gate==None or gate():
                    # sequential publish, could do in parallel, but we don't have to worry about captures this way
                    alternator = iter(topic_value)
                    for topic,value in zip(alternator,alternator): # by 2's
                        if obj.fault:
                            await self.mqtt_client.publish( topic+"/fault", json.dumps( obj.fault ) )
                        else:
                            await self.mqtt_client.publish( topic, json.dumps( value() ) )
                else:
                    debug(f"not-gate'd _publisher() {topic_value}",level=2)

        # save topic/value pairs for `self.topic_for_get('all')`: `local/get/services/minus80/all`
        alternator = iter(topic_value)
        for topic,value in zip(alternator,alternator): # by 2's
            self.all_publish.append( [ topic, value ])

        # test obj.ready_now, then _gated_multi_publisher( *topic_value, gate=gate)
        await _if_ready_now( obj, _gated_multi_publisher, *topic_value, gate=gate)

        debug(f"##PubWhenReady: {topic} via {obj.__class__.__name__}")

    async def systemd_watchdog_ping(self):
        if not os.environ.get('NOTIFY_SOCKET',None):
            debug("WARNING: Not systemd, so no watchdog recover")
        debug(f"Watchdog {self.__class__.WatchdogSec} sec, ping every {self.__class__.systemd_watchdog_period}")

        while(True):
            # alleged NOOP if not run by systemd
            debug(f"systemd watchdog...", level=2)
            self.__class__.sdnotifier.notify("WATCHDOG=1")
            await asyncio.sleep(self.__class__.systemd_watchdog_period)
        
    async def loop(self, first_time):
        # required.
        # have to busy wait if you want to do nothing
        debug(f"LOOP...", level=(0 if first_time else 2))

        # start the services with delay-between, which will roughly cause publishing to be spread out
        max_per_window = 10
        window_secs = 60
        between = window_secs/max_per_window if not os.environ.get('NODELAY',False) else 0

        if USEGPIO:
            # NB: we do not use the busio.I2C for actual i2c services, they use the board.I2C singleton
            i2c = busio.I2C(board.SCL, board.SDA)
            debug("I2C instantiated!", level=(0 if first_time else 2))
            spi = board.SPI()
            debug("SPI instantiated", level=(0 if first_time else 2))

            start = time.monotonic()
            debug("I2C lock wait...", level=(0 if first_time else 2))
            while not i2c.try_lock():
                pass
            try:
                debug(f"took {time.monotonic()-start:2.2}", level=(0 if first_time else 2))
                debug("Scanning I2c (lockup indicates i2c in use?)...", level=(0 if first_time else 2))
                debug("scan ", [f"0x{x:x}" for x in i2c.scan()] , level=(0 if first_time else 2))
            finally:
                i2c.unlock()
            i2c=None

        async with asyncio.TaskGroup() as tg:

            # The premise is that if the async world gets stuck, so will watchdog-ping
            tg.create_task( self.systemd_watchdog_ping(), name="systemd_watchdog_ping")

            if USEGPIO:
                minus80 = MAX31865Service(config_dir = self.service_config_dir )
                tg.create_task( minus80.run(), name='minus80.run' )
                tg.create_task( 
                    self.publish_value_when_ready( minus80, self.topic_minus80, (lambda: minus80.value) ),
                    name='publish_value_when_ready minus80'
                )
                await asyncio.sleep(between)

            if USEGPIO:
                bme = BMEService( config_dir = self.service_config_dir )
                tg.create_task( bme.run(), name='bme.run' )
                tg.create_task( 
                    self.publish_value_when_ready( bme,
                        self.topic_roomtemp, (lambda: bme.temperature),
                        self.topic_humidity, (lambda: bme.humidity) 
                    ),
                    name='publish_value_when_ready bme'
                )
                await asyncio.sleep(between)

            # FIXME: add topics() like: local/get/tof/tof1/value and local/get/$me/values
            # which means dry'ing these services

            debug("bottom of create_tasks, all launched", level=(0 if first_time else 2))
            #if first_time:
            #    debug(f"DEBUG closing")
            #    await self.mqtt_client.disconnect()

        #while(True):
        #    await asyncio.sleep(1)
        debug("FELL off bottom of LOOP: tasks died", level=(0 if first_time else 2))


    async def cleanup(self, e):
        await super().cleanup(e)

        print(f"do your cleanup, like GPIO.cleanup(), because we are exiting, because {e}")


# RUN
# note that Agent.run() does argparse for '-h|--help' in the base .run()
AllServicesAgent( ).async_run() # internally does asyncio.run( agent.run() )
