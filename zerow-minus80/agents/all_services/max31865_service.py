#!/usr/bin/env -S sh -c 'exec "$(dirname $(realpath $(dirname $0)))/../.venv/bin/python3" "$0" "$@"'
"""
freezer temperature
"""

# blinka!
import time,sys,os
import asyncio

import board,busio, digitalio # blinka

if __name__ == '__main__':
    libs_base = os.path.dirname( sys.argv[0] ) + '/../..'
else:
    libs_base = os.path.dirname( sys.argv[0] ) + '/..'

sys.path.append( f"{libs_base}/lib")
sys.path.append( f"{libs_base}/lib-pump-pi")
import adafruit_max31865
from service import ServiceBase
from simple_debug import debug

class MAX31865Service(ServiceBase):
    # really a singleton
    # .temperature, .humidity

    RNOMINAL=100.0
    RREF=430.0

    def __init__(self, spi=busio.SPI(board.SCK, board.MOSI, board.MISO), cs_pin=None, interval=10, config_dir=None):
        # cs_pin is either the string like "D5" (gpio numbering) or digitalio.DigitalInOut(board.D5)
        self.spi = spi
        self.cs_pin = cs_pin # override in config config/agents/all_services/$this.json
        self.interval = interval # override in config config/agents/all_services/$this.json
        super().__init__(config_dir=config_dir)

        if self.cs_pin == None:
            raise Exception("Need a cs_pin (from config or init)")
        if isinstance(self.cs_pin,str):
            self.cs_pin = digitalio.DigitalInOut(getattr(board, self.cs_pin))

    async def run(self):
        try:
            start = time.monotonic()
            # FIXME: doesn't seem to check for presence! omg
            debug(f"{self.__class__.__name__} cs_pin {(self.cs_pin._pin)} nominal {self.__class__.RNOMINAL} resistor {self.__class__.RREF}")
            rtd = adafruit_max31865.MAX31865(self.spi, self.cs_pin, wires=3, rtd_nominal=self.__class__.RNOMINAL, ref_resistor=self.__class__.RREF)

            debug(f"took {time.monotonic()-start:2.2}",level=2)

            # some SPI checking
            debug(f"bias {rtd.bias}, autoconv {rtd.auto_convert}, {rtd.fault}, raw {rtd.read_rtd()}, ohm {rtd.resistance}",level=3)
            rtd.clear_faults()

            while(True):

                # Fault check
                faults = { k:f for k,f in zip(
                        'highthresh lowthresh refinlow refinhigh rtdinlow ovuv'.split(' '),
                        rtd.fault
                    )
                }
                if next( (True for x in faults.values() if x != 0), None ):
                    self.fault = f"FAULT {self.__class__.__name__} SPI mosi/miso reversed? bad CS (using CE0: see config note)? {faults}"
                    self.fault = f"FAULT {self.__class__.__name__} {faults}"
                    debug(self.fault)
                    rtd.clear_faults()
                    self.ready_now.set()

                else:

                    # Temp
                    value = rtd.temperature
                    if value == None:
                        self.value = value
                        self.fault = f"FAULT {self.__class__.__name__} no temp, bad SPI wiring? {value}"
                        debug(self.fault,level=3)
                        self.ready_now.set()
                    elif value < -200:
                        # if not extant, we get -242.02...
                        self.value = value
                        self.fault = f"FAULT {self.__class__.__name__} bad temp, no SPI data: probe disconnected? SPI mosi/miso reversed? bad CS (using CE0: see config note)? {value}"
                        debug(self.fault,level=3)
                        self.ready_now.set()
                    else:
                        self.value = value
                        debug(f"## MAX {value}",level=3)
                        self.fault = None
                        self.ready_now.set()


                await asyncio.sleep(self.interval)

        finally:
            pass

if __name__ == '__main__':
    async def main():
        if os.environ.get('DEBUG',None) == None:
            os.environ['DEBUG'] = '3'
        me = MAX31865Service( config_dir= 'config/agents/all_services' )

        asyncio.create_task( me.run() )

        while True:
            await me.ready_now.wait()
            me.ready_now.clear()
            if not me.fault:
                debug(f"{me.__class__.__name__} {me.value}C")
    asyncio.run( main() )

