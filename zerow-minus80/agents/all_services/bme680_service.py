#!/usr/bin/env -S sh -c 'exec "$(dirname $(realpath $0))/../.venv/bin/python3" "$0" "$@"'
"""
Room temperature and humidity
"""

# blinka!

import time,sys,os
#from adafruit_bus_device import i2c_device
import asyncio

if __name__ == '__main__':
    libs_base = os.path.dirname( sys.argv[0] ) + '/../..'
else:
    libs_base = os.path.dirname( sys.argv[0] ) + '/..'

sys.path.append( f"{libs_base}/lib")
from service import ServiceBase
from simple_debug import debug

import board,busio

import adafruit_bme680

class BMEService(ServiceBase):
    # really a singleton
    # .temperature, .humidity

    def __init__(self, 
        interval_secs=10, # seconds, override with config/agents/all_services/bme680_service.json "interval" : n 
        i2c=board.I2C(),
        config_dir=None, # default
        ):
        self.i2c = i2c
        self.interval = interval_secs
        super().__init__(config_dir=config_dir)

        self.temperature = 0
        self.humidity = 0

    async def run(self):
        try:
            start = time.monotonic()
            try:
                bme = adafruit_bme680.Adafruit_BME680_I2C(self.i2c, refresh_rate = 0.1 )
            except ValueError as e:
                if "No I2C device at address" in str(e) :
                    debug(f"ERROR {self.__class__.__name__}: {e}")
                    debug(F"Skipping {self.__class__.__name__}",level=2)
                    bme = None
                else:
                    raise e

            #print(f"took {time.monotonic()-start:2.2}")
            # optional

            while(True):

                if bme:
                    self.temperature = bme.temperature
                    self.humidity = bme.relative_humidity
                    self.ready_now.set()

                    await asyncio.sleep(self.interval)
                else:
                    await asyncio.sleep(10*60)

        finally:  # unlock the i2c bus when ctrl-c'ing out of the loop
            print("exit")
            try:
                self.i2c.unlock()
            except ValueError as e:
                if str(e) != 'Not locked':
                    raise e

if __name__ == '__main__':
    async def main():
        import sys
        #i2c = busio.I2C(board.SCL, board.SDA)
        me = BMEService(config_dir= 'config/all_services')
        asyncio.create_task( me.run() )

        while True:
            await me.ready_now.wait()
            me.ready_now.clear()
            debug(f"{me.__class__.__name__} {me.temperature}C {me.humidity}%")
    asyncio.run( main() )

