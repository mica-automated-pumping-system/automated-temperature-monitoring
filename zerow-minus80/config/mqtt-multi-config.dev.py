"""
Use:
    from mqtt_local_config import connection as mqtt_local_config # from ./lib/

    async with mqtt.Client(
        **mqtt_local_config,
        ) as client:
"""

import paho.mqtt as paho

# paho mqtt args
connections = [
    {
    "key" : "local",
    "hostname" : "localhost",
    "port" : 1884,
    #"mdns_service" : "amps-pi_mqtt_broker",
    # client_id
    # tls...
    "clean_session" : True, # if a short enough down-time, resync to the commands
    "keepalive" : 8, # trigger Will
    "clean_start" : paho.client.MQTT_CLEAN_START_FIRST_ONLY,
    },
    {
    "key" : "lan",
    "hostname" : "localhost",
    "port" : 1885,
    #"mdns_service" : "amps-pi_mqtt_broker",
    # client_id
    # tls...
    "clean_session" : True, # if a short enough down-time, resync to the commands
    "keepalive" : 8, # trigger Will
    "clean_start" : paho.client.MQTT_CLEAN_START_FIRST_ONLY,
    },
    {
    # pretend we are adafruit
    "key" : "cloud0",
    "hostname" : "localhost",
    "port" : 1884,
    # client_id
    # tls...
    "clean_session" : True, # if a short enough down-time, resync to the commands
    "keepalive" : 8, # trigger Will
    "clean_start" : paho.client.MQTT_CLEAN_START_FIRST_ONLY,
    },
    {
    "key" : "xcloud0",
    "hostname" : "io.adafruit.com",
    "port" : 8883,
    #"mdns_service" : "amps-pi_mqtt_broker",
    # client_id
    # tls...
    "clean_session" : True, # if a short enough down-time, resync to the commands
    "keepalive" : 8, # trigger Will
    "clean_start" : paho.client.MQTT_CLEAN_START_FIRST_ONLY,
    "tls_params" : mqtt.TLSParameters(),
    "username" : aio_key()['IO_USERNAME'],
    "password" : aio_key()['IO_KEY'],
    },
    {
    "key" : "ssh_tunnel_account",
    "hostname" : "io.adafruit.com",
    "port" : 8883,
    "clean_session" : True, # if a short enough down-time, resync to the commands
    "keepalive" : 8, # trigger Will
    "clean_start" : paho.client.MQTT_CLEAN_START_FIRST_ONLY,
    "tls_params" : mqtt.TLSParameters(),
    "username" : aio_key('config/aio.ssh_tunnel_account.key')['IO_USERNAME'],
    "password" : aio_key('config/aio.ssh_tunnel_account.key')['IO_KEY'],
    },
]
