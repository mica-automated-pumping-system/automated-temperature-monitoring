{
    "#" : "On .../DEMO, make sure the demo agent is running. It handles .../REVERT",
    "actions" : [
        {
        "topic" : "local/commands/demo-data/demo_data/DEMO",
        "value" : "1",
        "command" : ["/usr/bin/systemctl","--user","start","agents__startup-demo-mode.py.service"]
        }
    ]
} 
