# run by
#   tools/demo_data.py config/demo-data.py
#
# Generate demo data (stop regular data generating agents)
# Triggered by some other agent: e.g. tools/agent-mqtt-action.py or agents/physical-button-action.py
# Exits on a topic,value specified here (`revert_on`)

# see the DSL exported to us by toosl/demo-data.py

# for starting/stopping us
replace_systemd_unit(
    'agents__all_services.py.service',
    'agents__boron_service.py.service',
    'agents__network_status.py.service',
    revert_on = topic_for_command("REVERT"), # topic_for_command("revert")
)

# all_services.py

publishing(
    'local/events/minus80/minus80',
    file='demo/minus80.csv',
    interval=json_file('config/agents/all_services/MAX31865Service.json')['interval']/10
).change_on(
    topic_for_command("HOT"),
    file=None,
    data=-70,
).change_back_on( topic_for_command("COLD") )

publishing(
    'local/events/minus80/room/humidity',
    file='demo/humidity.csv',
    interval=json_file('config/agents/all_services/MAX31865Service.json')['interval']/8
)
publishing(
    'local/events/minus80/room/temp',
    file='demo/temp.csv',
    interval=json_file('config/agents/all_services/BMEService.json')['interval']/90
)

# wancheck

publishing(
    'local/events/minus80/network-status/wan',
    data=1,
    interval=json_file('config/agents/all_services/WANCheckService.json')['interval']/2
)
publishing(
    'local/events/minus80/network-status/wifi',
    data=1,
    interval=json_file('config/agents/all_services/WANCheckService.json')['interval']/2
)
publishing(
    'local/events/minus80/network-status/wifi-radio',
    data=1,
    interval=json_file('config/agents/all_services/WANCheckService.json')['interval']/2
)
publishing(
    'local/events/pin/freezer-alarm',
    data=1,
    interval=30
)
