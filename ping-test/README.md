Testing TCP-ping.

LIBRARIES
"Streaming" by Peter Polidoro <peter@polidoro.io> V 6.1.1
"Every" by Alan Grover <awgrover@gmail.com> V 3.1.0

LINKS
ln -s ../alarm-framework/AwgLogger.h AwgLogger.h
ln -s ../alarm-framework/bright_leds_neopixel_dotstar.h bright_leds_neopixel_dotstar.h
ln -s ../alarm-framework/DNSService.h DNSService.h
ln -s ../alarm-framework/freememory.h freememory.h
ln -s ../alarm-framework/LEDHeartBeat.h LEDHeartBeat.h
ln -s ../alarm-framework/LinkList.h LinkList.h
ln -s ../alarm-framework/lwip_TCP_Ping.h lwip_TCP_Ping.h
ln -s ../alarm-framework/Makefile Makefile
ln -s ../alarm-framework/PingService.h PingService.h
ln -s ../alarm-framework/secret_s.h secret_s.h
ln -s ../alarm-framework/Service.h Service.h
ln -s ../alarm-framework/WifiService.h WifiService.h
ln -s ../alarm-framework/lwip_DNS.h lwip_DNS.h

ln -s ../alarm-framework/array_size.h array_size.h
ln -s ../alarm-framework/lwip_DNS_mDNS_RASPBERRY_PI_PICO_W.h lwip_DNS_mDNS_RASPBERRY_PI_PICO_W.h
