//#define DEBUG_RP2040_PORT Serial
//#define DEBUG_ESP_MDNS

#include <functional>

#include "freememory.h"
#include "array_size.h"
#include <ExponentialSmooth.h>
#define LOG_LEVEL LOG_LEVEL_WARN
#include "AwgLogger.h"
#include <Every.h>
#define SERVICE_MANAGER_DEBUG 2
#define USE_WIFISERVICE 1
#include "Service.h"
#include "PingService.h"
#include "WifiService.h"
#include "LEDHeartBeat.h"
#include "DNSService.h"
//#include <PubSubClient.h>

//#define DEBUG_OUTPUT Serial
//#define DEBUG_ESP_MDNS_RESPONDER
//#define DEBUG_ESP_MDNS_INFO
//#define DEBUG_ESP_MDNS_ERR
//#define DEBUG_ESP_MDNS_TX
//#define DEBUG_ESP_MDNS_RX

// to get debug output:
// Set debug-level:all, AND debug-port:serial
// edit ~/.arduino15/packages/rp2040/hardware/rp2040/3.3.0/libraries/LEAmDNS/src/LEAmDNS_Priv.h
// and add
//#define DEBUG_ESP_MDNS
//#define DEBUG_ESP_PORT Serial

#include <LEAmDNS.h>
#include "LEAmDNS_Priv.h"
//static_assert(0, DEBUGV("bob"));

struct WAN_hosts_t {
  char const* const name;
  const int port;
};
WAN_hosts_t const WAN_Hosts[] = {
  // All of these cloud providers/cdn's rotate IP-addresses rapidly:
  // a minute, 10 seconds, etc.
  // But, usually will tcp-connect on old ip-addresses for a while.
  // Unless we get really unlucky with all dns ttl expiring and ip-addresses going away at the same time, 
  // at least some will tcp-connect.
  { "google.com", 80 },
  //{"172.217.215.138",80}, // google to test ip resolution
  { "awgdart.local", 8811 },
  //{ "does8notexist77g.com", 80 },
  { "yahoo.com", 80 },
  { "bing.com", 80 },
  { "aws.amazon.com", 80 }, // WILL timeout when TTL expires (more often)
  //{ "240.0.0.1", 80}, // reserved unused, so timeout
  { "cloudflare.com", 80 },
  { "verizon.com", 80 },
  { "alibaba.com", 80 },
  { "akamai.com", 80 }  // ttl is actually about 10secs!
};
char* WAN_host_names[array_size(WAN_Hosts)] = {};  // will have to copy the pointers below

//template<typename T>
char const* const* to_names(WAN_hosts_t const withnames[], const unsigned int ct) {
  // convert arbitrary structures to a char*[]
  for (unsigned int i = 0; i < ct && i < array_size(WAN_Hosts); i++) {
    WAN_host_names[i] = const_cast<char*>(withnames[i].name);
  }
  return const_cast<char const* const*>(WAN_host_names);
}

DNSService PingDNS( to_names(WAN_Hosts, array_size(WAN_Hosts)), array_size(WAN_Hosts) );  // i.e. WAN detect

constexpr unsigned long PING_TIMEOUT = 2000;      // our test servers should be fast
// nb: same size as DNSService
TCP_Ping* pingables[array_size(WAN_Hosts)] = {};  // fill in first time in loop()

constexpr unsigned long PING_INTERVAL = 30;  // secs
constexpr int PING_IN_FLIGHT = 2;
constexpr boolean RSTISSUCCESS = true; // a `reset` means the host is there
// nb: same size as DNSService
PingService Pinger(pingables, array_size(WAN_Hosts), PING_IN_FLIGHT, RSTISSUCCESS, PING_INTERVAL); // add to service after filling in pingables below

WiFiService LabWifiService;

//PubSubClient dumy;

int start_free;

void setup() {
  unsigned long start = millis();
  start_free = freeMemory();

  serial_wait();
  LOGX << endl;
  LOGX << (millis() - start);
  LOGX << F(" Start " __FILE__ " " __DATE__ " " __TIME__) << endl;
  //if (Serial) LOGGING_EXPLAIN;

  LOG << F("LWIP_DNS_SUPPORT_MDNS_QUERIES ") << LWIP_DNS_SUPPORT_MDNS_QUERIES << endl;

  // Wait for user go ahead if we are plugged in to a usb port
  if (Serial) {
    //LOG_DEBUG << F("NL is \\n 0x") << _HEX('\n') << endl;

    Serial << F("Proceed? ") << endl;

    static const unsigned long pattern[] = { 800, 25 }; // want a signal, but brief "unflash"
    static Every::Pattern waiting_pulse(array_size(pattern), pattern);

    while (Serial.available() <= 0) { 
      if (waiting_pulse()) LedBuiltinToggle();
      delay(5);
    }
    while (Serial.available() > 0) Serial << Serial.read();
    // FIXME: some commands here: enable aio, enable boron, enable wifi, default=enable all
    Serial << endl;
  }

  ServiceManager.add_service(new LEDHeartBeat());
  ServiceManager.add_service(&LabWifiService);
  ServiceManager.add_service(&PingDNS);
  //ServiceManager.add_service(&Pinger);

  LOG << F("Initial ServiceManager.begin()") << endl;
  ServiceManager.begin();
  LOGX << endl;

  LOG << (millis() - start)
      << F(" Setup Done, free ") << freeMemory() << F(" used ") << (start_free - freeMemory())
      << endl
      << endl
      << F("*** LOOP:") << endl
      << endl;
}

void serial_wait() {  // Standard block to wait for serial on "USB CDC", aka native usb, e.g. samd21 etc's
  // if not plugged in to usb, continue eventually
  // This also gives you 3 seconds to upload if you program has a hard-hang in it
  static Every::Timer serial_wait(3 * 1000, true);  // how long before continuing
  static Every fast_flash(50);
  Serial.begin(115200);
  // always wait, assume serial will work after serial_wait time
  while (!(Serial || serial_wait())) {
    if (fast_flash()) LedBuiltinToggle();
    delay(10);  // the delay allows upload to interrupt
  }
  if (Serial) {
    while (!(serial_wait())) {  // also, particle serial console is slow to notice us
      if (fast_flash()) LedBuiltinToggle();
      delay(10);  // the delay allows upload to interrupt
    }
  }
}

void loop() {

  watch_freemem_change();
  watch_loop_delay();

  ServiceManager.run();

  dns_ping_fixup();
  //test_mdns();
  //leamdns_test();

}

void leamdns_test() {
  static boolean first = true;
  static Every try_conn(20 * 1000, true);

  if (first) {
    first = false;
    LOG << F("LeamDNS debug?") << endl;
    DEBUG_EX_INFO(LOG << F("LEAmDNS debug enabled") << endl);
    MDNS.begin("dumy");
  }
  if (WiFi.status() == WL_CONNECTED && try_conn()) {
    LOG << F("Try MDNS...") << endl;
    uint32_t u32AnswerCount = MDNS.queryService("_http", "_tcp", 200);
    //uint32_t u32AnswerCount = MDNS.queryService("ANY", "ANY", 1000); won't work, gets prefixed with '_'
    LOG << F("Saw ct ") << u32AnswerCount << endl;
    for (uint32_t u = 0; u < u32AnswerCount; ++u) {
      const char* pHostname = MDNS.answerHostname(u);
      // FIXME: static/pre-allocated, tie to tcp_ping, cast (ip_addr_t*) gets the address}
      IPAddress ip = MDNS.answerIP(u);
      LOG << F("  H=") << pHostname << F(" = ") << (ip_addr_t)ip << endl;
    }
    MDNS.removeQuery();
  }
}
void test_mdns() {
  static Every try_conn(20 * 1000);
  static WiFiClient wificlient;
  if (WiFi.status() == WL_CONNECTED && try_conn()) {
    LOG << F("Try CONN...") << endl;
    if (wificlient.connect("awgdart.local", 8811)) {
      LOG << F("** CONN by hostname") << endl;
      wificlient.stop();
    } else {
      LOG_ERROR << F("** CONN failed by mdns hostname") << endl;
      /*
      if (wificlient.connect("192.168.1.3", 8811)) {
        LOG << F("** CONN by ip") << endl;
        wificlient.stop();
      } else {
        LOG_ERROR << F("** CONN failed by ip") << endl;
      }
      */
    }
  }
}
void dns_ping_fixup() {
  // FIXME: looks like an event-driven dependency.
  static boolean did_fixup = false;
  if (!did_fixup) {
    // we have to wait for PingDNS to create the ip_addr_t structs, which is actually at begin()
    if (PingDNS.status == ServiceInterface::Statuses::OK) {
      did_fixup = true;
      LOG_VERBOSE << F("Setup after PINGDNS...") << endl;

      // copy ip_addr_t* to lwip_Ping objects, i.e. shared with DNSService (lwip_DNS)
      for (unsigned int i = 0; i < array_size(WAN_Hosts); i++) {
        pingables[i] = new TCP_Ping(&(PingDNS.dns[i]->ip_addr), WAN_Hosts[i].port, PING_TIMEOUT);
      }
      LOG_VERBOSE << F("Add Pinger w/ copied ip_addr_t count ") << array_size(WAN_Hosts) << endl;
      ServiceManager.add_service(&Pinger);
      Pinger.begin();  // ha!
    }
  }
}

void watch_loop_delay() {
  // put at top of loop
  // FIXME: add like this to ServiceManager, with ifdef
  static boolean first_time = true;
  static unsigned long last_loop;
  static ExponentialSmooth<float> long_term(100);
  static Every report_speed(5 * 60 * 1000);

  if (first_time) {
    last_loop = millis();
    first_time = false;
    return;
  }

  unsigned long loop_elapsed = millis() - last_loop;
  last_loop = millis();
  if (loop_elapsed > (long_term.value() + max(long_term.value() / 20, 5.0))) {
    // warn on 5% change
    long_term.reset(loop_elapsed);
    LOG_WARN << F("Slow loop ") << long_term.value() << endl;
  } else {
    long_term.average(loop_elapsed);
  }

  if (report_speed()) {
    LOG_INFO << F("Loop Speed ") << long_term.value() << endl;
  }
}

void watch_freemem_change() {
  // FIXME: move to ServiceManager
  // place at top of loop
  static boolean first_time = true;
  static int last_memory;

  if (first_time) {
    last_memory = freeMemory();
    first_time = false;
    LOG_INFO << F("Freememory (1st) ") << last_memory << endl;
    return;
  }

  int now_memory = freeMemory();
  if (now_memory != last_memory) {
    int change = now_memory - last_memory;
    LOG_INFO << F("Freememory change ") << change << endl;
  }
  last_memory = now_memory;
}
