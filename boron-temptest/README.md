# Monitor the -80 freezer, MQTT messages to local mqtt broker, with LTE/battery failover straight to Adafruit-IO

For the Particle Boron.

DO NOT USE ARDUINO IDE. Must use the particle dev system.

## Setup this directory

Upon `git checkout` this directory contains only some files, like zServicesList.ino.
The base .ino and other files are in ../alarm-framework. 
In linux/bsd: `./make-from-alarm-framework` will generate this README, .gitignore, zServicesList.ino, and link in the basic files.
We link to them, so they are only in one place.
Note the .gitignore.

For windows, manually create the shortcuts listed below, after "LINKS".
For linux, execute the lines after "LINKS", or run:
    (cd ..; ./make-from-alarm-framework --regen)

## Setup Particle Dev

I needed to install the particle-cli first, to debug the usb connection
    bash <( curl -sL https://particle.io/install-cli )
    for me: rm ~.bash_profile, already had path
The clue is:
    particle usb list
        crashes with some NULL error

Download and install the udev rules
https://docs.particle.io/archives/installing-dfu-util/#installation-linux
sudo udevadm control --reload-rules && udevadm trigger
Now the particle usb list works, and the "connect" works in:

## local build with neopo
I could locally build with neopo:

    https://github.com/nrobinson2000/neopo

    bash <(curl -sL neopo.xyz/install)
    # did not set permissions (my umask?), so fix:
    sudo chmod a+r `which neopo`
    sudo chmod -R a+r /usr/local/lib/python3.6/dist-packages/neopo
    sudo find /usr/local/lib/python3.6/dist-packages/neopo -type d | xargs chmod a+x
    neopo install

### non-local build with workbench
Need the workbench to install the local build tools:
https://docs.particle.io/getting-started/setup/setup-options/

Install workbench:
https://docs.particle.io/quickstart/workbench/#linux

https://docs.particle.io/getting-started/developer-tools/workbench/#local-build-and-flash

## Links

LINKS
ln -s ../boron-minus80/Makefile
