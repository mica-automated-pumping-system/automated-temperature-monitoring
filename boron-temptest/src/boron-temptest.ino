#include <Streaming.h> // Mikal Hart <mikal@arduiniana.org>
#include <every.h> // Alan Grover <awgrover@gmail.com>

#include <Adafruit_MAX31865.h>

// Use software SPI: CS, DI, DO, CLK
//Adafruit_MAX31865 thermo = Adafruit_MAX31865(D6, D4, D3, 13);
// use hardware SPI, just pass in the CS pin
Adafruit_MAX31865 thermo = Adafruit_MAX31865(D14);

// The value of the Rref resistor. Use 430.0 for PT100 and 4300.0 for PT1000
// Measured as 305ohms!
// Very sensitive to this value. Larger == larger temp
#define RREF      430.0
// The 'nominal' 0-degrees-C resistance of the sensor
// 100.0 for PT100, 1000.0 for PT1000
#define RNOMINAL  100.0

void setup() {

  //clear_bright_leds(); // always too bright
  delay(1000);

  // Standard block to wait for serial on "USB CDC", aka native usb, e.g. samd21 etc's
  // if not plugged in to usb, continue eventually
  // This also gives you 3 seconds to upload if you program has a hard-hang in it
  unsigned long start = millis();
  static Every::Timer serial_wait(3 * 1000, true); // how long before continuing
  static Every fast_flash(50);
  Serial.begin(115200);
  // always wait, assume serial will work after serial_wait time
  pinMode(7, OUTPUT);
  while (! (Serial || serial_wait()) ) {
    if (fast_flash()) digitalWrite( D7, ! digitalRead(D7) ); 
    delay(10); // the delay allows upload to interrupt
  }
  while (! (serial_wait()) ) {
    if (fast_flash()) digitalWrite( D7, ! digitalRead(D7) ); 
    delay(20); // the delay allows upload to interrupt
  }
  Serial.println("\n");
  Serial.print( millis() - start );
  Serial.println(F(" Start " __FILE__ " " __DATE__ " " __TIME__));

  Serial << endl;

  thermo.begin(MAX31865_3WIRE);  // set to 2WIRE or 4WIRE as necessary
  Serial << F("lower ") << thermo.getLowerThreshold() 
  << F(" upper ") << thermo.getUpperThreshold() 
  << F(" fault N ") << thermo.readFault(MAX31865_FAULT_NONE)
  << F(" fault A ") << thermo.readFault(MAX31865_FAULT_AUTO)
  << F(" fault R ") << thermo.readFault(MAX31865_FAULT_MANUAL_RUN)
  << F(" fault F ") << thermo.readFault(MAX31865_FAULT_MANUAL_FINISH)
  << endl;


  Serial << (millis() - start) << F(" Setup Done, free ") << endl << endl;
}

void loop() {
  uint16_t rtd = thermo.readRTD();

  Serial << "LIB: RTD value: " << rtd;
  float ratio = rtd;
  ratio /= 32768;
  Serial << " Ratio = "; Serial.print(ratio,8);
  Serial << " Resistance = "; Serial.print(RREF*ratio,8);
  Serial << " Temperature = "; Serial.println(thermo.temperature(RNOMINAL, RREF));

  {
  //float per_degree = 0.00385;
  //Serial << "X: Resistance = "; Serial.print(385*ratio,8);
  Serial << "XX: Temperature = "; Serial.println(thermo.temperature(RNOMINAL, 427.0)); // larger rref==larger temp
  /*
  float resistance = 385*ratio;
  float A = 0.0039083;
  float B = -5.775E-7;
  float C = -4.183E-12;
  float r0 = 100;
  
  // resistance/r0 = 1 + A*t + B*t*t;
  // 0 = (1-r/r0) + A*t + B*t*t
  float t = (-A + sqrt( A*A - 4 * B * (1-resistance/r0) ) ) / (2*B);
  Serial << "X: Resistance = "; Serial.print(resistance,8);
  Serial << " Temperatute = " << t << endl;
  */
  }


  // Check and print any faults
  uint8_t fault = thermo.readFault();
  if (fault) {
    Serial.print("Fault 0x"); Serial.println(fault, HEX);
    if (fault & MAX31865_FAULT_HIGHTHRESH) {
      Serial.println("RTD High Threshold"); 
    }
    if (fault & MAX31865_FAULT_LOWTHRESH) {
      Serial.println("RTD Low Threshold"); 
    }
    if (fault & MAX31865_FAULT_REFINLOW) {
      Serial.println("REFIN- > 0.85 x Bias"); 
    }
    if (fault & MAX31865_FAULT_REFINHIGH) {
      Serial.println("REFIN- < 0.85 x Bias - FORCE- open"); 
    }
    if (fault & MAX31865_FAULT_RTDINLOW) {
      Serial.println("RTDIN- < 0.85 x Bias - FORCE- open"); 
    }
    if (fault & MAX31865_FAULT_OVUV) {
      Serial.println("Under/Over voltage"); 
    }
    thermo.clearFault();
  }
  Serial.println();
  delay(1000);
}
