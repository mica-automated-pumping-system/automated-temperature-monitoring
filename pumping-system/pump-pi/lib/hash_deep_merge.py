def deep_merge(adict, newer):
    # return a copy of a, with b merged in (recursive merge)
    # Thus, `newer` will override values
    # cow I think

    def _deep_merge(a,b):
        into = {}

        for k in set(a.keys()) | set(b.keys()):
            if k in a.keys() and not k in b.keys():
                into[k] = a[k] # only a
                continue
            if k in b.keys() and not k in a.keys():
                into[k] = b[k] # only b
                continue

            if not isinstance(b[k],dict):
                into[k] = b[k] # no merge, replace
            elif not isinstance(a[k],dict) and isinstance(b[k],dict):
                into[k] = b[k] # no merge, replace
            elif isinstance(a[k],dict) and isinstance(b[k],dict):
                into[k] = _deep_merge(a[k],b[k])
        return into

    if adict==None and newer==None:
        return None
    if not (isinstance(adict,dict) and isinstance(newer,dict)):
        raise Exception("Expected 2 dictionarys, saw {adict} {newer}")

    return _deep_merge(adict,newer)

import os
if os.getenv('PYTEST',None):
    ct=0
    def cmp(a,b):
        global ct
        ct +=1
        if a!=b:
            raise Exception(f"Merge wasn't right: {a} EXPECTED {b}")

    cmp( deep_merge(None,None), None)
    cmp( deep_merge({},{}), {})

    # absent on one side
    cmp( deep_merge({},{'a':1}), {'a':1})
    cmp( deep_merge({'a':1},{}), {'a':1})

    # actual merge
    cmp( deep_merge({'a':1},{'b':2}), {'a':1,'b':2})
    cmp( deep_merge({'a':{'aa':11}},{'b':{'bb':22}}), {'a':{'aa':11},'b':{'bb':22}})

    # override
    cmp( deep_merge({'a':1},{'a':2}), {'a':2})
    # replace dict w/value
    cmp( deep_merge({'a':{}},{'a':2}), {'a':2})
    cmp( deep_merge({'a':2},{'a':{}}), {'a':{}})

    # deep
    # absent on one side
    cmp( deep_merge({'A':{}},{'A':{'a':1}}), {'A':{'a':1}})
    cmp( deep_merge({'A':{'a':1}},{'A':{}}), {'A':{'a':1}})

    # actual merge
    cmp( deep_merge(
        { 'a' : { 'b':1, 'c':2 }, 'd':4 },
        { 'a' : { 'b':3 }}),
        { 'a' : { 'b':3, 'c':2 }, 'd':4 }
    )

    cmp( deep_merge(
        {'_layout': {'x': {'pos': 0, 'incr': 1, 'd': 10}, 'y': {'pos': 0, 'incr': 0, 'd': 100}}},
        {'_layout': {'x': {'pos': 1, 'incr': 2}, 'y': {'pos': 3, 'incr': 4}}}),
        {'_layout': {'x': {'pos': 1, 'incr': 2, 'd': 10}, 'y': {'pos': 3, 'incr': 4, 'd': 100}}},
    )

    print(f"## Tests {ct}")
