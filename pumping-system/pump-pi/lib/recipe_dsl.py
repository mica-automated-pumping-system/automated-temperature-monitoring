"""
Used by a recipe/dsl.py type runner to run a separate script of just dsl,
or you can use it in a lib/agent.

See the RecipeStep docstring for writing a step.

Recipe for tools/run-dsl-recipe.py:

    recipe = any of the "steps" from recipe-dsl or recipe-dsl-maps
    # run with tools/run-dsl-recipe.py $yourrecipe.py

Standalone
    # your agent .py
    #!/usr/bin/env python3.11
    import asyncio
    import sys,os,json,re
    sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib-pump-pi" )
    sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib" )

    from recipe-dsl import *
    from recipe-dsl-maps import *


    async def loop(self) {

        recipe = any stuff from the dsl...

        await recipe()

"""
import os,time,inspect,asyncio,json,types
from simple_debug import debug

# FIXME: announce step.step.step for the recipe...
# FIXME: which implies a recipe "name"

class RecipeStep():
    """Base class for isinstance() detection. no functionality.
    Each sub class of RecipeStep is a node in the recipe tree,
    Sort of like a single pass compilation.

    To add the dsl, make a subclass of this, with
        class yourdslstep(RecipeStep) :
            def __init__(self, why, *data, up=0, at=0):
                super().__init__(up=up+1, at=at) # duration defaults to 0

                # FIXME: not a 'must'?
                self.totals = {'secs':5} # MUST set this up, it is fetched by things like sequence() to get total duration.

                # other properties are optional
                self.at = at or defined_at(up+1) # gets the line number we were called from, for including in messages later
                self.up=up # if you use .at

                # save context and arguments (this "RecipeStep" could be invoked more than once)
                self.why = why # description, or name, for use in messages later. optional.
                self.data = data # save the data

            def state(self):
                # return your state, where you can re-start from, as if paused or stopped there
                # use this pattern:
                #   [
                #   self.__class__.__name__,
                #   { key : value, ... }, # kwargs to resume()
                #   child-state
                #   ]
                return [self.__class__.__name__, {'repeat_i' : self.repeat_i , 'i' : self.i}, self.steps[self.i].state() ]

            def resume(self, step_name, rest, **kwargs):
                # called before __call__, to give you back `state`
                # you probably want to specify the kwargs, not ** them
                # note the slightly different order: self.__class__.__name__, child-state, **{ ... }
                # the step_name is checked against your class-name before the call
                maybe set self._some_resume_state = firstkwarg ...

            async def __call__(self):
                # when we execute the "recipe", we invoke () on each step:
                # recipe_steps = [ yourdslstep("doing good work", blahblah),... ]
                # for s in recipe_steps:
                #   await s() # note the "extra" ()
                await super().__call__() # counts steps, set self.internal to not count

                # maybe announce this step somehow, or debug, or whatever
                print(f"doing {self.why}, called from {self.at}")

                # if you've saved resume data
                do resume thing
                maybe do yourchild.resumex( *saved resume `rest` ) # nb: convenience resume
                and/or maybe do yourchild.resume( explicit args )

                # Typically, _do_ some async thing
                # But, doesn't have to be async
                await blahblah(blah, self.data) # use the data

        async handle_cancel(self, e):
            debug(f"Cancel during {self}", up=1)

    See various lib/recipe-dsl*.py for more examples.
    """

    step_ct=0 # how many steps we did
    pause_time=0 # total pause time
    lib_agent=None # set by our importer, it's a hack! only applies to comments() and other mqtt lab-automation stuff FIXME

    def __init__(self, duration=0, human_id_prefix=None, why=None, at=0, up=0, base_progress_data=None, internal=False):
        # FIXME remove .duration
        self.duration=duration
        self.totals = {'secs':duration}
        self.internal=internal
        self.why=why
        self.at = at or defined_at(up=1+up) # subclass should call w/ it's(up+1)
        self.up = up
        self.base_progress_data=base_progress_data
        self.human_id_prefix = ''

    def __str__(self):
        return f"<{self.__class__.__name__} {self.why if 'why' in dir(self) else ''}>"

    def human_id(self):
        return f"{self.human_id_prefix}{self.at}"

    def resumex(self, step_name, kwargs={}, rest=None ):
        """Convenience fn to allow: x.resumex( *[step_name, kwargs, rest] )
            to resume( step_name, rest, **kwargs)
            with optional rest, kwargs
            NB: Note the order (which is the same as save_state()) vs resume() order
        """
        debug(f"Resume w/ {self.__class__.__name__}.resume( step_name={step_name}, rest={rest}, {kwargs} )")
        if step_name != self.__class__.__name__:
            raise Exception(f"Expected the saved state of a recipe step to have the same name as this step ('{step_name}'), saw '{self.__class__.__name__}'")

        self.resume( step_name, rest, **kwargs )

    async def __call__(self):
        if not self.internal:
            RecipeStep.step_ct += 1

    async def handle_cancel(self, e):
        debug(f"Cancelled during [{self.step_ct}] {self}", up=1)

    async def say_progress(self, annotation, progress_data=None, status=None, up=0):
        """Send out a comment with progress info
            id: i.j.k. recipe [i]'s
            thread-id: to discard previous status annotations
            annotation:
            line_number: script line number (at)
            human_id: line-numbers instead of i's
        """
        self.payload = {
            '#' : annotation,
            'human_id' : self.human_id(),
            'at' : self.at, # line number
            }

        # merge base_progress_data and progress_data (if any)
        if self.base_progress_data != None or progress_data != None:
            pd = { **(self.base_progress_data if self.base_progress_data != None else {}), **(progress_data if progress_data != None else {}) }
            self.payload['progress_data'] = pd
            #debug(f"## should addded tag_id: {self.payload}")

        if status != None:
            self.payload['status'] = status

        #debug(f"Progress {self.payload} -> {self.lib_agent.topic_for_event()}", up=1)
        await RecipeStep.lib_agent.mqtt_client.publish( self.lib_agent.topic_for_event(self.at), json.dumps(self.payload), retain=False, qos=1 )

class parallel(RecipeStep):
    """parallel("what this parallel does", recipestep, recipsteep...)
        runs the steps in asyncio "parallel"
        announces start, and done
    """
    def __init__(self, why, *forks, up=0, at=0 ):
        super().__init__(up=up+1, at=at)
        self.why=why
        self.i = None
        self.forks = forks
        self._resume = {}
        self._tasks = []

        #FIXME remove .duration

        for step in forks:
            self.duration = max( self.duration, step.duration )

            # All others are cumulative
            # FIXME: maybe need a flag for max/sum for totals?
            for k,v in step.totals.items():
                self.totals.setdefault(k, 0)
                self.totals[k] += v

        self.duration += 2 * 0.001 # progress messages

        self.totals['secs'] = self.duration

    def human_id(self):
        sofar = str(self.i+1) if self.i != None else '' # FIXME: would be nice to say how many finished
        return f"{self.human_id_prefix}{self.at}[{sofar}|{len(self.forks)}]"

    def state(self):
        # we only save forks that were interrupted, all others are done
        # some forks may not have been intended to run, so is None in _tasks
        # it is possible to get a cancel before we create_tasks
        # thus, _tasks might contain RecipeSteps OR tasks or None(s)
        unfinished = {}
        for i,possible in enumerate(self._tasks):
            # we need the `i` relative to .forks
            if possible:
                if possible == None:
                    # wasn't attempted (due to resuming)
                    pass
                elif isinstance( possible, asyncio.Task ):
                    # got as afar as create_task, so, was it not done?
                    if not possible.done() or possible.cancelled():
                        unfinished[i] = self.forks[i].state()
                elif isinstance( possible, RecipeStep ):
                    # didn't even get as far as create_task
                    unfinished[i] = self.forks[i].state()
                else:
                    raise Exception(f"Expected Task or RecipeStep, saw {possible.__class__} {possible}")
                
        return [self.__class__.__name__, {'forks':unfinished}]

    def resume( self, step_name, rest, forks):
        # state looks like: forks: {i: child_state, ...} ]
        # only for forks that that didn't finish
        # with no `rest`

        # keys in json are always str, fix them
        for i in list(forks.keys()):
            forks[int(i)] = forks.pop(i)

        self._resume = forks

    async def __call__(self):
        await super().__call__()
        # FIXME: maybe we pass a context in here? i.e. cycle n of repeat x
        """starts them in order, but if there is an internal async, can move on to the next one.
            other ways:
            parallels = [ create_task( x ) for x in alist ]
            await parallels
            (remembering to "lazy" it)
        """

        # NB: we don't want a cancel to get in here before we build _tasks list
        #   So, no `await` till after building

        start = time.monotonic()
        self._tasks = []

        # We need all the tasks collected before starting any
        # because a cancel needs to cause a save_state of the whole set
        # And, if resuming, may not be all forks
        for self.i,fork in enumerate( self.forks ):
            fork.human_id_prefix = f"{self.human_id()}."
            if not self._resume:
                # if not resuming, everyone goes in
                self._tasks.append( fork )
            elif fstate:=self._resume.get(self.i,None):
                # only those in _resume go in
                fork.resumex( *fstate )
                self._tasks.append( fork )
            else:
                self._tasks.append(None) # we need to keep _tasks coresponding to .forks
        debug(f"Actual tasks {len(self._tasks)}/{len(self.forks)}")

        # We can get cancel anytime now

        await self.say_progress(f"parallel start: {self.why} ({len(self.forks)} in max {h_duration(self.duration)})")

        # it is possible to get a cancel before we create_tasks
        # thus, _tasks might contain RecipeStep OR tasks OR None(s)
        async with asyncio.TaskGroup() as tg:
            for i,x in list(enumerate(self._tasks)):
                if x != None:
                    self._tasks[ i ] = tg.create_task(x())

        self._resume = {}
        self.i=None
        actual_count = len([x for x in self._tasks if x != None]) # attempted FIXME: somehow get that in the human_id
        await self.say_progress(f"parallel done: {self.why} ({actual_count}/{len(self.forks)} in {h_duration(time.monotonic()-start)}/{h_duration(self.duration)})")
        # FIXME: cancel: record state of each fork task

def p_totals(parallel_step):
    """Helper/wrapper around a parallel() to generate a `totals` message, before starting the parallel
    Oblivious to the actual total's keys, just makes a list
    .../totals [ {totals},... ]
    """

    if not isinstance(parallel_step, parallel):
        raise Exception(f"Expected a single `parallel` step: saw p_totals({parallel_step.__class__.__name__} {parallel_step})")

    # [{'ml': 10772.5, 'secs': 868275.0, 'human_time': '10d 01:11:15'}, {'ml': 10772.5, 'secs': 868275.0, 'human_time': '10d 01:11:15'} ]
    # ensure we have seconds, should be ensured by base class RecipeStep
    # FIXME: could do a consistent protocol: .subtotals=[...] if any, totals={grandtotals}
    totals = [ ( {'secs':0.0} | step.totals ) for step in parallel_step.forks ]
    for t in totals:
        t['human_time'] = h_duration( t['secs'] )

    todo = [ send(f"/totals{'/resume' if RecipeStep.lib_agent.parsed_args.resume else ''}", totals) ]
    if not RecipeStep.lib_agent.parsed_args.totals_only:
        todo.append( parallel_step )
    return sequential(f"totals for {parallel_step.why}", *todo, up=1 )

class sequential(RecipeStep):
    def __init__(self, why, *steps, up=0, at=0):
        super().__init__(up=up+1, at=at)
        if not isinstance(why,str):
            raise Exception(f"Expected 1st argument to be `why`, saw: {why.__class__.__name__} {why}")
        self.why=why
        self.steps = steps
        self._resume = [0, []]
        self.i=None

        for step in steps:
            for k,v in step.totals.items():
                self.totals.setdefault(k, 0)
                self.totals[k] += v
            self.duration += step.duration
        self.duration += (2 + len(steps)) * 0.001 # progress comments

    def human_id(self):
        sofar = f"{self.i+1}" if self.i != None else ''
        return f"{self.human_id_prefix}{self.at}[{sofar}/{len(self.steps)}]"

    def state(self):
        return [self.__class__.__name__, {'i':self.i} , self.steps[self.i].state() ]

    def resume(self, step_name, rest, i):
        self._resume = [i, rest] # rest is child_state

    # runs them in order
    async def __call__(self, human_id_prefix_override='', progress=True):
        try:
            await super().__call__()

            self.i=None
            resume_i,child_resume_state = self._resume # 0,[] if no resume

            if progress:
                start = time.monotonic()
                await self.say_progress(f"sequence ({h_duration(self.duration)} for {len(self.steps)} steps): {self.why}")
            for self.i,step in enumerate(self.steps):
                if resume_i != None and self.i < resume_i:
                    debug(f"# Skip seq[{self.i}] till {resume_i}")
                    continue

                #debug(f"## at [{self.i}] {step}")
                step.human_id_prefix = f"{self.human_id_prefix}{human_id_prefix_override}" # we don't need to include ourselves

                if child_resume_state:
                    debug(f"Set child state {step.__class__.__name__}.resume( {child_resume_state} )")
                    step.resumex( *child_resume_state )
                    # don't use again
                    self._resume = [0, [] ]
                    resume_i,child_resume_state = self._resume 
                resume_i = None

                await step() # FIXME: exactly where we want to tell it something about context?
            self._resume = [0,[]]
            if progress:
                await self.say_progress(f"sequence done ({h_duration(time.monotonic()-start)}/{h_duration(self.duration)} for {len(self.steps)}/{len(self.steps)} steps): {self.why}")
        except asyncio.CancelledError as e:
            if not progress:
                # yech, signals that the sub-class will handle it
                raise e

            await self.say_progress(f"sequence done ({h_duration(time.monotonic()-start)}/{h_duration(self.duration)} for {(self.i or 0)+1}/{len(self.steps)} steps): {self.why}", status='stopped')
            await self.handle_cancel(e)
            raise e

def defined_at(up=0):
    # who called our client
    # FIXME: should find code "not in these classes"
    frame = inspect.currentframe()
    for i in range(0,up+1):
        poss_frame = frame.f_back
        if poss_frame: # prevent too far
            frame = poss_frame
    #print(dir(frame))
    #RecipeStep.lib_agent.exit(0)

    return frame.f_lineno

def h_duration(secs):
    h = f"{secs % 60.0 :0.4f}" if secs <= 1 else ( f"{secs % 60:0.2f}" if secs < 600 else f"{int(secs % 60):02}" )
    if (secs % 60) < 10:
        h = "0" + h

    mins = int(secs/60)
    if mins == 0:
        h += " secs"
    else:
        h = f"{int(mins % 60):02}:" + h

        hrs = int(mins/60)
        h = f"{int(hrs % 24):02}:" + h

        days = int(hrs/24)
        if days > 0:
            h = f"{days}d " + h
    return h

class repeat(RecipeStep):
    def __init__(self, why, count, *steps, up=0, at=0, base_progress_data=None):
        """like a generator of n times, but generates comments for each cycle.
            why is the comment, gets "...{}...".format(i)
            FIXME: nested repeats should allow reference to the [i] of each level: %sofar as the composed message's of parents
        """
        super().__init__(up=up+1, at=at, base_progress_data=base_progress_data)

        # FIXME: have to do all the same stuff as sequential
        # I think we are forgetting to set the human-id correctly
        # Can't subclass sequential?

        self.why = why
        self.sequential = sequential(why, *steps, up=self.up+1, at=self.at) # not using sequential's at/up
        self.repeat_i = None
        self.count = int(count) # int should be done for us, but...
        self._repeat_resume = [None,None]

        for step in steps:
            for k,v in step.totals.items():
                self.totals.setdefault(k, 0)
                self.totals[k] += count * v
            self.duration += step.duration
        self.duration += (2 + count) * 0.001 # progress messages

    def human_id(self):
        sofar = f"{self.repeat_i+1}" if self.repeat_i != None else ''
        return f"{self.human_id_prefix}{self.at}[{sofar}/{self.count}]"

    def state(self):
        return [self.__class__.__name__, {'repeat_i' : self.repeat_i}, self.sequential.state() ]

    def resume( self, step_name, rest, repeat_i):
        # state e.g. ( step_name=repeat, rest=['comment'], {'i': 1, 'repeat_i': 0} )
        # 'repeat_i` is our repeat iteration
        # `i` is the sequence's iteration
        sequential_state = rest # actually, the sequence's child_state
        self._repeat_resume = [ repeat_i, sequential_state ]

    async def __call__(self):
        try:
            start = time.monotonic()
            self.repeat_i = None
            progress_data = { 'why':self.why, 'sofar':0, 'count':self.count }
            await self.say_progress(f"repeat start :"+self.why.format(f"{self.count} times {h_duration(self.duration)}"), progress_data=progress_data)
            if self.count==0:
                return

            for self.repeat_i in range(self.count):
                skip_till_repeat_i, sequential_state = self._repeat_resume
                if skip_till_repeat_i != None:
                    # doing this inside the loop saves us from having to reset skip_till_repeat_i etc. & self._resume
                    if self.repeat_i < skip_till_repeat_i:
                        debug(f"Skip repitition [{self.repeat_i}]")
                        continue
                    self._repeat_resume = [None,None] # only once

                sofar = time.monotonic() - start
                sofar = f" {h_duration(sofar)}" if self.repeat_i > 0 else ''

                progress_data = { 'why':self.why, 'sofar':self.repeat_i+1, 'count':self.count }
                await self.say_progress( self.why.format(f"{self.repeat_i+1}/{self.count}{sofar}"),progress_data=progress_data)

                # treat our list as a sequence each time
                if skip_till_repeat_i != None:
                    self.sequential.resumex( *sequential_state )
                seq = self.sequential
                await seq( human_id_prefix_override=self.human_id()+'.', progress=False )

            progress_data = { 'why':self.why, 'sofar':self.count, 'count':self.count }
            await self.say_progress(f"repeat done: "+self.why.format(f"{self.count}/{self.count} times {h_duration(time.monotonic()-start)}/{h_duration(self.duration)}"),progress_data=progress_data)

        except asyncio.CancelledError as e:
            await self.say_progress(f"repeat stopped ({h_duration(time.monotonic()-start)}/{h_duration(self.duration)}): "+self.why.format(f"{self.repeat_i+1}/{self.count} times"), status='stopped')
            await self.handle_cancel(e)
            raise e

class pause(RecipeStep):
    def __init__(self, secs,  why=None, up=0, at=0, base_progress_data={}):
        super().__init__(secs, why=why, up=up+1, at=at, base_progress_data=base_progress_data)
        self.progress = 0
        self._resume = 0
        self._last_consumed_secs = 0
        if why:
            self.base_progress_data['why'] = why
        self.base_progress_data['duration'] = secs

    def human_id(self):
        return f"{self.human_id_prefix}{self.at}[{h_duration(self.progress)}/{h_duration(self.duration)}]"

    def state(self):
        return [self.__class__.__name__, {'sofar':self.progress}]

    def resume(self, step_name, rest, sofar):
        # resume with `sofar` seconds used
        # FIXME: should save-state with timestamp, and then resume = sofar - elapsed-since-saved, and don't pause if > duration
        #   but do record actual pause
        self._resume = float(sofar)

    async def __call__(self):
        #debug(f"Pause start: {h_duration(self.duration)}")
        start = time.monotonic()
        await super().__call__()
        self.progress = self._resume
        try:
            await self.say_progress(f"pause {h_duration(self.progress)}/{h_duration(self.duration)}", progress_data={'elapsed':round(self.progress,1)})

            pause_time = (self.duration - self._resume) if os.getenv('DEBUGFAKESLEEP', None) == None else int(os.getenv('DEBUGFAKESLEEP', None))/10.0
            #debug(f"pause for {pause_time} (DEBUGFAKESLEEP={os.getenv('DEBUGFAKESLEEP', None)})")
            RecipeStep.pause_time += pause_time # just a rough estimate, badly wrong for parallel and resume

            while self.progress < (self.duration if os.getenv('DEBUGFAKESLEEP', None) == None else int(os.getenv('DEBUGFAKESLEEP', None))/10.0):
                # we do pause in increments, so we can do progress
                incremental = min(10, pause_time)
                await asyncio.sleep( incremental )

                # reality based rather than calc'd, prevents accumulating errors
                self.progress = time.monotonic() - start + self._resume
                pause_time = self.duration - self.progress
                if pause_time < 0:
                    pause_time = 0

                await self.say_progress(
                    f"pause {h_duration(self.progress)}/{h_duration(self.duration)}", 
                    progress_data={'elapsed':round(self.progress,1), 'consume_secs':self.duration}
                    )
            #debug(f"Pause done: {h_duration(self.progress)}/{h_duration(self.duration)}")

            self._resume = 0 # must reset in case in a repeat

        except asyncio.CancelledError as e:
            self.progress = time.monotonic() - start
            self._resume = self.progress
            await self.say_progress(f"pause stopped {h_duration(self.progress)}/{h_duration(self.duration)}", progress_data={'elapsed':round(self.progress,1)})
            await self.handle_cancel(e)
            raise e

class send(RecipeStep):
    """Sends a topic/payload"""
    def __init__(self, topic, payload, up=0, at=0):
        # "/topic" is relative to self.lib_agent.topic_for_event()
        # otherwise, it's the full topic
        super().__init__(duration=0.001, up=up+1, at=at)
        self.topic= self.lib_agent.topic_for_event(topic[1:]) if topic.startswith('/') else topic 
        self.payload=payload

    def state(self):
        # we'll only ever save state if we got interrupted, otherwise we finish
        # but, it looks like we can get `cancelled` after the progress is sent, but before await returns
        # so, we don't actually know if we really sent the progress or not (unless we finish the await)
        return [self.__class__.__name__]

    def resume(self, step_name, rest, **kwargs):
        pass # we always redo, because if state was saved, we were interrupted

    async def __call__(self):
        await super().__call__()

        # no dependency on resume-state, we just redo
        debug(f"Send {self.topic} : {self.payload}")
        await self.lib_agent.mqtt_client.publish( self.topic, json.dumps(self.payload) ) # FIXME: qos & retain

class comment(RecipeStep):
    """Sends a status mqtt with "why".
    FIXME: that's actually part of the lab-automation protocol, not base recipe-dsl, but I use it in parallel/sequential etc.
    """
    def __init__(self, msg, up=0, at=0):
        super().__init__(duration=0.001, up=up+1, at=at)
        self.msg = msg

    def state(self):
        # we'll only ever save state if we got interrupted, otherwise we finish
        # but, it looks like we can get `cancelled` after the progress is sent, but before await returns
        # so, we don't actually know if we really sent the progress or not (unless we finish the await)
        return [self.__class__.__name__]

    def resume(self, step_name, rest, **kwargs):
        pass # we always redo, because if state was saved, we were interrupted

    async def __call__(self):
        await super().__call__()

        # no dependency on resume-state, we just redo
        await self.say_progress( self.msg) # FIXME: ideally know that it got sent or not

class waitfor(RecipeStep):
    """Wait-for a topic, with some pattern of payload.
        waitfor( "why", mqtttopic, None|''|{...}, duration=estimate-secs, timeout=secs|None )
            For a dict, kv in the pattern are tested against the actual payload,
                extra keys are ignored.
    """

    def __init__(self, why, topic, pattern, retained=None, duration=0.0, timeout=None, up=0, at=0):
        if not isinstance(why,str):
            raise Exception(f"Expected 1st argument to be `why`, saw: {why.__class__.__name__} {why}")
        super().__init__(duration=0.01, up=up+1, at=at) # FIXME: what about indefinite duration
        self.why = why
        self.topic = topic
        self.pattern = pattern
        self.retained = retained
        self.duration = duration
        self.timeout = timeout

        # when cancelled, collect:
        self._done = False
        self.elapsed_sofar = 0

    def state(self):
        # We either finished, or are still waiting with timeout
        return [
            self.__class__.__name__,
            [],
            { 'done' : self._done, 'elapsed' : self.elapsed_sofar }
        ]

    def resume(self, step_name, rest, **kwargs):
        raise Exception("Not implemented yet")

    async def __call__(self):
        self._done = False
        start = time.monotonic() # FIXME: prob need elapsed for state
        await super().__call__()

        wait_msg = h_duration(self.duration) if self.duration > 0 else '?'
        wait_msg += f"/{h_duration(self.timeout)}" if self.timeout else '/inf'

        await self.say_progress(f"waitfor: {self.why}: {wait_msg}. {self.topic} == {self.pattern}")
        mqtt_message = await self.lib_agent.send_and_waitfor( 
            send=None,payload=None, # no initial send
            response=self.topic, matching=self.pattern, 
            retained=self.retained,
            timeout=self.timeout 
        )
        debug(f"## Got {mqtt_message}")
        self._done = True
        await self.say_progress(f"waitfor done: {self.why}: in {h_duration(start-time.monotonic())}/{wait_msg} {mqtt_message.topic} == {mqtt_message.payload}")
        return mqtt_message

def patch_recipestep_call(actual_step, after_super_call, concurrent=False):
    """Patch (and return) actual_step, for exactly one __call__, unless env NOPATCHSTEP=1.
    `concurrent` = False awaits after_super_call, =True does create_task to let it run concurrent with actual_step.
    Effectively inserting after_super_call() after super().__call__
    on actual_step.__call__:
        Changes the inheritance tree of actual_step to:
            actual_step : patched : actual_step's original base
        The `patched` class implements a __call__:
           rez=super().__call__ # the original super().__call__
           after_super_call()
           unpatch class # restore inheritance
           return rez 
    """
    if os.getenv('NOPATCHSTEP',None):
        return actual_step

    #debug(f"Patch {actual_step.__class__.__name__} w/ {after_super_call}")

    was_class = actual_step.__class__
    actual_super_class = was_class.__mro__[1]
    was_super_call = actual_super_class.__call__
    _concurrent_ref = []

    async def inserted_call(xself): # we are actually a method
        # wraps super's .call
        #debug(f"doing {xself.__class__.__name__}({was_class.__name__}).inserted as the super().__call__")
        #debug(f"1st, call actual super {actual_super_class.__name__}.{was_super_call}")
        rez = await was_super_call(xself)
        #debug(f"doing our after_super_call {after_super_call} (and then the rest of the normal .call)")
        if concurrent:
            _concurrent_ref.append( asyncio.create_task( after_super_call(xself) ) ) # prevent GC of weak ref
        else:
            await after_super_call(xself)
        return rez # rez prob not needed

    class patcher(was_class):
        # We are the base class now, so we need to patch the super-class.__call__
        #   us() -> original() -> originalsuper()
        # we override .call to patch actual's super().call
        # all other methods == actual's
        # no init, because we shouldn't `new` this class
        async def __call__(self, *args, **kwargs):
            #debug(f".call! {was_class.__name__} in {self.__class__.__name__}")

            # patch
            actual_super_class.__call__ = inserted_call
            try:
                # do
                #debug(f"Patched call will: Call actual {was_class.__name__}.__call__. (which should do the patched super: {self.__class__.__mro__}[2])")
                return await super().__call__(*args, **kwargs)
                #debug(f"Done actual __call__")
                return rez # rez prob not needed
            finally:
                # unpatch
                #debug(f"Restore {was_class.__name__}'.call to {was_super_call}")
                actual_super_class.__call__ = was_super_call
                #debug(f"Restore class {self.__class__.__name__} to {was_class.__name__}")
                actual_step.__class__ = was_class # do we unpatch? one shot
                #debug(f"Expect next step")

    #debug(f"insert pather class {patcher}")
    actual_step.__class__ = patcher

    return actual_step

def argparse_add_arguments(parser):
    # ./tools/run-dsl-recipe.py calls us to add to the argparse instance
    sofar = [ parser.epilog ] if parser.epilog else []
    sofar.append( "env NOPATCHSTEP=1 ... # disable patch_recipestep_call(actual,f) patching, act as if plain `actual`")
    sofar.append( "env DEBUGFAKESLEEP=1 ... # treat all pause() as $val/10 sec")
    parser.epilog = "\n".join( sofar )
