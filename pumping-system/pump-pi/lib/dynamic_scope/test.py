#!/usr/bin/env python3.11
import sys,os
sys.path.append( os.path.dirname( sys.argv[0] ) + "/lib-pump-pi" )
from dynamic_scope import *

# global
a = "bob"

with dynamic_scope(globals(), 'a', 'joe'):
    assert a=='joe'
assert a=='bob'

# dict
b = { 'a': "jane", 'c':'test' }
with dynamic_scope(b, 'a', 'sara'):
    assert b['a']=='sara'
    assert b['c']=='test'
assert b['a']=='jane'
assert b['c']=='test'

b = { 'c':'test' }
with dynamic_scope(b, 'a', 'sara'):
    assert b['a']=='sara'
    assert b['c']=='test'
assert ('a' in b) == False
assert b['c']=='test'

# list
c = [ 0, 1 ]
with dynamic_scope(c, 0, 99):
    assert c[0]==99
    assert c[1]==1
assert c[0]==0
assert c[1]==1

c = [ 0 ]
with dynamic_scope(c, 1, 99):
    assert c[0]==0
    assert c[1]==99
    assert len(c) == 2
assert len(c) == 1
assert c[0]==0, c


# class

class Test:
    def __init__(self):
        self.a = 11

inst = Test()
with dynamic_scope(inst,'a', 99):
    assert inst.a==99
assert inst.a==11

with dynamic_scope(inst,'b', 98):
    assert inst.a==11
    assert inst.b==98
assert inst.a==11
assert 'b' not in dir(inst)
