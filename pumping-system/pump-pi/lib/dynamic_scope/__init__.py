"""
Change the value of something for the duration of the block.

with dynamic_scope('$some-global-name', new_value):
    do stuff

with dynamic_scope(something_with_attrs, '$some-attr-name', new_value):
    # works with classes (get/set-attr), dict (get/=), lists ([index])
    do stuff
"""
from contextlib import contextmanager
import inspect

@contextmanager
def dynamic_scope(base, name, new_value):
    """Change the value of something for the duration of the block. Takes (something_with_attrs|globals(), '$some-attr-name', new_value)"""

    class Whack:
        # dumy unique marker
        def __init__(self, data):
            self.data=data
        def __repr__(self):
            return f"{super().__repr__()} {self.data}"

    # figure out new_value, getter(), and setter(v)
    if isinstance(base,dict):
        if name in base:
            getter = lambda: base[name]
            def setter(v):
                base[name] = v
        else:
            getter = lambda: Whack # dumy
            def setter(v):
                if v==Whack:
                    del base[name]
                else:
                    base[name] = v

    elif isinstance(base,list):
        if len(base) > name:
            getter = lambda: base[name]
            def setter(v):
                base[name] = v
        else:
            getter = lambda: Whack(len(base)) # dumy
            def setter(v):
                if isinstance(v,Whack):
                    #print(f"restore to [0:{v.data}] from {base}")
                    del base[v.data:len(base) ]
                    #print(f"    -> {base}")
                else:
                    base.extend( (None for x in range(0, 1+name-len(base)) ) )
                    base[name] = v
                    #print(f"set to [{name}]={v} in new len {len(base)} in extended ...{range(0, 1+name-len(base))} -> {base}")

    else: # class
        if name in dir(base):
            getter = lambda: getattr(base,name)
            def setter(v):
                setattr(base,name,v)
        else:
            getter = lambda: Whack # dumy
            def setter(v):
                if v==Whack:
                    delattr( base, name )
                else: 
                    setattr(base,name,v)

    #print(f"base is {base.__class__.__name__}")

    was =  getter()

    #print(f"before: {was} -> {new_value}")
    try:
        setter(new_value)
        yield
    finally:
        setter(was)
        #print(f"after {getter()}")

