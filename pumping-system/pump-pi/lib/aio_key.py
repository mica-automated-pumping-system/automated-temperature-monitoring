import re

def aio_key(config='config/aio.key'):
    # return { envvar:v } extracted from the aio.key file
    # makes no effort cache it
    env = {}
    with open(config,'r') as fh:
        for l in fh:
            if m:=re.match(r'export\s+(.+)',l):
                kv = re.sub(r'"','',m.group(1))
                k,v = kv.split('=',2)
                env[k] = v
    return env
