#!/usr/bin/env python3.11
'''
USAGE: (execute this lib file to output this template)

    # your agent file: maps/pump-pi/$dir/$agent.py
    #!/usr/bin/env python3.11

    import asyncio
    import sys,os,json,re
    sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib" )
    sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib-pump-pi" )
    from simple_debug import debug
    import paho.mqtt as paho # some constants|--help'
    import aiomqtt as mqtt # for errors
    import agent

    # write your
    class SomeAgent(agent.Agent):
        """put your description here, which will be announced, like
        We are the special agent for doing good work
        explain explain
        """
        # lifecycle is:
        # SomeAgent()
        # .argparse()
        # .read_mqtt_config()
        # if --help, show help & exit
        # connect with `will` based on .description
        # taskgroup.create_task( .listener( ) )
        # taskgroup.create_task( .advertise() )
        # taskgroup.create_task( .loop() ) # after mqtt-connect, and above
        # await .cleanup(exception) if we catch an exception
        #   always call super().cleanup(exception) if you override

        mqtt_config = {
            "device" : "stepper", # advertise's topic = local/devices/$device/$name
            "name" : "pumpbox1",
            "pump_ct" : 6,

            # 'connection' : {...} a paho-mqtt config, default is config/mqtt-local-config.py
            #       "mdns_service" : "amps-pi_mqtt_broker", to resolve mdns by service name
            # override/setup in argparse():
            # ThisClass.mqtt_config['connection'] = mqtt_config_reader.connection( self.action_config['mqtt_config'] )
            #   where mqtt_config could = "config/mqtt-local-config.py
            # ThisClass.mqtt_config['name'] = MQTTAction.mqtt_config['connection']['hostname']

            # also see config/mqtt-aio-config.py
            # see https://sbtinstruments.github.io/asyncio-mqtt/configuring-the-client.html
            # and pip aiomqtt/client.py to figure out details
        }

        def __init__(self, args):
            # ... if you want
            super().__init__() # minimal setup
            print( f"deal with args... {args}, argparse has not happened yet")

        async def argparse(self):
            # if you need to handle/look at any argparse
            await super().argparse()
            debug( f"self.parsed_args, {self.parsed_args}, has the argparse.args result")

        # LEGACY/OBSOLETE: def interesting_topics(self):
            """Add to topics that are useful to print out for --topics"""
            return super().interesting_topics() + [ self.topic_for_event('MYTOPIC') ]

        def topics(self):
            """
            For the typical pattern, list topics+handler and they'll show up for --topics and be subscribed.
            Automatically adds the handler's docstring as `description:`.
            To inhibit showing up in --topics, use "interesting":False.
            To document as a published topic, omit "handler".
            To add help text in the Listener section, "handler" : None.
            """
            return super().topics( {
                    # listener
                    self.topic_for_command("bob") : {
                        "handler" : self.handle_local_get_self,
                        "description" : "some text, if omitted, will use the handler's doc-string"
                    },
                    # for multiple listeners for the same topic:
                    self.topic_for_command("joe") : [
                        { "handler" : self.handle_local_get_sibling },
                        { "handler" : self.handle_ring_bell },
                    ],
                    # to add documentation for the "publisher" section, with no actual action
                    self.topic_for_command("about-these-topics") : {
                        "handler" : None, # no actual handling, this is documentation
                        "description" : "Have to have description, else what's the point?"
                    },
                    # to document publishing topics (you have to write the publishing code yourself)
                    self.topic_for_command("bob") : {
                        # omit "handler"
                        "description" : "Have to have description, else what's the point?"
                    },
                }
            #, include_default=False # prevents default topics
            )

        def argparse_add_arguments(self, parser ):
            # add your argparse arguments, override mqtt_config, etc
            parser.set_defaults(mqtt_config='config/mqtt-multi-config.py') # override --mqtt-config

        # async def advertise(self ):
            # override for initial advertise, e.g. "don't"
            # probably override last_will_payload() too
            # pass # don't

        # def last_will_payload(self):
            # override
            # e.g. return None for no last_will, nor say_quit()
            # return None

        #async def listener(self):
            # optional
            # subscribes to topics(), assigning handlers
            # Only need this if you want to override. e.g. "don't listen", or something not in topics()
            # We `await yourhandler`, so other messages won't be handled till you finish.
            # Maybe use asyncio.create_task() to spawn long-running things.
            # Thus, by default, there are no race-conditions between handlers,
            # (but there would be if you create_task())

            # your stuff if not in topics():
            # await self._subscribe( self.topic_for_get(), [self.handle_local_get_devices] ) 
            
            # print("see How To Exit below")

        # each handler
        async def handle_local_get_devices(self, message):
            # message.topic is a str, message.payload is json-decoded {...}, [...], or str (absence of payload is {})
            raise Exception("Implement things like me")

        async def after_mqtt_connect(self):
            # if you want to handle more mqtt connects yourself, override this
            # first entry of self.mqtt_config['connections'][...] is already open as self.mqtt_client,
            # by default, it does nothing
            # called as asyncio.create_task( self.after_mqtt_connect() )
            # e.g. 

            async for client,first_time in self.mqtt_connect_with_retry(**self.mqtt_config['connections']['cloud-mqtt'],will=will):
                # this block will happen for each reconnect!, nb `first_time`
                setup more async stuff...
                don't exit block!

        async def loop(self, first_time):
            # required.
            # have to busy wait if you want to do nothing
            raise Exception("Implement things like me")


            #setup
            #start async work stuff

            while(True):
                await asyncio.sleep(1)
                print("""
                poll...
                etc

                see How To Exit below
                """)

        async def cleanup(self, e):
            await super().cleanup(e)

            print(f"do your cleanup, like GPIO.cleanup(), because we are exiting, because {e}")


    # RUN
    # note that Agent.run() does argparse for '-h|--help' in the base .run()
    SomeAgent( args maybe ).async_run() # internally does asyncio.run( agent.run() )

How To Exit
    To do a clean exit, which nicely kills off an async tasks, and sends the 'quit' status.
    Should be able to call this from any `def async`.

    await self.exit($statuscode, $exception-if-you-have-it)
    return # uh... cause this whole chain of awaitables to die...?
'''

import asyncio
import json,sys,os,time,re,datetime,argparse,signal,subprocess,traceback
from contextlib import asynccontextmanager
from copy import copy
StartupFile = sys.argv[0]
sys.path.append( os.path.dirname( StartupFile ) + "/../lib" )
sys.path.append( os.path.dirname( StartupFile ) + "/../lib-pump-pi" )
import aiomqtt as mqtt
import paho.mqtt as paho
import mqtt_config_reader
#from mqtt_local_config import connection as mqtt_local_config

from simple_debug import debug 

# Override these as needed
HelpEpilog = '' # inject to add an argparse help epilog
AnnounceIncoming = 0 # debug level for logging incoming messages, default = noisy

class Agent:

    def __init__(self):

        self._sorttopics = True # for --topics

        # set this for the real exit(), we handle exiting in .async_run()
        self.exit_code = 0
        self._already_say_quit = False

        # this relies on construction outside of the toplevel asycio.run( awaitable )
        # we need this to exit w/o error
        self.root_tasks = [] # tasks we shouldn't cancel (i.e. the base asyncio.run)

        self.ct = 0 # incoming message count
        self.mqtt_out_queue = asyncio.Queue(10) # publish messages: { topic: payload: retain: }
        self.subscribe_handlers = {} # topic(str/match) : handler

        self.mqtt_client = None
        self.dispatch_incoming_ready = asyncio.Event()

    def state_file(self):
        return self._relative_json_file('state')

    def config_file(self):
        return self._relative_json_file('config')

    def _relative_json_file(self, json_basedir):
        return self._relative_agent_file(json_basedir, 'json')

    def _relative_agent_file(self, basedir, extension):
        # return $basedir/$agent/basename.$extension for the sysv.arg[0]
        # assumes agents/$script or tools/$script, i.e. 2 part
        # e.g. state/stepper/serial-stepper.json # NB: no .py

        # We assume that pwd is the directory containing config/ & state/ & $us/$us.py
        # BUT when started by systemd, arg[0] is an absolute path,
        # FIXME: hackish: assumes agents are always: ..../$category/$specific.py
        
        # now try to be agnostic
        agent = os.path.dirname( StartupFile )
        if agent.startswith('/'):
            # absolute arg[0]
            basedir = os.path.join(os.path.dirname( agent ), basedir)
            agent = os.path.basename(agent) # remove abs part 
        filepart = os.path.basename( StartupFile )
        basefilepart, ext = os.path.splitext( filepart )
        #debug(f"relfile SF '{StartupFile}' agent '{agent}' fpart '{filepart}' bfp '{basefilepart}.{ext}'")

        path = os.path.join(basedir, agent, basefilepart + '.' + extension ) # e.g. state/stepper/serial_stepper.json
        debug(f"relative file {basedir}/ : {path}")
        return path;

    def load_json_config(self, state_override=True, remove_comments=True):
        # return the json config from config_file(), or None
        # reads the state file, and merges into the config values
        # thus, state overrides config
        # Unless you say False
        # `remove_comments` will drop keys that start with '#'
        #   tries to recursively descend into dicts and lists

        config = None

        if os.path.isfile( self.config_file()):
            with open(self.config_file(),'r') as fh:
                try:
                    config = json.load( fh )
                    #debug(f"loaded config for {self.config_file()} = {config}")
                except json.decoder.JSONDecodeError as e:
                    if 'Expecting value: line 1 column 1 (char 0)' in str(e):
                        pass # white space file
                    else:
                        raise e

        else:
            debug(f"No config for {self.config_file()}")

        if state_override:
            state = self.load_state()
            if state:
                debug("State file will override config...")
                if config==None:
                    config={}
                config.update( state )
                debug(f"loaded state to get {config}", level=2)

        if remove_comments:
            self.remove_comments(config)
        debug(f"final config {config}", level=2)

        return config

    def remove_comments(self, jsonish):
        # removes '#' keys from a dict/list structure, recursively
        # destructive!

        if isinstance(jsonish, list):
            for x in jsonish:
               self.remove_comments(x)
        elif isinstance(jsonish, dict):
            allkeys = list(jsonish.keys())
            for k in allkeys:
                if k.startswith('#'):
                    del jsonish[k]
                else:
                    self.remove_comments( jsonish[k] )

    def load_state(self):
        # return the json state from state_file(), or None
        if os.path.isfile( self.state_file()):
            with open(self.state_file(),'r') as fh:
                rez = json.load( fh )
                #debug(f"saved state for {self.state_file()} = {rez}")
                return rez
        else:
            debug(f"No saved state for {self.state_file()}")
            return None

    def save_state(self):
        # Saves .state() to state_file()
        # need to makedir here, because state_file() could be overridden elsewhere (and effective only after __init__)
        os.makedirs(os.path.dirname(self.state_file()), exist_ok=True)

        debug(f"Save state to {self.state_file()}")
        with open(self.state_file(),'w') as fh:
            fh.write( json.dumps( self.state(), sort_keys=True, indent=4 ) )
            fh.write("\n")
        
    def ensure_directory(self, dir=None, file=None ):
        # mkdir for dir, or file.parent
        # pick one of those
        debug(f"ensure d {dir} f {file}")
        if file:
            dir, dumy = os.path.split(file)
        os.makedirs(dir, exist_ok=True)  

    def topic_for_device(self, postfix=None, with_device=True, with_id=True):
        return self.topic_for_local(category='devices', with_device=with_device, with_id=with_id, postfix=postfix)
    def topic_for_event(self, postfix=None,with_device=True, with_id=True):
        return self.topic_for_local(category='events', with_device=with_device, with_id=with_id,postfix=postfix)
    def topic_for_get(self, postfix=None, with_device=True, with_id=True):
        return self.topic_for_local(category='get', postfix=postfix, with_device=with_device, with_id=with_id)
    def topic_for_command(self, postfix=None, with_device=True, with_id=True):
        return self.topic_for_local(category='commands', with_device=with_device, with_id=with_id, postfix=postfix)
    def topic_for_local(self, category, postfix=None, prefix=None, with_device=True, with_id=True):
        # to build a topic: local/[$prefix/]$category/$devicetype/$id[/$postfix]
        # category is usually 'devices', 'events'
        topic = [ 'local' ]
        if prefix != None:
            topic.append( prefix )
        if category != None:
            topic.append( category )
        if with_device:
            topic.append( self.mqtt_config['device'] )
        if with_id:
            topic.append(self.mqtt_config['name'])
        if postfix != None:
            topic.append( postfix )
        #debug(f"# topic { '/'.join(topic) }")
        return "/".join( (str(x) for x in topic))

    def description(self):
        desc = self.__doc__.lstrip().rstrip()
        return {
            "name" : self.mqtt_config['name'],
            "topic" : self.topic_for_device(),
            "description" :  desc,
            # FIXME: add in other config info, like number-of-pumps, interval, etc.
        }

    async def advertise(self ):
        debug(f"# advertise {self.topic_for_device()}",level=2)
        await self.mqtt_client.publish( self.topic_for_device(), json.dumps( self.description() ), retain=True, qos=1 )

    def queue_a_message(self, topic, payload, retain=False):
        # use the async publisher "thread" to publish, rather than "your" "thread"
        # if the queue is full, will discard
        try:
          self.mqtt_out_queue.put_nowait( { 'topic' : topic, 'payload' : payload, 'retain' : retain } )
        except asyncio.QueueFull:
            sys.stderr.write(f"Outgoing mqtt queue full, discard message: {topic} -> {payload}")
        return

    async def publisher(self):
        # publishes messages from the out queue

        while(True):
            # msg = { topic: ... payload:... }
            msg = await self.mqtt_out_queue.get()

            # client can consider it done (asynchronously)
            self.mqtt_out_queue.task_done()

            if 'topic' in msg and 'payload' in msg:
                #debug(f".publish {msg}")
                await self.mqtt_client.publish( msg['topic'], json.dumps( msg['payload'] ), retain=msg.get('retain',False), qos=1 )
                
            else:
                sys.stderr.write("Bad mqtt_out_queue entry: {msg}\n")

    async def _subscribe(self, topic, handler, *args):
        # _subscribe( topic, def handler(message) )
        # FIXME: add shortcut _subscribe( topic, def handler(yourargs), args,... )
        # registers a handler for a topic (wildcards allowed)
        #   (dispatched at dispatch_incoming())
        #   the handler will be called w/ await yourhandler( message ) in another task_group
        #       message is aiomqtt.Message type with .payload = str|number|json-decode(if [] or {})
        #       the mqtt_client is self.mqtt_client
        # topic/topic/# -> wildcard match
        # as should topic/+/topic but not yet
        # topic='#' will never trigger a handler

        if not isinstance(topic,str):
            raise Exception(f"Expected a 'str' topic, saw {topic.__class__} : {topic}")

        # we can have a list of handler-dicts, tolerate a single
        if not isinstance(handler,list):
            handler = [handler]

        if len(handler) == 0:
            # no handlers is one of the topics() for publishing, not subscription
            debug(f"Publish topic, not subscription: {topic}", level=2)
            return

        matching_topic = self.subscribe_topic(topic)

        self.subscribe_handlers[matching_topic] = handler

        await self.mqtt_client.subscribe( topic )
        handler_desc = [ h for h in handler ] if len(handler) > 1 else (handler[0] if handler else "<documentation>")
        debug(f"subscribed to {topic}{f' as {matching_topic}' if isinstance(matching_topic,re.Pattern) else ''} :: {handler_desc}",level=2)
        #debug(f"### sofar {self.subscribe_handlers}")

    async def _unsubscribe(self, topic):
        # removes a handler for a topic, complement of _subscribe

        if not isinstance(topic,str):
            raise Exception(f"Expected a 'str' topic, saw {topic.__class__} : {topic}")

        matching_topic = self.subscribe_topic(topic)

        # seems a slightly better order, we will ignore messages immediately

        rez = self.subscribe_handlers.pop(matching_topic, None)

        await self.mqtt_client.unsubscribe( topic )

        debug(f"unsubscribed from {topic}{f' as {matching_topic}' if isinstance(matching_topic,re.Pattern) else ''} {'WASNT SUBSCRIBED' if rez==None else ''}")

    def subscribe_topic(self, topic):
        # give the `self.subscribe_handlers` key for the topic
        # i.e. plain topic or the regex
        if re.search(r'\+|(#$)', topic):
            topic_pieces = topic.split('/')
            new_topic_pieces = []
            for i,piece in enumerate(topic_pieces):
                if piece=='+':
                    new_topic_pieces.append( r'[^/]+' )
                elif piece=='#' and i+1==len(topic_pieces):
                    pass # drop it
                else:
                    new_topic_pieces.append( re.escape( piece ) )
            topic = re.compile( '^' + '/'.join(new_topic_pieces) )
        return topic

    async def send_and_waitfor(self,
        send, payload, # topic & payload
        response, matching, # topic and list of matching payloads
        timeout, # in secs
        retained=None,
        progress_callback=None # a callback( mqtt_message ) for all responses
        ):
        """When you have a send/response pattern, the poor man's session.
        You can just do waitfor by supplying None for `send`.
        `await` will block until one of the [matching] matches the response payload.
            matching can be list (match any of), or a single item
            a dict: matches if all the k,v are in the payload, ignores payload's other k,v
            string|number: matches if the payload is a simple string/number
            for an empty payload, match on {} # FIXME: arguably a bug
        `retained` can be 1, 0, or None
        `timeout` may be None, for no timeout
        """

        # fun with closures

        async def message_response(response, matching, timeout):
            # await to get this setup (subscribed)
            # await the _result_ of this:
            # eventual_response = await message_response()
            # send mqtt etc...
            # await eventual_response

            #debug(f"wait for '{response}' -> {matching} w/timeout {timeout}")
            if not isinstance(matching,list):
                # uniform treatment hereafter
                matching=[ matching ]

            # for the_response() closure
            hit = asyncio.Event()
            mqtt_response = None

            async def the_response():
                # this is what is returned, await it to get the mqtt_response,
                await hit.wait()
                # FIXME: subscribing & unsubscribing each time costs us up to 0.03 secs. do a delayed unsubscribe bounded by repeats?
                await self._unsubscribe( response ) #, matches_payload ) FIXME
                return mqtt_response

            async def matches_payload(message):
                nonlocal mqtt_response
                # the listener system calls us for our response-topics
                payload = message.payload
                #debug(f"  test {payload} == {matching}")

                if retained != None and message.retain!=retained:
                    return

                for payload_pattern in matching:
                    #debug(f"    test == {payload_pattern}")

                    # `not` tests to allow one place to handle hit
                    if payload==None and payload_pattern==None:
                        debug(f"    HIT None==None")
                        hit.set()

                    # FIXME: matching function omg say smartmatch

                    elif isinstance(payload,dict) and isinstance( payload_pattern,dict ):
                        # Only tests items in payload_pattern. others are irrelevant
                        # FIXME: re
                        #debug(f"      test dict...")
                        mismatch = next( (False for x in payload_pattern.items() if x[1]!=payload.get( x[0] )), None)
                        if mismatch==None:
                            debug(f"    HIT dict=extant dict")
                            hit.set()

                    elif payload == payload_pattern: # str|number
                        debug(f"    HIT value=value")
                        hit.set()

                    else:
                        #debug(f"  no match")
                        pass

                    # one place to handle hit
                    if hit.is_set():
                        mqtt_response = message
                        return # all done
                    else:
                        if progress_callback:
                            progress_callback( message )

                # didn't match this payload
            # end matches_payload()

            # Register # FIXME: change the subscription handler system to have a list of handlers (subscribe/unsub on ref count)
            already_topic,already_handler = self.find_subscription_handler( response )
            if already_topic != None:
                raise Exception(f"That topic is in use by us: '{already_topic[0]}' {already_topic[1]}")

            await self._subscribe( response, [matches_payload] )

            # the returned thing waits for the hit and unsubs
            return the_response()

        try:
            eventual_response = await message_response( response, matching, timeout) 
            if send != None:
                await self.mqtt_client.publish( send, json.dumps(payload), retain=False, qos=1 )
            #debug(f" wait for {response} -> {matching} w/timeout {timeout}")
            actual_response = None
            if timeout == None:
                actual_response = await eventual_response
            else:
                async with asyncio.timeout(timeout):
                    actual_response = await eventual_response
            debug(f" await response! {actual_response}")
            return actual_response
        except asyncio.exceptions.CancelledError as e:
            debug(f"unsub because cancel: {response}")
            await self._unsubscribe( response )
            raise e
         
    async def keep_connected(self):
        # repeats the block, reconnecting as necessary
        retry = True
        retry_ct = 0
        while(retry):
            try:
                yield retry_ct
            except mqtt.exceptions.MqttError as e:
                if 'Disconnected during message iteration' in str(e):
                    if self.exit_code != 0:
                        traceback.print_exception(e)
                        debug(f"Disconnected during self.exit...")
                        return
                    else:
                        traceback.print_exception(e)
                        debug(f"Disconnected while listening {self.__class__.__name__}")
                        debug(f"RETRYING in 5 seconds")
                        self.reconnect()
                        retry  = True
                        retry_ct += 1
                else:
                    raise e

    def payload_more_convert(self, payload):
        """by default, we only to json on [...] and {...} payloads,
            this method is more aggressive:
                null : None
                floats
                True|False -> bool
        """
        if payload == 'null' or payload == None:
            return None
        v = {'True':True,'true':True,'False':False,'false':False}.get(payload,None)
        if v != None:
            return v
        try:
            v = float(payload)
            return v
        except ValueError:
            return payload
            
    async def dispatch_incoming(self):
        # as requested by _subscribe, usually dispatched to handle_x methods
        # We `await yourhandler`, so other messages won't be handled till you finish.
        # Maybe use asyncio.create_task() to spawn long-running things.
        # Thus, by default, there are no race-conditions between handlers,
        # (but there could be if you create_task())

        self.dispatch_incoming_ready.set() # signal we are ready for incoming

        try:
            async for message in self.mqtt_client.messages:
                topic = str(message.topic)
                message.topic = topic
                self.ct += 1
                # don't echo the "ping"
                if message.payload != b'""' and self.parsed_args.debug_show_incoming:
                    debug(f"In {datetime.datetime.now()} [{self.ct}]> {topic} : {message.payload}  (retained {message.retain})")
                #debug(f"  poss: {self.subscribe_handlers} ? {topic in self.subscribe_handlers}")

                handler_key, handler = self.find_subscription_handler( topic )
                debug(f"  found handler? {topic} -> [{handler_key}] {handler}", level=AnnounceIncoming)
                if handler_key==None:
                    # no-key means nothing registered as handler
                    debug(f"  No handler for '{topic}")
                    #debug(f"### in {self.subscribe_handlers}")
                    continue
                if handler == None:
                    # no-handler means a publishing topic, not subscription topic
                    debug(f"WARNING: should not have subscribed to topic (publish only topic): {topic}")

                # str'ify and json decode
                if message.payload != None:
                    message.payload = str( message.payload, 'utf-8')
                if message.payload.startswith("[") or message.payload.startswith("{"):
                    try:
                        message.payload = json.loads( message.payload )
                    except json.decoder.JSONDecodeError as e:
                        debug(f"  bad json payload {message.payload}\n\t{e}")
                        continue
                elif message.payload==None or message.payload=='':
                    message.payload={}
                #else:
                # could be a bare number/boolean/etc

                debug(f"  handler call {topic} -> {handler}",level=2)
                # spawn, we do wait and that might block other incoming, do create_task() inside
                start = time.monotonic()
                async with asyncio.TaskGroup() as tg:
                    for i,h in enumerate(handler):
                        tg.create_task( h(message), name=f'handler[{i}] {topic}' )
                #debug(f"back from handler {handler}")
                if time.monotonic() - start > 2:
                    debug(f"Warning, a handler took a while (>2secs), maybe that handler should create_task() so it doesn't block other incoming messages? Consider race-conditions! {message.topic} -> {handler}")

        except mqtt.exceptions.MqttError as e:
            if 'Disconnected during message iteration' in str(e):
                if self.exit_code != 0:
                    # I hope this is always a side-effect of some other exception
                    debug(f"Disconnected during self.exit...")
                else:
                    traceback.print_exception(e)
                    debug(f"Disconnected while listening {self.__class__.__name__}")
                    raise e
            raise e
        except (asyncio.exceptions.CancelledError,ExceptionGroup) as e:
            traceback.print_exception(e)
            # FIXME: this is unhandled, should crash. e.g. an AttributeError during handler(message)
            debug(f"## CANC {e.__class__.__name__}: |{e}|")
            raise asyncio.exceptions.CancelledError
        except Exception as e:
            debug(f"During dispatch, some other {e}")
            await self.exit(1, e)

    def mqtt_topic_match(self, topic, pattern ):
        # the mqtt topic match rules
        # FIXME: do this once at "add topic to subscribe", everything is a re

        if isinstance(pattern,re.Pattern):
            #debug(f"  # Test {topic} vs RE {pattern}")
            if pattern.search(topic):
                return True
            else:
                return False

        # could be a + or # pattern

        esc_pattern = re.escape( pattern )
        # now we have to remove the escape on + and #
        esc_pattern = re.sub( r'\\\+','+', esc_pattern)
        esc_pattern = re.sub( r'\\\#','#', esc_pattern)

        # none of are regex patterns should trigger re-substitution
        having_wildcards = re.sub(r'^\+/', '[^/]+/', esc_pattern)
        having_wildcards = re.sub(r'/\+$', '/[^/]+', having_wildcards)
        having_wildcards = re.sub(r'/\+/', '/[^/]+/', having_wildcards)
        having_wildcards = re.sub(r'/#$', '/.+', having_wildcards)
        having_wildcards += "$"

        #debug(f"  # Test {topic} vs {pattern} --> {having_wildcards} ")
        return re.search( having_wildcards, topic )

    def find_subscription_handler(self, topic):
        # find the subscribe handler for a topic
        for subscribe_topic,handler in self.subscribe_handlers.items():
            #debug(f"Possible {subscribe_topic}")
            if self.mqtt_topic_match( topic, subscribe_topic ):
                #debug(f"Matched {subscribe_topic} -> {handler}")
                return ( subscribe_topic, handler)
        return (None,None)

    async def listener(self):
        # call with super()
        # typical ping
        # specifically at the various levels: general /devices, /devices/$class, and for us
        # cancels better than .gather
        # We do setup a subscribe->handler here
        # But, the calling of the handler is from the dispatch_incoming() task
        async with asyncio.TaskGroup() as tg:
            ct=0
            for t,handler_info in self.topics().items():
                ct += 1
                # (a handler key that exists but is None is a dumy for --help)
                if isinstance(handler_info,dict):
                    handler_info = [handler_info]

                # skip absent and None handlers
                handlers = [ x['handler'] for x in handler_info if 'handler' in x and x['handler'] ]
                #debug(f"## _sub to {t}\n\tw/ {handler_info} \n\t-> {handlers}")

                tg.create_task( self._subscribe( t, handlers ), name=f'subscribe {t}' )

                # let subscribes execute, if we have a lot
                # otherwise, we get a warning from mqtt_async
                # the message is misleading, it is really "...pending subscribe calls", but says:
                #   There are 11 pending publish calls.
                if (ct % 10) == 0:
                    for x in range(0,10):
                        await asyncio.sleep(0.0001) # have to do it multiple times, or a long wait
        debug(f"### subscribed for all `get`'s",level=2)

    async def handle_echo(self, message):
        # debug/test handler for a listen
        debug(f"SAW: {message.topic} = '{message.payload}'")

    async def handle_local_get_self(self, message):
        """Repeats advertisement: local/devices/$device/$name {{name, topic, description...}}
        Listens on the several topics local/devices[/$device[/$name]]
        """
        await self.advertise()

    async def cleanup(self, e):
        debug(f"CLEANUP because exception: {e.__class__.__name__} {e}")
        # this usually does nothing, because the mqtt task has probably died and closed
        await self.say_quit(e)

    async def say_quit(self, e=None):
        # supply an exception if you have it
        if not self._already_say_quit and 'name' in self.mqtt_config.keys():
            self._already_say_quit = True
            # un-advertise only if name is defined, allow for "no unadvertise" which is a hack
            #debug(f"mqtt_client? {self.mqtt_client} conn? {self.mqtt_client._client.is_connected()}")

            if self.mqtt_client and self.mqtt_client._client.is_connected() and (payload:=self.last_will_payload()):
                if isinstance(e, KeyboardInterrupt):
                    payload['exception'] = 'KeyboardInterrupt'
                elif isinstance(e, asyncio.exceptions.CancelledError):
                    # ignore, not the real error...
                    pass
                else:
                    payload['exception'] = f"{e.__class__.__name__} {e}"

                await self.mqtt_client.publish(
                    self.topic_for_device(), 
                    json.dumps( payload ), 
                    retain=True, 
                    qos=0 
                    )

    def argparse_add_arguments(self, parser ):
        """override:
        parser.add_argument() for any of your stuff
        then parser.parse_args() will be called
        then your override of argparse() can examine self.argv
        or any other method can look at self.argv
        Default is to read rest of arguments as self.parsed_args.argv
        """
        parser.add_argument('argv', nargs='*', help="args")

    def topics_add_docstring(self, topic_info ):
        # add the `description` from docstring
        for topic,handler_info in topic_info.items():
            # we can have a list of [ {handler:}, {handler:} ], or bare dict {handler:}
            if isinstance(handler_info,dict):
                handler_info = [handler_info]
            for info in handler_info:
                if 'handler' in info:
                    if not info.get('description',None):
                        info['description'] = (info['handler'].__doc__ or "").lstrip().rstrip() or "<no descrption|docstring (fixme)>"
                else:
                    if not info.get('description',None):
                        info['description'] = "<no description (fixme)>"
                    
                #print(f"add {topic} {info['handler']} -> {info['handler'].__doc__}")
        return topic_info

    def legacy_topics(self):
        # using legacy `interesting_topics`?
        if m:=getattr(self,'interesting_topics',None): # ready for removal of interesting_topics()
            return m.__qualname__ != 'Agent.interesting_topics'
        return false
        
    def topics(self, more={}, include_default=True):
        all = {
            self.topic_for_get() : { "handler" : self.handle_local_get_self },
            self.topic_for_get(with_id=False) : { "handler" : self.handle_local_get_self, "interesting":False },
            self.topic_for_get(with_device=False, with_id=False) : { "handler" : self.handle_local_get_self, "interesting":False },
        } if include_default else {}
        all.update(more)
        return self.topics_add_docstring(all)

    def interesting_topics(self):
        """Legacy: Use topics(). Topics that are useful to print out for --topics
            FIXME: Should delete when other code is fixed
        """
        return [
            self.topic_for_device(),
            self.topic_for_get(with_device=False, with_id=False),
            self.topic_for_get(with_id=False),
            self.topic_for_get(),
            #self.topic_for_event(),
            #self.topic_for_command(),
        ]

    async def argparse(self, autohelp=True):
        """super() && override to access the parsed args before .run()
        parse command line args just before .run()
        (because awkward to do in right place at __init__)
        self.parsed_args is  the argparse.ArgumentParser
        self.parsed_args.argv is the rest of the args unless you override argparse_add_arguments
        override .argparse_add_arguments(self,parser) to add_arguments
        `autohelp` # handle --help normally. false means set .help if --help. useful for modifying help
        """

        # note the --help hack hack hack
        parser = argparse.ArgumentParser(
            description=self.__doc__, 
            formatter_class=argparse.RawDescriptionHelpFormatter,
            epilog=HelpEpilog,
            add_help=autohelp,
            )

        self.argparse_add_arguments( parser ) # allow adding/overriding
        if not autohelp:
            parser.add_argument('--help','-h', help="shouldn't see this", action='store_true')

        # allow override of default by argparse_add_arguments()
        mqtt_config_default = parser.get_default('mqtt_config') or 'config/mqtt-local-config.py'
        parser.add_argument('--mqtt-config', help=f"config/$x.py that defines a `connection`. Overrides the .json. try --mqtt-config-help. default {mqtt_config_default}")
        parser.set_defaults(mqtt_config=mqtt_config_default)

        parser.add_argument('--mqtt-config-help', help="explain a --mqtt-config config/mqtt-config.py file", action='store_true')
        parser.add_argument('--topics', help="list major topics and exit", action='store_true')
        parser.add_argument('--debug-show-incoming', help="debug print incoming messages", action='store_true')

        # have to intercept to prevent errors about required args etc.
        if next( (x for x in sys.argv[1:] if x=='--mqtt-config-help'), None):
            print("""# an mqtt-config.py file (usually in config/):
            You have a choice of a single mqtt-config format, or a multiple format.

            ## Multiple format (multiple mqtt configs):
            # You get these imports for free:
            #   import paho.mqtt as paho # config enums
            #   import aiomqtt as mqtt # especially for mqtt.TLSParameters()
            #   from aio_key import aio_key # especially for things like aio_key('config/aio.rhoover.key')['IO_USERNAME']
            connections = [ # note plural var name: exactly "connections"
                # each config:
                {
                    # The `key` lets you find a config by name: self.mqtt_config['connections']["some descriptive key"]`,
                    # The first entry in `connections[]` is the default connection: `self.mqtt_client`
                    "key" : "some descriptive key",
                    # All the remaining are paho mqtt connect arguments (via aiomqtt Client() constructor)
                    "hostname" : "amps-pi.local",
                    "port" : 1884,
                    "clean_session" : True, # look up this mqtt-parameter, True seems a good default for us
                    "keepalive" : 8, # trigger's Will after this many seconds of lost connection
                    "clean_start" : paho.client.MQTT_CLEAN_START_FIRST_ONLY, # good default
                    # if you need a username/password (optional):
                    "username" : aio_key('config/aio.rhoover.key')['IO_USERNAME'], # aio_key() is "from aio_key import aio_key"
                    "password" : aio_key('config/aio.rhoover.key')['IO_KEY'],
                    # to enable tls:
                    "tls_params" : mqtt.TLSParameters(), # `mqtt` is provided, it's "import aiomqtt as mqtt"
                },
                {
                    "key" : "some other one",
                    ...
                }

            ]
            # Several agents want configs with keys:
            #   'lan' (some mqtt on the local wifi, the pump machine)
            #   'cloud0' (a cloud mqtt server, assumptions are that it is io.adafruit.com)
            #   'rhoover' (a specific login to io.adafruit.com for some functions)
            # The boron mqtt channel is not listed here, since it uses serial to send to the boron then via mqtt

            ## Single format:
            connection = { # note singular var name: exactly "connection"
                # no 'key'
                # all other paho mqtt connect arguments like above, for one connection
                ...
            }
            """)
            exit(0)

        args = parser.parse_args()
        self.parsed_args = args
        #debug(f"ARGPARSED {self.parsed_args}")

    def resolve_mdns(self, connection ):
        # if mdns_service is a key, resolve for host/port
        if connection.get('mdns_service',None):
            debug("NOT implemented `resolve_mdns`")

    async def mqtt_connect_with_retry(self,will,**connection_config):
        # A generator that returns the same aiomqtt Client object each time
        # Yields: aiomqtt Client, first_time-bool
        # Each yield should be when state == connected
        # If you return from the yield, it will retry connecting!
        # Throw if you don't want a retry
        _visible = copy(connection_config)
        _visible.pop('password',None)

        ct = 0
        while (True and ('exit_code' not in dir(self) or self.exit_code == 0) ): # for wifi/tcp-ip/connection failures 
            retry = "" if ct==0 else f"[{ct}]"
            debug(f"Connect {retry} w/ { _visible }",level=(0 if (ct % 10 == 0) else 2)) # no password
            sys.stdout.flush()
            ct += 1

            self.resolve_mdns( connection_config )
            try:
                async with mqtt.Client(
                    **connection_config,
                    # the `will` is NOT triggered on ^c. Will trigger if you ^s and wait for it to time out, or kill -9
                    will=will
                    ) as client:
                        try:
                            yield client, ct==1
                        except mqtt.exceptions.MqttError as e:
                            debug(f"Exception MQTT during yield {e}")
                            traceback.print_exception(e)
                            raise e
                        #we see Cancelled because we are calling the fn w/task-group in it
                        #we dont know why, we want to
                        except (Exception, ExceptionGroup, asyncio.exceptions.CancelledError) as e:
                            debug(f"Exception during yield {e}")
                            traceback.print_exception(e)
                            raise e
                        """ fixme: publish on errors/^c
                        except (Exception, asyncio.exceptions.CancelledError) as e:
                            await self.mqtt_client.publish( self.topic_for_device(), json.dumps( self.description() ), retain=True, qos=1 )
                            raise e
                        """

            except asyncio.exceptions.CancelledError as e:
                # should be caught by mqtt.Client(), bah
                debug("^C in W/RETRY")
                #await client.disconnect()
                raise e

            except mqtt.exceptions.MqttError as e:
                debug(f"in connect w/retry: {e.__class__.__name__} <{connection_config['hostname']}> {e}, retrying in a few...",level=(0 if (ct % 10 == 1) else 2))
                await asyncio.sleep(5) # might be wifi down... # fixme: backoff?

            except (Exception, ExceptionGroup ) as e:
                debug(f"Exception during  with mqtt.Client (open) {e}")
                traceback.print_exception(e)
                raise e

    def print_topics(self):
        if 'device' in self.mqtt_config:
            if self.legacy_topics():
                debug("HAS Legacy Topics method: interesting_topics(). Convert to topics()?")
                # legacy
                for t in sorted(self.interesting_topics()) if self._sorttopics else self.interesting_topics():
                    print(t)
            else:
                # new topics() style
                #debug("HAS topics()")
                our_topics = self.topics()

                # try to adjust to length
                max_topic_len = 0
                for t in sorted(our_topics.keys()):
                    if len(t) > max_topic_len:
                        max_topic_len = len( t )
                def print_publish_subscribe(subscribe_flag):
                    #debug(f"### printing w/ {subscribe_flag}")
                    for t in sorted(our_topics.keys()):
                        handler_info = our_topics[t]
                        if isinstance(handler_info,dict):
                            handler_info = [handler_info]
                        for info in handler_info:
                            if not info.get('interesting',True):
                                continue

                            handler = info.get('handler',False) # absent -> publisher, present&None -> listener-help
                            #debug(f"## {t} : {info} h: {not not handler} == {(not not handler) == subscribe_flag}")

                            descs = [ x.rstrip().lstrip() for x in info['description'].split("\n") ]

                            # Absence of handler -> False, but None means "pretend to be listener for help purposes"
                            if (handler != False) == subscribe_flag:
                                print(f"{t:{max_topic_len}} : {descs[0]}")
                                for d in descs[1:]:
                                    print(f"{'':{max_topic_len}}   {d}")

                conn_info = self.__class__.mqtt_config['connection']
                conn_desc = []
                if 'username' in conn_info:
                    conn_desc.append(f"{conn_info['username']}@")
                conn_desc.append(f"{conn_info['hostname']}")
                if 'port' in conn_info:
                    conn_desc.append(f":{conn_info['port']}")
                if 'tls_params' in conn_info:
                    conn_desc.append(" (tls)")

                print(f"# Listening: {''.join(conn_desc)}")
                print_publish_subscribe(True)
                print("# Publishing:")
                print_publish_subscribe(False)
            exit(0)
        else:
            print("No topics (because `.mqtt_config` has no `['device']`, so 'not a device'")
            exit(0)

    def last_will_payload(self):
        # also used for say_quit()
        # override. returning None inhibits last_will and say_quit() message
        return {**self.description(), "status":"quit"}

    def last_will(self):
        # override to inhibit a last-will
        will = None
        if 'name' in self.mqtt_config.keys():
            # no will if we didn't provide name
            if lwp := self.last_will_payload():
                will = mqtt.Will(
                        topic=self.topic_for_device(), 
                        payload=json.dumps(lwp),
                        qos=1,
                        retain=True
                        )
                debug(f"## Has will: {will.payload}")
        return will

    async def run(self):
        # this should be wrapped by wrapper_run() to deal with exceptions
        # and async_run() to deal with exceptions

        await self.argparse()
        self.read_mqtt_config()
        if self.parsed_args.topics:
            self.print_topics()
            exit(0)

        debug("start")

        will = self.last_will()

        async def setup_mqtt_tasks(first_time):
            self.dispatch_incoming_ready.clear()

            try:
                """
                FIXME: move things out of the client-open if possible?
                FIXME: the if-true in dispatch_incoming
                """
                async with asyncio.TaskGroup() as tg:
                    # start a some things in parallel

                    tg.create_task( self.after_mqtt_connect(first_time), name='after_mqtt_connect' )
                    await asyncio.sleep(0)

                    tg.create_task( self.publisher(), name='publisher' )
                    await asyncio.sleep(0)

                    debug("Start dispatch_incoming", level=(0 if first_time else 2))
                    tg.create_task( self.dispatch_incoming(), name='dispatch_incoming')

                    # ready for incoming before we could possibly get any incoming
                    await self.dispatch_incoming_ready.wait() # Event from dispatch_incoming

                    debug("Setup listener", level=(0 if first_time else 2))
                    # finish setup before loop(), because the user's logic that may evoke incoming packets
                    await self.listener()

                    # should be our first outgoing, which could lead to incoming
                    debug("Start advertise",level=(0 if first_time else 2))
                    tg.create_task( self.advertise(), name='advertise' )

                    debug("spawned mqtt tasks",level=(0 if first_time else 2))

                    debug(f"Start loop (last async) {self.__class__.__name__}",level=(0 if first_time else 2))
                    # FIXME: probably just create_task, do a sleep(0.25 before to let others get started)
                    # also means moving the self.exit_code out of `with group`
                    await self.loop(first_time)

                    if self.exit_code == 0:
                        debug(f"Loop {self.__class__.__name__} finished w/o exception")
                    else:
                        debug(f"Loop {self.__class__.__name__} finished w/error-code {self.exit_code}")

            except KeyboardInterrupt as e:
                if self.exit_code == 0:
                    self.exit_code = 1
                traceback.print_exception(e)
                debug(f"SAW ^CCCC {e}")

            # DEBUGGGING FIXME
            except (asyncio.exceptions.CancelledError, KeyboardInterrupt) as e: # if we need to do something at this level
                debug(f"Cancelled in setup_mqtt_task {e}")
                raise e
            # bubble up to our wrapper to signal exit/done

            except ExceptionGroup as e:
                # have to look at e..exceptions[0...] for specific error if you care
                debug(f"ExcGROUP in setup_mqtt_task {e}")
                if len(e.exceptions) > 1:
                    for i,x in enumerate(e.exceptions):
                        debug(f"ALSO [{i}] {x}")

                if isinstance(e.exceptions[0], mqtt.exceptions.MqttError):
                    if 'Disconnected during message iteration' in str(e.exceptions[0]):
                        # this happens in listener() if connection is disconnected
                        debug(f"Saw disconnect during setup_mqtt_tasks: {e.exceptions[0]}")
                        traceback.print_exception(e.exceptions[0])
                        #debug(f"---###--- not rethrowing, recycling")
                        #traceback.print_exception(e)
                        return # i.e. retry
                    else:
                        debug(f"OTHER during setup {e.exceptions[0]}")

                debug(f"SAW B {e}")
                if self.exit_code == 0:
                    self.exit_code = 1
                await self.say_quit(e)
                traceback.print_exception(e)

            except Exception as e: # this shouldn't be able to happen
                await self.say_quit(e)
                traceback.print_exception(e)
                debug(f"SHOULDN'T HAPPEN {e}")

        retry_ct =-1 
        async for client,first_time in self.mqtt_connect_with_retry(**self.mqtt_config['connection'],will=will):
            retry_ct += 1
            debug(f"CONNECT/RETRY generator [{retry_ct}]first_time? {first_time}", level=(0 if (retry_ct % 10 == 0) else 2) )
            self.mqtt_client = client # "global" main/default mqtt
            await setup_mqtt_tasks(first_time)
            debug("retry mqtt_connect_with_retry",level=2)

    def read_mqtt_config(self):
        # the actual agent's .connection is optional
        #connection = {} or mqtt_local_config or self.mqtt_config.get('connection',{})
        # Connection is from `--mqtt-config $file` | config/mqtt-local-config.py | self.mqtt_config['connection']
        # fixme: maybe a --no-mqtt-config-file that you can set in code to always use self.mqtt_config['connection']
        connections = mqtt_config_reader.connection( self.parsed_args.mqtt_config )
        if not connections:
            connections = [ self.mqtt_config['connection'] ]
        if not connections:
            raise Exception(f"Expected {self.parsed_args.mqtt_config} (empty or not-found), OR self.mqtt_config['connection'] (empty)")
        self.mqtt_config['connection'] = connections[0] # main
        # "other" connection configs
        #debug(f"raw connections {connections}")
        self.mqtt_config['connections'] = {}
        for i,conn in enumerate(connections):
            if key:=conn.get('key',None):
                self.mqtt_config['connections'][key] = conn
                del conn['key']
            else:
                self.mqtt_config['connections'][i] = conn
        #debug(f"Connections {self.mqtt_config['connections']}")

    async def after_mqtt_connect(self, first_time):
        # do your thing
        pass

    def async_run(self):
        try:
            asyncio.run( self.run_wrapper() )
        except KeyboardInterrupt: # this shouldn't happen
            # For ^c
            #   An asyncio.exceptions.CancelledError appears in .run_wrapper() first, which should .cleanup(), then re-throw it
            #   then KeyboardInterrupt appears here

            # probably nobody handled this
            if self.exit_code == 0:
                self.exit_code = 1
            debug("^C")
            # and then we just run off the end of function
        except asyncio.exceptions.CancelledError as e:
            # CancelError should only be for .run() "main" (presumably because of our .exit),
            # CancelError should only be for .run() "main" (presumably because of our .exit),
            #   it does .cleanup and re-throws to us
            debug(f"## TOPLEVEL CANCEL")
            pass # expected for exit
            # and then we just run off the end of function
        except Exception as e:
            # all other exceptions crash us!
            debug(f"### Caught final {e}")
            raise e

        if self.exit_code:
            debug(f"Error exit {self.exit_code}")
        else:
            debug(f"# Normal exit")
        exit(self.exit_code) # sigh

    async def run_wrapper(self):
        # we should be the "main" task
        # wrapper around run() to implement the default exception handling

        # handle a "normal" sigterm: i.e. from systemd
        # the exception is for informative purposes only, not thrown
        # note exit-status=0
        # NB: This is the earliest we can setup the signal-handler (for asyncio), possible for sigterm to happen earlier
        # Which will cause 'Caught exception in on_socket_close: Event loop is closed', but still exit(0)
        #   Anytime after the first await, you won't see that warning.
        for sig in [signal.SIGTERM]:
            asyncio.get_event_loop().add_signal_handler(sig, lambda: asyncio.create_task(self.exit(0,Exception('SIGTERM'))))

        try:
            await self.run()
        except asyncio.exceptions.CancelledError as e:
            # no str(e). sigh
            # it's ok, someone (us.exit() presumably) cancelled for exit
            debug("SUPERVISER CANCELLED EXCEPTION")
            await self.cleanup(e)
            raise e
        except (KeyboardInterrupt, Exception) as e:
            # Sees raised exceptions from loop()
            debug(f"SUPERVISER RUN EXCEPTION {e}")
            await self.cleanup(e)
            raise e

    async def exit(self, exit_code, e=None):
        """Do an orderly exit().
        supply the exception if you have one
        """

        # FIXME: I don't think we are doing cancel in the right order
        # should let the wrapper_run do it all
        # otherwise, I think .exit gets called twice:
        # once for whatever coroutine sees the error, 
        # and then we cancel wrapper_run, which calls this again

        self.exit_code=exit_code # early, can serve as a flag

        # print the error and stack
        traceback.print_exception(e)
        if isinstance(e,ExceptionGroup):
            if "unhandled errors in a TaskGroup" in str(e):
                # pick one
                e = e.exceptions[0]

        # have to cleanup now, because .cancel() will close mqtt
        # and presumably break other cleanup stuff that depends on some state
        await self.cleanup(e)

        # FIXME: probably wait (like with .gather) on all the tasks we cancel. I think that's safe. and a should-do

        #debug(f"root tasks {[x._coro for x in self.root_tasks]}")
        debug(f"Cancel .all_tasks except self & root...")
        for i,t in enumerate(asyncio.all_tasks()):
            #debug(f"t[{i}] {t._coro} {t in self.root_tasks}")
            if t == asyncio.current_task() or t in self.root_tasks:
                # we leave the root_tasks alone (and ourselves!)
                debug(f"  cancel later: {t._coro}")
                pass
            elif t.done() or t.cancelled() or t.cancelling() != 0:
                debug(f"Done somehow {t._coro} done {t.done()} canc {t.cancelled()} canc>0 {t.cancelling()}")
            else:
                # debug(f"Cancel {t._coro}...")
                t.cancel()
        
        # Canceling wrapper_run triggers:
        #   wrapper_run exception CancelError
        #   which does .cleanup()
        #   which re-throws
        #   to async_run exception
        #   which exits
        #   

        # we shouldn't have to cancel self, because we are doing exit anyway?
        #if not self.root_tasks:
            #debug(f"Cancel self {t._coro}...")
            #asyncio.current_task().cancel()

        # I think it gets hung here sometimes, like someone is not done
        await asyncio.sleep(0.001)
        debug(f"Should be remaining tasks only root: {[x for x in asyncio.all_tasks() if not x.done() and not x.cancelled() and x.cancelling==0]}")
        debug(f"Would expect the root task(s) to finish now {self.root_tasks}")
        exit(self.exit_code)

if __name__ == '__main__':
    import re
    for line in __doc__.split("\n"):
        line = re.sub('^    ','',line)
        print(line)
