#!/usr/bin/env python3.11
# try --help
# see AIOStatusUpdater as main class

import asyncio
import sys,os,json,re,datetime,typing,traceback
from pathlib import Path
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib-pump-pi" )
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib" )
from simple_debug import debug

module_dir = re.sub(r'\.py$','', __file__)

class RateLimiter:
    """Track `max` events per `secs`. Sliding window

    Usage
        # for simple case, just set to the max
        # for giving priority to some things, set to `max - n` 
        #   where `n` is max high-priority things
        #   and use `.allowed(extra=...)`
        #   obviously, this reduces "max" per window for non-priority events
        MaxEventsPerWindow = 10
        WindowLengthSeconds = 10
        limiter = RateLimiter( MaxEventsPerWindow, WindowLengthSeconds )
        ...
        while(...)
            if self.ratelimiter.allowed():
                not rate limited: can do it
            
            # as if MaxEventsPerWindow where +2:
            if self.ratelimiter.allowed(extra=2):
                not rate limited: can do it
                
    """
    def __init__(self, max, secs):
        self._calls = []
        self.max=max
        self.secs = secs

    def status(self):
        # sofar / max
        return (len(self._calls), self.max)

    def allowed(self, extra=0):
        now = datetime.datetime.now()
        oldest = now - datetime.timedelta(seconds=self.secs)
        # remove aged-out
        drop = -1 # drop up to this
        for i,v in enumerate(self._calls):
            #debug(f"  [{i}] = {now-v} {v<oldest}")
            if v < oldest:
                drop = i
            if v > oldest:
                break
        if drop>=0:
            #debug(f"Drop [..{drop}], have {len(self._calls)} oldest {now - self._calls[0]}")
            del self._calls[0:drop+1]
        if len(self._calls) >= self.max + extra:
            #debug(f"Skip, have {len(self._calls)} oldest {now - self._calls[0]}")
            return False
        else:
            #debug(f"Add, have {len(self._calls)} oldest {now - self._calls[0] if self._calls else 'n/a'}")
            self._calls.append( now )
            return True

class ForwardSupportMixin:
    """For the `forward` dsl and behavior
        Registers the forwarding, 
    """
    def __init__(self, *args, **kwargs):
        self.by_module = {} # module-path : [ list of topics it is forwarding ]
        self.ratelimiter = None # add one if you need it
        super().__init__(*args, **kwargs)

    def setup_forward(self, in_topic, predicate, out_topic, transform, priority=False ):
        # We accumulate the forwardings because we want to be able to say them at --topics time
        # without having already subscribed
        #
        # called by the module: local_forward( ... )
        # handled by _forwarding() w/same args
        debug(f"Setup {self.__class__.__name__} {self._forwarding_via}")
        debug(f"  {in_topic} -> {out_topic}")

        # by module so we can un-load by module
        if not self._forwarding_via  in self.by_module.keys():
            self.by_module[ self._forwarding_via ] = []
        self.by_module[ self._forwarding_via ].append( 
            {
            'in_topic':in_topic, 'predicate' : predicate,
            'out_topic':out_topic, 'transform' : transform,
            'priority':priority
        } )

    def interesting_forwardings(self):
        forwardings = []
        for inout_list in self.by_module.values():
            for inout in inout_list:
                forwardings.append( f"{inout['in_topic']} -> {inout['out_topic']}" )
        return forwardings

    def fixup_forward_topic(self, topic):
        if next((x for x in (x for x in self.__class__.__mro__ if x != self.__class__) if '.fixup_forward_topic' in dir(x)),None):
            return super().fixup_forward_topic(topic)
        else:
            return topic

    async def subscribe_forwarders(self, taskgroup):
        def xxsubscribe(tg, inout):
            # aaaarg, closure over the arguments because no block scope in python
            tg.create_task( self._subscribe( self.fixup_forward_topic(inout['in_topic']), lambda message: self._forwarding(message, **inout ) ) )

        for inout_list in self.by_module.values():
            for inout in inout_list:
                debug(f"Subscribe: {inout['in_topic']} {inout}")
                xxsubscribe(taskgroup, inout)

    async def _forwarding(self, message, in_topic, predicate, out_topic, transform, priority=False):
        # handles messages that might be forwarded
        # module local_forward(...) -> self.setup_forward -> loop -> foreach self.by_module -> subscribe w/lambda

        # we don't want old messages, just current
        if message.retain:
            return

        debug(f"# IN {message}, pred {predicate} priority {priority}")
        #debug(f"  out: {out_topic} / trans {transform}")
        if self.ratelimiter and not self.ratelimiter.allowed(extra = 5 if priority else 0):
            debug(f"Rate Limit")
            #debug(f"   for {message.topic}")
            return

        def catching( fn, fail_prefix, *args, **kwargs ):
            # pattern for try/fail report
            # returns the value or Throws on failure
            try:
                return fn( *args, **kwargs )
            except Exception as e:
                # just give up on errors
                # Should notify someone so we'll clean it up later
                print("During out_topic transform")
                traceback.print_exception(e)
                debug("# Not Forwarded!")
                self.queue_a_message( self.topic_for_event("errors"), ["During out_topic transform"] + traceback.format_exception(e), retain=True )
                raise Exception("--PLUGIN FAILED--")

        try: # for the catching() patterns

            # FIXME: catch errors from predicate() and transform(). print them, mqtt them, and unsubscribe the topic 
            pred = catching( predicate, 'During predicate', message )

            if not pred:
                debug(f"  but doesn't match predicate: {message.payload}")
            else:
                debug(f"  inpayload {message.payload}")

                # Transorm topic
                if isinstance(out_topic,typing.Callable):
                    out_topic = catching( out_topic, 'During out_topic transform', message )

                # Transform the payload
                new_payload = catching( transform, 'During payload transform', message, message.payload )

                #debug(f"  match predicate! transformed {new_payload}")
                #debug(f"  send {self.other_side.mqtt_config['connection']['hostname']}: {self.other_side.fixup_forward_topic(out_topic)}")
                if os.getenv('NOAIO',None):
                    debug(f"## PUBLISH {datetime.datetime.now()} {self.other_side.fixup_forward_topic(out_topic)} {json.dumps( new_payload )}")
                else:
                    try:
                        await self.other_side.mqtt_client.publish( self.other_side.fixup_forward_topic(out_topic), json.dumps( new_payload ), retain=False, qos=0 )
                    except mqtt.error.MqttCodeError as e:
                        if 'The client is not currently connected' in str(e):
                            await asyncio.sleep(5) # time to fix wifi?
                            await self.reconnect()

        except Exception as e:
            if str(e).startswith("--PLUGIN FAILED--"):
                return # was skipped
            else:
                raise e
