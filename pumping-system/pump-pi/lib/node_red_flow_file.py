# class to do things with the flow-file

import json,subprocess
import os,sys
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib-pump-pi" )
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib" )
from simple_debug import debug


class NodeRedFlowFile:
    """
    Deduces a flow.json if you don't specify
    """

    Fd_type_short = {
        # fd/cli : [ type:, kind: ]
        None : [ None,None],
        'tab': ['flexdash tab', None ],
        'panel': ['flexdash container', 'Panel'],
        'grid': ['flexdash container', 'StdGrid'],

        # regular node-red
        'group': ['group', None],
        }
    Fd_container_types = [ x[0] for x in Fd_type_short.values() if x[0] != None ]

    def __init__(self, project_dir='.node-red/projects', flow_file=None):
        self.project_dir = project_dir

        # Use the () for reading, the _ for replacing, so caching/regen works
        # dependencies, regen if you change
        # _flow <- _kv, _parents # flow(regen=True) will regen both

        self._flow_file = flow_file
        self._flow = None
        self._kv = None # id -> node
        self._parents = None # childid -> parent

    def flow_file(self):
        if self._flow_file == None:
            rez = subprocess.run( ['find',self.project_dir,'-name','flow.json'], capture_output=True )
            if rez.returncode != 0:
                exit(rez.returncode)
            possibles = [ x for x in rez.stdout.decode('utf8').split('\n') if x != None and x!='' ]
            if len(possibles) > 1:
                sys.stdout.write("# Choose one:\n\t" + '\n\t'.join(possibles) + "\n")
            if len(possibles) == 0:
                sys.stdout.write(f"# No flow.json in {nr_project_dir}\n")
            self._flow_file = possibles[0]
        return self._flow_file
    
    def flow(self, regen=False):
        # the decoded json
        # regen to remake _kv, _parents, etc
        if self._flow == None:
            with open(self.flow_file(), 'r') as fh:
                self._flow = json.load(fh)
        if regen or self._kv == None or self._parents == None:
            self._kv = {}
            self._parents = {}
            for node in self._flow:
                self._kv[ node['id'] ] = node
                if children:=node.get('fd_children'):
                    if isinstance(children,str):
                        node['fd_children'] = children.split(',')[1:] # leading ,
                    for c in node['fd_children']:
                        self._parents[c] = node
                else:
                    node['fd_children'] = []
            

        return self._flow


    def add_node(self, node, flow):
        #node['z'] = flow['id'] # config nodes do not have a z
        self.flow().append(node)
        self.flow(regen=True)
        #self._kv[ node['id'] ] = node

    def delete_by_name(self, name, type=None, kind=None, fd_type=None ):
        found = self.find_node( name, type, kind, fd_type )
        self.delete( [ x['id'] for x in found ] )

    def delete(self, nodes ):
        # can be actual nodes or ids
        node_ids = [ x if isinstance(x,str) else x['id'] for x in nodes ]

        for tbd_id in node_ids:
            nr_node =  self._kv[tbd_id]

            self.unparent(nr_node)

        self._flow = [ x for x in self._flow if x['id'] not in node_ids ]
        self.flow(regen=True)
        
    def set_group(self, node, group):
        if group==None:
            return # nothing to do
        if not 'x' in node.keys():
            return # non-canvas nodes don't go in a group

        if group['type']!='group':
            raise Exception(f"Expected a type='group', saw {group}")

        group['nodes'].append( node['id'] )
        node['g'] = group['id']

    def set_fd_parent(self, child, parent):
        # add flexdash parent
        #debug(f"Add child {child['id']} to {parent}")

        if parent==None:
            raise Exception(f"Expected a parent, saw {parent}")

        if 'fd_children' not in parent.keys():
            raise Exception(f"Can't set fd parent for a non-fd-widget parent {parent}")

        parent['fd_children'] += [ child['id'] ]

        if not 'kind' in child.keys() and not child['type'].startswith('fd-'):
            raise Exception(f"Can't set fd parent for a non-fd-widget child {child}")
        if parent['type'] not in self.Fd_container_types:
            raise Exception(f"A {child['type']} requires a parent 'flexdash container', saw {parent}")

        if child['type'] == 'flexdash container' and child['kind'] == 'StdGrid':
            if parent['type'] != 'flexdash tab':
                fail(f"A grid requires a parent 'tab', saw {parent}")
            child['tab'] = parent['id']

        elif child['type'] in self.Fd_container_types:
            child['parent'] = parent['id']

        else:
            child['fd_container'] = parent['id']

        self._parents[ child['id'] ] = parent

        #debug(f"Fixup parent {parent['id']} in child {child}")

    def add_to_parent(self, child, parent):
        # not for tabs...
        self.unparent(child)

        if 'fd_children' in parent.keys():
            self.set_fd_parent(child,parent)
        else:
           self.set_group( child, parent )

    def remove_from_parent(self, nr_node, child_key, parent_key):
        # helper, for a child/parent key correspondance
        if child_key in nr_node.keys() and nr_node[child_key]:
            # not if parent = '',[],None
            parent = self._kv[ nr_node[child_key] ]
            if parent:
                #debug(f"Had parent (child:{nr_node}) -> {parent} in it's [{parent_key}]")
                parent[parent_key].remove( nr_node['id'] )
            else:
                # FAIL? should have a parent, but maybe not linked yet
                pass

    def unparent(self, child):
        # ensure no parent
        self.remove_from_parent( child, 'tab', 'fd_children' ) # StdGrid
        self.remove_from_parent( child, 'parent', 'fd_children' ) # Panel, fd-widgets
        #self.remove_from_parent( child, 'g', 'nodes' ) # group

    def find_node(self, name, type=None, kind=None, fd_type=None):
        # returns [] or a list
        if fd_type:
            type,kind = self.Fd_type_short[fd_type]

        found = []
        for node in self.flow():
            #debug(f"# {name} ?= {node.get('name',None)}")
            matches = next( (node for x in ['name','label','title','id'] if node.get(x,'') == name), None )
            if (matches
                and (type == None or (type == node['type'] and (kind==None or kind==node['kind'])))
                ):
                found.append(node)
        return found

    def wire(self, *portnum_from_to):
        # portnum can be skipped == 0
        # from and to can be: a node, an id, a name:type|kind

        if len(portnum_from_to) == 2:
            portnum = 0
            from_node, to_node = portnum_from_to
        elif len(portnum_from_to) == 3:
            portnum, from_node, to_node = portnum_from_to
        else:
            raise Exception("Expected 2 or 3 arguments for wire([portnum,] from, to), saw {portnum_from_to}")

        def resolve_node(x, what):
            if isinstance(x,str):
                found = self.find_node( x )
                if len(found)!=1:
                    raise Exception(f"Expected exactly 1 `{what}` (~='{x}'), found {found}")
                return found[0]
            elif isinstance(x,dict):
                return x
            else:
                raise Exception(f"Expected {what} to be a str (find) or a dict (node), saw {x}")
       
        from_node = resolve_node(from_node,'from')
        to_node = resolve_node(to_node,'to')

        if not 'wires' in from_node.keys():
            from_node['wires'] = []
        if len( from_node['wires']) - 1 < portnum :
            from_node['wires'][portnum] = []
        from_node['wires'][portnum].append( to_node['id'] )

    def print_node(self, node, indent=0):
        moniker = next( (node[x] for x in ['name','label','title','id'] if node.get(x,'') != '' ))
        if moniker == '':
            moniker = node['id']
        print(f"{'  ' * indent}{node.get('kind', node['type'])} '{moniker}'")

    def write(self, filename=None):
        with open( filename or self.flow_file(), 'w') as fh:

            # put fd_children back
            for node in self.flow():
                fd_children = node.get('fd_children',None)
                if fd_children == None:
                    pass
                elif node['fd_children']:
                    node['fd_children'] = ',' + ','.join( node['fd_children'] )
                else:
                    node.pop('fd_children')

            fh.write( json.dumps(self.flow(), indent=4, sort_keys=True) )
            fh.write("\n")
        debug(f"# Wrote {filename}")
