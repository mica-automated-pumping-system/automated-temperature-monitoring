#!/usr/bin/env python3.11

"""
Conversions and utilities for the AIO parameters. json, csv, gdrive, aio, etc.
Should have only explicit a->b conversions
To chain conversions, e.g. gdrive->aio, call convert(from,to), which returns false if not possible.
To get a chain, use chain_of_conversions(from,to).
To get the the chain of all conversions, use chain_of_conversions().

fixme: incorporate aio-feeds-analyze, aio-gets, aio-upload-repairs?

See tools/aio-param-conversion for some more info on the to/from options and formats.

"""

import typing,re,json,pathlib,time,subprocess
from datetime import datetime
from copy import copy
import os,sys
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib-pump-pi" )
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib" )
from simple_debug import debug

import pyexcel_ods3 as ods

# protocol: 
# conversion between formats is always done by a convert_$fromfmt_$tofmt()
# and it's __doc__ has more comments

def fail(msg):
    sys.stderr.write(msg)
    sys.stderr.write("\n")
    exit(1)

def read_aio_data(from_file):
    # from JSON aio file
    with open(from_file,'r') as fh:
        aio_params = json.load(fh)
    if not aio_params:
        fail(f"# Expected some data in the aio json: {from_file}")

    # feed-info format
    if isinstance(aio_params,list):
        new_data = {} # i : { param: v } ... 
        for afeed in aio_params:
            if m:=re.match(r'p(\d+)\.p\d+-(.+)', afeed['key']):
                # (skip non pn.pn-, which also skips groups)

                group = int( m.group(1) ) - 1 # pump_i style
                key = m.group(2)
                value = afeed['last_value']
                if not group in new_data:
                    new_data[group] = {}
                new_data[group][key] = value
        aio_params = new_data

    # aio_json format:
    elif isinstance(aio_params,dict):
        if len(aio_params)>6:
            fail(f"# Expected at most 6 pumps in the aio json, saw {len(aio_params)} in {from_file}")

        # cleanup top-level key -> int
        for k in list(aio_params.keys()):
            # json keys must be strings
            # strip leading 'p' if any
            if m:=re.match(r'p?(\d+)',k):
                new_k = int( m.group(1) ) - 1
            else:
                fail(f"# Expected all keys to be 'pn' or 'n', saw {k} in {from_file}")
            
            new_data = aio_params.pop(k)
            aio_params[new_k] = new_data

            # fixup prefixes like p6.p6-
            for pname in list( new_data.keys() ):
                new_pname = re.sub(r'p\d+\.p\d+-','',pname)
                new_data[ new_pname ] = new_data.pop(pname)

    else:
        raise Exception(f"Expected a dict (pn-json format, or list-feed-info format: saw {aio_params}")

    debug(f"AIO pump count {len(aio_params)}")
    #debug(aio_params); exit(1)
    return aio_params

def convert_json_py(from_file, to_file, parsedargs):
    """Convert an aio.json (or GC.json) to aio-*-recipe.py (e.g. .gdrive -> *.ods -> *.json -> aio-recipe/aio-*-recipe.py)
    (for recipe/dsl.py).
    See ods->json for some info.
    """
    import aio_to_recipe
    aio_to_recipe.to_recipe(from_file,to_file)

def convert_json_csv(from_file, to_file, parsedargs):
    """Convert an aio.json (or GC.json) to "ryan csv" format (e.g. .gdrive -> *.ods -> *.json -> *.csv)
    See ods->json for some info.
    """
    import aio_to_recipe
    aio_to_recipe.to_csv(from_file,to_file)

def convert_json_ods(from_file, to_file, parsedargs):
    """Convert a pump-settings .json to .ods (cf. .aio -> .json -> .ods -> edit -> .aio/.gdrive)
    The .json is just the whole AIO feed set as json.
        (or from a GC saved settings file, top-level keynames are slightly different)
    Uses `--ods-template aio-json/aio-params-template.ods` by default:
    Sheets[1+] in the template .ods are un-interpreted, just copied
    Sheet[0]:
        Expected to be the "template". see aio-json/aio-params-template.ods.
        Copied from the template .ods and renamed to $oldname-$timestamp.
            I.e. we overwrite the values in the 1st tab.
        Where the params will go: 1 row for each param, 6 columns for pumps.
        We assume cols A & B are human-friendly labels/info. Copied.
        NB:
        Col C must be the aio param name, with the pump-number prefix removed,
            ie. p6.p6-pv-clean -> pv-clean
            We copy params from the json to the ods if there is a param-name.
        Col D..I are the values for pumps 1..6
        Cols J+ are copied.
    The python ods library doesn't seem to preserve formatting. bummer.
    """

    template = parsedargs.ods_template or 'aio-json/aio-params-template.ods'
    if not template:
        fail("# required: --ods-template something.ods\n")

    if parsedargs.noop:
        sys.stderr.write(f"# read {from_file} ... copy {template} ... write {to_file}\n")
        return

    if not os.path.isfile(from_file):
        fail(f"# Must exist (in-file): {from_file}")
    if not os.path.isfile(template):
        fail(f"# Must exist: -ods-template {template}")

    # the json data
    aio_params = read_aio_data(from_file)

    # Copy template
    workbook = ods.get_data( template )    
    if not workbook:
        fail(f"# Expected the template .ods to have some sheets in it: {template}")

    debug(f"Sheets {list(workbook.keys())}")

    # using sheet[0]
    sheetname, sheetdata = tuple(workbook.items())[0]
    debug(f"Sheet[0] = '{sheetname}'")
    if not sheetdata:
        fail(f"# Expected the template.ods[sheet0] to have some data: {template}['{sheetname}']")

    # name + timestamp
    # trying to use creation time rather than mod-time (hopefully more like when it was fetched rather than edited)
    new_name = sheetname + "-" + datetime.fromtimestamp(pathlib.Path(from_file).stat().st_ctime).strftime('%Y-%m-%d_%H%M')
    workbook[ new_name ] = workbook.pop( sheetname )
    debug(f"New sheet name: {new_name}")

    # cols A & B are uninterpreted/preserved
    # col C is the param name
    #     * so match
    #     * rows @ D..I are pump-n's values
    #         * only for C==$param,
    #     other rows are uninterpreted/preserved

    colD = ord('D') - ord('A')
    debug(f"Updating col[{colD}...]")
    update_ct = 0
    for ss_row in sheetdata: 
        aio_param_name = (ss_row[2:3] or [None])[0] # 'C'
        if not aio_param_name or aio_param_name=='':
            continue
        #debug(f"AIOP {aio_param_name}")

        for pump_i, params in aio_params.items():
            if aio_param_name in params.keys():
                pvalue = params[ aio_param_name ] 
                ss_row[ colD + pump_i ] = pvalue if pvalue != None else ''
                update_ct += 1
    debug(f"Updated {len(aio_params)} pumps, {update_ct} values in spreadsheet for {update_ct/len(aio_params)}/pump")

    # write data
    ods.save_data( to_file, workbook )
    debug(f"Wrote {to_file}")

def convert_gdrive_ods(from_file, to_file, parsedargs):
    """Download google-drive sheets to .ods:
    .gdrive $something.ods # downloads the `new-aio-settings` from the `config/google-drive-upload-folder.url`.
    $something.gdrive $somethingelse.ods # downloads `$something` from the `config/google-drive-upload-folder.url`.
            Which should be a google-sheet.
    """

    with open('config/google-drive-upload-folder.url','r') as fh:
        path = fh.read()
    path = path.rstrip()
    cmd = ['tools/gdrive', 'download', 'new-aio-settings', '--path', path, to_file]
    print("'" + "' '".join(cmd) + "'")
    if not parsedargs.noop:
        proc = subprocess.run(cmd)
        if proc.returncode != 0:
            fail(f"FAIL: {cmd}")

def convert_json_aio(from_file, to_file, parsedargs):
    """Send the .json pump settings to AIO.
    Ignores any actual filename part of the `to`.
    json as per aio-json/aio-feed-info.json (e.g. as fetched by --- .aio aio-json/aio-feed-info.json).
    Only sends changed values (updating aio-json/aio-feed-info.json), 
    rate limited.
    See more info in the .aio -> .json converter help.
    Try -n to see changes without sending.
    User account as specified in config/aio.key"""

    cmd = ['tools/aio-params', 'repair', from_file ]
    if parsedargs.noop:
        cmd.append('-n')
    print("'" + "' '".join(cmd) + "'")
    proc = subprocess.run(cmd)
    if proc.returncode != 0:
        fail(f"FAIL: {cmd}")

def convert_ods_json(from_file, to_file, parsedargs):
    """The json-template is from AIO via lib/aio_to_recipe.py. Really just the whole feed set as json: pump_n format.
        (or from a GC saved settings file, top-level keynames are slightly different)
    Uses a `--json-template x.json`:
    Sheets[1+] are un-interpreted/copied
    Sheet[0]:
        Where the params will go: 1 row for each param, 6 columns for pumps.
        We assume cols A & B are human-friendly labels/info. Ignored.
        Col C must be the aio param name, with the pump-number prefix removed,
            ie. p6.p6-pv-clean -> pv-clean
            We copy params to the json if there is a param in the json-template
        Col D..I are the values for pumps 1..6
    """

    template = parsedargs.json_template or 'aio-json/aio-feed-info.json'
    if not template:
        fail("# required: --json-template something.json\n")

    if parsedargs.noop:
        sys.stderr.write(f"# read {from_file} ... use {template} ... write {to_file}\n")
        return

    if not os.path.isfile(from_file):
        fail(f"# Must exist (in-file): {from_file}")
    if not os.path.isfile(template):
        fail(f"# Must exist: --json-template {template}")

    # the json template-data
    all_aio_params = read_aio_data(template)
    #debug(f"all {all_aio_params}")

    # ready to read workbook sheet[0]
    workbook = ods.get_data( from_file )    
    if not workbook:
        fail(f"# Expected the template .ods to have some sheets in it: {from_file}")

    debug(f"Sheets {list(workbook.keys())}")

    # using sheet[0]
    sheetname, sheetdata = tuple(workbook.items())[0]
    debug(f"Sheet[0] = '{sheetname}'")
    if not sheetdata:
        fail(f"# Expected the template.ods[sheet0] to have some data: {from_file}['{sheetname}']")

    # cols A & B are uninterpreted/preserved
    # col C is the param name
    #     * so match
    #     * rows @ D..I are pump-n's values
    #         * only for C==$param,
    #     other rows are uninterpreted/preserved

    # sadly, we need some special cases:
    #   (and pc-* are ints below)
    on_offs = ['op-clean', 'op-mix', 'op-fill', 'cf-active']
    str_values = ['cf-type']

    debug(f"Updating new aio params...]")
    update_ct = 0
    new_aio = {}
    colD = ord('D') - ord('A')
    for ss_row in sheetdata: 
        aio_param_name = (ss_row[2:3] or [None])[0] # 'C'
        if not aio_param_name or aio_param_name=='':
            continue
        #debug(f"AIOP {aio_param_name}")

        for pump_i, pvalue in enumerate(ss_row[ colD: colD+6 ]):
            #debug(f"  consider {aio_param_name}[{pump_i}] {pvalue} ? { aio_param_name in all_aio_params[pump_i] }")
            pump_n = pump_i+1
            if len(new_aio) <= pump_i:
                new_aio[pump_n] = {}
            if len(all_aio_params)-1 < pump_i:
                fail(f"Expected [{pump_i}] in template, max is {len(all_aio_params)-1}")
            if aio_param_name in all_aio_params[pump_i].keys():
                if aio_param_name in on_offs:
                    pvalue = 'ON' if pvalue=='ON' or pvalue==True or pvalue==1 else 'OFF'
                elif aio_param_name in str_values:
                    pass # is what it is
                elif isinstance(pvalue,float) or isinstance(pvalue,int) or re.match(r'\d+(\.\d+)?',pvalue):
                    pvalue = float(pvalue)
                if aio_param_name.startswith('pc-') or aio_param_name=='cf-copy-from':
                    pvalue = int(pvalue or 0)
                # might be hackish to assume 0 for None/''
                new_aio[pump_n][aio_param_name] = pvalue if pvalue != '' else 0
                update_ct += 1
    debug(f"Updated {len(new_aio)} pumps, {update_ct} params for {update_ct/len(new_aio)}/pump")

    # write data
    write_aio_data( to_file, new_aio )

    debug(f"Wrote {to_file}")

def convert_aio_json(from_file, to_file, parsedargs):
    """Download AIO params 
    Ignores any actual filename part of the `from`.
    Makes pn.pn-$key .json (e.g. .io-json/aio-feed-info.json)."""

    ss_config = 'config/agents/serial-stepper.json'
    if os.path.exists(ss_config):
        with open(ss_config, 'r') as fh:
            data = json.load(fh)
    else:
        debug(F"Warning: no config file, defaulting to 'pump_ct' : 1: {ss_config}")
        pump_ct=1
    pump_ct = data.get('pump_ct',None)
    if pump_ct == None:
        debug(F"Warning: using default 'pump_ct' : 1: {ss_config}")
        pump_ct = 1
    debug(F"pump_ct {pump_ct}")

    cmd = [ 'tools/aio-fetch', str(pump_ct), to_file ]
    print("'" + "' '".join(cmd) + "'")
    if not parsedargs.noop:
        proc = subprocess.run(cmd)
        if proc.returncode != 0:
            fail(f"FAIL: {cmd}")

def convert_ods_gdrive(from_file, to_file, parsedargs):
    """Upload .ods to google-drive sheets:
    $something.ods .gdrive # uploads to `new-aio-settings` in the `config/google-drive-upload-folder.url`.
    $somethingelse.ods $something.gdrive # uploads `$something.ods` to the `config/google-drive-upload-folder.url`.
            Converts to native google-docs sheets."""
    with open('config/google-drive-upload-folder.url','r') as fh:
        path = fh.read()
    path = path.rstrip()
    cmd = ['tools/gdrive', 'upload', from_file, path, '--new-name', to_file ]
    print("'" + "' '".join(cmd) + "'")
    if not parsedargs.noop:
        proc = subprocess.run(cmd)
        if proc.returncode != 0:
            fail(f"FAIL: {cmd}")

def write_aio_data( to_file, new_aio ):
    # fixup, put the pn.pn- prefix back
    for pump_n in list(new_aio.keys()):
        pump_data = new_aio[ pump_n ]

        # rename pump-n
        if isinstance(pump_n,int) or isinstance(pump_n,str) and re.match(r'\d+$'):
            new_pump_n = f'p{pump_n}'
            new_aio[ new_pump_n ] = new_aio.pop(pump_n)
            pump_n = new_pump_n

        # rename all the params
        for k in list( pump_data.keys() ):
            if not re.match(f'p\d+-', k):
                pump_data[ f'{pump_n}.{pump_n}-{k}' ] = pump_data.pop(k)
            
    with open(to_file,'w') as fh:
        fh.write( json.dumps(new_aio, indent=2, sort_keys=True) )
        fh.write("\n")

def visit_tree( subtrees=None, xseen=set(), depth=0 ):
    # NOT CURRENTLY USED
    # traverse
    # Call with no args to start from root.
    # each recurese: give us the list of "forests" [ { 'type' : name, 'tree' : [...} } ... ]
    # yields depth, type, has-been-seen-on-this-branch(cycle/end of branch)
    #print(f"{'  '*depth}[{depth}] ########### forest {subtrees}")
    #print(f"{'  '*depth}[{depth}] seen {xseen}")

    if subtrees == None:
        yield from visit_tree( tree.values() ) 
        return

    for entry in sorted( subtrees, key=lambda st: st['type'] ):
        print(f"{'  '*depth}Consider { entry['type'] } : {entry}")
        if entry['type'] in xseen:
            #print(f"{'  '*(depth+1)}seen")
            yield depth, entry['type'], True
            continue
        else:
            yield depth, entry['type'], False
            #print(f"{'  '*(depth+1)}recurse { entry['tree'] }")
            newseen = copy(xseen)
            newseen.add( entry['type'] )
            yield from visit_tree( entry['tree'], newseen, depth+1 )
            #print(f"{'  '*(depth+1)} <donerec>")

def build_chains(trees, depth=0, seen={}):
    chains = []
    for tree in trees:
        fromext = tree['type']
        if fromext in seen.keys():
            continue
        seen[ fromext ] = True
        sub_chains = build_chains( tree['tree'], depth+1, copy(seen) )
        del seen[ fromext ]
        #debug(f"{'  ' * depth} visit {fromext} subchains {sub_chains}")
        if not sub_chains:
            chains.append([fromext] )
        else:
            for a_sub_chain in sub_chains:
                chains.append( [ fromext, *a_sub_chain] )
    return chains

converters = {}
for converter_n,converter_f in [x for x in globals().items() if isinstance(x[1],typing.Callable) and x[0].startswith('convert_')]:
   from_fmt,to_fmt = re.search(r'convert_([^_]+)_([^_]+)', converter_n).groups()
   converters[ converter_n ] = { 'conversion': f".{from_fmt} -> .{to_fmt}", 'fn':converter_f, 'from':from_fmt, 'to':to_fmt, 'doc' : converter_f.__doc__ }

# build a->b/tree and chains
tree={} # from : { 'type' : name, 'tree' : [tree-entries], # so tree['gdrive'] = { 'type':'gdrive', 'tree' [ the to's ] }
for c in converters.values():
    if c['from'] not in tree:
        tree[ c['from'] ] = { 'type' : c['from'], 'tree':[] }
    if c['to'] not in tree:
        tree[ c['to'] ] = { 'type' : c['to'], 'tree':[] }
    tree[ c['from'] ]['tree'].append( tree[ c['to'] ] )
#debug(f"Tree {tree['json']}")
chains = build_chains(tree.values())
#debug(f"Chains {chains}")

def convert(fromfile, tofile, parsedargs):
    # uses convert_x_x(), chaining if necessary
    # -n should inhibit any actual changes

    fromext = re.search(r'\.([^.]+)$', fromfile)
    toext = re.search(r'\.([^.]+)$', tofile)

    if not fromext or not toext:
        fail(f"Expected from & to have extensions, saw from: {fromfile}, to: {tofile}")

    fromext = fromext.group(1)
    toext = toext.group(1)
    #print(f"from '{fromext}' to '{toext}'")

    found_chains = find_chains(fromext, toext)
    
    found_chains = sorted( found_chains, key=lambda c: len(c) )
    if True or parsedargs.show_chain:
        debug(f"# Chains (will use first):")
        for c in found_chains:
            debug(" "," ".join(c) )
    found = found_chains[0]

    step_from = fromfile
    tempfile = f"aio-json/temp-{datetime.now().isoformat()}"
    for i,type_from in enumerate(found[0:len(found)-1]):
        type_to = found[i+1]
        converter_name = f"convert_{type_from}_{type_to}"
        #print(f"[{i}] {converter_name}")
        converter = getattr(sys.modules[__name__], converter_name, None)
        if not converter:
            raise Exception(f"No method for {type_from}->{type_to}: {converter_name}")
        step_to = tofile if i==len(found)-2 else f"{tempfile}.{type_to}"
        if parsedargs.show_chain:
            print( f"[{i+1}] {converter_name}( {step_from}, {step_to} )" )
        converter( step_from, step_to, parsedargs )
        step_from = step_to

def find_chains(fromext, toext):
    #debug(f"Find for {fromext}->{toext}")
    #return [ chain for chain in chains if chain[0]==fromext and chain[-1]==toext ]
    matching_chains = []
    for chain in chains:
        if chain[0] != fromext or toext not in chain:
            continue

        # we know it's in there
        found_chain = []
        for x in chain:
            found_chain.append(x)
            if x == toext:
                break
        matching_chains.append( copy(found_chain) )
    #debug(f"Found {matching_chains}")
    return matching_chains

