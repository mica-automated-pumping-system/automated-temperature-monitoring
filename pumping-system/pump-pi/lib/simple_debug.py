"""
Provides debug(msg, ddata=None, level=0)
    prints msg + ddata, returning ddata
    IFF level>= env DEBUG
env DEBUG=0...n --- # larger n == larger verbosity: debug("something",level=2)
env DEBUGPID=1 --- # include process-id
env DEBUGPID=something --- # include `something` as prefix
"""

import inspect,sys,os
from datetime import datetime

def debug(msg,ddata=None,up=0,level=0):
    # if lambda...
   
    limit_level = int(os.getenv('DEBUG','0'))
    if level <= limit_level:
        frame = inspect.currentframe()
        for i in range(0,up+1):
            poss_frame = frame.f_back
            if poss_frame: # prevent too far
                frame = poss_frame

        frame_code = frame.f_code
        fname = frame_code.co_filename
        command_dir = os.path.dirname( os.path.realpath(sys.argv[0]) )
        fname = os.path.relpath( fname, command_dir)
        show_pid = os.getenv('DEBUGPID',None)
        pid = '' if show_pid==None or show_pid=='0' else (f"{os.getpid():6d} " if show_pid=='1' else f" {show_pid:6s}")
        sys.stderr.write( f"[{datetime.now().isoformat(timespec='seconds')}{pid} {fname}:{frame.f_lineno}] {msg} {ddata if ddata!=None else ''}\n" )
    return ddata
