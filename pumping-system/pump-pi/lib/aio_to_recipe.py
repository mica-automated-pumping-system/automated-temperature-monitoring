#!/usr/bin/env python3.11
# using an "aio" .json, generate a recipe from it. recipe/dsl.py runs it.
# Use tools/aio-param-conversion .gdrive|*.ods aio-json/*.json
# see --- --help

"""
Converted from GC/lib/pump/pump.py

SEE tools/aio-param-conversion for various conversion from gdrive/aio.

The "aio" ("pn format") json is a "recipe" for a pump run:
* Defines cycles/sequences of Fill, Mix, Clean.

Given a set of pumps, p1..p6 (note 1-based).

There is a "group" for each pump, which is 0-based:
* {"0" { k:data, ... } }
* each "k" is like "p1.p1-someop-some-key"
** NB: "pn" is 1 based!
* Note the repeats of "pn" for the `k`. We want to treat the key as just `someop-some-key`

There are fairly consistent prefixes and suffixes, like "op-mix" and "ps-mix" etc. See the code below.

It works out to 3 sets of parameters, for 3 "operations": Fill, Mix, Clean.
* Fill runs first, then Mix. See the code below for the nested cycles within each.
* Clean is supposed to be by itself.

config:
* microsteps in config/agents/serial-stepper.json, default 1

"""

from copy import copy
import asyncio
import sys,os,re,json
from datetime import datetime
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib-pump-pi" )
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib" )
from recipe_dsl import h_duration
from simple_debug import debug
from aio_key import aio_key

class ToRecipe:
    """Get the settings from an aio.json ("n" style) and produce a recipe/dsl.py compatible output
        (we can also use a GC "pn" style)
        self.generate_recipe
    """

    def __init__(self, json_file, recipe_file):

        # all set by argparse()
        self.pump_ct = 0
        self.recipe_file = recipe_file
        self.json_file = json_file

        self.pump_config = {} # ['p1' -> 0] = the aio data
        self.config = {}
        self.read_config()
        self.read_json()

    def read_config(self):
        # config could be from several places
        ss_config = 'config/agents/serial-stepper.json'
        if os.path.exists(ss_config):
            with open(ss_config, 'r') as fh:
                data = json.load(fh)
        else:
            debug(F"Warning: no config file, defaulting to 'microsteps' : 1: {ss_config}")
            data={ 'microsteps':1 }
        self.config['microsteps'] = data.get('microsteps',None)
        if self.config['microsteps'] == None:
            debug(F"Warning: using default 'microsteps' : 1: {ss_config}")
            self.config['microsteps'] = 1
        debug(F"Microsteps {self.config['microsteps']}")
        
    def read_json(self):
        with open(self.json_file,'r') as fh:
            self.pump_config = json.load(fh)
        # json doesn't allow int keys
        # and saved files on GC are 'p4' style keys
        for k in list(self.pump_config.keys()):
            # We want to be [0] in here
            if k.startswith('p'):
                newk = int(k[1:]) - 1
            else:
                newk = k
            self.pump_config[ int(newk) ] = self.pump_config.pop(k)
        #debug(F"JSON {json.dumps(self.pump_config,indent=2)}") 

    def write_csv_ryan(self):
        # FIXME: use existing aio.json always
        #debug(f"after pk {json.dumps(self.pump_config,indent=2)}")
        for pump_i in sorted( self.pump_config.keys() ):
            pump_n = pump_i + 1 # convenience

            config = self.pump_config[ pump_i ]

            # strip pn.pn- from all keys
            for k in list(config.keys()):
                new_k = re.sub(r'^p\d+\.p\d+-','', k)
                config[new_k] = config.pop(k)

            copy_from_key = "cf-copy-from"

            # do the copy thing
            if config[copy_from_key] not in [None,'',0,"0"]:
                copy_from = int( float(config[copy_from_key]) ) - 1
                config = copy( self.pump_config[ copy_from ] )
                #debug(f"Copy {pump_n} from {copy_from+1}")
                config[ copy_from_key ] = copy_from + 1
                self.pump_config[ pump_i ] = config

            #debug(f"Cleaned/maybe copy {pump_n} {json.dumps(self.pump_config[pump_i],indent=2)}")
            #debug(f"Cleaned/maybe copy {pump_n} [0][pc-mix-cycles-per-operation]={ self.pump_config[0]['pc-mix-cycles-per-operation']}")

        with open(self.recipe_file,'w') as f:

            aio_config_keys = [
                # in spreadsheet order!
                'cf-active',
                'cf-copy-from',
                'op-mix',
                'op-fill',
                'op-clean',
                'xcf-step-per-ml',
            ]

            aio_parameters_to_lines = [
                # aio-feed | None-for-blank-line

                None, # config
                *aio_config_keys,

                None, # repeats
                'pc-mix-cycles-per-operation',
                'pc-mix-increments-per-cycle',
                'pc-mix-loops-per-increment',
                'pc-fill',
                'pc-clean',

                None, # ml
                'pv-mix-in-cycle-start-in',
                'pv-mix-increment-add-in',
                'pv-mix-loop-out',
                'pv-mix-loop-in',
                'pv-fill',
                'pv-clean',
                None, # total-ml

                None, # ml/min
                'ps-mix-cycle-start-in',
                'ps-mix-increment-add-in',
                'ps-mix-loop-out',
                'ps-mix-loop-in',
                'ps-fill',
                'ps-clean',
                None, # total-fill-secs
                None,

                # None, # pause
                'pw-mix-cycle-start-in',
                'pw-mix-increment-add-in',
                'pw-mix-loop-out',
                'pw-mix-loop-in',
                'pw-mix-increment-end',
                'pw-mix-cycle-end',
                'pw-mix-operation-end',
                'pw-fill',
                'pw-clean',
                None, # total-pause
            ]
            aio_fill_keys = [
                *aio_config_keys,
                'pc-fill',
                'pv-fill',
                'ps-fill',
                'pw-fill',
                None,
            ]
            aio_clean_keys = [
                *aio_config_keys,
                'pc-clean',
                'pv-clean',
                'ps-clean',
                'pw-clean',
                None,
            ]
            aio_mix_keys = (set(aio_parameters_to_lines) - set(aio_fill_keys) - set(aio_clean_keys)) | set(aio_config_keys)
            debug(f"mixkeys {aio_mix_keys}")

            for param in aio_parameters_to_lines:
                row = ['' if param==None else param]
                for pump_i in sorted( self.pump_config.keys() ):
                    pump_n = pump_i + 1 # convenience

                    config = self.pump_config[ pump_i ]
                    #debug(f"write {pump_n} [0][pc-mix-cycles-per-operation]={ config['pc-mix-cycles-per-operation']}")

                    # the value
                    if config['cf-active'] == 'ON':
                        if (
                            config['op-mix'] == 'ON' and param in aio_mix_keys
                            or config['op-clean'] == 'ON' and param in aio_clean_keys
                            or config['op-fill'] == 'ON' and param in aio_fill_keys
                            or param==None
                            ):
                            v = '' if param==None else config[ param ]
                            if v == None:
                                v = ''
                            else:
                                v = str(v)
                        elif param.startswith('ps-'):
                            # need a "1" for the /ml-sec calc
                            v = 1
                        else:
                            #debug(f"Skip {pump_i}")
                            #debug(f"  op-mix? { config['op-mix'] } and {param} in {aio_mix_keys} ? { param in aio_mix_keys }")
                            #debug(f"  op-fill? { config['op-fill'] } and {param} in {aio_fill_keys} ? { param in aio_fill_keys }")
                            v = ''
                    else:
                        v = ''
                    row.append( str(v) )
                f.write( ",".join(row) + "\n")
        debug(F"# Wrote {self.recipe_file}")

    def generate_recipe(self):
        with open(self.recipe_file,'w') as f:
            self.recipe_line(f, 0, '"""')
            self.recipe_block(f, 1, f"""
                Converted AMPS AIO settings recipe (by {sys.argv[0]})
                From {self.json_file}
                {len(self.pump_config)} pumps
                # FIXME: more "from" info
                {datetime.now()}
            """)
            self.recipe_line(f, 0, '"""\n')

            indent = 0

            # Nb: /totals or /totals/resume
            self.recipe_line(f, indent, f'recipe = p_totals( parallel("{ os.path.basename(self.recipe_file) } {self.pump_ct} pumps", \n')

            indent += 1

            unindent = indent # how many to close/unindent for this prologue

            for pump_i in sorted( self.pump_config.keys() ):
                config = self.pump_config[ pump_i ]
                pump_n = pump_i + 1 # convenience

                def toi(v):
                    return int(float(v))

                def v(short_key, converter=None):
                    # a bit shorter, + conversion
                    raw_v = config[ short_key ]
                    return (converter(raw_v) if (converter and raw_v != None) else raw_v)

                # because we can copy, and because convenience
                # strip the "pn.pn-" from the keys
                for k in list(config.keys()):
                    short = re.sub(r'^p\d+\.p\d+-','', k)
                    config[ short ] = config.pop( k )

                copy_from_i = int(float(config['cf-copy-from'])) - 1 if config['cf-copy-from'] else None
                if copy_from_i != -1 and copy_from_i != None and copy_from_i != pump_i:
                    debug(f"Copy {pump_i + 1} <- {copy_from_i+1}")
                    config = self.pump_config[ copy_from_i ]

                #debug(f"config [{pump_i}]: {config}")

                self.recipe_line(f, indent, f"# Pump {pump_i+1}")
                self.recipe_line(f, indent, f'sequential( "Pump {pump_n}",\n')

                if config['cf-active'] != 'ON':
                    self.recipe_line(f, indent, f"# off\n")
                else:

                    # correct the step-per-ml (based on full steps) by the microsteps
                    self.recipe_line(f, indent+1, f"steps_per_ml({pump_n}, {v('xcf-step-per-ml')} * {self.config['microsteps']}),\n" )
                    self.recipe_block(f, indent+1, self.generate_mix_part(config, pump_i, pump_n))
                    self.recipe_block(f, indent+1, self.generate_fill_part(config, pump_i, pump_n))
                    self.recipe_block(f, indent+1, self.generate_clean_part(config, pump_i, pump_n))

                self.recipe_line(f, indent, f"),\n")

            for ii in reversed(range(0,unindent)):
                comma = ',' ## if ii>0 else ''
                self.recipe_line(f, ii, f'){comma})')
        debug(F"# Wrote {self.recipe_file}")

    def generate_clean_part(self, config, pump_i, pump_n):
        def toi(v):
            return int(float(v))

        def v(short_key, converter=None):
            # a bit shorter, + conversion
            raw_v = config[ short_key ]
            return (converter(raw_v) if (converter and raw_v != None) else raw_v)

        if v('op-clean') != 'ON':
            return ''
        else:
            return(
                f"""
                # CLEAN
                comment("clean p{pump_n} -{float(v('pv-clean')) * v('pc-clean', toi)} ml in {v('pc-clean', toi)} cycles"),
                repeat( "clean p{pump_n} cycle {{}}", {v('pc-clean', toi)},
                    pause( {v('pw-clean')}, why='clean', base_progress_data={{'pump_n':{pump_n}}} ), # {h_duration( float(v('pw-clean')) )}
                    pump_ml( {pump_n}, why="clean", ml=-{v('pv-clean')}, rate={v('ps-clean')}/60.0 ),
                    base_progress_data={{'pump_n':{pump_n}}}
                ),

                """
            )


    def generate_fill_part(self, config, pump_i, pump_n):
        def toi(v):
            return int(float(v))
        def tof(v):
            return float(v)

        def v(short_key, converter=None):
            # a bit shorter, + conversion
            raw_v = config[ short_key ]
            return (converter(raw_v) if (converter and raw_v != None) else raw_v)

        if v('op-fill') != 'ON':
            return ''

        # fill should be positive ml's

        recipe_lines = f"""
            # FILL
            comment("fill p{pump_n} {float(v('pv-fill')) * v('pc-fill', toi)} ml in {v('pc-fill', toi)} cycles"),
            repeat("fill p{pump_n} cycle {{}}", {v('pc-fill', toi)},
                pump_ml({pump_n}, why="fill", ml={v('pv-fill',tof)}, rate={v('ps-fill',tof)}/60.0),
                pause({v('pw-fill',tof)}, why='fill', base_progress_data={{'pump_n':{pump_n}}}),
                base_progress_data={{'pump_n':{pump_n}}}
            )
        
        """

        return recipe_lines

    def generate_mix_part(self, config, pump_i, pump_n):
        # FIXME: we also generate the flexdash widgets,
        # which is a subset of the steps, in the same order, and needs to be wired to a message extract/converter
        # FIXME: we want the `pump_n` for progress_data in each step, but e.g. `repeat` has no such attribute.
        # how to pass things like that into things like `repeat`? the `tag_id` idea... `base_progress_data`...

        def toi(v):
            return int(float(v))
        def tof(v):
            return float(v)

        def v(short_key, converter=None):
            # a bit shorter, + conversion
            raw_v = config[ short_key ]
            return (converter(raw_v) if (converter and raw_v != None) else raw_v)

        if v('op-mix') != 'ON':
            return ''

        # remember that pv-mix-loop-out needs to be negative'd

        # NB: AIO rates are per minute!
        recipe_lines = f"""
            # MIX
            
            repeat("mix p{pump_n} cycle {{}}", {v('pc-mix-cycles-per-operation', toi)},
                pump_ml({pump_n}, why="cycle-start", ml={v('pv-mix-in-cycle-start-in')}, rate={v('ps-mix-cycle-start-in')}/60.0 ),
                pause({v('pw-mix-cycle-start-in')}, why='cycle-start-in', base_progress_data={{'pump_n':{pump_n}}} ),

                repeat("mix p{pump_n} increment {{}}", {v('pc-mix-increments-per-cycle',toi)},
                    pump_ml({pump_n}, why="increment-add-in", ml={v('pv-mix-increment-add-in')}, rate={v('ps-mix-increment-add-in')}/60.0 ),
                    pause({v('pw-mix-increment-add-in')}, why='increment-add-in', base_progress_data={{'pump_n':{pump_n}}} ),

                    repeat("cycle p{pump_n} increment loop {{}}", {v('pc-mix-loops-per-increment', toi)},
                        pump_ml({pump_n}, why="loop-out", ml={-v('pv-mix-loop-out',tof)}, rate={v('ps-mix-loop-out')}/60.0 ),
                        pause({v('pw-mix-loop-out')}, why='loop-out', base_progress_data={{'pump_n':{pump_n}}} ),
                        pump_ml({pump_n}, why="loop-in", ml={v('pv-mix-loop-in')}, rate={v('ps-mix-loop-in')}/60.0 ),
                        pause({v('pw-mix-loop-in')}, why='loop-in', base_progress_data={{'pump_n':{pump_n}}} ),
                        base_progress_data={{'pump_n':{pump_n}}}
                    ),

                    #pause({v('pw-mix-increment-add-in')}, why='increment-add-in',  base_progress_data={{'pump_n':{pump_n}}} ),
                    pause({v('pw-mix-increment-end')}, why='increment-end',  base_progress_data={{'pump_n':{pump_n}}} ),
                    base_progress_data={{'pump_n':{pump_n}}}
                ),

                pause({v('pw-mix-cycle-end')}, why='cycle-end', base_progress_data={{'pump_n':{pump_n}}} ),
                base_progress_data={{'pump_n':{pump_n}}}
             ),
            pause({v('pw-mix-operation-end')}, why='operation-end', base_progress_data={{'pump_n':{pump_n}}} ),
            comment("mix p{pump_n} end"),
            
        """
        return recipe_lines

    def recipe_line(self, fh, indent, line):
        fh.write("  " * indent)
        fh.write(line)
        fh.write("\n")

    def recipe_block(self, fh, indent, block):
        # the block is a """\n\tfirstline\n\t...lines"""
        # we treat the first line as the 0-indent-ref, strip that leading, and indent
        strip_length = 0
        lines = block.split("\n")
        for i,l in enumerate(lines):
            if i==0:
                # first line is the """ w/o any content
                continue
            if i==1:
                strip_length = len(re.match(r'\s*', l).group(0))
            if i==len(lines)-1 and len(  l[strip_length:] ) == 0:
                # last line is often ....""" w/no content
                continue

            fh.write( "  " * indent)
            fh.write( l[strip_length:] )
            fh.write("\n")
            
def to_recipe(from_file,to_file):
    converter = ToRecipe(from_file,to_file)
    converter.generate_recipe()

def to_csv(from_file,to_file):
    converter = ToRecipe(from_file,to_file)
    converter.write_csv_ryan()
