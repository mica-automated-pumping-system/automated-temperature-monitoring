'''
dsl for node-red flexdash
'''

import random,json,subprocess
from contextlib import contextmanager
from copy import copy
from math import ceil

import os,sys
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib" )
from simple_debug import debug
import node_red_flow_file as flow
from hash_deep_merge import deep_merge

nr_flow = flow.NodeRedFlowFile()

# Fluid, with's and the "ambient"
class Fluid:
    # want an ambient for this: global
    # kv set
    def __init__(self):
        self._stack= [{}]

    # nest/unnest

    def _push(self, **kwargs):
        this_level = self._stack[-1]
        self._stack.append( deep_merge( this_level, kwargs) )

    def _pop(self):
        self._stack.pop()

    def _get(self, *names, missing=None):
        # any of the names, or missing value
        return next( ( self._stack[-1][x] for x in names if x in self._stack[-1].keys() ), missing)
        

    @contextmanager
    def __call__(self, **kwargs): # as the fluid
        try:
            #debug(f"with fluid {kwargs}")
            self._push(**kwargs)
            yield self
        finally:
            self._pop()

    
    def __getattr__(self, name):
        #debug(f"  get[{name}] {self._stack[-1]}")
        try:
            return self._stack[-1][ name ]
        except KeyError as e:
            fail(f"Variable `{name}` not found in the fluid {self}")

    def __str__(self):
        return str(self._stack[-1])

def fail(msg,up=0):
    debug(msg,up=2+up)
    raise Exception("Fail")
    exit(1)

_nr_context = Fluid()
# initializing layout: row-wise (and defaults)
_nr_context._push( 
    _layout= { 
        'x' : { 'pos':100, 'origin':0, 'incr':1, 'd':200 },
        'y' : { 'pos':20, 'incr':0, 'd':40 },
    }
)

_nodes = []

def _not_impl():
    debug("Not implemented",up=1)
    exit(1)

_nr_project_dir = '.node-red/projects' # const

@contextmanager
def flow(name):
    # set context to the flow name
    debug(f"flow file {nr_flow.flow_file()}")

    rez = nr_flow.find_node(name,'tab')
    if len(rez) != 1:
        fail(f"Can't find a (single) flow ~= {name}",up=1)
    flow_node = rez[0]
    debug(f"Flow {flow_node}")
    with _nr_context( flow_node=flow_node ):
        #debug(f"In a flow: {_nr_context}")
        yield flow_node

@contextmanager
def default(**kwargs):
    # default values for nodes
    # k:v for all
    # $type: { kv... } for a type (==the method name that makes it?)
    new_def = deep_merge( _nr_context._get('default',missing={}), kwargs )
    with _nr_context( default = new_def ):
        #debug(f"Default {_nr_context.default}")

        yield _nr_context._stack[-1]

@contextmanager
def tab(name, **kwargs):
    # set context to the tab name
    rez = nr_flow.find_node(name,fd_type='tab')
    if len(rez) == 0:
        fail(f"Expected tab to exist already: '{name}'")
    else:
        if len(rez) > 1:
            fail(f"Can't find a (single) grid ~= {name}",up=1)
        tab = rez[0]
        _set_defaults_and_kw( tab, 'tab', kwargs )

    with _nr_context( tab=tab ):
        debug(f"In a tab: {_nr_context._get('tab')}")
        yield tab

@contextmanager
def grid(name, **kwargs):
    # set context to the grid name
    rez = nr_flow.find_node(name,fd_type='grid')
    if len(rez) == 0:
        grid = _make_grid(name, **kwargs)
    else:
        if len(rez) > 1:
            fail(f"Can't find a (single) grid ~= {name}",up=1)
        grid = rez[0]
        _set_defaults_and_kw( grid, 'grid', kwargs )

    with _nr_context( grid=grid ):
        with group( name+"-group" ):
            with layout(1,0): # begin a row
                debug(f"In a grid: {_nr_context._get('grid')}")
                yield grid

        # next row
        _nr_context._layout['x']['pos'] = _nr_context._layout['x']['origin']
        _nr_context._layout['y']['pos'] += _nr_context._layout['y']['d']

@contextmanager
def group(name, **kwargs):
    rez = nr_flow.find_node(name,fd_type='group')
    if len(rez) == 0:
        group = _make_group(name, **kwargs)
    else:
        if len(rez) > 1:
            fail(f"Can't find a (single) group ~= {name}",up=1)
        group = rez[0]
        _set_defaults_and_kw( group, 'group', kwargs )

    with _nr_context( group=group ):
        yield group
        
        # fixup h & w
        h = 2 * 20 # top/bottom margin
        w = 2 * 20
        min_x = None
        max_x = None
        for n in _nr_context.group['nodes']:
            other_node = nr_flow._kv[n]
            #debug(f"  min/max from {other_node}")

            h += 40 # by examination
            min_x = min( min_x, other_node['x'] ) if min_x != None else other_node['x']
            max_x = max( max_x, other_node['x'] ) if max_x != None else other_node['x']

        # nodes are centered. punting on nested groups
        w += (max_x + 50) - (min_x - 50)

        group['w'] = w
        group['h'] = h

@contextmanager
def layout(x_incr=None, y_incr=None, dx=None,dy=None, below=None, set_left_margin=False):
    # set to:
    # move x and/or y increments each time
    # where an incr is the dx|dy in coords
    # `below` is the name (or "name:type") of a node, start y below this
    # `set_left_margin` captures the current x as x-origin
    # nb: 10 seems like the grid
    # Default d's are from root context

    # so many optionals
    new_layout={ 
        'x' : { 'pos': _nr_context._layout['x']['origin'] },
        'y' : { },
    }
    if x_incr!=None:
        new_layout['x']['incr'] = x_incr
    if y_incr!=None:
        new_layout['y']['incr'] = y_incr
    if dx!=None:
        new_layout['x']['d'] = dx
    if dy!=None:
        new_layout['y']['d'] = dy

    if set_left_margin:
        new_layout['x']['pos'] = _nr_context._layout['x']['pos']
        new_layout['x']['origin'] = _nr_context._layout['x']['pos']

    if below!=None:
        #debug(f"Split name/type: {below} -> {below.split(':')}")
        simple_name,*type = below.split(':')
        type = type[0] if type else None
        rez = nr_flow.find_node(simple_name,fd_type=type)
        if len(rez) > 1:
            for x in rez:
                nr_flow.print_node(x,indent=1)
            fail(f"Expected only one 'node' to do a `below` ~= {below} (see list above)",up=1)
        below_node = rez[0]
        debug(f"Below! {below_node}")
        # get start
        # "round" to our increment
        x_d = float(_nr_context._layout['x']['d']) or 1
        y_d = float(_nr_context._layout['y']['d']) or 1
        other_x = below_node['x']
        other_y = ( below_node['y'] + below_node['h'] ) if below_node['type'] == 'group' else below_node['y']
        x = int( ceil( other_x / x_d ) * x_d )
        y = int( ceil( other_y / y_d ) * y_d ) + y_d
        new_layout['x']['pos'] = x
        new_layout['x']['origin'] = x
        new_layout['y']['pos'] = y

    with _nr_context( _layout=new_layout ):
        debug(f"## _layout {_nr_context._layout}")
        yield new_layout

@contextmanager
def panel(name, **kwargs):
    # set context to the panel name
    rez = nr_flow.find_node(name,fd_type='panel')
    if len(rez) == 0:
        panel = _make_panel(name, **kwargs)
        debug(f'Make a panel {panel}')
    else:
        if len(rez) > 1:
            fail(f"Can't find a (single) panel ~= {name}",up=1)
        panel = rez[0]
        _set_defaults_and_kw( panel, 'panel', kwargs )

    with _nr_context( panel=panel ):
        with group( name+"-group" ):
            debug(f"Panel '{name}' stuff in group {_nr_context._get('group')}")
            #debug(f"In a panel: {_nr_context._get('panel')}")
            with layout(0,1, set_left_margin=True):
                yield panel

                total_rows = 0
                for n in panel['fd_children']:
                    child = nr_flow._kv[n]
                    total_rows += child['fd_rows']

                panel['rows'] = int(total_rows / 2.0 + 0.5)

@contextmanager
def postfix_names(postfix):
    # to add a postfix to node names
    with _nr_context( name_postfix=postfix):
        #debug(f"## postfix w/{postfix}")
        yield postfix

def delete_all():
    # delete everything in this tab|grid|panel context
    
    root = _nr_context._get('panel','grid','tab')
    if root == None:
        fail(f"Won't delete everything! Not in a panel|grid|tab")

    fd_ids = []

    def collect_down(node):
        for n in node.get('fd_children',[]):
            fd_ids.append( n )
            collect_down(nr_flow._kv[n])

    collect_down(root)
    debug(f"Delete all in {root['name']}: {len(fd_ids)}")
    #for x in fd_ids:
    #    nr_flow.print_node(nr_flow._kv[x],1)
    nr_flow.delete( fd_ids )

### node makers

def _id():
    i16 = ( random.choice( range(0,16) ) for x in range(1,16) )
    return ''.join( ( chr( ord('0') + x) if x < 10 else chr( ord('a') + x-10 ) for x in i16 ) )

def fd_container():
    # current container (for widgets)
    return _nr_context._get('panel','grid','tab')

def fd_container_id():
    # current container id (for widgets), or None
    c = fd_container()
    return c['id'] if c else None

def add_node(name, default_name, settings, node):
    # add the node, doing some fixup:
    #   set `id`
    #   set `name`
    #       with possible postfix
    #   set the 'z' (flow), 'fd_container'
    #   set the x,y
    #   copy any defaults in, from the `with defaults` context:
    #       common defaults
    #       defaults[default_name]
    #      But, only if the keyword exists in the node already
    #   copy settings in
    #      But, only if the keyword exists in the node already
    #   set the parent to the `with x` context

    node['id'] = _id()

    if name==None:
        name=''
    node['name' ] = name

    if name=='pump-col':
        debug(f"Add node, z? {node}")
    if 'z' in node.keys():
        node['z'] = _nr_context.flow_node['id']
    #debug(f"Add node, z! {node}")

    if 'fd_container' in node.keys():
        node['fd_container'] = fd_container_id()
        if node['fd_container'] == None:
            raise Exception(f"Expected a fd-container to be the context (for a fd-widget), saw none, for: {node}\n\tContext: {_nr_context}")
    elif node['type'].startswith('fd-'):
        raise Exception(f"Expected a type=fd-* to have fd_container key, saw {node}")

    if p:= _nr_context._get('name_postfix'):
        node['name'] += str(p)

    if 'x' in node.keys():
        if not 'y' in node.keys():
            raise Exception("Internal, node had an 'x', but no 'y': {node}")
        # for every node, increment the position
        layout = _nr_context._layout
        node['x'] = layout['x']['pos']
        node['y'] = layout['y']['pos']
        layout['x']['pos'] += layout['x']['incr'] * layout['x']['d']
        layout['y']['pos'] += layout['y']['incr'] * layout['y']['d']

    _set_defaults_and_kw( node, default_name, settings )

    nr_flow.set_group( node, _nr_context._get('group') )
    if node['type'].startswith('fd-') or node['type']=='flexdash container':
        nr_flow.set_fd_parent( node, _nr_context._get('panel','grid','tab') )
    nr_flow.add_node(node, _nr_context.flow_node)
    debug(f"Added {node['type']:15}" + (f":{node['kind']} " if 'kind' in node.keys() else '') + f"{node['name']:20} {node['id']}")
    return node

def _set_defaults_and_kw( node, default_name, settings ):
    for k,v in _nr_context._get('default',missing={}).items():
        if not isinstance(v,dict):
            # all node types, if they have it
            if k in node.keys():
                node[k] = v
            elif os.getenv('NRSTRICT'):
                raise Exception(f"Is not an allowed default, '{k}', for {node}")
        elif k==default_name:
            # specific keys for this "type"
            for dk,dv in v.items():
                if dk in node.keys():
                    node[dk] = dv
                elif os.getenv('NRSTRICT'):
                    raise Exception(f"Is not an allowed default['{default_name}'], '{dk}', for {node}")

    for k,v in settings.items():
        if k in node.keys():
            node[k] = v
        elif os.getenv('NRSTRICT'):
            raise Exception(f"Is not an allowed setting, '{k}', for {node}")

def wire(*portnum_from_to):
    # portnum can be skipped == 0
    # from and to can be: a node, an id, a name:type|kind
    nr_flow.wire( *portnum_from_to )

def markdown(name, text, **kwargs):
    return add_node(name, 'markdown', kwargs, 
        {   
        "id": _id(),
        "type": "fd-markdown",
        "z": None,
        "fd_container": None,
        "fd_cols": "1",
        "fd_rows": "1",
        "fd_array": False,
        "fd_array_max": 10,
        "name": name,
        "title": "",
        "popup_info": "",
        "text": text,
        "x": 200,
        "y": 220,
        "wires": []
        }
    )

def stat(name, **kwargs):
    return add_node(name, 'stat', kwargs,
        {   
        #"id": _id(),
        "type": "fd-stat",
        "z": None,
        "fd_container": None,
        "fd_cols": 1,
        "fd_rows": 1,
        "fd_array": False,
        "fd_array_max": 10,
        "name": name,
        "title": kwargs.get('title',''),
        "popup_info": "",
        "unit": "°F",
        "value": "#", # nan
        "color": "",
        "low_color": "blue",
        "high_color": "pink",
        "low_threshold": "",
        "high_threshold": "",
        "low_regexp": "",
        "high_regexp": "",
        "chip": "false",
        "iso_prefix": "true",
        "zoom": 1,
        "x": 270,
        "y": 200,
        "wires": []
        }
    )

def gauge(name, **kwargs):
    return add_node(name, 'gauge', kwargs,
        {   
        #"id": _id(),
        "type": "fd-gauge",
        "z": None, # fixup
        "fd_container": None,
        "fd_cols": "3",
        "fd_rows": "1",
        "fd_array": False,
        "fd_array_max": 10,
        "name": name,
        "title": kwargs.get('title',''),
        "popup_info": "",
        "value": '#', # nan
        "unit": "",
        "arc": 180,
        "min": 0,
        "max": 6,
        "color": "green",
        "low_color": "blue",
        "high_color": "pink",
        "low_threshold": None,
        "high_threshold": 6,
        "base_color": "grey-lighten-3",
        "needle_color": "white",
        "radius": 70,
        "stretch": False,
        "x": 620,
        "y": 280,
        "wires": []
        }
    )

def sparkline(name, **kwargs):
    return add_node(name, 'sparkline', kwargs, 
        {   
        "id": "5ff53527b52097b5",
        "type": "fd-spark-line",
        "z": None,
        "fd_container": None,
        "fd_cols": 3,
        "fd_rows": 1,
        "fd_array": False,
        "fd_array_max": 10,
        "name": "",
        "title": "",
        "popup_info": "",
        "value": [ 0 ],
        "color": "blue",
        "fill_color": "",
        "text_color": "",
        "show_value": True,
        "unit": "ml",
        "x": 620,
        "y": 340,
        "wires": []
        }
    )

def _make_grid(name, **kwargs):
    return add_node(name, 'grid', kwargs,
        {
        #"id": _id(),
        "type": "flexdash container",
        "name": '',
        "title": '',
        "kind": "StdGrid",
        "fd_children": [],
        "tab": None, # parent will fixup
        "min_cols": 4,
        "max_cols": "10",
        #"parent": "", # always
        #"solid": False,
        "cols": "1",
        "rows": "1",
        "unicast": "ignore"
        }
    )

def _make_tab_xxx(name, **kwargs): # don't use. not properly implemented
    _not_impl()
    return add_node(name, 'tab', kwargs,
        {
        #"id": _id(),
        "type": "flexdash tab",
        "name": name,
        "icon": None,
        "title": kwargs.get('title',''),
        "fd_children": [],
        "fd": "e8f5aea52ab49500" # a type: "flexdash dashboard"
        }
    )

def _make_panel(name, **kwargs):
    return add_node(name, 'panel', kwargs,
        {
        #"id": _id(),
        "type": "flexdash container",
        "name": name,
        "title": '',
        "kind": "Panel",
        "fd_children": [],
        "tab": "",
        "min_cols": "1",
        "max_cols": "20",
        #"parent": "5706d648a3e5e7bf",
        "solid": False,
        "cols": "1",
        "rows": "1"
        }
    )

def _make_group(name, **kwargs):
    return add_node(name, 'panel', kwargs,
        {
        #"id": _id(),
        "type": "group",
        "z": None,
        "name": "",
        "style": {
            "label": True
        },
        "nodes": [],
        "x": 134,
        "y": 459,
        "w": 552,
        "h": 122
        }
    )


def fd_dump():
    nr_flow.write( 'new_flow.json' )
