"""
Get the `connection` (paho-mqtt connection params) from config/$x.py:

import mqtt_config_reader

connection = mqtt_config.connection( "config/$x.py" )

"""

from copy import copy
import aiomqtt as mqtt
from aio_key import aio_key
import paho.mqtt as paho

def connection( config_file ):
    with open(config_file, mode='r') as fh:
        # FIXME: feedback on any exception
        code = fh.read()
        # FIXME: feedback on any exception
        compiled_code = compile( code, config_file, mode='exec')
        code = None

    script_globals = { 'paho' : paho, 'mqtt' : mqtt, 'aio_key' : aio_key }
    exec( compiled_code, script_globals )
    connection = script_globals.get('connection',None) 
    if connection:
        connections = [ connection ]
    else:
        connections = script_globals.get('connections',None)
    if not connections:
        raise Exception(f"Expected a `connection = {...}` or `connections = [...]`, saw neither in {config_file}")

    # debug print
    for i,conn in enumerate(connections):
        _visible = copy(conn)
        _visible.pop('password',None)
        #print(f"## config {config_file}[{i}] -> {_visible}")

    return connections
