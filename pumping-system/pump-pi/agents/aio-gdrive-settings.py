#!/usr/bin/env python3.11

import asyncio
import json,re,typing,pathlib
from datetime import datetime
import subprocess
import sys,os
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib-pip" )
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib" )
from simple_debug import debug
import paho.mqtt as paho # some constants|--help'
import agent

class RunAIO(agent.Agent):
    """We get/set the settings in AIO, to/from the gdrive.
    """
    # lifecycle is:
    # SomeAgent()
    # .argparse()
    # connect with `will` based on .description
    # taskgroup.create_task( .listener( ) )
    # taskgroup.create_task( .advertise() )
    # taskgroup.create_task( .loop() )
    # await .cleanup(exception) if we catch an exception
    #   always call super().cleanup(exception) if you override

    AIORecipeDir = 'aio-recipes'
    AIOJSONDir = 'aio-json'
    UploadUrlFile = "config/google-drive-upload-folder.url"
    AIOODSTemplate = 'aio-json/aio-params-template.ods'
    FeedInfoFile = 'aio-json/aio-feed-info.json'

    mqtt_config = {
        "device" : "recipe",
        "name" : "aio-settings",

        "connection" : { # paho mqtt args
            "hostname" : "localhost",
            "port" : 1884,
            # client_id
            # tls...
            "clean_session" : True, # if a short enough down-time, resync to the commands
            "keepalive" : 8, # trigger Will
            "clean_start" : paho.client.MQTT_CLEAN_START_FIRST_ONLY,
        }
    }

    def __init__(self):
        super().__init__() # minimal setup
        os.makedirs( self.AIORecipeDir, exist_ok=True )

    async def argparse(self):
        # if you need to handle/look at any argparse
        await super().argparse()

    def interesting_topics(self):
        return super().interesting_topics() + [
            self.topic_for_command('aio-json/gdrive-to-recipe'),
            self.topic_for_command('aio-json/fetch') + "\t optional {upload:true}",
            self.topic_for_command('aio-json/gdrive-to-aio'), # No file-name option + " {file:name-in-upload-folder|none for google-doc native `new-aio-settings`}",
        ]

    async def listener(self):
        # optional
        # subscribes to topics, assigning handlers
        await super().listener() # default behavior for mqtt .../get

        # your stuff:
        # FIXME: static, and adds to interesting_topics()
        await self._subscribe( self.topic_for_command('aio-json/gdrive-to-recipe'), self.gdrive_to_recipe )
        await self._subscribe( self.topic_for_command('aio-json/fetch'), self.fetch_aio_settings)
        await self._subscribe( self.topic_for_command('aio-json/gdrive-to-aio'), self.gdrive_to_aio)

        # FIXME: how to stop an gdrive_to_aio? like run-aio: a flag==runing task, cancel it w/done message
        # await self._subscribe( self.topic_for_command('stop'), self.handle_stop)

    # each handler
    async def handle_stop(self,message):
        pass # FIXME

    def read_upload_folder_url(self):
            upload_url = None
            if os.path.exists( self.UploadUrlFile ):
                with open( self.UploadUrlFile, 'r') as fh:
                    upload_url = fh.readline().strip()
                    debug(f"Upload url {upload_url}")

            return upload_url

    async def gdrive_to_recipe(self, message):
        # local/commands/recipe/aio-settings/aio-json/fetch
        # Fetch the current params from AIO, create .csv, .ods, .json
        #   which can be fetched via node-red as http:/aio-csv/$file, or aio-json/$file, or aio-ods/$file
        # Git's the csv, json, and ods each time
        # If payload {upload:true}, we'll invoke: tools/gdrive upload $timestamp-$thejson-fetched.json `cat config/google-drive-upload-folder.url`

        timestamp = datetime.now().astimezone().strftime('%Y-%m-%dT%H:%M:%S%z')
        json_file = f"{self.AIOJSONDir}/aio-settings.json"
        csv_file = f"{self.AIOJSONDir}/aio-settings.csv"
        ods_file = f"{self.AIOJSONDir}/aio-settings.ods"
        recipe_file = f"{self.AIORecipeDir}/aio-{timestamp}-recipe.py"

        result_files = [ json_file, csv_file, ods_file ] # git'd

        response = { 
            "json" : json_file,
            "recipe" : recipe_file,
            "csv" : csv_file,
            "ods" : ods_file,
            "timestamp" : timestamp,
            "#" : [], # removed if empty
        }

        async def sort_json():
            with open(json_file,'r') as fh:
                data=json.load(fh)
            with open(json_file,'w') as fh:
                fh.write(json.dumps(data,indent=2,sort_keys=True)) # and sorts, so more git friendly
                fh.write("\n")
            return None
        
        # Figure out upload values
        upload_url = None # also flag for "are we doing upload to gdrive"
        if message.payload.get('upload',None):
            debug(f"Try upload to gdrive")
            upload_url = self.read_upload_folder_url()

            if not upload_url:
                debug(f"Could not get shared-link for upload dir from file: {self.UploadUrlFile}")
                return None

        dumy, new_upload_name = os.path.split(ods_file)
        new_upload_name,ext = os.path.splitext(new_upload_name)
        new_upload_name = f"{timestamp}-{new_upload_name}-fetched{ext}"

        def _ensure_git():
            return self.ensure_git(result_files)

        async def _noop():
            return None

        def finished():
            return self.mqtt_client.publish( 
                self.topic_for_event('aio-json/archive'),
               json.dumps( { 'finished' : True } )
            )

        cmds = [
            # a string, for shell=true, 
            # or a [] for the usual command
            # or a async-callable for some processing

            [ 'tools/aio-param-conversion', '.gdrive', json_file],
            sort_json,
            [ 'tools/aio-param-conversion', json_file, recipe_file ],
            [ 'tools/aio-param-conversion', json_file, csv_file],

            [ 'tools/aio-param-conversion', '--ods-template', self.AIOODSTemplate, json_file, ods_file ],
            _ensure_git,
            f'cd {self.AIOJSONDir} && git add -f { " ".join( [ os.path.basename(x) for x in result_files ] ) }',
            f'cd {self.AIOJSONDir} && git commit --quiet -a -m fetched',
            # may not upload to gdrive
            [ 'tools/gdrive', 'upload', '--newname', new_upload_name, ods_file, upload_url ] if upload_url else _noop,
            finished,
        ]
        
        debug(f"Fork cmds list")
        await self.mqtt_client.publish( self.topic_for_event('aio-json/fetch/progress'), json.dumps( {'#':"Starting"} ) , retain=True)

        asyncio.create_task( self.run_command_list(cmds, response, self.topic_for_event('aio-json/fetch')) )

    async def fetch_aio_settings(self, message):
        # local/commands/recipe/aio-settings/aio-json/fetch
        # Fetch the current params from AIO, create .csv, .ods, .json
        #   which can be fetched via node-red as http:/aio-csv/$file, or aio-json/$file, or aio-ods/$file
        # Git's the csv, json, and ods each time
        # If payload {upload:true}, we'll invoke: tools/gdrive upload $timestamp-$thejson-fetched.json `cat config/google-drive-upload-folder.url`

        json_file = f"{self.AIOJSONDir}/aio-settings.json"
        csv_file = f"{self.AIOJSONDir}/aio-settings.csv"
        ods_file = f"{self.AIOJSONDir}/aio-settings.ods"

        result_files = [ json_file, csv_file, ods_file ] # git'd

        response = { 
            "json" : json_file,
            "csv" : csv_file,
            "ods" : ods_file,
            "timestamp" :datetime.now().strftime('%Y-%m-%d_%H:%M'),
            "#" : [], # removed if empty
        }

        async def sort_json():
            with open(json_file,'r') as fh:
                data=json.load(fh)
            with open(json_file,'w') as fh:
                fh.write(json.dumps(data,indent=2,sort_keys=True)) # and sorts, so more git friendly
                fh.write("\n")
            return None
        
        # Figure out upload values
        upload_url = None # also flag for "are we doing upload to gdrive"
        if message.payload.get('upload',None):
            debug(f"Try upload to gdrive")
            upload_url = self.read_upload_folder_url()

            if not upload_url:
                debug(f"Could not get shared-link for upload dir from file: {self.UploadUrlFile}")
                return None

        dumy, new_upload_name = os.path.split(ods_file)
        new_upload_name,ext = os.path.splitext(new_upload_name)
        new_upload_name = datetime.fromtimestamp(pathlib.Path(ods_file).stat().st_ctime).strftime('%Y-%m-%d_%H:%M') + "-" + new_upload_name + "-fetched" + ext

        def _ensure_git():
            return self.ensure_git(result_files)

        async def _noop():
            return None

        def finished():
            return self.mqtt_client.publish( 
                self.topic_for_event('aio-json/archive'),
               json.dumps( { 'finished' : True } )
            )

        cmds = [
            # a string, for shell=true, 
            # or a [] for the usual command
            # or a async-callable for some processing

            #[ 'tools/aio-to-recipe', '--no-recipe', '--cache', '--cache-file', json_file, '6', 'dumy' ],
            [ 'tools/aio-param-conversion', '.aio', json_file],
            sort_json,
            #[ 'tools/aio-to-recipe', '--csv-ryan-mode', '--use-cache', '--cache-file', json_file, '6', csv_file ],
            [ 'tools/aio-param-conversion', json_file, csv_file],

            [ 'tools/aio-param-conversion', '--ods-template', self.AIOODSTemplate, json_file, ods_file ],
            _ensure_git,
            f'cd {self.AIOJSONDir} && git add -f { " ".join( [ os.path.basename(x) for x in result_files ] ) }',
            f'cd {self.AIOJSONDir} && git commit --quiet -a -m fetched',
            # may not upload to gdrive
            [ 'tools/gdrive', 'upload', '--newname', new_upload_name, ods_file, upload_url ] if upload_url else _noop,
            finished,
        ]
        
        debug(f"Fork cmds list")
        await self.mqtt_client.publish( self.topic_for_event('aio-json/fetch/progress'), json.dumps( {'#':"Starting"} ) , retain=True)

        asyncio.create_task( self.run_command_list(cmds, response, self.topic_for_event('aio-json/fetch')) )

    async def run_command_list( self, cmds, response, final_topic ):
        # Usage (probably): asyncio.create_task( run_command_list(...) )
        # Meant for a listener to do a bunch of things
        #
        # given a list of commands
        # run them, stopping if failure, else doing a final response on the final_topic
        # see usage for examples
        # cmds = [
        #   ['blah','blah', {'capture':True}] # create_subprocess_exec, capture stdout/stderr to response['#']=[]
        #   [ 'blah blah blah' ] # create_subprocess_shell, no capture yet
        #   somefn # await somefn() -> None or process.returncode==0 for OK, or process.returncode!=0 for fail
        #   ]

        for cmd_i,cmd in enumerate(cmds):

            debug(f"Fetch: {cmd}")
            if isinstance(cmd,list):

                command_args = {} # usually none
                if isinstance(cmd[-1],dict):
                    command_args = cmd.pop()
                    if 'capture' in command_args:
                       # don't pass this one through
                       ca = command_args.pop( 'capture' ) 
                       if ca:
                            command_args |= {'stdout':asyncio.subprocess.PIPE, 'stderr':asyncio.subprocess.PIPE}

                rez = await asyncio.create_subprocess_exec(*cmd, **command_args) # subprocess.run(cmd)
                await rez.wait()

                if 'stdout' in command_args or 'stderr' in command_args:
                    if 'stdout' in command_args:
                        s = await rez.stdout.read()
                        if s:
                            response['#'].append( s.decode('utf-8') )
                    if 'stderr' in command_args:
                        s = await rez.stderr.read()
                        if s:
                            response['#'].append( s.decode('utf-8') )
                            sys.stderr.write( s.decode('utf-8') )

            elif isinstance(cmd,str):
                rez = await asyncio.create_subprocess_shell(cmd) # subprocess.run(cmd, shell=True)
                await rez.wait()
            elif isinstance(cmd,typing.Callable):
                rez = await cmd()
            else:
                raise Exception(f"WAT? {cmd.__class__.__name__} {cmd}")

            if rez and rez.returncode != 0:
                response['status'] = "fail" # FIXME: need to get status etc correct
                response['exitstatus'] = rez.returncode
                response['command'] = cmd
                break

            # progress if ok
            #if cmd_i+1 < len(cmds): # exclude last one? because we do a message at end...?
            message_command = " ".join(cmd) if isinstance(cmd,list) else cmd
            await self.mqtt_client.publish( 
                final_topic + "/progress", 
                json.dumps( { 'i':f"{cmd_i+1}/{len(cmds)}", '#': str(message_command) } ) 
            )

        if not response['#']:
            # remove if empty
            response.pop('#')
        else:
            response['#'] = "\n".join( response['#'] )

        if not 'status' in response.keys():
            response['status'] = "done"
            response['finished'] = True
        debug(f"Fetch: status {response['status']}")

        await self.mqtt_client.publish( final_topic, json.dumps( response ) , retain=True)

    async def ensure_git(self, files_to_git):
        git_dir = f"{self.AIOJSONDir}/.git"
        if not os.path.isdir( git_dir ):
            to_add = " ".join( [ os.path.basename(x) for x in files_to_git ] )
            # ensures a starting file, because we want .gitignore == '*'
            cmd = f'cd {self.AIOJSONDir} && git init && touch {to_add} && git add -f {to_add}'
            debug(f"Fetch git-init {cmd}")

            # try to prevent accidents
            with open(f"{self.AIOJSONDir}/.gitignore",'w') as fh:
                fh.write("*\n")

            rez = await asyncio.create_subprocess_shell(cmd) # subprocess.run(cmd, shell=True)
            await rez.wait()
            return rez
        return None

    async def gdrive_to_aio(self, message):
        # local/commands/recipe/aio-settings/aio-json/gdrive-to-aio [{file:nametofetch}]
        # Download the file, update AIO with any new/getting-old params
        # Git's the file
        # Only new-aio-settings

        upload_url = self.read_upload_folder_url()

        if not upload_url:
            # FIXME: report mqtt message I think
            debug(f"Could not get shared-link for upload dir from file: {self.UploadUrlFile}")
            return None

        gdrive_name = 'new-aio-settings' # no payload spec, unsafe. message.payload.get('file', 'new-aio-settings')
        ods_file = f"{self.AIOJSONDir}/{gdrive_name}.ods"
        baseish,ext = os.path.splitext(ods_file)
        json_file = baseish + ".json"

        result_files = [ ods_file, json_file, self.FeedInfoFile ] # git'd

        response = { 
            "ods" : ods_file,
            "timestamp" :datetime.now().strftime('%Y-%m-%d_%H:%M'),
            "#" : [], # removed if empty
        }


        async def sort_json():
            with open(json_file,'r') as fh:
                data=json.load(fh)
            with open(json_file,'w') as fh:
                fh.write(json.dumps(data,indent=2,sort_keys=True)) # and sorts, so more git friendly
                fh.write("\n")
            return None
        

        def _ensure_git():
            return self.ensure_git(result_files)

        async def _noop():
            return None

        async def finished():
            await self.mqtt_client.publish( 
                self.topic_for_event('aio-json/archive'),
               json.dumps( { 'finished' : True } )
            )

        cmds = [
            # a string, for shell=true, 
            # or a [] for the usual command
            # or a async-callable for some processing

            # FIXME: move these 2 to tools/aio-param-conversion
            [ 'tools/gdrive', 'download', '--path', upload_url, gdrive_name, ods_file ],
            [ 'tools/aio-params', 'get-feed-info' ],
            [ 'tools/aio-param-conversion', '--json-template', self.FeedInfoFile, ods_file, json_file ],
            _ensure_git,
            f'cd {self.AIOJSONDir} && git add -f { " ".join( [ os.path.basename(x) for x in result_files ] ) }',
            f'cd {self.AIOJSONDir} && git diff --exit-code --quiet --cached || git commit --quiet -a -m fetched',
            [ 'tools/aio-params', 'repair', '--current', json_file, '--older', '30d', {"capture":True} ],
            finished,
        ]
        
        debug(f"Fork cmds list")
        await self.mqtt_client.publish( self.topic_for_event('aio-json/gdrive_to_aio/progress'), json.dumps( {'#':"Starting"} ) , retain=False)

        asyncio.create_task( self.run_command_list(cmds, response, self.topic_for_event('aio-json/fetch')) )

    async def loop(self, first_time):
        # required. can simply loop forever if everything is handled in listener() and handle_*()'s
        while(True):
            await asyncio.sleep(1e6)

    def argparse_add_arguments(self, parser ):
        parser.add_argument('--cache', help="after fetching AIO settings, cache it as .aio.json, then continue", action='store_true')
        parser.add_argument('--use-cache', help="instead of the AIO settings, read the cache'd .aio.json, then continue", action='store_true')

RunAIO().async_run() # internally does asyncio.run( agent.run() )

'''
How To Exit
To do a clean exit, which nicely kills off an async tasks, and sends the 'quit' status.
Should be able to call this from any `def async`.

await self.exit($statuscode, $exception-if-you-have-it)
return # uh... cause this whole chain of awaitables to die...?
'''
