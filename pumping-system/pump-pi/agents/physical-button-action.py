#!/usr/bin/env -S sh -c 'exec "$(dirname $(realpath $(dirname $0)))/.venv/bin/python3" "$0" "$@"'

import asyncio
import sys,os,json,re,numbers,time
from random import choice
from copy import copy
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib" )
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib-pump-pi" )
from simple_debug import debug
from rate_limiter import RateLimiter
from every.every import Every,Timer
import paho.mqtt as paho # some constants|--help'
import agent

USEGPIO = not os.environ.get('NOGPIO',False)

try:
    import gpiozero
    from gpiozero import Button
    import board

    if 'SPI' not in dir(board): # proxy for "has gpio stuff"
        raise ModuleNotFoundError("No SPI")
except ModuleNotFoundError as e:
    import inspect
    agent.HelpEpilog = f'\nGPIO disabled (not running on a pi?). See {__file__}:{inspect.currentframe().f_lineno - 3}\n{e}'
    USEGPIO=False

import agent

class PinSetup:
    # NB: any parameters should be added to to_dict()
    def __init__(self, pin, mqtt_client, invert=False, pull=1, event=-1, messages=[], rate_limit=None, keepalive=None, latch=None, as_boolean=False, sequence_messages=False):
        # NB: any parameters should be added to to_dict()

        self.button=None # gpiozero Button
        self.pin=pin # is a key in the config, thus string
        self.mqtt_client = mqtt_client
        self.pull=pull
        self.event=event
        if self.event not in (-1,0,1):
            raise Exception(f"Internal: self.event should only be -1,0,1 (down,cross,up), saw: {self.event.__class__.__name__} {self.event}")
        self.sequence_messages=sequence_messages
        self.sequence_messages_i=0 # to sequence through messages
        self.messages=messages
        self.last_value = None # yet
        self.invert=invert # Use True for typical "closed switch pulls-down, with idle=pull-up"
        self.as_boolean = as_boolean

        self.rate_limit = None
        if rate_limit:
            self.rate_limit = RateLimiter( rate_limit['max'], rate_limit['secs'] )
            self.flapping = rate_limit.get('flapping', 10*rate_limit['secs'] )
            self.flapping_timer = Timer( self.flapping )

        self.keepalive = keepalive

        self._latch = latch # to save to file
        self.latch = None
        if latch:
            self.latch = [False,True][ latch['dir'] ] # also, used as the flag: when != None
            self.latch_timer = Timer( latch['duration'] )
            debug(f"  latching active'ated to latch.dir: {self.latch}", level=2)


        # for env GPIOVALUES=
        self._igv_timer = Timer(0.00001) # may not get used
        #print(f"#### running? {self.pin} {self._igv_timer.running}")
        self._igv_timer.start() # so it expires first time
        #print(f"#### running? {self.pin} {self._igv_timer.running}")

    def setup(self):
        # True if we set it up
        if USEGPIO:
            pud=[ False, None, True ][self.pull + 1]
            debug(f"Pull (t=up,f=down,none=float) -> {pud}")
            try:
                if 'gpiozero' in globals():
                    debug(f".setup {self.pin}")
                    # hold_time is not used
                    self.button = Button(self.pin, pull_up=self.pull, bounce_time=0.1, hold_time=2)
            except gpiozero.exc.PinInvalidPin as e:
                debug(f"ERROR: Not a valid pin: {self.pin}")
                return False

            debug(f"Add GPIO Pin {self.pin}")

            # NB: the event handlers run in a separate thread, so _no_ asyncio!

            def on_down():
                debug(f"DOWN {self.pin}")
                self.loop.call_soon_threadsafe(
                    asyncio.create_task, self.publish_on_event(direction=1)
                    #, name="publish on down" 
                )
                debug(f"DOWN done {self.pin}",level=2)

            def on_up():
                debug(f"UP {self.pin}")
                self.loop.call_soon_threadsafe(
                    asyncio.create_task, self.publish_on_event(direction=0), # name="publish on up" 
                )
                debug(f"UP done {self.pin}",level=2)

            # not used yet:
            def on_held():
                debug(f"HELD {self.pin}")
                asyncio.create_task( self.publish_on_event(direction=1, force=True), name="publish on held" )
                debug(f"HELD done {self.pin}", level=2)

            # Handler on event

            if self.event == 0 or self.event == -1:
                # falling|both
                self.button.when_pressed = on_down
                debug("  setup on_pressed", level=2 )
            # [sic] not an else for "both"
            if self.event == 0 or self.event == 1:
                # raising|both
                self.button.when_released = on_up
                debug("  setup on_released", level=2 )

        return True

    async def publish_event(self, hit, read_fn, force=False, is_exact_value=False):
        """ Actually publish our event
            if `hit`
            with payload from read_fn(), substituting into config[ourpin].payload
            with rate_limiting, flapping protect, latching, invert_boolean
            `force` overrides `.rate_limit`
            `is_exact_value` uses value from read_fn() w/o fixups (booleanize etc)
        """
        # something to send?
        if (
            # actual event
            hit
            # flapping expired, we need to publish
            or (self.rate_limit and self.flapping_timer())  
            ):

            debug(f"Event on pin {self.pin}", level=2)

            if self.latch != None:
                # this has to be called to let it expire, even if rate-limited. we look at .running below
                self.latch_timer() # not testing it here (see below: self.latch_timer.running), just letting it expire

            # If we didn't have the debug() in the rate limit block below
            # we could move the rate-limit block here, and avoid some code (don't need to read pin if rate-limited), etc.

            value = read_fn()
            debug(f"  actual [{self.pin}] value {value}",level=4)

            def _invert_booleanize():
                # so we can use in debug statements too
                nonlocal value
                # invert for sending as last thing
                if not is_exact_value:
                    if self.invert:
                        debug(f"  inverting from {value} -> {not value}", level=2)
                        value = 0 if value==1 else 1
                    if self.as_boolean:
                        value = not not value

            # rate-limit? (should not rate-limit if flapping_timer expired because flapping_timer > rate-limit)
            # and, of course, don't rate limit if `force`
            if self.rate_limit and not force:
                if not self.rate_limit.allowed():
                    _invert_booleanize() # for debugging, invert/booleanize
                    debug(f"Rate-limited! [{self.pin}] {'inverted to this:' if self.invert else 'actual'} {value} ", level=2)

                    if not self.flapping_timer.running:
                        self.flapping_timer.start()
                    return  # rate-limited!

            # latch'ing active?
            if self.latch != None:
                debug(f"  latching active to latch.dir: {self.latch}", level=2)
                if self.latch_timer.running:
                    # we ignore changes away from `dir`
                    debug(f"  latched from `{value}` -> latch.dir: {self.latch} (no send)", level=2)
                    # don't send during latching, "during latch" implies last sent value was latch.dir
                    # and we would merely send latch.dir this time
                    return 
                else:
                    debug(f"  wasn't latching from `{value}` to latch.dir: {self.latch}", level=2)

                if value == self.latch:
                    # in the future, we _will_ ignore changes away from `dir` till latch_timer() expires
                    # (alway restart if == )
                    if not self.latch_timer.running:
                        debug(f"  future latching to latch.dir `{self.latch}` because saw matching: {value}", level=3)
                    self.latch_timer.start()

            
            _invert_booleanize() # as needed

            # final value!
            debug(f"  [{self.pin}] -> Value {value.__class__.__name__} {value}", level=2)

            # publish

            def _send(topic_payload):
                debug(f"## Send {topic_payload['topic']} {topic_payload.get('payload','')} v={value}...", level=3)
                payload = copy(topic_payload.get('payload',None))
                if isinstance(payload,str):
                    payload = re.sub(r'\$value', str(value), payload)
                    payload = json.dumps(payload)
                    debug(f"  ## str fixed up: {payload} <- v:{value.__class__.__name__} {value}", level=3)
                elif isinstance(payload,dict):
                    debug(f"  ## dict: {payload} <- v:{value.__class__.__name__} {value}", level=3)
                    if 'value' in payload:
                        payload['value'] = value
                    else:
                        if to_subst := next( (k for k,v in payload.items() if isinstance(v,str) and re.search(r'\$value',v) ), None ):
                            payload[to_subst] = re.sub(r'\$value', str(value), payload[to_subst])
                        elif to_subst := next( (k for k,v in payload.items() if v==None), None ):
                            payload[to_subst] = value
                    payload = json.dumps(payload)
                elif payload == None:
                    debug(f"  ## other: {payload} <- v:{value.__class__.__name__} {value}", level=3)
                    payload = value
                else:
                    # list, I guess
                    payload = json.dumps(payload)
                if self.last_value != value:
                    debug(f"  Async publish {topic_payload['topic']}: {payload.__class__.__name__} : {payload} <- was {self.last_value.__class__.__name__} {self.last_value}")
                asyncio.create_task( self.mqtt_client.publish( topic_payload['topic'], payload ), name=f"publish for {self.pin}" )

            # Send!

            if self.sequence_messages:
                # since "next", increment
                _send( self.messages[ self.sequence_messages_i ] )
                self.sequence_messages_i = ( self.sequence_messages_i + 1 ) % len( self.messages )
            else:
                # send all messages on event
                for topic_payload in self.messages:
                    _send(topic_payload)

            debug(f"  update last_value={value}", level=4)
            self.last_value = value
            #debug(f"Sent topics")

    async def publish_on_event(self, direction=None, force=False):
        """
            Publish our topic+payload for `direction`.
            direction=1|0 # actual button event, otherwise keepalive/GPIOVALUES
            force=True to read and publish current value
        """
        debug(f"Event for pin {self.pin} dir={direction} w/force={force}... {'using-gpio' if 'gpiozero' in globals() else 'no GPIO'}", level=2)

        def _value_to_publish():
            # returns hit bool, read-value fn
            # GPIO test/read (maybe force)
            if 'gpiozero' in globals():
                debug(f"  use GPIO w/force?={force}", level=4)
                read_fn = lambda: self.button.value if direction==None else direction

                if force:
                    debug(f"  force", level=3)
                    hit = True

                # actual detect
                else:
                    hit = direction != None
                    
            # NOGPIO
            else:
                hit,read_fn = ( force, lambda : choice((1,0)) )
                if force:
                    debug(f"  NOGPIO random...")

            return (hit, read_fn)

        hit, read_fn = _value_to_publish()
        debug(f"  [{self.pin}] hit={hit} force={force}",level=2)
        await self.publish_event(hit, read_fn, force=force)

    async def publish_on_keepalive(self):
        """
            Do keepalive loop, GPIOVALUES (once)
            So, start from parent loop
        """
        debug(f"Keepalive loop for pin {self.pin} ? {self.keepalive}.. {'using-gpio' if 'gpiozero' in globals() else 'no GPIO'}")
        if self.keepalive:
            pass

        while True:
            if self.keepalive:
                debug(f"Maybe keepalive [{self.pin}] = {self.last_value}",level=4)
                if self.last_value != None:
                    debug(f"  use_last={self.last_value}", level=3)
                    hit = True
                else:
                    # this shouldn't happen (see "# special case setup" below)
                    debug(f"  (don't) use_last={self.last_value}")
                    hit = False

                read_fn = lambda: self.last_value

                debug(f"[{self.pin}] Keepalive hit={hit}", level=2)
                # this resends "last value", and we don't want to re-invert etc
                await self.publish_on_event(force=True)
                debug(f"  SENT", level=4)
                await asyncio.sleep(self.keepalive)
            else:
                await asyncio.sleep(20)

    def xxx_do_gpio_stuff(self): # FIXME: not done

        def _parse_env_gpio_value():
            # if debugging, or testing on non-pi (env NOGPIO=1 ...)
            # then use the provided `env GPIOVALUES=$pin:$v,$v,$v,$pin:...`
            #   where $v is
            #       0|1 are as-if read pin values
            #       +n.n is seconds of delay between, nb: '+' disambiguates 1|0 second
            # Returns the next value for this pin: True|False|float . T|F are pin values, float is delay

            # FIXME: use mock gpiovalues?

            # We do GPIOVALUES once, at start. 

            if 'GPIOVALUES' not in globals():
                global GPIOVALUES

                env_val = os.environ.get('GPIOVALUES', None)
                GPIOVALUES={}
                if env_val != None:
                    # split into pin: v...,pin:, v...,pin: sets
                    p_m= re.split(r'(\d+):', env_val)
                    if len(p_m) < 2:
                        raise Exception(f"GPIOVALUES shoud =$pin:$v,...,$pin,$v.... Saw: {env_val}")
                    #print(f"### Splitted0 {p_m}")
                    p_m.pop(0) # extra ''
                    #print(f"### Splitted {p_m}")

                    # for the pairs
                    while p_m:
                        pin = p_m.pop(0)
                        values = p_m.pop(0).split(',')

                        if values[-1] == '':
                            # trailing '' if multiple pins in list
                            values.pop()
                        #print(f"### p {pin} : {values}")

                        GPIOVALUES[ int(pin) ] = [ (True if x=='1' else False) if x in ('0','1') else float(x)  for x in values ]

                debug( f"### GPIOVALUES parsed at {self.pin}:",GPIOVALUES )
                if GPIOVALUES and self.pin not in GPIOVALUES:
                    debug(f"  WARNING self.pin not in GPIOVALUES: [{self.pin}] GPIOVALUES {GPIOVALUES}")


        _parse_env_gpio_value() # need to setup GPIOVALUES global

    def cleanup(self):
        self.button.pin_factory.release_all( self.button )

    def __str__(self):
        return f"<PinSetup {self.pin} " + str(self.to_dict()) + ">"

    def to_dict(self):
        d = { "invert":self.invert, "pull":self.pull, "event":self.event, "keepalive":self.keepalive, "messages":self.messages, "latch" : self._latch, "as_boolean" : self.as_boolean, "sequence_messages" : self.sequence_messages }
        if self.rate_limit:
            d['rate_limit'] = { "max": self.rate_limit.max, "secs" : self.rate_limit.secs }
            if self.flapping != None:
                d['rate_limit']['flapping'] = self.flapping
        return d

class UIButtoner(agent.Agent):
    """
    This agent maps digital-pin transistions (e.g. switches) (on gpio-pins) to actions.
    When the pin-transition happens, we publish the topic with a payload.
    We use callbacks (gpiozer Button .when_pressed), but we might miss events
    See the configuration and defaults in `Example`.

    **This is all disabled till I figure out a better behavior:**
    Add:
        local/commands/ui/buttoner/add/<pin> { "topic" : "a topic", payload={...} }
        local/commands/ui/buttoner/add/<pin>/<topic...> {payload}
        If you omit the payload, we will send {} FIXME?
        You can have more than 1 topic:payload on a pin.
            but, a duplicate topic:payload is only sent once.
    Remove
        local/commands/ui/buttoner/remove/<pin> { "topic" : "a topic", "payload" : {...} }
        local/commands/ui/buttoner/remove/<pin>/<topic> {payload}
        local/commands/ui/buttoner/remove/<pin> { "topic" : "a topic", "all":True} # removes all payloads for the topic
    List
        local/get/ui/buttoner/pins -> local/events/ui/buttoner/pins [ {pin:x, topic:x},...]
        local/get/ui/buttoner/<pin> -> "" or "topic"

    The assignments of pin->topic are saved, and loaded automatically on startup.
    to state/agents/physical-button-action.json
    **End disabled**

    config/agents/physical-button-action.json
    Example:
        {
            "#" : "You can put in comments using '#' as the key",
            # Each pin gets one pin-setup, and possibly many messages
            "18" : { # Numbers are BCM pins, "BOARDnn" is physical numbering
                "#" : "You can put in comments using '#' as the key",
                "pull" : 0, # -1 for pull-down, 0 for none, 1 for pull-up. Default is 1: pull-up.

                "#" : "these parameters are in order of application:

                # Send on which edge-crossing. BEFORE invert
                # Note that for `0` we have to read the pin-value after the edge-detect,
                #   so it might not be what the edge-detect saw.
                # For -1|1 (fall|rise), we know the value, so don't have to read, 
                #   so it will be what edge-detect saw.
                "event" : 0, # -1 for falling, 0 for both, 1 for rising. Default -1: falling

                # send the last-value again, it it has been `keepalive` seconds since the last send
                # Nb: forces an initial read, and publish, of the gpio pin when we start
                # Nb: will be rate-limited
                "keepalive" : 60.0, # default is none: don't do keepalive

                # if rate-limit exceeded, will ignore transistions for `flapping` seconds, and then send current value
                # default is `null`: no rate-limit.
                "rate_limit" : { "max" : 1, "secs" : 10, "flapping" : 60 }, # `flapping` defaults to 10*secs

                # ignore transitions to the opposite of `dir` for `duration` seconds after a transition to `dir`
                # i.e. keep `dir` for at least `duration`. useful for oscillating values that go flatline on "off"
                # so, will not send a message if "during latch duration"!
                # nb: this also suppresses changes during the latch-duration
                #   e.g. given latching.dir=1, events 10101...latch-duration...10 
                #   will send a single True for the first 5 events,  then nothing, then a True for the last 2 events
                #   because the latch-duration started on the first `1`
                # you might want to combine with a rate-limit
                # `dir` is BEFORE invert'ing
                "latch" : { "dir" : 1, "duration" : 0.1 },  # null is default: no latching. dir=0|1

                # send the opposite of the read value. True for typical "closed switch shorts to ground, with pull-up"
                "invert" : false, # default is `false`: no invert

                "as-boolean" : false, # true will force values to `true`/`false`, default values are `1`/`0`

                # will substitute the pin value for $value, or {value:}
                "messages" : [ # default is no-messages. which seems useless.
                    { 
                        "topic": "local/devices/publisher/aio-expensive", 
                        "payload" : "$value" # none->button_value, $value gets substituted, {"value":x} x->button_value
                    }
                ]
            }
        }


    You can use GPIO.BCM numbering: integer,
    or board numbering as string: "BOARD40"
    (or any other gpiozero pin-numbering string).

    example debug values (nb. BCM numbers)
    env DEBUG=2 NOGPIO=1 GPIOVALUES=13:1,1,0,+0.5,0,0,1,0,0.3,0,0.3,0 agents/physical-button-action.py
    """
    mqtt_config = {
        "device" : "ui",
        "name" : "buttoner",

        "connection" : { # paho mqtt args
            "hostname" : "localhost",
            "port" : 1884,
            # client_id
            # tls...
            "clean_session" : True, # if a short enough down-time, resync to the commands
            "keepalive" : 8, # trigger Will
            "clean_start" : paho.client.MQTT_CLEAN_START_FIRST_ONLY,
        }
    }

    def __init__(self):
        super().__init__() # minimal setup

        self.assignments = {} # key:PinSetup() # read from our config and state

        # FIXME? should config always override the last state? i.e. predefined buttons
        changed = self.read_pin_assignment()
        #changed = self.load_json_config()
        #changed |= self.read_pin_assignment( self.state_file() ) # disabled till figured out
        debug(f"Setup {self.assignments}")

        # We rewrite the state file
        # which captures config setup.
        if changed:
            pass
            # self.save_state()

    def read_pin_assignment(self):
        # read them from a file
        # return true for changed

        changed = False

        debug(f"Read pin_assignments")

        pin_assignments = self.load_json_config()

        # empty file is giving []
        if pin_assignments == [] or pin_assignments == None:
            pin_assignments = {}

        for pin in list(pin_assignments.keys()): # `pin` is a string!
            x = pin_assignments[pin]

            # FIXME: if existing PinSetup, merge the messages
            # NB: the .mqtt_client has to be set, but "self" doesn't have it yet, see `loop`
            ps = PinSetup( pin, mqtt_client=None, **x )
            if ps.setup():
                changed = True
                self.assignments[pin] = ps
            else:
                debug(f"Skipped initial pin setup: {pin} : {x['messages']}")

        debug(f"Pin assignments {self.assignments}")

        return changed

    async def argparse(self):
        # if you need to handle/look at any argparse
        await super().argparse()

        # disbled (no arg)
        if False and self.parsed_args.list:
            with open(self.state_file(),'r') as fh:
                for l in fh:
                    sys.stdout.write(l)
            exit(0)

    def topic_for_add(self):
        return self.topic_for_command('add')
    def topic_for_remove(self):
        return self.topic_for_command('remove')
    def topic_for_list(self):
        return self.topic_for_get('pins')

    def topics(self):
        # FIXME: Getting called multiple times? cache it
        debug("Setup Topics")
        #debug(f"## pa {self.assignments}")
        pin_publishing = {}
        if self.assignments:
            pin_publishing[ 
                self.topic_for_event('#', with_device=False, with_id=False ) 
                ] = { "description" : f"# Pin configuration from {self.config_file()}" }
        for p,defn in self.assignments.items():
            #debug( f"  ## {p} : {defn}")
            for m in defn.messages:
                topic = m['topic']
                payload = m['payload']
                #debug(f"    ## {p} : {m}")
                pin_publishing[ topic ] = { "description" : f"<{p}> -> {topic} = {payload}" }
        #debug(f"## pub {pin_publishing}")

        return super().topics( {
            # listen
            self.topic_for_get('+') : { 
                "handler" : self.handle_local_get_pins
            },
            #self.topic_for_command('add/#') : {
            #    "handler" : self.handle_local_add_pin,
            #    "description" : "both: /add/<pin> payload,  /add/<pin>/<topic>"
            #},
            #self.topic_for_command('remove/#') : {
            #    "handler" : self.handle_local_remove_pin,
            #    "description" : "both: /remove/<pin> payload, /remove/<pin>/<topic>"
            #},

            # publish:
            self.topic_for_event("<pin>"): { "description": "Boolean for the pin when it changes" },
            **pin_publishing,
        })

    # each handler
    async def handle_local_remove_pin(self, message):
        # message.topic is a str, message.payload is json-decoded {...}, [...], or str (absence of payload is {})
        # /remove/<pin> {payload} # all:True
        # /remove/<pin>/<topic> {payload}

        base_len = len(self.topic_for_get().split('/') )
        tail = str(message.topic).split('/')[base_len:] # -> add / <pin> / ...
        pin = tail[1]
        rest = tail[2:]
        debug(f"ADD pin={pin} rest={rest}")

        if not re.match(r'\d+$', pin ):
            debug(f"Expected add/<int>/..., but saw {message.topic}")
        pin = int(pin)

        if pin not in self.assignments.keys():
            debug(f"Remove, but that pin has no assignment {pin}")
            return
        
        pin_assignments = self.assignments[pin]  # [ {topic:$topic},... ]

        if  rest:
            # add/<pin>/topic { payload }
            topic = '/'.join(rest)
            payload = message.payload
            all = False
        else:
            # add/<pin> { topic:, payload: }
            if not isinstance(message.payload, dict):
                debug(f"Expected payload dictionary for {message.topic}, but saw {message.payload.__class__.__name__} {message.payload}")
                return

            topic = message.payload.get('topic','')
            payload = message.payload.get('payload','')
            all = message.payload.get('all',False)
        
        # we only handle topic w/payload assignments right now
        # (i.e. no recipes, etc)

        # same topic+payload or all:
        old_assignment = {"topic":topic, "payload":payload}
        for i in list(reversed(range(0, len(pin_assignments)))):
            topic_payload = pin_assignments[i]
        raise Exception("IMPLEMENT")
        self.assignments[pin] = [tp for tp in pin_assignments if not (all or old_assignment == tp) ]
        changed = [ tp for tp in pin_assignments if not tp in self.assignments[pin] ]
        if not self.assignments[pin]:
            assignment = self.assignments.pop(pin)
            if USEGPIO:
                assignament.cleanup()

        debug(f"Removed {pin} -> {changed}")
        if changed:
            self.save_state()

    async def handle_local_add_pin(self, message):
        # message.topic is a str, message.payload is json-decoded {...}, [...], or str (absence of payload is {})
        # /add/<pin> {payload}
        # /add/<pin>/<topic>

        base_len = len(self.topic_for_get().split('/') )
        tail = str(message.topic).split('/')[base_len:] # -> add / <pin> / ...
        pin = tail[1]
        rest = tail[2:]
        debug(f"ADD pin={pin} rest={rest}")

        if not re.match(r'\d+$', pin ):
            debug(f"Expected add/<int>/..., but saw {message.topic}")
        pin = int(pin)

        if  rest:
            # add/<pin>/topic { payload }
            topic = '/'.join(rest)
            payload = message.payload
        else:
            # add/<pin> { topic:, payload: }
            if not isinstance(message.payload, dict):
                debug(f"Expected payload dictionary for {message.topic}, but saw {message.payload.__class__.__name__} {message.payload}")
                return
            if len(excess:=set(message.payload.keys()) - set(['topic','payload']) ):
                debug(f"Expected only topic,payload keys, saw extra {excess} in {message.topic} -> {message.payload}")
                return 

            topic = message.payload.get('topic','')
            if not topic:
                debug(f"No topic in payload {message.topic} -> {message.payload}")
                return
            payload = message.payload.get('payload','')
        
        # we only handle topic w/payload assignments right now
        # (i.e. no recipes, etc)

        if self.setup_pin( pin, topic, payload):
            self.save_state()

    
    def state(self):
        d = { str(x.pin) : x.to_dict() for x in self.assignments.values() }
        debug(f"state {d}")
        return d

    async def handle_local_get_pins(self, message):
        """
        .../pins -> dict of pin:config
        .../all -> read and publish each pin value
        .../$pinnumber -> that pin's config
        """
        # message.topic is a str, message.payload is json-decoded {...}, [...], or str (absence of payload is {})
        base_len = len(self.topic_for_get().split('/') )
        tail = str(message.topic).split('/')
        tail = tail[-1]
        #debug(f"TAIL {tail}")

        if tail == 'pins':
            payload = { str(pin):assignment.to_dict() for pin,assignment in self.assignments.items() }
            await self.mqtt_client.publish( self.topic_for_event('pins'), json.dumps( payload ) )
        elif tail == 'all':
            debug(f"## Read/pub all...")
            for assignment in self.assignments.values():
                debug(f"## Read/pub {assignment}...")
                await assignment.publish_on_event(force=True)
        elif re.match(r'\d+$', tail ):
            pin = tail
            debug(f"GET pin {pin}")
            debug(f"assign: {self.assignments}")
            actions = self.assignments.get(pin,[])
            await self.mqtt_client.publish( self.topic_for_event(str(pin)), json.dumps( actions.to_dict() ) )
        else:
            debug(f"Bad get/<pin>: {message.topic}")

    async def loop(self, first_time):

        # need to setup `mqtt_client` and asyncio loop in each PinSetup
        for assignment in self.assignments.values():
            assignment.mqtt_client = self.mqtt_client
            async_loop = asyncio.get_running_loop()
            assignment.loop = async_loop

        # Do GPIOVALUES at start (once)
        if gpio_values := os.environ.get('GPIOVALUES', None):
            debug(f"Has GPIOVALUES, do once... {gpio_values}")
            debug(f"DONE GPIOVALUES")

        has_keepalive = False

        # run keepalive
        async with asyncio.TaskGroup() as tg:
            for assignment in self.assignments.values():
                if assignment.keepalive:
                    debug(f"Has keepalive: [{assignment.pin}] {assignment.keepalive}")
                    tg.create_task( assignment.publish_on_keepalive() )
                    has_keepalive = True
            debug(f"  finished keepalive setup: has? {has_keepalive}")
                
        if not has_keepalive:
            debug(f"No keepalive")
            while True:
                await asyncio.sleep(20.000)

        raise Exception("Should never get here")

    async def cleanup(self, e):
        await super().cleanup(e)
        if USEGPIO:
            for assignment in self.assignments.values():
                assignment.cleanup()

    def argparse_add_arguments(self, parser ):
        parser.set_defaults(mqtt_config='config/mqtt-multi-config.py') # override

# RUN
# note that Agent.run() does argparse for '-h|--help' in the base .run()
UIButtoner( ).async_run() # internally does asyncio.run( agent.run() )
