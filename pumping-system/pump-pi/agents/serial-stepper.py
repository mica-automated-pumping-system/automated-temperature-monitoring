#!/usr/bin/env python3.11
"""
    Implements a reversed motor: config/agents/$0 { "reversed" : True }
"""

import asyncio
import sys,os
import re,time
from datetime import datetime

sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib-pip" )
import asyncio_mqtt as mqtt # to make Topic etc
import paho.mqtt as paho # some constants
import serial

sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib" )
from arduino_port import ArduinoPort
import agent
from simple_debug import debug
from every.every import Every,Timer

class SerialStepper(agent.Agent):
    """
    We are a relative stepper logic agent: using USB/Serial to talk to a realtime microcontroller.
    local/devices/stepper/$id|$name
    """

    def __init__(self):
        super().__init__()
        self.pump_ct = 0
        self.reversed = False

        self.target_queue = asyncio.Queue(10) # move to { pump: steps: hz: }, negative steps for backwards
        self.faster = 1.0 # a debug property you can set via mqtt, to run faster (or slower)

    mqtt_config = {
        "device" : "stepper",
        "name" : "pumpbox1", # fixme: in a config, then a per-config system.d

        "connection" : {
            "hostname" : "localhost",
            "port" : 1884,
            # client_id
            # tls...
            "clean_session" : True, # if a short enough down-time, resync to the commands
            "keepalive" : 8, # trigger Will
            "clean_start" : paho.client.MQTT_CLEAN_START_FIRST_ONLY,
        }
    }

    def argparse_add_arguments(self, parser ):
        parser.epilog = "env NOARDUINO=1 ... # Don't try arduino"

    async def argparse(self):
            config = self.load_json_config()
            if config:
                self.pump_ct = config['pump_ct']
                self.reversed = config['reversed']
                debug(f"{self.config_file()} {config}")
            else:
                debug(f"no {self.config_file()} {config}")
            await super().argparse()

    def interesting_topics(self):
        return super().interesting_topics() + [ 
           self.topic_for_command(),
           self.topic_for_command('+'),
           self.topic_for_command('stop'),
           self.topic_for_command(postfix='faster'),
        ]

    async def listener(self):

        # default for "get"
        await super().listener()

        # commands
        # local/commands/devices/stepper/pumpbox1
        # FIXME: should publish these as a guide to messages:

        asyncio.gather(
            # FIXME: the listen list and the interesting-topics list should be from the same structure...
            # { value: n }
            self._subscribe( self.topic_for_command(postfix='faster'), self.handle_set_value ),
            # stop
            self._subscribe( self.topic_for_command('stop'), self.handle_stop_command ),
            # { "steps":200,"hz":1600 }
            self._subscribe( self.topic_for_command('+'), self.handle_pump_command ),
            # {"pump":1,"steps":200,"hz":1600}
            self._subscribe( self.topic_for_command(), self.handle_generic_command ),
        )
        debug(f"Listening for commands")

    async def cleanup(self, e):
        debug("TERMINATING")
        await super().cleanup(e)

    async def handle_set_value(self, message):
        # To set some state of this agent:
        # local/commands/stepper/pumpbox1/faster { "value" : hz-multiplier } # for subsequent commands, debugging
        # Update `allowed` below for other/future state properties

        if not (m:= re.search(r'/([^/]+)$', str(message.topic))):
            debug(f"Unexpected set command {message.topic} -> {message.payload}")
            return

        if 'value' not in message.payload.keys():
            debug(f'no "value" key for set command {message.topic} -> {message.payload}')
            return
        value = message.payload['value']

        property = m.group(1)
        allowed = { 'faster':float }
        property_type = allowed.get(property,None)
        if not property_type:
            debug(f"Unexpected property for set command {message.topic} -> {message.payload} (allowed {allowed})")
            # FIXME: send and event w/error?
        if not isinstance(value,property_type):
            debug(f"Unexpected type of value for set command {message.topic} -> {message.payload} (allowed {allowed})")
            # FIXME: send and event w/error?

        if property_type==int:
            # tolerate float looking things for int
            value=int(value)
        
        debug(f"Set property {property}={value}")
        setattr(self, property, value)

    async def handle_generic_command(self, message):
        # pump # not in topic: local/commands/stepper/pumpbox1
        if 'pump' in message.payload.keys():
            # rebuild topic as if including pump#
            message.topic = mqtt.Topic( str(message.topic) + f"/{ message.payload['pump'] }" )
            #debug(f"command> {message.topic} {message.payload}")
            await self.handle_pump_command(message)
        else:
            debug(f"No handler for command (no pump: key) {message.topic} {message.payload}")

    async def handle_stop_command(self, message):
        # FIXME: this should cause us to emit a pos:$sofar, finished=True, but currently does pos=0
        self.target_queue.put_nowait( 'X' )

    async def handle_pump_command(self, message):
        # pump # in topic: local/commands/stepper/pumpbox1/1
        # We use the topic's pump-#
        if not (m:= re.search(r'/(\d+)$', str(message.topic))):
            debug(f"Unexpected pump command {message.topic} -> {message.payload}")
            return

        pump = m.group(1)
        if pump:
            def valid_steps():
                try:
                    message.payload['steps'] = int( message.payload['steps']) 
                    if abs(message.payload['steps']) > 2**31:
                        debug(f"  pump command abs('steps') too large (vs {2**31}): {message.payload['steps']}")
                        return False
                    return True
                except ValueError as e:
                    debug(f"  pump command 'steps' not an int: {e}")
                    return False
            def valid_hz():
                try:
                    message.payload['hz'] = float( message.payload['hz']) 
                    if message.payload['hz'] < 0.0 or message.payload['hz'] > 9999.9999:
                        debug(f"  pump command 'hz' negative, or too large (vs 9999.9999): {message.payload['hz']}")
                        return False
                    return True
                except ValueError as e:
                    debug(f"  pump command 'hz' not an float: {e}")
                    return False

            try:
                pump = int(pump)
                if pump < 1 or pump > self.pump_ct:
                    # FIXME: send mqtt error message
                    debug(f"  pump command pump-number too-small (<1) or too large (> {self.pump_ct}): {pump}")
                    return
            except ValueError as e:
                debug(f"  pump command pump-number not an int: {e}")
                return

            #debug(f"command pump[{pump}]: {message.payload}")
            # FIXME check types more. On bad, send mqtt message (feeback seems important)?
            if 'steps' in message.payload.keys() and 'hz' in message.payload.keys() and valid_steps() and valid_hz():
                # { pump: steps: hz: }
                self.target_queue.put_nowait( {**message.payload, 'pump':pump} )
            else:
                debug(f"  pump command expects steps: and hz:, saw {message.payload}")
        else:
            debug(f"No handler for pump command {message}")

    async def loop(self, first_time):

        # connect to arduino usb serial
        MaxResultLength = 2 + 10*7 # chars, for readline() timeout (+ EOL)
        MaxCommandLength = 30 # chars, for write() timeout (+ EOL) # still got a write timeout
        ResponseTimeout = 0.1 # arduino should never take longer than that to respond

        self.queue_a_message(
            self.topic_for_device( f"arduino" ),
            { "status" : "disconnected" }
        )

        # pyserial-asyncio

        # Try serial connect
        # We stall till it happens

        if os.environ.get('NOARDUINO',None)=='1':
            serialport = None
            debug(f"# NO ARDUINO serial should be None: {serialport}")
        else:
            serialport = ArduinoPort(
                    initial_connect=False,
                    timeout= 10.0/115200 * (MaxResultLength+1) + ResponseTimeout,
                    write_timeout = 10.0/115200 * (MaxCommandLength+1),
            ) # uses pyserial
            #debug(f"# ARDUINO serial: {serialport}")


        first_time = True
        if serialport:
            while(True):
                try:
                    serialport.reconnect()
                    if serialport.port:
                        # got it, go on
                        break

                    if first_time:
                        print("Waiting for identifiable arduino port")
                        first_time = False
                    await asyncio.sleep(1)
                except serial.serialutil.SerialException as e:
                    if 'could not open' in str(e):
                        debug("Retry serialport in 1 sec")
                        if first_time:
                            first_time = False
                        await asyncio.sleep(1)
                    else:
                        raise e

        self.queue_a_message(
            self.topic_for_device( f"arduino" ),
            { "status" : "connected" }
        )

        stepper = RemoteStepper(serialport, self.pump_ct, self.reversed)
        # FIXME: get our pump_ct from RemoteStepper, OR configure arduino from our's
        RemoteStepper.pump_ct = self.pump_ct # FIXME: have it get the ct from the arduino

        say_stats = Every(1)
        avg = 0 # was = speed
        worst=0
        exp = 4
        expw = 100
        ct=0

        # current target/status

        send_pos_interval = Every(1)

        def send_pos( pump ):
            def send(more={}):
                payload = pump.to_dict()
                if self.reversed:
                    payload['steps'] *= -1
                    payload['pos'] *= -1

                self.queue_a_message(
                    self.topic_for_event( str(pump.pump) ), 
                    # we count down from target
                    payload | more
                )
            if pump.just_finished():
                send( { "finished" : True } )
            elif pump.is_running():
                send( )

        ## main

        debug("Start polling")
        while(True):
                start = time.time()

                # Send commands to arduino
                if not self.target_queue.empty(): # we are only consumer
                    # steps: hz: pump:
                    target = await self.target_queue.get()

                    if isinstance(target,str):
                        # strings are just commands, do it
                        # must have a \n
                        target = target.rstrip()
                        target += '\n'
                        stepper.write_with_retry(target, retries=2, echo=True )
                        continue

                    # Everthing else is a "go to target"
                    # [ pump,hz,steps ]

                    if self.faster > 0 and self.faster != 1:
                        debug(f"Faster! {self.faster}")
                        target['hz'] *= self.faster

                    stepper.go( 
                        # pump, hz, steps
                        **target
                    )
                    # notify position: start
                    send_pos( stepper.motors[ target['pump']-1 ] )

                    send_pos_interval.start() # reset timer

                    if os.environ.get('DUMY'):
                        print(f"Goto {target}")

                # FIXME: if all finished, just heartbeat
                if stepper.all_finished():
                    stepper.heartbeat()
                    for pump in stepper.motors:
                        send_pos(pump) # handles finished=true

                else:
                    stepper.check_status()

                    # FIXME: if a motor goes to finish, send_pos
                    # else
                    # FIXME: for each motor != 0
                    if send_pos_interval():
                        #debug(F"Pos {stepper.positions}")
                        for pump in stepper.motors:
                            send_pos(pump) # handles finished=true
                    
                if stepper.check_alive() == False:
                    # just turned false
                    # FIXME: send a log message?
                    self.queue_a_message(
                        self.topic_for_device( f"arduino" ),
                        { "status" : "disconnected" }
                    )

                # FIXME: only if we did nothing
                if not stepper.serial_messages():
                    await asyncio.sleep(0.01)  # waiting too long can make messages pile up in serial

                # stats
                if say_stats():
                    ct += 1

                    elapsed = time.time() - start
                    avg = (exp-1) * avg / exp + elapsed/exp
                    if ct % 100 == 0:
                        print(f'\t\t\t\t## {avg}')
                    d_avg = avg-elapsed
                    if d_avg > worst:
                        print(f'{d_avg * 1e5}  {ct}')
                        worst = d_avg
                    else:
                        worst = (expw-1) * worst / expw + abs(d_avg)/expw


class Target:
    # a specific motor
    def __init__(self, pump, hz, steps ):
        self.pump=pump # 1 based numbering
        self.hz = hz
        self.steps = steps
        self.pos = 0
        # only goes true at .just_finished()
        # not included in to_dict()
        self.finished = True # i.e. not just finished

    def is_running(self):
        return self.pos != 0

    def just_finished(self):
        if self.pos == 0 and not self.finished:
            self.finished=True
            return True
        else:
            return False

    def to_dict(self):
        return { "pump":self.pump, "hz":self.hz, "steps":self.steps, "pos":self.pos }

    def __repr__(self):
        return str(self)
    def __str__(self):
        return str(self.to_dict() | {'finished':self.finished})
    def __format__(self, fmt):
        return str(self)

class RemoteStepper:
    # the remote can volunteer info

    def __init__(self, serial, pump_ct, reversed):
        self.serial = serial

        self.ping_interval = Every(0.8)
        self.ping_timeout = Timer(0.4) # should respond w/in that time
        self.live = True # has seen ping recently. True on connect
        self.check_status_interval = Every(0.1)
        self.reversed = reversed

        self.pump_ct = pump_ct # loaded at argparse time from config

        # FIXME: an each-motor class
        self.motors = [ Target(i+1,0,0) for i in range(self.pump_ct) ] 

        self.ping()

    def all_finished(self):
        return next( (False for m in self.motors if m.is_running()), True )

    def heartbeat(self):
        """check for alive"""
        if self.ping_interval():
            self.ping()

    def ping(self, print_echo=False):
        """Send ping request, start timeout
        """

        if self.write_with_retry( '#\n', echo=False ):
            # hope we start a ping longer intervals than ping_timeout
            self.ping_timeout.start()

    def saw_ping(self):
        self.ping_timeout.running = False
        self.ping_interval.start()
        self.live = True

    def check_alive(self):
        """False if we just turned not-alive,
        None if not-alive,
        True if recently proof of life
        """

        if self.live and self.ping_timeout():
            self.live = False
            print("FAIL arduino ping")
            return False # just turned false

        return None if self.live==False else True

    def write_with_retry(self, msg, retries=2, echo=True ):
        # return true if we managed to write, false or throw if more serious fail
        for i in range(retries):
            try:
                if self.serial: # debug mode if None
                    self.serial.write(msg)
                if echo:
                    print(f"< {datetime.now()} : {msg}")
                return True

            except serial.serialutil.SerialTimeoutException as e:
                time.sleep(0.1)
            except serial.serialutil.SerialException as e:
                if 'write failed' in str(e):
                    time.sleep(0.1)
                    self.serial.reconnect()
                else:
                    raise e
            except Exception as e:
                print(f"E: {e.__class__}")
                raise e

            return False

    def serial_messages(self):
        """process incoming messages. 
        Return true if we read something
        FIXME: put on a async task vs our polling loop, on listen tasks?
        """

        # OSError: [Errno 5] Input/output error on other app taking port
        # OSError: [Errno 5] Input/output error on physical disconnect
        if not self.serial:
            # debug mode if None
            self.saw_ping() # pretend ok

            # fake motor progress
            message = ''
            for m in self.motors:
                if m.finished:
                    break
                message += f"{m.pos}"
                dir = 1 if m.pos > 0 else -1
                m.pos = abs(m.pos) - abs(m.steps)/5
                if m.pos <= 0:
                    m.pos = 0
                m.pos *= dir
                time.sleep(0.25)
            if message != '':
                message = 'm ' + message
            else:
                return False

        else:
            if ( self.serial.port.in_waiting > 0 ):
                message = self.read_with_retry(2)
            else:
                return False

        # things that shouldn't timeout now
        if message != '':
            self.saw_ping() # anything counts

        if not message.endswith('\n'): 
            print(message + "...")
            return True

        # remove eol
        message = message.rstrip()

        # Messages
        
        if message.startswith("#"):
            # we did saw_ping() above
            # This might be a ping response, or just a status from arduino
            if len(message) > 1:
                print(f"> {datetime.now()} : {message}")

        elif message.startswith('m '): 
            # m [-]pos ... # for 0..pump_ct-1 
            if m:=re.findall(r'-?\d+', message): # crude
                # motor positions
                if not m:
                    print(f"Bad 'm' from arduino: {message}")

                else:
                    #debug(f"m {message} {m}")
                    for i,p in enumerate( m ):
                        # FIXME: check that we got all of them
                        self.motors[i].pos = int(p)
                        if self.reversed:
                            self.motors[i].pos *= -1
                    #if next( (True for x in self.motors if x.pos!=0), None):
                    #    debug(f"POS {self.motors}")

        else:
            # echo other
            print(f"?> {datetime.now()} : {message}")

        return True

    def read_with_retry(self, retries):
        """
        On timeout, we get nothing or a string w/o \r\n
        Danger, blocking
        """
        for _i in range(retries):
            try:
                response = self.serial.readline()
                waiting = self.serial.port.in_waiting
                if waiting > 0:
                    print( f"> (read {len(response)} waiting {waiting})" )
                return response.decode('utf8','strict')
            except serial.serialutil.SerialException as e:
                if 'device reports readiness' in str(e):
                    debug("TIMEOUT")
                    continue
                else:
                    raise e

        return None

    def go(self, pump,hz,steps): # set from a **dict
        motor_i = pump - 1 # index vs pump-number
        # G motor hz.hz +steps

        if self.reversed:
            steps *= -1

        command = f"G {int(motor_i)} {hz:.5} {'+' if steps>=0 else '-'}{abs(steps)}\n"
        debug(f"{datetime.now()} {command}")
        
        # FIXME: check for motor_ct
        self.motors[ motor_i ] = m = Target( pump, hz, steps )
        m.pos = steps
        m.finished = False

        self.check_status_interval.running = False
        self.write_with_retry( command )
        self.check_status_interval.start()

    def check_status(self):
        """Ask for status
        """
        if (self.check_status_interval()):
            self.write_with_retry( 'm\n', echo=False )

SerialStepper().async_run()
