async def stopper(a_step):
    # we are concurrent with the pause
    await asyncio.sleep(0.1) # mqtt round-trip
    await a_step.lib_agent.mqtt_client.publish( a_step.lib_agent.topic_for_stop(), '' )

recipe = parallel("Parallel Test",
    pause(0.2),
    pause(0.05),
    patch_recipestep_call( pause(0.2), stopper, concurrent=True)
)
