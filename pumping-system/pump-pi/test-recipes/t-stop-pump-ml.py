async def stopper(a_step):
    await a_step.lib_agent.mqtt_client.publish( a_step.lib_agent.topic_for_stop(), '' )
    await asyncio.sleep(0.1) # mqtt round-trip
    debug(f"stopper actions done")

recipe = patch_recipestep_call( comment("Comment"), stopper )
# Run ml-stepper with:
# env DEBUGSHORT=1 agents/ml-stepper.py

recipe = sequential("Set & pump test",
    steps_per_ml(1, 86.0),
    patch_recipestep_call( pump_ml(1, 200, 100), stopper)
)
