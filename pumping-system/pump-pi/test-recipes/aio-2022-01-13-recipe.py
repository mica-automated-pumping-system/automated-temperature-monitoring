"""Converted Recipe"""






recipe = parallel("aio-2022-01-13",

    # Pump 1
    sequential( "Pump 1",
      steps_per_ml(1, 86.0215),

      # MIX
      comment("mix p1 1 per op"),
      repeat( "mix p1 cycle {}", 1, 
          pump_ml( 1, ml=20.0, rate=2.0/60.0 ),
          comment("#FIXME: pause should announce its duration, all steps announce themselves"),
          pause( 300.0 ),
      
          repeat( "mix p1 increment {}", 20, 
              pump_ml( 1, ml=16.375, rate=2.0/60.0 ),
              pause( 300.0 ),
      
              repeat( "cycle p1 increment loop {}", 6.0, 
                  pump_ml( 1, ml=-10.0, rate=2.0/60.0 ),
                  pause( 60.0 ),
                  pump_ml( 1, ml=10.0, rate=2.0/60.0 ),
                  pause( 60.0 ),
              ),
      
              pause( 300.0 ),
              comment("cycle p1 increment loop end"),
          ),
      
          pause( 0.0 ),
      ),
      pause( 14400.0 ),
      
      # FILL
      comment("fill p1 347.5 in 30 cycles"),
      repeat( "fill p1 cycle {}", 30, 
          pump_ml( 1, ml=347.5, rate=2.0/60.0 ),
          pause( 14400.0 ),
      ),
      
      # CLEAN
      comment("clean p1 -347.5 in 30 cycles"),
      repeat( "clean p1 cycle {}", 0, 
          pause( 0.0 ),
          pump_ml( 1, ml=-0.0, rate=0.0/60.0 ),
      ),
      
    ),

    # Pump 2
    sequential( "Pump 2",

      steps_per_ml(2, 86.0215),

      # MIX
      comment("mix p2 1 per op"),
      repeat( "mix p2 cycle {}", 1, 
          pump_ml( 2, ml=20.0, rate=2.0/60.0 ),
          comment("#FIXME: pause should announce its duration, all steps announce themselves"),
          pause( 300.0 ),
      
          repeat( "cycle p2 increment {}", 20, 
              pump_ml( 2, ml=16.375, rate=2.0/60.0 ),
              pause( 300.0 ),
      
              repeat( "cycle p2 loop {}", 6.0, 
                  pump_ml( 2, ml=-10.0, rate=2.0/60.0 ),
                  pause( 60.0 ),
                  pump_ml( 2, ml=10.0, rate=2.0/60.0 ),
                  pause(  60.0 ),
              ),
      
              pause( 300.0 ),
          ),
      
          pause( 0.0 ),
      ),
      pause( 14400.0 ),
      comment("mix p2 end"),
      
      # FILL
      comment("fill p2 347.5 in 30 cycles"),
      repeat( "fill p2 cycle {}",  30, 
          pump_ml( 2, ml=347.5, rate=2.0/60.0 ),
          pause( 14400.0 ),
      ),
      
      # CLEAN
      comment("clean p2 -347.5 in 30 cycles"),
      repeat( "clean p2 cycle {}",  0, 
          pause( 0.0 ),
          pump_ml( 2, ml=-0.0, rate=0.0/60.0 ),
      ),
      
    ),

    # Pump 3
    # off

    # Pump 4
    # off

    # Pump 5
    # off

    # Pump 6
    # off
)
