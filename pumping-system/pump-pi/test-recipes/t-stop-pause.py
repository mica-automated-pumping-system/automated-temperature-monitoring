# to test stopping due to a stop message: 
#   tools/run-dsl-recipe.py $thisrecipe && cat state/tools/run-dsl-recipe/$thisrecipe.json
# to test resume (from above stop):
#   env NOPATCHSTEP=1 tools/run-dsl-recipe.py --resume $thisrecipe # observe the final progress

async def stopper(a_step):
    # This sends .../stop, to cause a stop of our recipe, at the step we wrap with patch_recipestep_call
    # NB: concurrent task so that the step's pause can start
    debug(f"_Stopper called!")
    #raise Exception("STOP")
    await asyncio.sleep(0.1) # start pause
    await a_step.lib_agent.mqtt_client.publish( a_step.lib_agent.topic_for_stop(), '' )
    # we are concurrent, so we just trust that the mqtt round-trip will finish before the target pause() finishes

recipe = patch_recipestep_call( pause(0.2), stopper, concurrent=True )
