#!/usr/bin/env -S sh -c 'exec "$(dirname $(realpath $(dirname $0)))/.venv/bin/python3" "$0" "$@"'

# try --help

import asyncio
import sys,os,json,re,argparse
from copy import copy
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib" )
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib-pump-pi" )
from simple_debug import debug
import paho.mqtt as paho # some constants|--help'
import mqtt_config_reader
import agent

# write your
class MQTTAction(agent.Agent):
    """
    on mqtt topic/value, run command(s).

    Meant to be used like something from agents/, but we require mqtt-config, topic, action arguments.
    So, not a standalone agents/*.

    Note the 2 invocation styles.
    """

    mqtt_config = {
        "device" : "ssh-tunnel",
        "name" : "<tbd>", # filled in from config

        # 'connection' : {...} a paho-mqtt config, default is config/mqtt-local-config.py
        # also see config/mqtt-aio-config.py
        # see https://sbtinstruments.github.io/aiomqtt/configuring-the-client.html
        # and pip /aiomqtt/client.py to figure out details
    }

    def __init__(self):
        # ... if you want
        super().__init__() # minimal setup
        #print( f"deal with args... {args}, argparse has not happened yet")

    async def argparse(self):
        # if you need to handle/look at any argparse
        await super().argparse()

        if getattr(self.parsed_args,'json_help', None):
            print(f"""# action .json file:
               {{
                    "mqtt_config" : "connect to this broker: `key` of a config in the {getattr(self.parsed_args,'mqtt_config','?')} # try --mqtt-config-help",
                    "actions" : [ # Can have multiple topic/value/command
                        {{
                        "topic" : "mqtt topic to listen on",
                        "value" : "exact value of entire payload, that must match",
                        "command" : [ "cli-command", "args..."],
                        "#" : "command may be just "command" instead of []",
                        "#" : "command must be absolute path, OR start with WORKINGDIR/ for relative to this file",
                        }}
                    ]
                }} 
            """)
            exit(0) # appears to be the right thing here

        if getattr(self.parsed_args,'json_config', None):
            with open( self.parsed_args.json_config,'r') as fh:
                self.action_config = { "mqtt_config" : "local", **json.load(fh) }
                debug(f"Config from {self.parsed_args.json_config}:\n{self.action_config}")

        else:
            self.action_config = {
                "mqtt_config" : self.parsed_args.mqtt_config,
                "actions" : [
                    {
                    "topic" : self.parsed_args.topic,
                    "value" : self.parsed_args.value,
                    "command" : self.parsed_args.command,
                    }
                ]
            }
        debug(f"Use mqtt_config[ '{self.action_config['mqtt_config']}' ]")

        # some sanity checks
        # check `topic` for valid aio format: f[eeds]/x | g[groups]/x.y
        for i,an_action in enumerate(self.action_config['actions']):
            # fixup
            if an_action["value"] == '':
                # because we do a hack'ish thing and convert empty payloads to {}, allow '' as the payload:
                    an_action["value"] = {} 
            if isinstance(an_action['command'],str):
                # normalize to [ command, args... ]
                an_action['command'] = [ an_action['command'] ]

            # for systemd compatibility
            # Fixup the args to be absolute
            an_action['command'] = [
                re.sub(f'^WORKINGDIR/', os.path.abspath(os.path.dirname( sys.argv[0] ) + "/../")+"/", arg ) 
                for arg in an_action['command']
            ]

            # sanity
            if not an_action['command'][0].startswith('/'):
                raise Exception(f"`command` must be absolute path (try WORKINGDIR to interpolate) saw [{i}] topic={an_action['topic']} : {an_action['command']}")

        #debug(f"Config {self.action_config}")

    def read_mqtt_config(self):
        super().read_mqtt_config() # uses [0]'th mqtt-config

        # Using the config/agents/$json-config's ['mqtt-config'] string as the name
        # Pick the config from the --mqtt-config 
        self.__class__.mqtt_config['connection'] = self.mqtt_config['connections'][ self.action_config['mqtt_config'] ]
        # debug(f"#### conf {self.__class__.mqtt_config['connection']}")
        self.__class__.mqtt_config['name'] = MQTTAction.mqtt_config['connection']['hostname']

    def argparse_add_arguments(self, parser ):
        parser.set_defaults(mqtt_config='config/mqtt-multi-config.py')

        # mqtt-broker, topic, value-pred, ssh-target-w/params

        def add_usingjson(subparser):
            subparser.add_argument('json_config', help="json w/mqtt-config-file and (topic,value,action) tuples. try --json-help")
            subparser.add_argument('--json-help', help="show format of .json", action='store_true')
            
        # fixme: obsolete? rework
        def add_usingcli(subparser):
            subparser.add_argument('mqtt_config', help="config/$x.py that defines a `connection`. try --mqtt-config-help")
            subparser.add_argument('command', help="command to execute")
            subparser.add_argument('arguments', help="command to execute", nargs='*', default=[])
            subparser.add_argument('--topic', help="topic to listen to", type=str, required=True)
            subparser.add_argument('--value', help="payload value to trigger on", type=str, required=True)

        # Manually do "subparsers" because we aren't using a "command" to distinquish them
        #debug(f"## argv {sys.argv[1:]}")
        if next( (x for x in sys.argv[1:] if x=='--help' or x=='-h'), None):
            print(self.__doc__)

            parser = argparse.ArgumentParser(description="# Specify one topic/action via cli args:")
            add_usingcli(parser)
            parser.print_help()

            print("\n")
            parser = argparse.ArgumentParser(description= '# Specify several via the .json (try --json-help)')
            add_usingjson(parser)
            parser.print_help()

            print("\n# Common")
            return

        elif next( (x for x in sys.argv[1:] if '.json' in x or x=='--json-help'), None):
            if next( (x for x in sys.argv[1:] if x=='--json-help'), None):
                sys.argv.append( "/dev/null" ) # need dumy for parser
            #debug(f"argv: {sys.argv[1:]}")
            add_usingjson(parser)
        else:
            add_usingcli(parser)

    def topics(self):
        """Add to topics that are useful to print out for --topics"""
        # not default topics
        published_topics = {}
        if 'parsed_args' in dir(self):
            for an_action in self.action_config['actions']:
                # aio requires a hack to update the username prefix
                if 'username' in self.mqtt_config['connection']:
                    an_action['topic'] = re.sub(r'\$IO_USERNAME', self.mqtt_config['connection']['username'], an_action['topic'])
                published_topics[ an_action['topic'] ] = {
                    "description" : f"On {json.dumps(an_action['value'])}, run {an_action['command']}",
                    "handler" : self.handle_action_on_pred
                }
        else:
            published_topics["<--topic $x>"] = { "description" : "from the .json" } # I don't think this can happen anymore

        return super().topics( published_topics, include_default=False )

    async def advertise(self ):
        # publishing an non-ai-format topic will cause a disconnect!
        pass # don't

    def last_will_payload(self):
        # because our primary mqtt connection is AIO, no last-will
        # fixme: maybe, pick a topic
        return None

    async def listener(self):
        # optional
        # subscribes to topics, assigning handlers
        # We `await yourhandler`, so other messages won't be handled till you finish.
        # Maybe use asyncio.create_task() to spawn long-running things.
        # Thus, by default, there are no race-conditions between handlers,
        # (but there would be if you create_task())
        
        # no default topics

        # your stuff:
        for an_action in self.action_config['actions']:
            # aio requires a hack to update the username prefix
            if 'username' in self.mqtt_config['connection']:
                an_action['topic'] = re.sub(r'\$IO_USERNAME', self.mqtt_config['connection']['username'], an_action['topic'])
            debug(f"Action {an_action}")

            topic = an_action['topic']
            debug(f"# listen on `{topic}` for `{an_action['value']}`")
            await self._subscribe( topic, self.handle_action_on_pred ) 

    # each handler
    async def handle_action_on_pred(self, message):
        # message.topic is a str, message.payload is json-decoded {...}, [...], or str (absence of payload is {})
        # payload could have anything: we just invoke the pred to check
        # and run the command

        debug(f"SAW {message.topic}='{message.payload}'")
        #debug(f"Consider {[ x['topic'] for x in self.action_config['actions'] ]}")
        debug("possible: ",[ f"{x['topic']}={x['value'].__class__.__name__} {x['value']} ? {x['value'] == message.payload}" for x in self.action_config['actions'] if x['topic'] == message.topic])
        if an_action:=next( (x for x in self.action_config['actions'] if x['topic'] == message.topic and x['value'] == message.payload), None):

            debug(f"HIT {an_action}")
            asyncio.create_task( self.launch_command(message, an_action) )
        else:
            debug("Ignored")

    async def launch_command(self, message, action ):
        # we do not attempt to manage the process/command
        # write a script to manage, and use that as the command

        debug(f"# Launch : {message.topic} = {message.payload} -> {action['command']}")

        proc = await asyncio.create_subprocess_exec(*action['command'])
        debug(f"# Launched [pid={proc.pid}]")

        await proc.wait()
        debug(f"# EXITED [pid={proc.pid}]: {message.topic} = {message.payload} -> {action['command']}")
        
    async def loop(self, first_time):
        # required.
        # have to busy wait if you want to do nothing

        #setup
        #start async work stuff

        print("Waiting, ^C to exit")
        while(True):
            await asyncio.sleep(1)

    async def cleanup(self, e):
        await super().cleanup(e)

        print(f"do your cleanup, like GPIO.cleanup(), because we are exiting, because {e}")

# RUN
# note that Agent.run() does argparse for '-h|--help' in the base .run()
MQTTAction().async_run() # internally does asyncio.run( agent.run() )

'''
How To Exit
To do a clean exit, which nicely kills off an async tasks, and sends the 'quit' status.
Should be able to call this from any `def async`.

await self.exit($statuscode, $exception-if-you-have-it)
return # uh... cause this whole chain of awaitables to die...?
'''
