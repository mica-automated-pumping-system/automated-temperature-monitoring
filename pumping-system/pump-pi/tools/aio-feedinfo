#!/usr/bin/env python3.11
"""
utils about aio, e.g. what feeds have notifications
"""

import subprocess,json,argparse
from datetime import datetime
import sys,os
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib" )
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib-pump-pi" )
from simple_debug import debug
from aio_key import aio_key

parsed_args=None

def run_cmd(cmd):
    if parsed_args.noop:
        print(f"[noop] {cmd}")
    else:
        subprocess.run(cmd)

def get_feed_info_file(which_aio, alt_aio, feed_info_file):
    aio_config = next( (x for x in ( which_aio, alt_aio ) if os.path.exists(x) ), None)
    if not aio_config:
        sys.stderr.write(f"Expected an existing AIO key file: {[x for x in ( which_aio, alt_aio ) if x != None]}\n")
        sys.exit(1)
    
    print(f"# Using {aio_config}")
    aio_config = aio_key( aio_config )
    print(f"# IO_USERNAME {aio_config['IO_USERNAME']}")
    cmd = ['curl', '-o',  feed_info_file + ".x", 
        f"https://io.adafruit.com/api/v2/{aio_config['IO_USERNAME']}/feeds?x-aio-key={aio_config['IO_KEY']}"
    ]
    run_cmd(cmd)
    run_cmd(["bash", "-c", f"json_pp < {feed_info_file}.x > {feed_info_file}"])
    print(f"# Fetched {feed_info_file}")

def find_notifiers(feed_info_file):
    if parsed_args.noop and not os.path.exists(feed_info_file):
        sys.stderr.write(f"[noop] No cache file, so can't print info: {feed_info_file}\n")
        return

    with open(feed_info_file, 'r') as fh:
        feed_info = json.load(fh)
    
    for feed in feed_info:
        if feed['status_notify']:
            #print(feed['key'], feed['feed_status_changes'])
            if schange := feed['feed_status_changes']:
                schange = schange[0] # latest
                change_info = f"last email {schange['created_at']} {schange['from_status']} -> {schange['to_status']}"
            else:
                change_info = "no notifications"
            print( f"{feed['key']:30} {feed['status']} {change_info}" )

parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument('--force', help="fetch fresh info, default is to reuse cache for 5 minutes", action='store_true')
parser.add_argument('--config-dir','--config', help="directory of the aio-key, and cache (default=config, but default cache is in log/)", default='config')
parser.add_argument('--aio-key','--aio', help="an aio key file (with IO_USERNAME=, IO_KEY=), default is $config/aio.key OR $config/aio.key.secret")
parser.add_argument('--aio-user', help="aio key file by just user part: $config/aio-{user}.key file, default per --aio-key")
parser.add_argument('--noop','-n', help="say commands but don't do them", action='store_true')
parsed_args = parser.parse_args()

# figure out files by config-dir and multiple possible options
aio_by_user = "aio-{}.key"
alt_aio_key = None
if parsed_args.aio_key:
    # this is the value that is used
    pass
elif parsed_args.aio_user:
    parsed_args.aio_key = parsed_args.config_dir + "/" + aio_by_user.format(parsed_args.aio_user) 
    alt_aio_key = parsed_args.aio_key + ".secret"
if not parsed_args.aio_key:
    parsed_args.aio_key = f"{parsed_args.config_dir}/aio.key"
    alt_aio_key = parsed_args.aio_key + ".secret"

feed_info_file="{}/aio-feed-info.json"
if parsed_args.config_dir == 'config':
    feed_info_file = feed_info_file.format('log')
else:
    feed_info_file = feed_info_file.format(parsed_args.config_dir)

if os.path.exists(feed_info_file):
    cache_age = datetime.now() - datetime.fromtimestamp(os.path.getmtime(feed_info_file))
else:
    cache_age = datetime.now() - datetime(1,1,1)

if (cache_age.seconds > 5 * 60) or parsed_args.force:
    print(f"cache out of date: GET")
    get_feed_info_file( parsed_args.aio_key, alt_aio_key, feed_info_file)
else:
    print("# using cache (try --force)")
find_notifiers(feed_info_file)
