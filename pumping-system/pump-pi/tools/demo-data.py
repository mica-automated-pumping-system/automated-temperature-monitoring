#!/usr/bin/env -S sh -c 'exec "$(dirname $(realpath $(dirname $0)))/.venv/bin/python3" "$0" "$@"'

"""Loads a file that uses our DSL to emit demo-data. See the class doc-string, or:
    Try `tools/demo-data.py --help` for info on the DSL.
"""

import asyncio
import sys,os,json,re,subprocess
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib" )
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib-pump-pi" )
from simple_debug import debug
import paho.mqtt as paho # some constants|--help'
import aiomqtt as mqtt # for errors
import agent

class DemoData(agent.Agent):
    """Runs demo-data via a DSL

    Run this manually, or from e.g. tools/agent-mqtt-action.py or agents/physical-button-action.py
    Have some agent send the `revert_on` message to exit (see `replace_systemd_unit()`).

    e.g. `tools/demo-data.py config/demo_data.py`

    To run at start-up ("permanent demo mode"):

    * create `agents/startup-demo-mode.py` (must have `.py`)
        #!/bin/sh
        exec tools/demo-data.py config/demo_data.py
    * `chmod u+x agents/startup-demo-mode.py` # must be executable
    * `(cd systemd.units/; make dev)` # creates the .service file
    * `ln $(pwd)/systemd.units/agents__startup-demo-mode.py.service ~/.config/systemd/user` # "add" to systemd
    * `systemctl --user start agents__startup-demo-mode.py.service` # start
    * the log is in `log/agents/startup-demo-mode.py.log`
    * Try `tools/demo-data.py config/demo_data.py --topics`
    """

    # lifecycle is:
    # SomeAgent()
    # .argparse()
    # .read_mqtt_config()
    # if --help, show help & exit
    # connect with `will` based on .description
    # taskgroup.create_task( .listener( ) )
    #       .topics() # register topics/handlers
    # taskgroup.create_task( .advertise() )
    # taskgroup.create_task( .loop() ) # after mqtt-connect, and above
    # await .cleanup(exception) if we catch an exception
    #   always call super().cleanup(exception) if you override

    mqtt_config = {
        "device" : "demo-data", # advertise's topic = local/devices/$device/$name
        "name" : "somefile", # replaced by file

        # 'connection' : {...} a paho-mqtt config, default is config/mqtt-local-config.py
        #       "mdns_service" : "amps-pi_mqtt_broker", to resolve mdns by service name
        # override/setup in argparse():
        # ThisClass.mqtt_config['connection'] = mqtt_config_reader.connection( self.action_config['mqtt_config'] )
        #   where mqtt_config could = "config/mqtt-local-config.py
        # ThisClass.mqtt_config['name'] = MQTTAction.mqtt_config['connection']['hostname']

        # also see config/mqtt-aio-config.py
        # see https://sbtinstruments.github.io/asyncio-mqtt/configuring-the-client.html
        # and pip aiomqtt/client.py to figure out details
    }

    def __init__(self):
        # ... if you want
        super().__init__() # minimal setup
        self.demo_config_topics = {} # topics w/interval,file,data to generate

    async def argparse(self):
        # if you need to handle/look at any argparse

        # read the demo-data-config now, so --help and --topics can see it

        # hack: allow --help w/o a config file
        if len(sys.argv) == 2 and sys.argv[1] == '--help':
            sys.argv.append('/dev/null')

        await super().argparse(autohelp=False)
        self.__class__.mqtt_config['name'] = os.path.splitext(os.path.basename(self.parsed_args.demo_config))[0]

        self.read_demo_data_config( self.parsed_args.demo_config )

        if self.parsed_args.help:
            await super().argparse()

    def dsl_replace_systemd_unit(self, *names, revert_on=None):
        """On startup, stop these systemd units.
        On `revert_on`=$topic, start them (if they were running), and exit
        """
        was_running = []
        for name in names:
            if subprocess.run(['systemctl', '--user', 'is-active', name]).returncode == 0:
                was_running.append( name.rstrip() )
                subprocess.run(['systemctl', '--user', 'stop', name], check=True)
        debug(f"systemd running {was_running}")
        
        async def _revert(message):
            for name in names:
                debug(f"systemd restart {name}")
                subprocess.run(['systemctl', '--user', 'start', name], check=True)

            # $INVOCATION_ID exists if we are under systemd (systemctl start). Get our name and stop us
            if (iid:=os.getenv('INVOCATION_ID', None)) and (proc:=subprocess.run(['tools/unit-by-invocation', '--user', iid], capture_output=True)).returncode == 0:
                subprocess.run(['systemctl','--user','stop', proc.stdout.rstrip()])
                debug("Stopping by systemd unit because of revert message. Will exit...")
                await asyncio.sleep(2) # we should die
            else:
                debug("Stopping by exit")
                
            await self.exit(0)
            
        # revert on topic
        if revert_on:
            self.demo_config_topics[ revert_on ] = {
                "description" : f"Quit demo mode and restore normal system on: {revert_on}",
                "handler" : _revert
            }

    def required_args(self, topic, interval, file, data ):
        """check for required args"""
        fail = []
        if interval==None or interval<=0:
            fail.append( "`interval` must be >0")
        if file==None and data==None:
            fail.append( "One of `file` or `data` is required")
        if file and not os.path.exists(file):
            fail.append( f"No data file: {file}" )

        if fail:
            msg = "\n\t".join(fail)
            raise Exception(f"Required args for setting up generation on topic '{topic}':\n\t{msg}")

    def dsl_publishing(self, topic, interval=None, file=None, data=None):
        """Registers a topic to start publishing on
        every `interval` seconds
        using data from `file` (some csv), OR literal `data`
        """
        debug(f"Publish on {topic} every {interval} using {file}{data}")
        self.required_args(topic, interval=interval,file=file,data=data)
        self.demo_config_topics[ topic ] = {
            "description" : f"Generate fake data every {interval} from { file or data }",
            # [0][1] for start|change_back, change_on
            "publishing" : [ {"interval":interval,"file":file,"data":data} ], # [1] is added by `change_on`
            "generators" : [ self._generator(topic, interval, file, data) ],
            "publishing_i" : 0, # alternates with 1 via `change_on` & `change_back_on`
        }

        return self

    def dsl_change_on(self, topic, interval=None, file=None, data=None):
        """Changes the `interval` & `file`|`data`
        when `topic` is seen
        other args same as publishing()
        default values are taken from `publishing(...)`
        Chains: `publishing(...).change_on(...)`
        You probably want a `change_back_on()`
        """
        # this is a dumy

    def handle_change_on(self, topic, which):
        """Switch generators for topic to [which]
        """
        self.demo_config_topics[topic]['publishing_i'] = which

    def change_on(self, topic, interval=None, file=None, data=None):
        """the actual chained method for dsl_change_on"""
        # chaining is a hack

        previous_topic = list(self.demo_config_topics.keys())[-1]
        previous_data = self.demo_config_topics[previous_topic]['publishing']

        new_data = {}
        new_data["interval"] = interval or previous_data[0]['interval']
        if file or data:
            new_data["file"] = file
            new_data["data"] = data
        else:
            new_data["file"] = previous_data[0]['file']
            new_data["data"] = previous_data[0]['data']
            
        self.required_args(topic, **new_data )
            
        previous_data.append( new_data )

        debug(f"When {topic}, Publish on {previous_topic} {new_data}")
        async def _change_helper(message):
            self.handle_change_on(previous_topic, 1)

        self.demo_config_topics[ topic ] = {
            "description" : f"Change {previous_topic} to every {new_data}",
            "handler" : _change_helper, 
        }

        self.demo_config_topics[ previous_topic ]['generators'].append( self._generator(topic, **new_data) )
        return self

    def dsl_change_back_on(self, topic):
        """Changes back to the original `interval` & `file`|`data`
        when `topic` is seen
        Chains: `publishing(...).change_on(...).change_back_on(...)`
        """
        # this is a dumy

    def change_back_on(self, topic):
        """the actual chained method for dsl_change_back_on"""
        # a hack
        previous_topic = list(self.demo_config_topics.keys())[-2] # 2 back
        previous_data = self.demo_config_topics[previous_topic]

        debug(f"When {topic}, change_back to {previous_topic} and {previous_data}")
        async def _change_helper(message):
            self.handle_change_on(previous_topic, 0)
        self.demo_config_topics[ topic ] = {
            "description" : f"Change back to {previous_topic} {previous_data['publishing'][0]}",
            "handler" : _change_helper, 
        }

    def read_demo_data_config(self, demo_config):
        debug(f"DEMO: {demo_config}")

        # build environment
        def json_load(f):
            with open(f,'r') as fh: 
                return json.load(fh)

        environment = {
            'os' : os,
            'sys' : sys,
            'json_file' : json_load,
            'topic_for_command' : self.topic_for_command,
        }
        # collect dsl_* methods
        dsl_help = [ "# DSL for recipes", "# these are imported into your recipe","" ]
        for m_name in dir(self):
            if m_name.startswith('dsl_') and callable(method:=getattr(self,m_name)):
                dsl_name = re.match(r'dsl_(.+)', m_name).group(1)
                environment[dsl_name] = method
                import inspect
                dsl_help.append( f"{dsl_name}{inspect.signature(method)}\n\t{method.__doc__}" )
        agent.HelpEpilog += "\n".join(dsl_help)
        #debug(f"##HE {agent.HelpEpilog}")

        with open(demo_config, mode='r') as fh:
            code = fh.read()
            compiled_recipe = compile( code, demo_config, mode='exec')
            code = None

        # executes the dsl, populates `rules`
        exec( compiled_recipe, environment )

    def topics(self):
        """
        For the typical pattern, list topics+handler and they'll show up for --topics and be subscribed.
        Automatically adds the handler's docstring as `description:`.
        To inhibit showing up in --topics, use "interesting":False.
        To document as a published topic, omit "handler".
        To add help text in the Listener section, "handler" : None.
        """
        return super().topics( self.demo_config_topics )

    def argparse_add_arguments(self, parser ):
        # add your argparse arguments, override mqtt_config, etc
        parser.set_defaults(mqtt_config='config/mqtt-multi-config.py') # override --mqtt-config
        parser.add_argument('demo_config', help=".py file using DSL")

    # async def advertise(self ):
        # override for initial advertise, e.g. "don't"
        # probably override last_will_payload() too
        # pass # don't

    # def last_will_payload(self):
        # override
        # e.g. return None for no last_will, nor say_quit()
        # return None

    # each handler
    async def handle_local_get_devices(self, message):
        # message.topic is a str, message.payload is json-decoded {...}, [...], or str (absence of payload is {})
        raise Exception("Implement things like me")

    async def _generator(self, topic, interval, file, data):
        """generate the next value after an interval
            `topic` for logging/debug 
            use: 
                g=this(...)
                in some loop
                    value = await anext( g )
        """
        debug(f"## Generator for {topic} {file or data}", level=4)
        if file:
            # Repeat the file contents
            while True:
                # Each value
                with open(file,'r') as fh:
                    for value in fh:
                        # when switching to the next generator, use it's delay for the interval
                        # (rather then emit immediately)
                        await asyncio.sleep(interval)
                        yield value.rstrip()
        else:
            # same value forever
            while True:
                await asyncio.sleep(interval)
                yield data


    async def loop(self, first_time):
        # required.
      # have to busy wait if you want to do nothing

        async def _publish(topic):
            if topic not in self.demo_config_topics:
                raise Exception(f"Not a topic in demo_config_topics (!!!) '{topic}' see {sorted(list(self.demo_config_topics.keys()))}")
            if 'publishing' not in self.demo_config_topics[ topic ]:
                raise Exception(f"No [publishing] in demo topic '{topic}': {self.demo_config_topics[ topic ]}")

            debug(f"## Will publish {topic} for {self.demo_config_topics[ topic ]['publishing']}")

            i=None
            try:
                while True:
                    # `i` can change each time we get a value, so a different `g`
                    i = self.demo_config_topics[ topic ]['publishing_i']
                    g = self.demo_config_topics[ topic ]['generators'][i]
                    #debug(f"## Generate {topic}[{i}] for {g}")
                    value = await anext( g )
                    debug(f"Generate {topic}[{i}] for {g} = {value}", level=4)
                    self.queue_a_message(topic, value)
            except StopAsyncIteration:
                debug(f"## Ran off end: generator for {topic}[{i}] {self.demo_config_topics[ topic ]['publishing'][i]}")

        async with asyncio.TaskGroup() as tg: 

            for topic in self.demo_config_topics.keys():
                # only the ones we generate for, which have a ['publishing'] entry
                if 'publishing' in self.demo_config_topics[topic]:
                    debug(f"## Generator start {topic}", level=3)
                    tg.create_task( _publish(topic) )
            
            while(True):
                sys.stdout.write("."); sys.stdout.flush()
                await asyncio.sleep(1)

    async def cleanup(self, e):
        await super().cleanup(e)

        print(f"do your cleanup, like GPIO.cleanup(), because we are exiting, because {e}")


# RUN
# note that Agent.run() does argparse for '-h|--help' in the base .run()
DemoData( ).async_run() # internally does asyncio.run( agent.run() )
"""
How To Exit
To do a clean exit, which nicely kills off an async tasks, and sends the 'quit' status.
Should be able to call this from any `def async`.

await self.exit($statuscode, $exception-if-you-have-it)
return # uh... cause this whole chain of awaitables to die...?
"""
