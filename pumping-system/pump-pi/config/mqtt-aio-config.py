"""
Use:
    from mqtt_aio_config import mqtt_aio_config # from ./lib/

    async with mqtt.Client(
        **mqtt_aio_config,
        ) as client:
"""

import paho.mqtt as paho
import asyncio_mqtt as mqtt
from aio_key import aio_key 

# paho mqtt args # FIXME: should be inherited from file config
connection = {
    "hostname" : "io.adafruit.com",
    "port" : 8883,
    # client_id
    # tls...
    "clean_session" : True, # if a short enough down-time, resync to the commands
    "keepalive" : 8, # trigger Will
    "clean_start" : paho.client.MQTT_CLEAN_START_FIRST_ONLY,
    "tls_params" : mqtt.TLSParameters(),
    "username" : aio_key()['IO_USERNAME'],
    "password" : aio_key()['IO_KEY'],
}

# don't see a way to put any user-params in 'connection', so
meta = { 
    'rate_limit' : 30,  # per minute
    }
