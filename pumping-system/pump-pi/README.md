# Raspberry Pi logic and ui

## Overview

The Raspberry Pi provides the non-realtime logic and UI. Node-Red, in particular, puts quite a load on a Pi, and the Pi 4 was chosen. It is noticeably slow, when compared to a laptop, etc. A Pi 3 would likely work, though a bit slower. We do assume multi-processors for parallel/responsive operations. Non of the GPIO is currently used, and there is minimal dependence on the fact that it is a Pi. Notably, there are some assumptions about the OS (apt, systemd).

It is intended that services be small and focused. 

MQTT was chosen as a "bus" for services. New services can be added dynamically, since they publish/subscribe at will to MQTT. There is a MQTT broker that _only_ communicates on the loopback, so is secure from outside the Pi. We try to keep the rate of MQTT messages relatively low (roughly 1/sec).

MQTT is then natural to communicate to internet servers.

Rather than have a hardware display, Node-Red is used to create a dashboard for LAN access (zerconf/bonjour). And, the Pi is expected to connect to the laboratory wifi. It is just another MQTT service. At this time, the Node-Red Browser interface does not use HTTPS (beware!). Our design intends a chromebook that sits in a holder on the pump-box.

Python was chosen as the main language, as it is currently the most familiar to users in the sciences. Node-Red uses javascript.

Git is used for the project, and in several places by services to keep versions of some files.

## Microservices

The microservices communicate over MQTT and thus only know about messages, not other services. Microservices are in the `agents` directory. They all have a command-line interface: try `--help`, and `--topics` to list (most) of the topics then send/receive.

The microservices are controlled by systemd as user services.

**other stuff about their protocol: advertising, etc**

* The `serial-stepper` agent receives requests to move motors, communicates over serial (USB) to the Arduino, and responds with any status. It speaks Hz and Steps.
* The `ml-stepper` agent speaks ml/sec and total-ml for pumping. It converts to/from steps and forwards messages for the stepper-agent.
* The `recipe-run-aio` executes recipes that can use a DSL for pumping ml's, pause, repeat. It sends messages to ml-stepper etc. It also handles some messages related to information about the recipes, and stopping.
* We have special agents that can fetch parameter sets from io.adafruit.com, and save them on drive.google.com

### Tools

like the `tools/stepper-target` to move motors.

### Writing a microservice

## GUI (node-red)

Node-red presents a dashboard on `amps-pi.local:1884/flexdash`. It requires a password. **Note** The connection is not encrypted, anyone can snoop wifi signals and capture the password, and ....

