/*
Test a sr-accel-stepper controlled by commands on Serial.

Very fast flash == No Serial connection (no recent incoming data).
Slow flash == Serial connection.
Assymetric slow flash (long on or long off) == Serial connection with pings
Stuttering/Fast == Probably good serial + some commands (like pings/status)

Wiring
  USB <-> Pi
  SPI SCK,MOSI -> shift-registers clock,data
  shift-register shift-register latch

So, using an itsy m4
  USB
  SCK
  MOSI
  latch 7

*/

#include <SPI.h>
#include <Streaming.h>
#include <every.h> // by awgrover

#include "array_size.h"

constexpr int LatchPin = 7;
constexpr int LATCHSTART = HIGH;
constexpr int LATCHIDLE = ! LATCHSTART;

constexpr int OUTCHIPS = 4;
constexpr int BITS_PER_MOTOR = 4;
constexpr int used_bits = 3; // can ignore other bits-per-motor, out sets them to high
constexpr bool SLOWSPI = false;

Every fast_flash(50);
// see comment in serial_commands()
bool serial_active = false;

struct s_stepper {
  unsigned long step_interval; // micros. so we don't have to do floats for hz
  int target;

  bool positive;
  unsigned long last_step; // micros

  int command_i;
  // temporary accumulators
  int t_hz; // really unsigned, but easier for ->accum()
  int t_target;
  bool t_positive;

  bool start_command(char x) {
    // 'G+hhhhssss' # decimal
    if (x=='G') {
      this->command_i = 0;
      this->t_hz = 0;
      this->t_target = 0;
      return true; // should be start of a command
    }
    else {
      return false;
    }
  }

  bool accum( byte x, int &value) {
    if (x >= '0' && x <= '9') {
      value = (value * 10) + (x-'0');
      return true;
    }
    else {
      return false;
    }
  }
      
      
  bool in_char(byte x) {
    // should only be called for a command sequence
    // so we know where we are
    // Returns True for eager-for-more, False for done-with-input
    switch (this->command_i) {
      case 0: // sign
        if (x=='+') this->t_positive = true;
        else if (x=='-') this->t_positive = false;
        else {
          Serial << F("bad sign at ") << this->command_i << F(" ") << x << endl;
          return false; // bad data
        }
        this->command_i += 1;
        return true;

      case 1: // step_interval
      case 2:
      case 3:
      case 4:
        if ( this->accum( x, this->t_hz ) ) {
          this->command_i += 1;
          return true;
        }
        else {
          Serial << F("bad step_interval at ") << this->command_i << F(" ") << x << endl;
          return false; // bad data
        }

      case 5: // target
      case 6:
      case 7:
      case 8:
        if ( this->accum( x, this->t_target ) ) {
          this->command_i += 1;
          if (this->command_i > 8) {
            // done
            this->step_interval = 1000000.0 / this->t_hz; // micros
            this->positive = this->t_positive;
            this->target = this->t_target * ( this->positive ? 1 : -1 );
            this->last_step = micros();
            Serial << F("# Go ") << this->step_interval << F(" ") << this->target << endl;
          }
          return true; // next...
        }
        else {
          Serial << F("bad step_interval at ") << this->command_i << F(" ") << x << endl;
          return false; // bad data
        }

      case 9: // the previous char was the last
        return false;

      default:
        Serial << F("Internal at ") << this->command_i << F(" ") << x << endl;
        return false;
    }
  }

  bool run() {
    if (this->target != 0) {
      if (micros() - this->last_step > this->step_interval) {
        this->last_step = micros(); // bad drift. but just testing anyway
        
        this->do_one_step();

        this->target -= this->positive ? 1 : -1;
        if (this->target==0) Serial << F("#DONE") << endl;
      }
    return true;
    }

  // not running
  return false;
  }

  byte fill_bit_vector( byte out_bits, byte bit_vector[], unsigned int bit_vector_len) {
    // build a full byte
    byte out_byte = 0;
    for (int i = 0; i < 8 / BITS_PER_MOTOR; i++) {
      out_byte |= out_bits << i * BITS_PER_MOTOR;
    }
    // build bit_vector
    for (unsigned int i = 0; i < bit_vector_len; i++) {
      bit_vector[i] = out_byte;
    }

    return out_byte;
  }

  void do_one_step() {
    // only called when a step needs to be done

    constexpr int MOTOR_CT = OUTCHIPS * 8 / BITS_PER_MOTOR; // nb: see REGISTER_CT, i.e. to get bytes divide by bits-per
    constexpr int REGISTER_CT = (int) ceil((float)MOTOR_CT * BITS_PER_MOTOR / 8);

    // bits: enable,step,dir
    // Need to set the dir w/o changing step
    // Three for very first step, and final step
    //  the extra cleanup step costs about 2% time
    // cw
    static const byte cw_pattern[] = { 0b100, 0b110, 0b100 }; // en|step|dir bits
    // ccw
    static const byte ccw_pattern[] = { 0b101, 0b111, 0b101}; // en|step|dir bits
    static byte bit_vector[ REGISTER_CT ];

    const byte *pattern = this->positive ? cw_pattern : ccw_pattern;

    for (unsigned int i=0; i<array_size(cw_pattern); i++) {
      byte p = pattern[i];

      digitalWrite(LatchPin, LATCHIDLE);
      fill_bit_vector(p, bit_vector, array_size(bit_vector) );

      // 20m is max (probably, depends on microchip, shift registers are somethin like 20ns)
      // 2m reduces fastest loop from 16.94hz to 10.48 hz, about .62 as fast,
      SPI.beginTransaction(SPISettings(SLOWSPI ? 1000000 : 2000000, LSBFIRST, SPI_MODE0));
      // sends 0...[n-1] [n], no matter what MSB/LSB first is!
      // so, nearest shift-register is [n]
      SPI.transfer(bit_vector, array_size(bit_vector)); // whacks bit_vector
      SPI.endTransaction();

      digitalWrite(LatchPin, LATCHSTART);
    }
  }
};

s_stepper stepper = { 0, 0 };

void setup() {
  unsigned long start=millis();

  // Get serial started
  Serial.begin(115200); // will wait for serial below

  clear_bright_leds();

  // SPI setup
  pinMode(LatchPin, OUTPUT);
  digitalWrite(LatchPin, LATCHIDLE);
  SPI.begin();

  // Wait for serial
  static Every serial_wait(2*1000); // how long before continuing. usu ~850, sometimes 1500
  pinMode(LED_BUILTIN, OUTPUT);

  // Wait for serial or timeout
  while(! (Serial || serial_wait()) ) { 
    if (fast_flash()) digitalWrite( LED_BUILTIN, ! digitalRead(LED_BUILTIN) );
    delay(10); // the delay allows upload to interrupt
    } 
  unsigned long took=millis();
  Serial.println(F("Start " __FILE__ " " __DATE__ " " __TIME__));
  Serial << F("Serial took ") << (took-start) << endl;
}

void loop() {
  static Every say_elapsed(1000);
  unsigned long loop_time = micros();

  bool running = stepper.run();
  unsigned long sc_loop_time = micros();
  if (! running) {
    // This is where you can do slow/disruptive things
    // without affecting stepping,
    // because we aren't stepping

    // FIXME: if interstep time > 1ms, we could do this anyway
    serial_active = !!Serial; // takes 1ms. see comment in serial_commands()
  }

  bool saw_input = serial_commands();

  if (say_elapsed() || saw_input) {
    // getting something like 34..60 micros while stepping, occasional 250 (due to Serial.avail)
    // That's probably around 20KHz stepping
    /*
    unsigned long end_loop_time = micros();
    unsigned long elapsed_loop_time = micros()-loop_time;
    Serial << F("Elapsed ") << (end_loop_time-loop_time) << endl;
    Serial << F("  run ") << (sc_loop_time-loop_time) << endl;
    Serial << F("  serial_commands ") << (end_loop_time-sc_loop_time) << endl;
    */
  }
}

bool serial_commands() {
  static Every say_stats(1000);
  unsigned long start=micros();

  static Every heartbeat(800);

  // Testing for active serial connetion takes 1milli! So, we don't.
  // (Weirdly, Serial.available() takes with 1micro, or ~140micros, not 1milli, even if no Serial connection)
  // We just do evidence based.
  // Our client should do a heartbeat status/ping, which will trigger our evidence
  static Timer timeout(1500); // still active if we see any input before this timeout

  if ( serial_active) {
    if (heartbeat() ) {
      // Serial << (!! Serial) << endl;
      digitalWrite( LED_BUILTIN, ! digitalRead( LED_BUILTIN ) );
    }
    else if (timeout()) {
      serial_active = false;
      }
  }
  else if (fast_flash()) {
    digitalWrite( LED_BUILTIN, ! digitalRead(LED_BUILTIN) );
  }
  unsigned long hb_time = micros();

  bool rez;

  // So, we just blithely check for available
  if (Serial.available() > 0) {
    serial_active = true;
    timeout.reset();

    static bool delegating_input = false;

    // Do the Manager() object, and each module has a static constructor that adds to it
    // Serial << F("avail ") << Serial.available() << endl;
    char x = Serial.read();

    // Responses are: data...\n
    bool start_command = true;

    if (delegating_input) {
      delegating_input = stepper.in_char( x );
      }

    if (! delegating_input) { // if still delegating input
      switch(x) {

        case '#': // ping
          //start_command = false; // so we don't stutter the LED_BUILTIN because of this command
          Serial << x << endl;
          break;

        case '?': // status
          Serial << x << stepper.target << endl;
          break;

        case 'G' : // go to target, direction, 4 bytes: G+bbbb
          delegating_input = stepper.start_command(x);
          break;

        case '\n' :
        case '\r' :
          delegating_input = false;
          break;

        default:
          start_command = false;
          Serial << F("#what? '") << x << F("'") << endl;
        }
      }

    if (start_command) {
      digitalWrite( LED_BUILTIN, ! digitalRead( LED_BUILTIN ) );
    }
    rez=true;
  }
  else {
    rez=false;
  }

  unsigned long sbcheck_time =micros();
  if (say_stats()) {
    /*
    Serial << F("  SC total ") << (sbcheck_time-start) << endl;
    Serial << F("    hb ") << (hb_time-start) << endl;
    Serial << F("    check ") << (sbcheck_time-hb_time) << endl;
    */
  }
  return rez;
}
