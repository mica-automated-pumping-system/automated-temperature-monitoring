#pragma once
/*
  We want AccelStepper acceleration, etc., but we are using SPI not pins, and several steppers.

  So, we subclass (class AccelStepperNoted) and note that a step is desired.

  And, have a helper (class AccelStepperShift) to collect all the stepper's desires,
  and send them out the SPI to shift registers.

  Since there are 2 common wirings for opto-isolated stepper controllers
  (the typical tb6600 based ones),
  Specify which direction for signals (default is HIGH, i.e. signals to "+" connection, "common anode"):

  #define STEPPER_SIGNALS HIGH

  Note that the Enable signals on those controllers is "not", i.e. HIGH to "+" is disable,
  But, also note that No-Connection is usually Enable (internal to the controller)!
  override (default is LOW):

  #define ENABLE_POLARITY LOW

  the tb6600 steps on falling edge,
  override (default is LOW)

  #define STEP_ON LOW

  Direction is Clockwise==STEPPER_SIGNALS, Counter-clockwise is !STEPPER_SIGNALS,
  but you can reverse it by setting this to false:

  #define STEPPER_NORMAL_DIRECTION = true

  The Shift-Register has the following defaults that you can override:
  (the pins are specified via the constructor)
  
  #define LATCH_ON HIGH

  The default is to have Output1, of the first shift-register, be the LAST bit of data that is shifted,
  so that each byte is "in order": 01110111 puts the LSB at output-1 (false reverses the order of EACH byte,
  not the whole bit-string).
  
  #define SR_MSBFIRST true

  The signals bits are Enable, Direction, Step, and not-connected,
  In MSB->LSB is [ not-connected, Dir, Step, Enable ], i.e. Enable is bit 0
  override by listing in a different order:

  #define SR_SIGNALS ( SR_EN, SR_STEP, SR_DIR,  SR_NC )
  
*/

#include <SPI.h>

#include <AccelStepper.h> // Mike McCauley <mikem@airspayce.com>
#include <Streaming.h> // Mikal Hart <mikal@arduiniana.org>

#include "freememory.h"
#include "every.h"
#include "begin_run.h"

/////// Signal configuration
#ifndef STEPPER_SIGNALS
  #define STEPPER_SIGNALS HIGH
#endif
#ifndef SR_SIGNALS
  #define SR_SIGNALS { SR_EN, SR_STEP, SR_DIR, SR_NC }
#endif
#ifndef ENABLE_POLARITY
  #define ENABLE_POLARITY LOW
#endif
#ifndef STEP_ON
  #define STEP_ON LOW
#endif
#ifndef LATCH_ON
  #define LATCH_ON HIGH
#endif
#ifndef SR_MSBFIRST
  #define SR_MSBFIRST true
#endif
#ifndef STEPPER_NORMAL_DIRECTION
  #define STEPPER_NORMAL_DIRECTION = true
#endif

/////// Debug/config stuff you can set
// Add an extra frame at the end of the shift-registers? i.e. A non-motor shift-register.
#ifndef NO_EXTRA_FRAMES
  #define NO_EXTRA_FRAMES 1
#endif

// true to print "done" everytime all motors finish
// probably too noisy when running a real pattern
#define DEBUGDONERUN 1


// the COUNT of motors to debug, stops after that
//  undef or -1 is don't,
//#define DEBUGBITVECTOR 0
// 1 to print the bit-vector every time
// 2 to print on "blink"
// 4 to print all bit-vectors( step, dir)
//#define DEBUGLOGBITVECTOR 4
// stop run()'ing after this number of steps, 0 means don't stop
//#define DEBUGSTOPAFTER 2
// print calcalation for frame[i] = value & mask
///#define    DEBUGFRAME 1
// Delay between each shift
//#define DEBUGSTUPIDSLOW 500

#ifndef DEBUGDONERUN
#define DEBUGDONERUN 0
#endif
#ifndef DEBUGBITVECTOR
#define DEBUGBITVECTOR -1
#endif
#ifndef DEBUGLOGBITVECTOR
#define DEBUGLOGBITVECTOR 0
#endif

#ifndef DEBUGSTOPAFTER
#define DEBUGSTOPAFTER 0
#endif
#ifndef DEBUGFRAME
#define DEBUGFRAME 0
#endif
#ifndef DEBUGSTUPIDSLOW
#define DEBUGSTUPIDSLOW 0
#endif

class AccelStepperNoted: public AccelStepper {
    // This class just notes that a step is requested.
    // A user of the instance should reset `do_step` once it is read.
    // Need to call the usual .runX(), and other .setX() as normal.
    // NB: the last step() will have .run() return false, so use do_step():
    //  amotor.run();
    //  if (amotor.do_step) { handle it, set do_step=false ...}

  public:
    inline boolean direction() {
      return _direction;  // expose, true == foward
    }

    const int motor_i; // my index

    boolean do_step; // true == do a step, reset after this is used

    bool enabled = true;

    // this super's constructor causes no use of pins
    AccelStepperNoted(int i) : AccelStepper(&dumyStep, &dumyStep) , motor_i(i), do_step(false) {}
    static void dumyStep() {}

    void step(long astep) {
      // step is just current position
      // we just note that a step is requested
      (void)(astep); // Unused

      if (DEBUGPOSPERSTEP == 2) Serial << F("<!M ") << motor_i << F(" ") << F(" d ") << distanceToGo() << F(" ") << millis() << endl;
      do_step = true;
    }

    int time_to(int position, float acceleration ) { // OMG private var again
      // how many microseconds
      // just an estimate, not good for short distances
      int distance = abs(position - currentPosition());
      int ms = int( 1000.0 * distance / maxSpeed() );
      //Serial << F("time_to ") << position << F(" from ") << currentPosition() << F(" dist ") << distance << F(" ms ") << ms << endl;
      return ms ;

      // d= 1/2 a t^2
      // 2d = a t^2
      // 2d/a = t^2
      // sqrt(2d/a) = t
      // BUT, we want to to accel half-way, then decl, so the `distance` is 1/2, and the time is *2
      //int ms =  int( 2 * sqrt( distance / acceleration ) * 1000 ); // sec to msec
      //Serial << F("time_to ") << position << F(" from ") << currentPosition() << F(" dist ") << distance << F(" @ accel ") << acceleration << F(" ms ") << ms << endl;
      //return ms ;
    }

    // we have to track the enabled state, because they become bits in the shift-register
    void enableOutputs() {
      // FIXME: this should trigger an shift-register update of the "dir_bits", if we don't step on the next cycle
      this->enabled = true;
    }
    void disableOutputs() {
      this->enabled = false;
    }
};

class AccelStepperShift : public BeginRun {
    // for many DFRobot TB6600
    //    input labeling seems confusing, but, they are optoisolators, so just think in terms of current
    //      signal to either - or +, as convenient (74HC595 would use +, tpic6c596 would use -)
    //      pul steps on transition to on.
    //      dir : on is forward, off is backwards (clock/counter-clock).
    //      en is actual !en: no current=enable
    // via a chain of shift-registers (3 bits per tb6600)
    //  74HC595 is serial-to-parallel: output on latch going high
    //  Wiring: Arduino   Shift-Register 74HC595  tb6600          Shift-register 74HC165
    //          3.3V      16 VCC 3V               dir+,pul+,en+   16 Vcc
    //          GND       8 GND                                   8 GND
    //          MOSI      14 SER (shiftin)
    //          MISO                                              9 Qh (shiftout) (7 is ~Qh)
    //          SCK       11 SRCLK                                2 clk (15 & 2 can be swapped)
    //          latch_pin 12 RCLK
    //          load-pin                                          1 SH/~LD (latch - read on low, high for shift)
    //                    13 ~OE pull low
    //                    10 ~SRCLR pull high
    //                    9 q7'/data-out -> SER
    //                    15 q0                   motor0 dir-
    //                    1 q1                    motor0 pul- (step)
    //                    q2/q3                   motor1, ...
    //                                                            11 A limit1
    //                                                            13 C limit2, 3 E limit 3, 5 G limit 4
    //                                                            11,12,13,14, 3,4,5,6 Do not let them float. pull low if not used
    //                                                            15 clk-inh, pull low
    //                                                            10 SER shiftin daisy chain to 9 Qh, pull low on last
    //          GND
    //
    //  5MHz clock max at 2v, 25MHz at 5v, so... at 3v...
    //  20mA max per output (?)
    //
    // To test for acceleration
    //    Set the driver to a very small current so the motor will slip (maybe 0.5A).
    //    Test various accelerations till no slip
    //    Set the driver back to operating current
    //
    // Indicators
    //    SCK is too fast to see (only on for about 60micros!)
    //    mosi gets left on, so not useful
    //    data doesn't even seem to change!
    // using an AccelStepper per motor
    //
    // This class
    // * constructs AccelStepperNoted per motor
    // * has run() to run all motors
    // * converts the "noted" steps into bit-vectors for fast spi-shift out
    //    to drive the tb6600's

    // driver chip: https://toshiba.semicon-storage.com/info/docget.jsp?did=14683&prodName=TB6600HG
    //"common anode"
    // pull up to logic level: PUL+, DIR+ and EN+
    // 8-15mA each signal
    // signals: pul-, dir-, en-
    // (swap those sets for "common cathode" and reverse signal polarities)
    // That's "1 pin" mode for AccelStepper
    // min 2.2micros for pulse width
    // Order:
    //  set direction & enable ~20micros
    //    also serves as >2.2micros between pulses
    //  set pulse ~20micros
    //  2.2micros
    //  reset pulse ~20micros
    //  total ~60microsec

  public:
    // per frame
    // invert if necessary
    enum SR_SIGNALS; // e.g. ( SR_EN, SR_STEP, SR_DIR,  SR_NC )
    static byte constexpr ENABLEBIT =  ( ENABLE_POLARITY ? (STEPPER_SIGNALS ? 1 : 0) : !(STEPPER_SIGNALS ? 1 : 0) ) << SR_EN;
    static byte constexpr NOT_ENABLEBIT =  ( ENABLE_POLARITY ? !(STEPPER_SIGNALS ? 1 : 0) : (STEPPER_SIGNALS ? 1 : 0) ) << SR_EN;
    static byte constexpr STEPBIT = ( STEP_ON ? (STEPPER_SIGNALS ? 1 : 0) : !(STEPPER_SIGNALS ? 1 : 0) ) << SR_STEP;
    static byte constexpr NOT_STEPBIT = ( STEP_ON ? !(STEPPER_SIGNALS ? 1 : 0) : (STEPPER_SIGNALS ? 1 : 0) ) << SR_STEP;
    static byte constexpr FWD_DIRBIT = ( STEPPER_NORMAL_DIRECTION ? (STEPPER_SIGNALS ? 1 : 0) : !(STEPPER_SIGNALS ? 1 : 0) ) << SR_DIR; // a pump is CCW for forward pumping
    static byte constexpr REV_DIRBIT = ( STEPPER_NORMAL_DIRECTION ? !(STEPPER_SIGNALS ? 1 : 0) : (STEPPER_SIGNALS ? 1 : 0) ) << SR_DIR;

    static boolean constexpr LATCHSTART = LATCH_ON; // 1 if the latch pulse is LOW->HIGH->LOW
    static boolean constexpr LATCHIDLE = ! LATCHSTART;

    static int constexpr MAX_SPEED = (4000) ; // empirical

    // We are using 8 bit shift registers
    // And 2 bits per motor (a "frame"): dir & step
    // We shift out MSB 1st, which works for the output shift-register
    //  but is backwards for the input shift-register.
    // so our bit-vector[0] is the first shifted out (farthest shift-register)
    //  arduino
    //  -> ...
    //  -> motor[1]:motor[0]
    //  -> led-bar goes here "extra frame"
    //  -> unused_frame[1]:unused_frame[0]... if any
    //  end-of-chain
    static constexpr int used_bits = 3; // en|step|dir step fires on high->low, direction high=ccw
    static constexpr int bits_per_frame = 4; // (unused per frame is possible)
    static constexpr int extra_frames = NO_EXTRA_FRAMES ? 0 : 8 / bits_per_frame; // the led-bar: 1 whole shift-register
    // cf unused_frames. use frame_i = unused_frames as index of 1st "extra frame"
    static constexpr byte frame_mask = (1 << bits_per_frame) - 1; // just the bits of the frame, i.e. n bits
    static constexpr byte used_mask = (1 << used_bits) - 1; // just the bits of the frame that are used, i.e. step & dir

    // State
    // NB: this MUST be in the same order as the initialization list of the constructor
    // or values can get corrupted.
    // Turn on "more" compiler warnings in preferences.

    const int motor_ct;
    const int latch_pin;
    const int *disable_motor; // list of motors to not use, end with -1
    // derived
    const int total_used_bits;
    const int byte_ct;
    const int unused_frames; // lsb frames that aren't used (because we need byte aligned)
    const int total_frames;

    AccelStepperNoted** motors; // an [motor_ct] of them
    boolean all_done = false; // goes true when all motors finish their targets
    boolean did_one_finish = false; // goes true|false after .run()
    #ifdef TIMING1
    long _timing_steps;
    unsigned long _timing_start;
    #endif

    // for blinking indicators, i.e. leaves led on for at least this time
    Timer recent_step = Timer(200, false);

    // frame order is lsb:
    // unused-frames, led-bar, motor0 ... motorN
    // an [] of bytes for the shift-register bits, for per-motor-frame
    byte* dir_bit_vector = NULL; // dir & step=reset for each frame
    byte* step_bit_vector = NULL; // dir&step for each frame
    boolean* usable_motor = NULL; // true if we should pay any attention to the motor at [i]

    AccelStepperShift(
      const int motor_ct,
      const int latch_pin,
      const int disable_motor[] // list of motors to not use, end with -1
    )
      : motor_ct(motor_ct)
      , latch_pin(latch_pin)
      , disable_motor(disable_motor)

        // pre-calculating a bunch of stuff
      , total_used_bits( (motor_ct + extra_frames) * bits_per_frame)
        // we have to fill out the bits to make a whole frame, i.e. ceiling()
      , byte_ct( ceil( total_used_bits / (sizeof(byte) * 8.0) ) ) // i.e. need a whole byte for a fraction
        // and fill out to byte align
      , unused_frames( byte_ct - (total_used_bits / (sizeof(byte) * 8)) )
      , total_frames( byte_ct * ( (sizeof(byte) * 8 / bits_per_frame) ) )
    {
      // not constructing `new` members till .begin()
    }

    void begin() {
      // our 2 "latches"
      pinMode(latch_pin, OUTPUT);
      digitalWrite(latch_pin, LATCHIDLE);

      // construct now, so we can control when memory is allocated
      Serial << F("BEGIN AccelStepperShift ") << endl;
      Serial << F(" Enable ") << SR_EN << F("=0b") << _BIN(ENABLEBIT) << F("/0b") << _BIN(NOT_ENABLEBIT)
            << F(" Step ") << SR_STEP << F("=0b") << _BIN(STEPBIT) << F("/0b") << _BIN(NOT_STEPBIT)
            << F(" Dir ") << SR_DIR << F("=0b") << _BIN(FWD_DIRBIT) << F("/0b") << _BIN(REV_DIRBIT)
            << endl;
      Serial       << F(" latch_pin ") << latch_pin
                   << F(" LATCHSTART ") << LATCHSTART
                   << F(" LATCHIDLE ") << LATCHIDLE
                   << endl;
      Serial << F(" Motors ") << motor_ct
             << F(" bit_vector bytes ") << byte_ct << F(" motor ct ") << motor_ct
             << F(" extra frames ") << extra_frames
             << F(" unused frames ") << unused_frames
             << F(" used bits ") << total_used_bits
             << F(" total frames ") << total_frames
             << endl;
      // I was getting corrupted data because initialization-list must be in same order as instance-vars.
      if ( unused_frames != (byte_ct - (total_used_bits  / ((int)sizeof(byte) * 8) ))) {
        Serial << F("Bad unused_frames again! free ") << freeMemory() << endl;
        while (1) delay(20);
      }
      if (DEBUGBITVECTOR >= 0) {
        Serial
            << F("(sizeof(byte) * 8.0) ") << (sizeof(byte) * 8.0) << endl
            << F("byte_ct - (total_used_bits  / (sizeof(byte) * 8) ") << (byte_ct - (total_used_bits  / (sizeof(byte) * 8) )) << endl
            ;
        Serial << F("Free ") << freeMemory() << endl;
        //while (1) delay(20);
      }
      Serial << F("DEBUGBITVECTOR ") << DEBUGBITVECTOR
             << F(" DEBUGLOGBITVECTOR ")  << DEBUGLOGBITVECTOR
             << F(" DEBUGPOSPERSTEP ")  << DEBUGPOSPERSTEP
             << F(" DEBUGSTOPAFTER ")  << DEBUGSTOPAFTER
             << F(" DEBUGFRAME ")  << DEBUGFRAME
             << F(" DEBUGSTUPIDSLOW ")  << DEBUGSTUPIDSLOW
             << endl;

      motors = new AccelStepperNoted*[motor_ct];
      for (int i = 0; i < motor_ct; i++) {
        motors[i] = new AccelStepperNoted(i);
      }

      // allocate the bit-vector
      // better be 8 bits/byte! Yes, there are cpu's with other ideas.

      step_bit_vector = new byte[byte_ct];
      memset(step_bit_vector, 0, sizeof(byte)*motor_ct);
      dir_bit_vector = new byte[byte_ct];
      memset(dir_bit_vector, 0, sizeof(byte)*motor_ct);

      // copy permanently disabled
      usable_motor = new boolean[motor_ct];
      memset(usable_motor, true, sizeof(boolean)*motor_ct);
      for (const int *disable = disable_motor; *disable != -1; disable++ ) {
        usable_motor[ *disable ] = false;
      }

      // default speed/accel
      for (int i = 0; i < motor_ct; i++) {
        // to 200 steps/sec in 0.1 sec
        // targetspeed = 1/2 * A * 0.1
        // (targetspeed*2)/0.1
        constexpr float time_to_max = 0.25;
        constexpr int acceleration = (MAX_SPEED * 2) / time_to_max;
        motors[i]->setAcceleration(acceleration);
        motors[i]->setMaxSpeed(MAX_SPEED); // might be limited by maximum loop speed
      }
    }

    boolean run() {

      if ( run_all() ) {
        set_led_bar();
        if (DEBUGLOGBITVECTOR==4) {
          Serial << F("<<<shift_out...") << endl;
        }
        shift_out();
        return true;
      }
      else if ( recent_step() ) {
        set_led_bar(true);
        shift_out();
      }
      return false;
    }

    void finish_loop() {
      // reset do_step's
      for (int i = 0; i < motor_ct; i++) {
        //if (motors[i]->do_step) Serial << F("Reset do_step [") << i << F("]") << endl;
        motors[i]->do_step = false;
      }
    }

    void set_led_bar(boolean clear = false) {
      // use led-bar for various indications
      // true clears some bits

      if (NO_EXTRA_FRAMES) return;

      static Every::Toggle spi_heartbeat(200); // "blink" one of the leds NB: class level!

      // shift register bits, we numbered the led from 9...0 in the comment
      static constexpr int heartbeat_bit = 7;
      static constexpr int recent_step_bit = 1;
      bool did_heartbeat = false;

      byte led_bits = 0;

      // SPI heartbeat
      did_heartbeat = spi_heartbeat(); // have to call () to cause .state to change
      led_bits |= (clear ? 0 : spi_heartbeat.state ) << heartbeat_bit;

      // Recent step ?

      if ( recent_step.running ) {
        led_bits |= (clear ? 0 : 1) << recent_step_bit;
      }

      // last frame: repeat because we send both bit-vectors
      dir_bit_vector[ 0 ] = led_bits;
      step_bit_vector[ 0 ] = led_bits;

      if ( did_heartbeat ) {
        if (DEBUGLOGBITVECTOR == 2) {
          Serial << F("OUT: ") << spi_heartbeat.state << F(" ") << _BIN(led_bits) << endl;
          dump_bit_vectors();
          //while (1) {};
        }
      }
    }

    boolean run_all() {
      // run all
      // capture bit vector
      // return true if ANY ran (have to shift all bits)
      // We should be overwriting the whole bit-vector buffer(s), all bits

      static int step_count = 0; // for debug, see DEBUGSTOPAFTER

      static Every say_pos(DEBUGPOSPERSTEP == 2 ? 100 : DEBUGPOSPERSTEP);
      boolean say = say_pos();
      // if (say) Serial << F("  running @ ") << millis() << F(" ") << endl;

      if (DEBUGSTOPAFTER && step_count >= DEBUGSTOPAFTER) return false;

      //Serial << F("run all, already all_done ? ") << all_done << endl;

      did_one_finish = false;

      boolean done = true; // is everybody finished on this loop?
      for (int i = 0; i < motor_ct; i++) {

        // ->run was about 4000 micros for 15 motors @ 8MHz clock (had extra debugging?)
        // got about 334 micros @ 48mhz (so about 3000 steps/sec max)
        // NB. AccelStepper thinks the protocol is:
        //    step(), if that was last step then ->run() will return false
        // Which means we will not see run()==true for the last step. thus do_step() instead:
        if ( motors[i]->run() || motors[i]->do_step ) {
          //Serial << F("dostep @") << i << F(" .do_step ") << motors[i]->do_step << F(" dist ") << motors[i]->distanceToGo() << F(" speed ") << motors[i]->speed() << endl;
          done = false;
          did_one_finish = motors[i]->distanceToGo() == 0;

          if (say && (DEBUGPOSPERSTEP == 1 || DEBUGPOSPERSTEP >= 100)) Serial << F("<M ") << i << F(" ") << motors[i]->currentPosition() << F(" d ") << motors[i]->distanceToGo() << F(" ") << millis() << endl;
        }
        else {
          // if (!all_done) Serial << F("  done ") << i << F(" to ") << motors[i]->currentPosition() << endl; // message as each motor finishes
        }

        //if ( motors[i]->currentPosition() != motors[i]->targetPosition() ) {
        //  Serial << F("expected move " ) << i << F(" to ") << motors[i]->targetPosition() << endl;
        //}
        //Serial << F("  motors at i done ? ") << i << F(" ") << done << endl;


        if (false && DEBUGBITVECTOR >= 0 && DEBUGBITVECTOR <= i && !all_done) {
          Serial
              << F("motor[") << i << F("] do_step ? ") << motors[i]->do_step
              << endl;
        }

        // add the step bit to our vector (might be 0 : no movement)
        collect_bit(i);
        if (false && !done) {
          Serial << F("Step for ") << i << F("  "); dump_bit_vector(step_bit_vector);
          Serial << F("         ") << i << F("  "); dump_bit_vector(dir_bit_vector);
        }

      }

      if (DEBUGLOGBITVECTOR==4 && ! done) {
        Serial << F("All collected") << endl;
        dump_bit_vectors();
      }
      if (!done) step_count++;

      //if ((!all_done && done)) Serial << F("All done @ ") << millis() << F(" steps ") << step_count << endl;

      all_done = done;
      //if (!all_done) Serial << F("!All_Done ? ") << all_done << endl;

      if (! done) recent_step.reset(); // restart indicator

      return ! done;
    }

    void collect_bit(int i) { // motor_i
      // collect the bits for "dir" and "step" bit_vectors
      // about 100micros for all 15 at 8MHz 32u4

      int frame_i = i;

      // the do_step flag is reset at finish_loop(), so we only handle a step once (till next step)

      if (motors[i]->do_step && DEBUGBITVECTOR >= 0 && DEBUGBITVECTOR >= i) {
        Serial << F("BV motor[") << i << F("] ")
               << F(" extra ") << extra_frames << F(" unused ") << unused_frames << F(" = ") << (extra_frames + unused_frames)
               << F(" | frame[") << frame_i << F("] ")
               << F(" direction ") << motors[i]->direction()
               << endl;
        Serial << F("  dist ") << motors[i]->distanceToGo() << endl;
        Serial << F("  mask 0b") << _BIN(frame_mask) << endl;
        //while (1) delay(20);
      }

      bool allow_step = true;

      // want the enable-bit, dir-bit, and step=0
      const byte dir_bits = (
        (motors[i]->direction() ? FWD_DIRBIT : REV_DIRBIT) 
        | NOT_STEPBIT
        | (motors[i]->enabled ? ENABLEBIT : NOT_ENABLEBIT)
      );

      // want the above AND step (unless ! allow_step)
      bool want_step = allow_step && motors[i]->do_step;
      const byte step_bits = (
        (motors[i]->direction() ? FWD_DIRBIT : REV_DIRBIT) 
        | (want_step ? STEPBIT : NOT_STEPBIT)
        | (motors[i]->enabled ? ENABLEBIT : NOT_ENABLEBIT)
      );

      if (motors[i]->do_step && DEBUGBITVECTOR >= 0 && DEBUGBITVECTOR >= i) {
        Serial << F("  new dir_bits ") << _BIN(dir_bits) 
          << F(" Dirb 0b") << _BIN(motors[i]->direction() ? FWD_DIRBIT : REV_DIRBIT) 
          << F(" !stepb 0b") << _BIN( NOT_STEPBIT )
          << endl;
        Serial << F("  new step_bits ") << _BIN(step_bits) 
          << F(" wanted? ") << want_step 
          << F(" sb 0b") << _BIN(STEPBIT)
          << endl;
        //while (1) delay(20);
      }

      int byte_i = set_frame( dir_bit_vector, frame_i, frame_mask, dir_bits );
      set_frame( step_bit_vector, frame_i, frame_mask, step_bits );

      if (motors[i]->do_step && (DEBUGLOGBITVECTOR==4 || (DEBUGBITVECTOR >= 0 && DEBUGBITVECTOR >= i))) {
        Serial << F("  dir byte[") << byte_i << F("] = ") << _BIN(dir_bit_vector[ byte_i ]) << endl;
        Serial << F("  step byte[") << byte_i << F("] = ") << _BIN(step_bit_vector[ byte_i ]) << endl;
      }

      // dump, then stop if debugging this motor
      if (motors[i]->do_step && (DEBUGLOGBITVECTOR >0 || (DEBUGBITVECTOR >= 0 && DEBUGBITVECTOR == i))) {
        for (int bi = byte_ct - 1; bi >= 0; bi--) {
          Serial << F("      ") << F(" ") << (bi > 10 ? "" : " ") << bi << F(" ");
        }
        Serial << endl;

        dump_bit_vectors();
        if (false && DEBUGBITVECTOR >= 0 && DEBUGBITVECTOR == i) while (1) delay(20);
      }
    }

    void dump_bit_vectors() {
      // just for debug print
      byte* twobvs[2] = { dir_bit_vector, step_bit_vector };
      for (byte* bv : twobvs) {
        dump_bit_vector(bv);
      }
      Serial << endl;
    }

    void dump_bit_vector(byte *bit_vector) {
      // NB; [8] is shifted out last, so is shift-register nearest ard
      // We reorder here so it reads right-to-left
      for (int bi = 0; bi < byte_ct; bi++) {
        for (int bit_i = 0; bit_i < 8; bit_i++) {
          if ( ! (bit_i % bits_per_frame) && bit_i != 0) Serial << ".";
          Serial << ( ( bit_vector[bi] & (1 << (7 - bit_i)) ) ? '1' : '0' );
        }
        Serial << " ";
      }
      Serial << endl;
    }

    void shift_out() {
      // Send all bits,
      // each is about 60micros at 4MHz spi
      // at least 2.2micros pulse durations, and each takes 60, so ok

      byte dir_copy[byte_ct];
      unsigned long start = millis();
      if (DEBUGLOGBITVECTOR >= 4) Serial << F("shift out slow? ") << DEBUGSTUPIDSLOW << F(" | ") << (DEBUGSTUPIDSLOW ? "T" : "F") << endl;

      // SPI.transfer(byte []) overwrites the byte[] buffer,
      // and we need to preserve it
      // SO make a copy of bit_vectors
      memcpy( dir_copy, dir_bit_vector, byte_ct * sizeof(byte));

      //dump_bit_vectors();

      // use beginTransaction() to be friendly to other spi users (& disable interrupts!)
      SPI.beginTransaction(SPISettings(SLOWSPI ? 1000000 : 20000000, (SR_MSBFIRST ? MSBFIRST : LSBFIRST), SPI_MODE0));

      // Each transfer reads the input shift-register too, but we capture on the 1st one

      // the driver wants the DIR bits to be set before a STEP pulse:
      // so step-bits==reset
      if (DEBUGLOGBITVECTOR >= 4) {
        Serial << F("1st dir    ");
        dump_bit_vector(dir_copy);
      }
      SPI.transfer(dir_copy, byte_ct);
      // latch signal needs to be 100ns long, and digitalWrite takes 5micros! so ok.
      digitalWrite(latch_pin, LATCHSTART); digitalWrite(latch_pin, LATCHIDLE);
      if (DEBUGSTUPIDSLOW) delay(DEBUGSTUPIDSLOW);

      if (DEBUGLOGBITVECTOR >= 3) {
        Serial << F("step       ");
        dump_bit_vector(step_bit_vector);
      }

      // STEP pulses to high (with same DIR)
      // The bit-vector is overwritten, but we don't care (we remake it each shift_out() )
      SPI.transfer(step_bit_vector, byte_ct);
      digitalWrite(latch_pin, LATCHSTART); digitalWrite(latch_pin, LATCHIDLE);

      // but stepper wants 1 microsecond for before resetting step signal
      if (DEBUGSTUPIDSLOW) delay(DEBUGSTUPIDSLOW); 
      else delayMicroseconds(1);

      // again, because the step bit is always reset in this bit-vector, i.e. finish the step pulse.
      memcpy( dir_copy, dir_bit_vector, byte_ct * sizeof(byte));
      if (DEBUGLOGBITVECTOR >= 3) {
        Serial << F("step-reset ");
        dump_bit_vector(dir_copy);
      }
      SPI.transfer(dir_copy, byte_ct); // this is "step pulse off"
      // last latch is below...

      SPI.endTransaction(); // "as soon as possible"

      // latch "commit" (usually falling edge)
      digitalWrite(latch_pin, LATCHSTART); digitalWrite(latch_pin, LATCHIDLE);

      //delay(3000);

      if (DEBUGSTUPIDSLOW) {
        Serial << F("-- - ") << (millis() - start) << endl;
        delay(DEBUGSTUPIDSLOW);
      }
    }

    inline int set_frame( byte * bit_vector, int frame_i, byte mask, byte value ) {
      // In the bit_vector
      // at the [frame_i] (lsb)
      // update the bits to `value`, masked by `mask`
      // returns the byte_i
      // byte[0] will be the furthest shift register [n]
      // and we are doing msbfirst, so bit[0] is the nearest bit

      // If we were 0=[0]:
      const int byte_i = (frame_i * bits_per_frame ) / (sizeof(byte) * 8);
      // But, we need 0=[n] byte, low-nybble
      const int r_byte_i = (byte_ct - 1) - byte_i;
      const int frames_per_byte = (sizeof(byte) * 8) / bits_per_frame; // better be multiple!
      const int offset = (frame_i % frames_per_byte ) * bits_per_frame; // frame0=0, frame1=2, frame2=3, etc
      if (DEBUGFRAME > 0) {
        Serial << F("    set frame ") << frame_i << F(" = 0b") << _BIN(value) << F(" mask ") << _BIN(mask)
               << F(" byte[") << r_byte_i << F("]")
               << F(" frameoffset ") << (frame_i % frames_per_byte ) << F(" << offset ") << offset
               << F(" == mask 0b") << _BIN(mask << offset) << F(" = ") << _BIN(value << offset)
               << endl;
      }
      bit_vector[ r_byte_i ] = ( bit_vector[ r_byte_i ] & ~(mask << offset) ) | (value << offset);
      return r_byte_i;
    }

    void fault() {
      //disable();
      Serial << "YIKES FAULT" << endl;
      while (1) {
        delay(100);
      }; // and lock up
    }
};

#undef DEBUGBITVECTOR
