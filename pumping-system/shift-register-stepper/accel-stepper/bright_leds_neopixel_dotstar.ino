#if defined(PIN_NEOPIXEL) && defined(NEOPIXEL_NUM) && NEOPIXEL_NUM > 0
  #include <Adafruit_NeoPixel.h>
  Adafruit_NeoPixel neopixels(NEOPIXEL_NUM, PIN_NEOPIXEL);
  void clear_bright_leds() {strip.begin();strip.clear();strip.show();}
#elif defined(DOTSTAR_NUM) && DOTSTAR_NUM > 0
  #include <Adafruit_DotStar.h>
  Adafruit_DotStar strip(DOTSTAR_NUM, PIN_DOTSTAR_DATA, PIN_DOTSTAR_CLK);
  void clear_bright_leds()  {strip.begin();strip.clear();strip.show();}
#elif defined(NEOPIXEL_BUILTIN)
  #include <Adafruit_NeoPixel.h>
  Adafruit_NeoPixel strip(1, NEOPIXEL_BUILTIN);
  void clear_bright_leds()  {strip.begin();strip.clear();strip.show();}
#else
inline void clear_bright_leds() {Serial << "NO DOT/NEO" << endl;}
#endif

//void setup() {
//  clear_bright_leds();
