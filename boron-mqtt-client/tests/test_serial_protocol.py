"""
    def test_x(  # magic prefix `test_`
        tmp_path # automagically gets a temp-dir, a fixture
        $fixture # run the @pytest.fixture $fixture, pass value as this var (recurse arg logic in fixtures!)
        )

    fixtures are "instantiated" once per test_x(), for it's whole tree-dependencies of fixtures
    @pytest.fixture(
        autouse=True # runs `x` for all tests that can see this, i.e. `x` not magically accessible
        scope="module" # once per module (file), not per test_x
            ="session"
    def x(...) 

    conftest.py
        any @pytest.fixture is visible to all test-files in same dir

"""
import pytest
import serial,subprocess
import os,re,time,signal
from datetime import datetime

def response(serial_port, resp, timeout):
    """
    assert response(port, b'....', timeout=4),"expected ..."
    """
    try:
        with Timeout(seconds=timeout):
            x=b''
            while x != resp: # b'$Pubx$=\n':
                x = serial_port.readline()
                if x != b'':
                    print(f"> {x}")
            return True
    except TimeoutError as e:
        return False
    except serial.serialutil.SerialException as e:
        if 'read failed: Timeout' in str(e):
            return False
        else:
            raise e

import paho.mqtt.client as paho
import ssl

def aio_subscribe(aio_userpass, topic ):
    """ setup the subscription """
    mqtt = paho.Client(client_id=None, clean_session=True, userdata=None)
    mqtt.username_pw_set(username=aio_userpass['IO_USERNAME'], password=aio_userpass['IO_KEY'])
    mqtt.tls_set( certfile=None,
                   keyfile=None,
                  cert_reqs=ssl.CERT_REQUIRED,
                  tls_version= ssl.PROTOCOL_TLS_CLIENT
                  )
    mqtt.connect('io.adafruit.com',port=8883)

    messages = []
    def subscriber(mqtt, userdata, message):
        # we just collect them, aio_messages reacts to them
        #print(f"## MQTT {message.topic}={message.payload}")
        messages.append(str(message.payload.decode("utf-8")))

    mqtt.on_message = subscriber
    mqtt.subscribe(topic, qos=1)
    mqtt.loop_start()
    return messages

def aio_message( messages, prefix, timeout=5 ):
    """ matches on prefix """
    started = time.monotonic()
    first_time = True
    try:
        with Timeout(seconds=timeout):
            while(True):
                if messages:
                    msg = messages.pop(0)
                    if msg.startswith(prefix):
                        return True
                    else:
                        print(f">>discard: {msg}")
                time.sleep(0.1)
                if time.monotonic() - started > 0.5 and first_time:
                    print(f"## waiting for mqtt '{prefix}...'")
                    first_time=False
    except TimeoutError as e:
        return False


class Timeout:
    """ 
        try:
            with Timeout(seconds=6):
                stuff
        except TimeoutError as e:
            raise AssertionError("unexpected " + str(e))

    """
    def __init__(self, seconds=1, error_message='Timeout'):
        self.seconds = seconds
        self.error_message = error_message
    def handle_timeout(self, signum, frame):
        raise TimeoutError(self.error_message)
    def __enter__(self):
        signal.signal(signal.SIGALRM, self.handle_timeout)
        signal.alarm(self.seconds)
    def __exit__(self, type, value, traceback):
        signal.alarm(0)

def backtic(*cmd,shell=False):
    proc = subprocess.run(
        cmd,
        shell=shell,
        stdout=subprocess.PIPE,
        check=False,
        encoding='utf-8',
        )
    return proc.stdout.rstrip()

@pytest.fixture(scope="module")
def boron_serial():
    possible = backtic("which particle >/dev/null && particle serial list | egrep '/dev/'", shell=True).split("\n")
    if possible:
        print(f"# usb via particle-cli: {possible}")
        possible = [ x.split(' ',2)[0] for x in possible]
        print(f"## usb via particle-cli: {possible}")
    else:
        possible = []
    possible.append( ['/dev/ttyUSB0','/dev/ttyUSB1'] )
    serial_dev = os.getenv('BORONUSB',None) or next( (x for x in possible if os.path.exists(x) ), None)
    assert serial_dev != None, "Looking for /dev/tty/USB*"
    print(f"### extant usb {serial_dev}")

    serial_port = serial.Serial(
        port=serial_dev, # to boron
        baudrate=115200,
        parity=serial.PARITY_NONE,
        stopbits=serial.STOPBITS_ONE,
        bytesize=serial.EIGHTBITS,
        timeout=2
    )
    serial_port.write(b'#\n')
    serial_port.readline()
    return serial_port
 
@pytest.fixture(scope="module")
def boron_serial_cleanup(boron_serial):
        boron_serial.write(b'\n')    

        was = boron_serial.timeout
        boron_serial.timeout = 0.0
        x="<>"
        while x != b'':
            time.sleep(0.1)
            x = boron_serial.readline() # cleanup
            print(">",x)

        boron_serial.timeout = was
        print("Prime serial and consume")
        return None
    
def consume(serial_device):
    x="<>"
    while x != b'':
        time.sleep(0.01)
        x = serial_device.readline() # cleanup
        print(">",x)

@pytest.fixture(scope="session")
def aio_userpass():
    aio_key_file = '../zerow-minus80/config/aio.key'

    assert os.path.exists(aio_key_file),f"Need {aio_key_file}"
    
    rez={}
    with open(aio_key_file,'r') as fh:
        for line in fh:
            #export IO_USERNAME="awgrover"
            #export IO_KEY="8a296a6f43e04be6be78df29180a8562"
            if m := re.match(r'export ([^=]+)=(.+)', line):
                key,value = m.groups()
                value = re.sub('"','',value) # strip "
                rez[key] = value
    print(f"## IO user/pass {rez}")
    return rez
    
@pytest.fixture(scope="module")
def do_ping(boron_serial):
    boron_serial.write(b'#\n')    
    assert response(boron_serial, b'#\n', timeout=1),"saw ping ack"

@pytest.fixture(scope="module")
def aio_login(boron_serial, boron_serial_cleanup, aio_userpass):
    if not os.environ.get('SKIPLOGIN',False):
        boron_serial.write(f'%Sup%{aio_userpass["IO_USERNAME"]} {aio_userpass["IO_KEY"]}\n'.encode())    
        assert response(boron_serial, b'%Sup%=\n', timeout=2),"saw user-pass ack"
        print("AIO login")

class TestSerialProtocol:
    def test_test(self):
        assert 1==1,"yes it does"
        #assert 1==2,"nope"
    
    def test_ping(self,boron_serial, boron_serial_cleanup):
        boron_serial.write(b'#\n')    
        x = boron_serial.readline()
        assert x == b'#\n',"Ping ack"

    def test_userpass(self, aio_login):
        pass

    def test_publish(self, boron_serial,aio_login):
        consume(boron_serial)

        boron_serial.write(f"$Pub$alarm.summary pytest {datetime.now().isoformat()}\n".encode())

        # can take time to connect
        assert response(boron_serial, b'$Pub$=\n', timeout=6),"saw response"

    @pytest.mark.wait_connect
    def test_aio_published(self, boron_serial, aio_userpass, aio_login):
        messages = aio_subscribe( aio_userpass, f'{aio_userpass["IO_USERNAME"]}/f/alarm.summary')

        prefix = "pytest-published"

        # the initial connect can take a bit due to polling for ready states
        time.sleep(1) # but, we don't need to do that after the boron has done initial connect. bummer

        boron_serial.write(f"$Pub$alarm.summary {prefix} {datetime.now().isoformat()}\n".encode())
        # can take time to connect
        assert response(boron_serial, b'$Pub$=\n', timeout=6),"saw response"

        # Actual mqtt response
        assert aio_message( messages, prefix ),"Published (round-trip)"
