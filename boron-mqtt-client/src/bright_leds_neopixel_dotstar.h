#pragma once

#if defined( PIN_NEOPIXEL )
#include <Adafruit_NeoPixel.h>
extern Adafruit_NeoPixel strip;
#endif

class LEDBuiltinClass {
  public:
#ifdef LED_BUILTIN
    // because some, esp32-c3, have a neopixel builtin, but not led-builtin

    void begin() {
      pinMode(LED_BUILTIN, OUTPUT);
    }

    void toggle() {
      digitalWrite( LED_BUILTIN, ! digitalRead(LED_BUILTIN) );
    }

    void set(boolean hilo) {
      digitalWrite( LED_BUILTIN, hilo);
    }

#elif defined( PIN_NEOPIXEL )
    // no LEDBUILTIN, so hack to treat it like one...

    // toggle/hilo is accomplished by remembering the last !=0 value
    //Adafruit_NeoPixel strip = Adafruit_NeoPixel(1, PIN_NEOPIXEL, NEO_GRB + NEO_KHZ800);

    uint8_t was[3] = {0xFF, 0x0, 0x0}; // FIXME: assumed RBG not WRGB

    void begin() {
      strip.begin();
      strip.setBrightness(20);
      strip.fill(0xFF0000);
      strip.show();
    }

    void toggle() {
      uint8_t is[3];
      memcpy( is, strip.getPixels(), 3 );
      uint32_t isis = strip.getPixelColor(0);
      if (isis == 0) {
        memcpy( strip.getPixels(), was, 3 );
      }
      else {
        memcpy( was, is, 3 );
        strip.setPixelColor(0, 0);
      }
      strip.show();
    }

    void set(boolean hilo) {
      uint8_t is[3];
      memcpy( is, strip.getPixels(), 3 ); // [0] is first pixel
      uint32_t isis = strip.getPixelColor(0);
      if (hilo == HIGH) {
        if (isis != 0) {
          return; // leave it on
        }
        else {
          memcpy( strip.getPixels(), was, 3 );
          //Serial << F("->") << _HEX(was[0]) << _HEX(was[1]) << _HEX(was[2]) << endl;
          strip.show();
        }
      }
      else {
        if (isis == 0) {
          return; // leave it off
        }
        else {
          memcpy( was, is, 3 );
          //Serial << F("<-") << _HEX(was[0]) << _HEX(was[1]) << _HEX(was[2]) << endl;
          strip.setPixelColor(0, 0);
          strip.show();
        }

      }

    }

#else
  static_assert(0,"Need a LED_BUILTIN or PIN_NEOPIXEL");
#endif

};
LEDBuiltinClass LedBuiltin; // do .begin, .toggle
// FIXME: this has gotten ridiculous

#if ( defined(PIN_NEOPIXEL) || defined(NEOPIXEL_BUILTIN) ) && ! defined(NEOPIXEL_NUM)
// adafruit qt_py_esp32-c3 does not set the _NUM. sigh
// nor some other adafruit built-in neopixel boards
#define NEOPIXEL_NUM 1
#endif

#if defined CPLAY_NEOPIXELPIN
void clear_bright_leds() {} // doesn't turn on a neopixel, by default
// CircuitPlayground.begin()
// CircuitPlayground.strip.clear(); CircuitPlayground.strip.show();}

#elif ( defined(PIN_NEOPIXEL) || defined(NEOPIXEL_BUILTIN) ) && defined(NEOPIXEL_NUM) && NEOPIXEL_NUM > 0
#ifdef LED_BUILTIN
#include <Adafruit_NeoPixel.h>
Adafruit_NeoPixel strip(NEOPIXEL_NUM, PIN_NEOPIXEL);
void clear_bright_leds() {
  LedBuiltin.begin();
  strip.begin();
  strip.clear();
  strip.show();
}
#elif defined(ARDUINO_ADAFRUIT_QTPY_ESP32C3)
#include <Adafruit_NeoPixel.h>
Adafruit_NeoPixel strip(1, PIN_NEOPIXEL, NEO_GRB + NEO_KHZ800);
void clear_bright_leds() {
  LedBuiltin.begin();
}
#elif defined(ADAFRUIT_NEOPIXEL_H)
Adafruit_NeoPixel strip(NEOPIXEL_NUM, PIN_NEOPIXEL);
void clear_bright_leds() {
  LedBuiltin.begin();
}
#else
static_assert(1 == 0, "No LEDBUILTIN and don't know if NeoPixel " ## ARDUINO_BOARD );
#endif // LED_BUILTIN vs NEO as builtin

#elif defined(DOTSTAR_NUM) && DOTSTAR_NUM > 0
#include <Adafruit_DotStar.h>
Adafruit_DotStar strip(DOTSTAR_NUM, PIN_DOTSTAR_DATA, PIN_DOTSTAR_CLK);
void clear_bright_leds()  {
  LedBuiltin.begin();
  strip.begin();
  strip.clear();
  strip.show();
}

#else
inline void clear_bright_leds() {
  LedBuiltin.begin();
  Serial.println("NO DOT/NEO");
}
#endif

//void setup() {
//  clear_bright_leds();

// Use these as highlevel

void LedBuiltinToggle() {
  LedBuiltin.toggle();
}
void LedBuiltinSet(boolean which) {
  LedBuiltin.set(which);
}
void LedBuiltinSet(uint32_t color) {
#if ( defined(PIN_NEOPIXEL) || defined(NEOPIXEL_BUILTIN) )
  strip.setPixelColor(0, color);
  strip.show();
#endif
}
