#pragma once

// MQTT config
struct mqtt_config_t {
  const char * const hostname;
  const int port;
  static constexpr unsigned int MAX_USERNAME=33;
  static constexpr unsigned int MAX_PASSWORD=33;
  char username[MAX_USERNAME] = "";
  char password[MAX_PASSWORD] = "";

};
mqtt_config_t mqtt_config = { "io.adafruit.com", 1883 }; // user/pass via CommandsService

