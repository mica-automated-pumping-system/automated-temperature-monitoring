#pragma once


/*
  Use with Streaming.h for logging.
  Use with Multiplex for logging to several streams.
  Mostly macros, so only pay for what you use.
  
  // Default is LOG_LEVEL_NONE (but `LOG << stuff` always logs)
  #define LOG_LEVEL LOG_LEVEL_INFO
  // This is the default
  #define LOG_ANNOTATIONS (LOG_ANNOTATE_LEVEL | LOG_ANNOTATE_FILE | LOG_ANNOTATE_MILLIS)
  // or you could define the whole prefix annotation
  #define LOG_ANNOTATE << "Boring "
  // You can include your own timestamp
  #define LOG_ANNOTATIONS_TIMESTAMP humanized_rtc() // return a char* or String
  // Default is Serial
  #define LOG_OUTPUT Serial1

  // all those defines BEFORE the include:
  #include "AwgLogger.h"

  LogMulti
  LOG_WARN << ...
  LOG_WARNX << ... // same level, no annotations, i.e. continue an output line
  LOG << ... // always
  LOGX << ... // always, no annotations
*/

#define AwgLogger_h

#include <Streaming.h>

#ifdef SPARK_PLATFORM
  // the particle build system has it's own log lib, with reversed order levels
  // since each .h/.cpp is compiled seperately, we can remove their LOG
  #undef LOG
  #undef LOG_DEBUG

  #define LOG_LEVEL_CMP <=
  #define LOG_LEVE_DEBUG LOG_LEVEL_ALL
  #define LOG_LEVEL_VERBOSE LOG_LEVEL_ALL
#else
  #define LOG_LEVEL_CMP >=

  enum LOG_LEVELS {
    LOG_LEVEL_NONE,
    LOG_LEVEL_ERROR,
    LOG_LEVEL_WARN,
    LOG_LEVEL_INFO,
    LOG_LEVEL_VERBOSE,
    LOG_LEVEL_DEBUG
  };
#endif

// Which annotations
// Need to be #defines for conditional log-prefix (#if)
// ...DMILLIS is the millis since last logging output: costs a little extra code
#define LOG_ANNOTATE_NONE 0b0
#define LOG_ANNOTATE_FILE 0b01
#define LOG_ANNOTATE_MILLIS 0b10
#define LOG_ANNOTATE_LEVEL 0b100
#define LOG_ANNOTATE_DMILLIS 0b1000

#ifndef LOG_OUTPUT
#define LOG_OUTPUT Serial
#endif

#ifndef LOG_LEVEL
#define LOG_LEVEL LOG_LEVEL_NONE
#endif

#ifndef LOG_ANNOTATIONS
#define LOG_ANNOTATIONS (LOG_ANNOTATE_LEVEL | LOG_ANNOTATE_FILE | LOG_ANNOTATE_MILLIS)
#endif

// tried a const function that calculates strrchr once, but wasn't any smaller
#define LOG_SHORT_FILENAME (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : strrchr(__FILE__, '\\') ? strrchr(__FILE__, '\\') + 1 : __FILE__)

#if LOG_ANNOTATIONS & LOG_ANNOTATE_LEVEL
#define LOG_ANNOTATE_LEVEL_X(level) << level << ' '
#else
#define LOG_ANNOTATE_LEVEL_X(level)
#endif

#ifdef LOG_ANNOTATIONS_TIMESTAMP
#define LOG_ANNOTATIONS_TIMESTAMP_X << LOG_ANNOTATIONS_TIMESTAMP << ' '
#else
#define LOG_ANNOTATIONS_TIMESTAMP_X
#endif

#if LOG_ANNOTATIONS & LOG_ANNOTATE_MILLIS
#define LOG_ANNOTATE_MILLIS_X << millis() << ' '
#else
#define LOG_ANNOTATE_MILLIS_X
#endif

#if LOG_ANNOTATIONS & LOG_ANNOTATE_DMILLIS
unsigned long LOG_ANNOTATE_DMILLIS_V() {
  static unsigned long last=0;
  const unsigned long now = millis(); // fetch once, saves code too
  const unsigned long delta= now-last;
  last = now;
  return delta;
}
#define LOG_ANNOTATE_DMILLIS_X << LOG_ANNOTATE_DMILLIS_V() << ' '
#else
#define LOG_ANNOTATE_DMILLIS_X
#endif

#if LOG_ANNOTATIONS & LOG_ANNOTATE_FILE
#define LOG_ANNOTATE_FILE_X << LOG_SHORT_FILENAME << ':' << __LINE__ 
#else
#define LOG_ANNOTATE_FILE_X
#endif

#define LOG_ANNOTATE_FUNC << ':' << __FUNCTION__

#ifndef LOG_ANNOTATE
#define LOG_ANNOTATE(level)  << '[' LOG_ANNOTATE_LEVEL_X(level) LOG_ANNOTATIONS_TIMESTAMP_X LOG_ANNOTATE_MILLIS_X LOG_ANNOTATE_DMILLIS_X LOG_ANNOTATE_FILE_X LOG_ANNOTATE_FUNC << F("] ")
#endif

#define LOG LOG_OUTPUT LOG_ANNOTATE(' ')
#define LOGX LOG_OUTPUT
#define LOG_ERROR if (LOG_LEVEL LOG_LEVEL_CMP LOG_LEVEL_ERROR) LOG_OUTPUT LOG_ANNOTATE('E')
#define LOG_ERRORX if (LOG_LEVEL LOG_LEVEL_CMP LOG_LEVEL_ERROR) LOG_OUTPUT
#define LOG_WARN if (LOG_LEVEL LOG_LEVEL_CMP LOG_LEVEL_WARN) LOG_OUTPUT LOG_ANNOTATE('W')
#define LOG_WARNX if (LOG_LEVEL LOG_LEVEL_CMP LOG_LEVEL_WARN) LOG_OUTPUT
#define LOG_INFO if (LOG_LEVEL LOG_LEVEL_CMP LOG_LEVEL_INFO) LOG_OUTPUT LOG_ANNOTATE('I')
#define LOG_INFOX if (LOG_LEVEL LOG_LEVEL_CMP LOG_LEVEL_INFO) LOG_OUTPUT
#define LOG_VERBOSE if (LOG_LEVEL LOG_LEVEL_CMP LOG_LEVEL_VERBOSE) LOG_OUTPUT LOG_ANNOTATE('V')
#define LOG_VERBOSEX if (LOG_LEVEL LOG_LEVEL_CMP LOG_LEVEL_VERBOSE) LOG_OUTPUT
#define LOG_DEBUG if (LOG_LEVEL LOG_LEVEL_CMP LOG_LEVEL_DEBUG) LOG_OUTPUT LOG_ANNOTATE('D')
#define LOG_DEBUGX if (LOG_LEVEL LOG_LEVEL_CMP LOG_LEVEL_DEBUG) LOG_OUTPUT

#define SSTRIZE(x) SSTRIZE_x(x)
#define SSTRIZE_x(x) #x
#define LOGGING_EXPLAIN Serial << F("LOGGER ") << F(SSTRIZE(LOG_OUTPUT)) << F(" level ") << LOG_LEVEL << F(" annot 0b" ) << _BIN(LOG_ANNOTATIONS) << endl
