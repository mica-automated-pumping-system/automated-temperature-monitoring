#pragma once

template <typename T>
class LinkList {
  public:
    T * const head;
    LinkList *rest = NULL;

    //LinkList() : head(NULL) {}
    //LinkList(T * const it) : head(it) {}
    template<typename... TE>
    LinkList( T * const one, TE const...  entries ) : head( one ) {
      T* const entries_array[] = { entries... };
      const int entries_ct = sizeof...(entries);

      for (int i = 0; i < entries_ct; i++) {
        append( entries_array[i] );
      }
    }

    void append(T * const it) {
      //if (Serial) LOG_DEBUG << F("    LL append 0x") << _HEX((unsigned long) it) << endl;
      LinkList *i;
      // not-recursing, stack (prob doesn't matter)
      for ( i = this; i->rest != NULL; i = i->rest ) {
        // to find end
      }
      i->rest = new LinkList( it );
    }
};
