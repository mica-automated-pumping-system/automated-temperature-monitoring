#pragma once

/*
  Usage:
    // #define SERVICE_MANAGER_LOG 0 // to quiet informational messages
    // #define SERVICE_MANAGER_LOG 1 // or higher for errors
    // #define SERVICE_MANAGER_LOG 2 // or higher for warnings
    // #define SERVICE_MANAGER_LOG 3 // or higher for informational
    // #define SERVICE_MANAGER_LOG 4 // or higher for more debug
    // #define SERVICE_MANAGER_LOG 100+i // specific debug about service [i]
    #include "Service.h"

    // create additional .ino files, implement ServiceInterface below.
    // create a zServiceList.ino (needs to be last by sorted name)
    volatile YourService yb; // init adds to service-manager


*/

#include "LinkList.h"
#include "AwgLogger.h"
#include "Every.h"

// fixme: use some more common logging lib
enum ServiceManagerVerbosity { 
  ServiceManagerVerbosity_Quiet, 
  ServiceManagerVerbosity_Error, 
  ServiceManagerVerbosity_Warning, 
  ServiceManagerVerbosity_Info, 
  ServiceManagerVerbosity_Debug 
};
#ifndef SERVICE_MANAGER_LOG
// see comments above, default==info
#define SERVICE_MANAGER_LOG ServiceManagerVerbosity_Info
#endif

#include <Streaming.h> // Mikal Hart <mikal@arduiniana.org>
#include "array_size.h"

class ServiceInterface {
  public:
    enum Statuses { INITIAL, OK, RECOVERY, FAIL, StatusesCount }; // keep initialization StatusesString below in sync
    static const __FlashStringHelper* StatusesString[];

    ServiceInterface::Statuses status = ServiceInterface::Statuses::INITIAL;
    //ServiceInterface(); // defined below

    virtual const char * const description() const = 0;
    virtual void begin() = 0;
    virtual void run() = 0;

    virtual const __FlashStringHelper* status_string() {
      return ServiceInterface::StatusesString[ status ];
    }

};
const __FlashStringHelper* ServiceInterface::StatusesString[] = { F("INITIAL"), F("OK"), F("RECOVERY"), F("FAIL")  };
static_assert( array_size(ServiceInterface::StatusesString) == ServiceInterface::Statuses::StatusesCount, "Not enough strings for enums");

class ServiceWithValue : public ServiceInterface {
    // for alarmtree
  public:
    int value = 0; // update duing .run
};

class ServiceJustValue : public ServiceWithValue {
    // for helpers from some Service that reads multiple values
  public:
    const char * const _description;
    // still need description() though
    ServiceJustValue(const char* const description) : _description(description) {}
    const char * const description() const {
      return _description;
    }

    void begin() {}
    void run() {}
};

template <typename T>
class ServiceManagerClass {
    // There should only be one, singleton fixme
  public:

    unsigned long run_too_long;

    Every *say_debug_run = new Every(300);

    LinkList<T> *services = NULL;

    ServiceManagerClass( unsigned long run_too_long = 1000 ) : run_too_long(run_too_long) {}

    virtual const char * const description() const {
      return "Service";
    }

    void add_service( T* const s) {
      if (Serial) {
        LOG << F("    0x") << _HEX((unsigned long) s) << F(" onto 0x") << _HEX((unsigned long) services) 
          << F(" ") << s->description()
          << endl;
      }
      if (!services) {
        services = new LinkList<T>( s );
      }
      else {
        services->append( s );
      }
    }

    void begin() {
      int service_count = 0;
      for (LinkList<T>* i = services; i != NULL; i = i->rest) {
        service_count += 1;

        if ( ServiceManagerVerbosity_Info <= SERVICE_MANAGER_LOG ) {
          LOG << description() << F(" [") << (service_count - 1) << F("] ");

          LOGX << "'" << i->head->description() << "'";

          if (ServiceManagerVerbosity_Debug <= SERVICE_MANAGER_LOG) {
            LOGX << F(" 0x") << _HEX( (unsigned long) i->head ) << F(" ");
          }
          LOGX << endl;
        }

        // Run it->begin()
        unsigned long begin_time = millis();
        i->head->begin();
        unsigned long begin_duration = millis() - begin_time;

        if (ServiceManagerVerbosity_Info <= SERVICE_MANAGER_LOG) {
          boolean started = (i->head->status == ServiceInterface::Statuses::OK)
            || (i->head->status == ServiceInterface::Statuses::INITIAL)
            || (i->head->status == ServiceInterface::Statuses::RECOVERY)
            ;
          LOG << F("    ") << description() << F(" [") << (service_count - 1) << F("] :")
                 << F("  <")
                 << (started
                    ? F("") 
                    : F("Fail: ") 
                    )
                 << i->head->status_string() << F(">")
                 << F(" in ") << begin_duration
                 << endl;
        }

      }
    }

    void run() {
      //LOG_DEBUG << F("SM.run 0x") << _HEX((unsigned long) this) << F("..") << endl;

      // rate limit debug output
      static Every::Timer allow_status_output(200); // treat as initially expired

      int service_count = 0;
      for (LinkList<T>* i = services; i != NULL; i = i->rest) {
        service_count += 1;
        //LOG_DEBUG << F("  sm[") << (service_count-1) << F("] ") << i->head->description() << F("...") << endl;

        if (SERVICE_MANAGER_LOG == 100 + (service_count - 1) && (*say_debug_run)() ) {
          LOG << F("Service [") << (service_count - 1) << F("] ");
          if (i->head->status != ServiceInterface::Statuses::OK) {
            LOGX << F("<") << ServiceInterface::StatusesString[ i->head->status ] << F("> ");
          }
          LOGX << F(": ") << i->head->description()
                 << endl;
        }

        ServiceInterface::Statuses was_state = i->head->status;
        unsigned long start=millis();
        i->head->run();
        unsigned long took=millis()-start;
        ServiceInterface::Statuses new_state = i->head->status;

        // seems unlikely that we need to rate-limit this debug output, and it would hide status changes: confusing
        if (
            (ServiceManagerVerbosity_Info <= SERVICE_MANAGER_LOG && new_state != was_state) // && allow_status_output())
            || ( ServiceManagerVerbosity_Warning <= SERVICE_MANAGER_LOG && took >= run_too_long)
          )
          {
          LOG << F("Service [") << (service_count - 1) << F("] ")
                 << F("<") << i->head->status_string() << F("> ")
                 << F(": ") << i->head->description();
          if ( took >= run_too_long ) LOGX << F(" SLOW ") << took;
          LOGX << endl;

          allow_status_output.reset();
        }
      }
      //LOG_DEBUG << F("...SM.run 0x") << _HEX((unsigned long) this) << endl;
    }



};


// after declarations



// The singleton
ServiceManagerClass<ServiceInterface> ServiceManager;

/*
   buidling tree now, can't do dynamic add
  // need to define after definition. yes, I don't want to do a .cpp
  ServiceInterface::ServiceInterface() {
  ServiceManager.add_service(this);
  };
*/
