#pragma once

#include "Service.h"
#include "LEDHeartBeat.h"
#include "Serial1Service.h"
#include "CommandsService.h"
#include "mqtt_config.h"
#include "BoronMQTTService.h"

// Includes needed in here:

namespace zServicesList {

BoronMQTTService boron_mqtt_service(&mqtt_config, 80 /* max */, 5*60 /* keepalive */); // need for the command-grammar action

void add_services() {
  // called from the main .begin(), to build the service list, and alarm tree

  // some references
  CommandsService_Commands::mqtt_service = &boron_mqtt_service;

  Serial << F("Setup service list") << endl;

  ServiceManager.add_service( new LEDHeartBeat() );
  ServiceManager.add_service( new Serial1Service() );
  ServiceManager.add_service( new CommandsService(&Serial) );
  ServiceManager.add_service( new CommandsService(&Serial1) );
  ServiceManager.add_service( &boron_mqtt_service );

  Serial << F("Done setting up service list") << endl;

  Serial << endl;
}


};
