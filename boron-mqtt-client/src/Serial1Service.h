#pragma once

class Serial1Service : public ServiceInterface {
  public:
    long baud;
    Serial1Service(long baud=115200) : baud(baud) {}

    const char * const description() const { return "Serial1"; }
    void begin()  { 
      // Seems to need the explicit pins for esp32 (cf. programming uses the uart)
      Serial1.begin(baud
      #ifdef ESP32
      , SERIAL_8N1, RX, TX
      #endif
      );
      status = ServiceInterface::Statuses::OK;
      LOG_INFO << description() << F(" baud ") << baud << endl;
    }
    void run() {}
};
