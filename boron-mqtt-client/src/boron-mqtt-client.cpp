/******************************************************/
//       THIS IS A GENERATED FILE - DO NOT EDIT       //
/******************************************************/

#include "Particle.h"
#line 1 "/home/awgrover/dev/contract/ryanhoover/minus80/boron-mqtt-client/src/boron-mqtt-client.ino"
/*
  Service framework.
  See zServicesList.h for actual services

  See Service.h for plugin mechanism.

  NB: seems to generate "warning: changing start of section .bss", which are said to be: harmless, not fixable
*/

#include "prefix.h" // "hook" for your early include/compile/checks

void setup();
void loop();
#line 12 "/home/awgrover/dev/contract/ryanhoover/minus80/boron-mqtt-client/src/boron-mqtt-client.ino"
#ifdef _ADAFRUIT_CIRCUITPLAYGROUND_H_
#include <Adafruit_CircuitPlayground.h>
#endif

#include <Streaming.h> // Mikal Hart <mikal@arduiniana.org>
#include <every.h> // Alan Grover <awgrover@gmail.com>

#include "freememory.h"

#ifndef YELLOW
#define YELLOW 0xFF8000
#endif
#ifndef RED
#define RED 0xFF0000
#endif
#ifndef GREEN
#define GREEN 0x00FF00
#endif
#ifndef BLUE
#define BLUE 0x0000FF
#endif


#define SERVICE_MANAGER_DEBUG 2
#include "Service.h"

// a bit of a hack: make these defined for zServiceList
#ifdef USE_MQTTSERVICE
#include "MQTTService.h" // FIXME: this is not a base service, is it? Alarmer currently assumes it
#endif

// forward declarations we need (and expect)
/*
namespace zServicesList {
extern void add_services();
#ifdef USE_MQTTSERVICE
extern MQTTService MQTT;
#endif
};
*/

#include "LEDHeartBeat.h"

#include "zServicesList.h"

int start_free;

volatile char *stack_start = NULL; // hack for stack used, only use in setup() dynamic scope

void setup() {
  volatile char _stack_start;
  stack_start = &_stack_start;
  start_free = freeMemory();

  clear_bright_leds(); // always too bright
  //delay(1000);

  // Standard block to wait for serial on "USB CDC", aka native usb, e.g. samd21 etc's
  // if not plugged in to usb, continue eventually
  // This also gives you 3 seconds to upload if you program has a hard-hang in it
  unsigned long start = millis();
  static Every::Timer serial_wait(3 * 1000, true); // how long before continuing
  static Every fast_flash(50);
  Serial.begin(115200);
  // always wait, assume serial will work after serial_wait time
  while (! (Serial || serial_wait()) ) {
    if (fast_flash()) LedBuiltinToggle();
    delay(10); // the delay allows upload to interrupt
  }
  if (Serial) {
    while (! (serial_wait()) ) { // also, particle serial console is slow to notice us
      if (fast_flash()) LedBuiltinToggle();
      delay(10); // the delay allows upload to interrupt
    }
  }
  Serial.println("\n");
  Serial.print( millis() - start );
  Serial.println(F(" Start " __FILE__ " " __DATE__ " " __TIME__));
  Serial <<  F("free ") << start_free << endl;

  zServicesList::add_services();
  Serial << F(" after building zServicesList, free ") << freeMemory()  << F(" used ") << (start_free - freeMemory()) << endl;
  { volatile char here; Serial << F("Stack used ") << (&here - stack_start) << endl; }
  ServiceManager.begin();

  Serial << endl;

  Serial << (millis() - start) 
      /* << F(" Setup Done, free ") << freeMemory() << F(" used ") << (start_free - freeMemory() - start_free) */ 
      << F(" Setup Done, free ") << freeMemory()  << F(" used ") << (start_free - freeMemory())
      << endl << endl;
}

void loop() {
  static boolean first = true;
  static unsigned long last_heartbeat = millis();
  #ifndef LOOP_TOO_LONG
  #define LOOP_TOO_LONG 100
  #endif
  constexpr unsigned long too_long = LOOP_TOO_LONG;
  
  if (first) {
    first = false;
    /* Serial << F("1st loop, free ") << freeMemory() << F(" used ") << (freeMemory() - start_free) << endl; */
  }

  ServiceManager.run();

  if ( millis() - last_heartbeat  > too_long ) {
    Serial << F("WARNING: was delayed longer than a heartbeat ") << too_long
           << F(" -> ") << (millis() - last_heartbeat)
           << endl;
  }
  last_heartbeat = millis();

}
