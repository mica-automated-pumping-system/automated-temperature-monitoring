#ifdef PREFIX_H
#error "Only include prefix.h once"
#endif

#ifndef PREFIX_H
#define PREFIX_H
#endif

/*
 Put things here that need to be compiled/included before anything else.
 E.g. architecture detect, etc.
 
 */

#ifndef SPARK_PLATFORM
#error "Must use Particle dev system, cannot use Arduino IDE"
#endif

#if PLATFORM_ID == PLATFORM_BORON
#define LED_BUILTIN 7
#endif

SYSTEM_THREAD(ENABLED);
SYSTEM_MODE(AUTOMATIC);

#define TOPIC_DIGIT_OBSOLETE 1

// Put the define in .vscode/settings.json
// #define DEBUG_MQTT_SERIAL_OUTPUT
