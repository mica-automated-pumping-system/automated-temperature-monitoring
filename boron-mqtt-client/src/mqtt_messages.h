  // n<payload[0..32]>\n
  struct mqtt_message_t {
    const char * const topic;
    const char * const payload_kv; // i.e. "blah" : "blah", extra payload, final is: "{ $payload_kv, $msgpayload }"
    void print() {
      Serial << topic << F(" = ") << F("{ ") << payload_kv << F(", ... }") << endl;
    }
  };

  // Corresponds to the commmands 0...n
  mqtt_message_t mqtt_messages[] = {
    { "hello", "" },
    { "alarm.temp", "" },
    { "alarm.power", "" },
    { "alarm.wifi", "" },
    { "minus80.minus80", "" }
  };
