#pragma once

// The "server" on the Boron

#include "Service.h"
#include "Parsing.h"
#include "mqtt_config.h"
#include "mqtt_messages.h"
#include "BoronMQTTService.h" // for the mqtt_service type 

//#define TOPIC_DIGIT_OBSOLETE // marker for obsoleting "old" mqtt publish command

// 1 for a little parsing debug output
#define DEBUG_PARSING 0

namespace CommandsService_Commands {
  // the grammar for our commands
  // Wanted single letter commands, but esp32's emit tons of garbage during programming and booting, so longer is safer

  // Who we are communicating with, e.g. for responses etc
  Stream *CurrentSerial = &Serial; // default init. it's a hack. set in 

  // things we need for some actions:
  BoronMQTTService* mqtt_service = NULL; // shouldn't change after 1st assignment in zServicesList.h

  // fixme: the parsing grammar is static overall, implying that CommandsService should be a singleton, but there are 2: Serial & Serial1

  // S<count>username password"
  Parsing::BaseClass * const secrets_a[] = {
    new Parsing::ParseString(F("%Sup%"), "%Sup%"), // "set user/pass"
    new Parsing::TillDelimiter(F("username"), ' ', mqtt_config.username, mqtt_config.MAX_USERNAME),
    new Parsing::TillDelimiter(F("password"), '\n', mqtt_config.password, mqtt_config.MAX_PASSWORD)
  };
  Parsing::Sequence secrets( F("mqtt secrets"), secrets_a, array_size( secrets_a ), 
    [](){
      *CurrentSerial << F("%Sup%=\n"); // ACK message
      Serial << F("M.u ") << mqtt_config.username << F(" M.p ") << mqtt_config.password << endl; // debug message
    }
  );

  #ifndef TOPIC_DIGIT_OBSOLETE
  // our build space for a message
  struct chosen_mqtt_message_t {
    unsigned int message_i;
    static constexpr unsigned int MAX_PAYLOAD=20;
    char payload[MAX_PAYLOAD];

    friend Stream & operator << (Stream &out, const chosen_mqtt_message_t &self) {
      // just for debugging, see BoronMQTTService.h .publish
      mqtt_message_t it = mqtt_messages[self.message_i];
      out << F("<[") << self.message_i << F("] ") 
        << it.topic << F(" = ") 
        << F("{") 
          << it.payload_kv 
          << (strlen(it.payload_kv) > 0 && strlen(self.payload) > 0 ? F(", ") : F(""))
          << self.payload 
        << F("}")
        << endl;
      return out;
    }
  };
  static chosen_mqtt_message_t chosen_mqtt_message;

  Parsing::BaseClass * const chosen_mqtt_a[] = {
    // nk:v\n
    new Parsing::ParseString(F("&MQTT&"), "&MQTT&"), 
    new Parsing::MoreHelp( // give Digit more help
      new Parsing::Digit(F("mqtt message"), array_size(mqtt_messages)-1,
        [](unsigned int i){ 
            chosen_mqtt_message.message_i = i;
            if (DEBUG_PARSING) {
              Serial << F("MQTT[") << i << F("] ") << endl;
            }
        }
      ),
      // The "more help", list of mqtt messages
      [](int indent){
        int m_i=0;
        for (auto m : mqtt_messages ) {
          for (int i=0; i < indent+4; i++) Serial << F(" ");
          Serial << F("[") << m_i << F("] ");
          m.print();
          m_i += 1;
        }
      }
    ),
    new Parsing::TillDelimiter(F("payload \"k\":\"v\""), '\n', chosen_mqtt_message.payload, chosen_mqtt_message.MAX_PAYLOAD, 
      [](char * const str){
        if (DEBUG_PARSING) {
          Serial << F("payload ") << str << endl;
        }
      } 
    ),
  };
  #endif // obsolete

  // MQTT publish ( `$Pub$topic payload`), with actual topic & verbatim-payload
  // nb: for AIO, the topic must have a prefix `$user/` that matches the "set" user (from %Sup%)

  // our build space for a message
  struct command_mqtt_message_t {
    static constexpr unsigned int MAX_TOPIC=100; // plus terminating \0
    char topic[MAX_TOPIC+1];
    static constexpr unsigned int MAX_PAYLOAD=1024; // plus terminating \0
    char payload[MAX_PAYLOAD+1];

    #ifdef SPARK_PLATFORM
    // particle's system
    friend Print & operator << (Print &out, const command_mqtt_message_t &self) {
      out << self.topic << F(" = ") 
        << '\'' << self.payload << '\''
        << endl;
      return out;
    }
    #else
    friend Stream & operator << (Stream &out, const command_mqtt_message_t &self) {
      // just for debugging, see BoronMQTTService.h .publish
      out << self.topic << F(" = ") 
        << '\'' << self.payload << '\''
        << endl;
      return out;
    }
    #endif
  };
  static command_mqtt_message_t mqtt_message;

  Parsing::BaseClass * const pub_a[] = {
    new Parsing::ParseString(F("MQTT publish $Pub$"), "$Pub$"),
    new Parsing::TillDelimiter(F("topic 100bytes"), ' ', mqtt_message.topic, mqtt_message.MAX_TOPIC),
    new Parsing::TillDelimiter(F("payload 1k"), '\n', mqtt_message.payload, mqtt_message.MAX_PAYLOAD),
  };
  Parsing::Sequence publish( F("mqtt publish"), pub_a, array_size( pub_a ), 
    [](){
      Serial << F("Publishing: ") << mqtt_message << endl; // debug message
        if (mqtt_service) {
          mqtt_service->publish( mqtt_message.topic, mqtt_message.payload );
          *CurrentSerial << F("$Pub$=\n"); // ACK message
          Serial << F("$Pub$=") << endl; // ACK message
        }
        else {
          Serial << F("$Pub$!") << endl; // NACK message
          Serial << F("WARNING: no mqtt_service yet!") << millis() << endl;
        }
    }
  );

  // actual commands
  Parsing::BaseClass * const commands_a[] = {
    new Parsing::ParseString(F("Ping #\\n"), "#\n", 
      [](){ 
        static unsigned int ping_ct = 0;
        if (ping_ct % 100 == 0) {
          Serial << F("(pinged ") << (ping_ct+1) << F(")") << endl; 
        }
        ping_ct += 1;
        *CurrentSerial << F("#\n");
      }
    ),
    &secrets,

    #ifndef TOPIC_DIGIT_OBSOLETE
    new Parsing::Sequence( F("mqtt"), chosen_mqtt_a, array_size( chosen_mqtt_a ),
      [](){
        // publish it
        Serial << chosen_mqtt_message;
        if (mqtt_service) {
          // FIXME: some feedback... on CurrentSerial
          mqtt_service->publish( mqtt_messages[chosen_mqtt_message.message_i], chosen_mqtt_message.payload );
        }
        else {
          // FIXME: feedback on CommandsService_Commands::CurrentSerial ?
          Serial << F("WARNING: no mqtt_service yet!") << millis() << endl;
        }
      }
    ),
    #endif // obsolete

    &publish,

    new Parsing::TillDelimiter(F("Discard till EOL"), '\n'), // doesn't fail
  };
  Parsing::Alternate commands( F("commands"), commands_a, array_size( commands_a ) );

  // Toplevel loop
  /* FIXME:
    The programming of the esp32 echos a bunch of binary crap on Serial1
      don't know what to do
    The esp32 boot-rom emits startup crap on Srial1, e.g.
      disable by pulling gpio15 to GND (inaccessable on qt py, yay)
    ESP-ROM:esp32c3-api1-20210207\r\n
    Build:Feb72021\r\n
    rst:0x1(POWERON),boot:0xd(SPI_FAST_FLASH_BOOT)\r\n
    SPIWP:0ee\r\n
    clo1\r\n
    l:0x40x40en:010,l\r\n
    en03c\r\n
  */

  // preceded by \n
  boolean saw_hello = false; // set by  `hello`, tested by `waiting_for`
  Parsing::ParseString hello(F("waiting for #\\n"), "#\n", // named so we can say the .why
    [](){
      saw_hello = true;
      Serial << F("Proceed!") << endl;
      // Ready message
      *CommandsService_Commands::CurrentSerial << F("\n#\n");
    }
  );
  Parsing::BaseClass * const waiting_for_a[] = {
    &hello,
    new Parsing::Discard() // till eol
  };
  Parsing::Loop waiting_for( F("waiting for"),
    new Parsing::Alternate( F("waiting for |"), waiting_for_a, array_size( waiting_for_a ) ),
    [](Parsing::Loop * z){ return saw_hello; }, // untill this, which means, saw "#\n"
    Parsing::Loop::DoNoHelp // don't dare respond to ?, because lots of noise sometimes
  );


  Parsing::BaseClass * const serial_interpreter_a[] = {
    &waiting_for,
    new Parsing::Loop(F("command-loop"), &commands, Parsing::Loop::DoPreHelp )
  };
  Parsing::Sequence serial_interpreter( F("init-commands"), serial_interpreter_a, array_size( serial_interpreter_a ) );
};

class CommandsService : public ServiceInterface {
  // Respond to commands on the serial port, mostly for demo & debug
  // Assumes the serial was setup
  // Waits for an initial "\n#\n"
  // Try '?' for help
  
  // mqtt messages are hardcode in `mqtt_messages` below

  public:

    Stream * const serial;

    boolean first_time = true;

    CommandsService(Stream * const serial) : serial(serial) {}

    const char * const description() const { return "Commands"; }
    void begin() {
      status = ServiceInterface::Statuses::OK;
    }

    void run() {
      static unsigned long unprintable_ct = 0; // debugging

      // it's a hack to let the grammar callbacks know what serial to respond to
      CommandsService_Commands::CurrentSerial = serial;

      if (first_time) {
        *CommandsService_Commands::CurrentSerial << CommandsService_Commands::hello.why << endl;
        *CommandsService_Commands::CurrentSerial << F("Try '?' for command help") << endl;
        first_time = false;
      }

      if (serial->available() >  0) {
        char achar = serial->read();

        if ( ! ( achar == '\n' || achar == '\r' || (achar >= 32 && achar <= 126) ) ) {
          // discard non-printables: we see a bunch during flashing of a esp32 (that's not nice)
          unprintable_ct += 1;
          if ( unprintable_ct % 200 == 0 ) Serial << F("Discarded unprintables ") << unprintable_ct << endl;
        }
        else if (achar == 0xD) {
          // tolerate \r\n \n\r, but we don't treat \r as eol!
          if (DEBUG_PARSING) Serial << F("discard \\r") << endl;
        }
        
        else if ( CommandsService_Commands::serial_interpreter.consume( achar ) ) {
          if (DEBUG_PARSING) {
            if ( true ) {
              // only printables
              Serial << F("consumed ");
              if (achar >= 32 ) {
                Serial << '\'' << achar << '\'';
              }
              else {
                Serial << F("0x") << _HEX(achar);
              }
              Serial << endl;
            }
          }

          // we never run off end of our parsing sequence
          if ( CommandsService_Commands::serial_interpreter.done ) {
            Serial << F("ERROR, OFF END") << endl;
            CommandsService_Commands::serial_interpreter.reset(); // to allow it to repeat
          }

          // still parsing
        }
        else {
          // x doesn't match, not-consumable
          // we never run off end of our parsing sequence
          Serial << F("ERROR top level, UNMATCHED ");
          if (achar >= 32 ) {
            Serial << '\'' << achar << '\'';
          }
          else {
            Serial << F("0x") << _HEX(achar);
          }
          Serial << endl;
          if ( CommandsService_Commands::serial_interpreter.error && ! CommandsService_Commands::serial_interpreter.at_start ) {
            CommandsService_Commands::serial_interpreter.say_error(achar);
          }
          // else { shouldn't happen }
          CommandsService_Commands::serial_interpreter.reset();
        }
      }
    }

};
