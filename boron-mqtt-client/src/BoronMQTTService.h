#pragma once

#include <MQTT.h> // https://github.com/hirotakaster/MQTT
#include "mqtt_config.h"

class BoronMQTTService : public ServiceInterface {
  /*
  Provide a publish(), which connects on demand, and is always QOS=2
  Keeps connection for some period of time,
  Provides status()
  */
  public:
    const mqtt_config_t *const mqtt_config;
    MQTT *client; // (server, 1883, MAXMQTT, MQTTKEEPALIVE, callback); // uses a TCPClient _client

    String _description;
    boolean configured = false; // cellular, time, username, password are ready/configured
    Every mqtt_initial_check = Every(500, true); // uh, polling sucks?
    String full_topic; // buffer space
    String full_payload; // buffer space
    Every::Timer connect_expire = Every::Timer( 30 * 60 * 1000 ); // only keep open this long
    boolean first_time = true;

    BoronMQTTService(const mqtt_config_t *const mqtt_config, int max, int keepalive) 
      : mqtt_config(mqtt_config),
      client(new MQTT(mqtt_config->hostname, mqtt_config->port, max, keepalive, NULL)) // no callback yet
      {
      _description += F("MQTT ");
      _description += mqtt_config->hostname;
      _description += F(":");
      _description += mqtt_config->port;

      full_payload.reserve(max+1);
      full_topic.reserve(max+1); // fixme...
      }

    const char * const description() const { return _description.c_str(); }
    void begin()  { 
      status = ServiceInterface::Statuses::OK;
      Serial << description() << endl;
    }

    void run() {
      if (! configured) {
        if (mqtt_initial_check()) {
          // poll for "everything ready"
          boolean ready = true;
          ready = ready && Cellular.ready();
          ready = ready && Time.isValid();
          ready = ready && strlen(mqtt_config->username) > 0;
          ready = ready && strlen(mqtt_config->password) > 0;
          if (first_time) {
            first_time=false;
            Serial << F("mqtt waiting for ");
            Serial << F(" Cellular ") << Cellular.ready() << F(" ->") << ready;
            Serial << F(" Time ") << Time.isValid() << F(" ->") << ready;
            Serial << F(" userpass ") << (strlen(mqtt_config->username) && strlen(mqtt_config->password)) << F(" ->") << ready ;
            Serial << endl;
          }

          if (ready) {
            Serial << F("MQTT initial connect") << endl;
            Serial << F("  ") << mqtt_config->username << F(":") << mqtt_config->password << F("@")
              << mqtt_config->hostname << F(":") << mqtt_config->port
              << endl;
            configured = true;
            publish( mqtt_messages[0].topic, "\"device\":\"Minus80Tag\"" );
          }
        }
      }
      else if ( connect_expire() ) {
        client->disconnect();
        Serial << F("Disconnect ") << millis() << endl;
      }
      else {
        client->loop();
      }
    }

  boolean publish( const char * const topic, const char* const payload ) {
    // (nb, not a MQTTService !)

    // we have to prefix with $username/f/ for AIO
    // we do not modify the payload: sent verbatim

    full_topic = mqtt_config->username;
    full_topic += "/f/";
    full_topic += topic;

    /* OBS wrapping
    full_payload = "{";

    // timestamp
    full_payload +="\"timestamp\":";
    full_payload += '"';
    if (Time.isValid()) {
      full_payload += Time.format(TIME_FORMAT_ISO8601_FULL);
    }
    full_payload += '"';

    // mqtt.payload
    if ( strlen(payload)>0 ) {
      full_payload += ',';
      full_payload += payload;
    }

    full_payload += '}';
    */ // OBS wrapping

    // full_payload = payload; // FIXME: on full obsoleting, skip the copy

    if (! configured) {
      Serial << F("Not ready to send mqtt: ");
    }
    else if ( connect() ) {
      // WARNING: QOS2 will be discarded by adafruit.io
      if ( client->publish( full_topic.c_str(), payload, MQTT::QOS1) ) {
        Serial << F(">");
      }
      else {
        Serial << F("<nopublish(notconnected)>");
      }
    }
    else {
      Serial << F("Can't connect! ");
    }
    Serial << full_topic << F(" = ") << payload << endl;

    return true;
  }

  boolean connect() {
    // or rather, ensure_connected
    if ( ! client->isConnected() ) {
      unsigned long start=millis();
      Serial << F("reconnect...") << start 
      << F(" ") << mqtt_config->hostname << F(":") << mqtt_config->port
      << endl;
      if (client->connect(System.deviceID().c_str(), mqtt_config->username, mqtt_config->password)) {
        connect_expire.reset();
        Serial << F("reconnected ") << millis() << F(" in ") << (millis()-start) << endl;
      }
      else {
        Serial << F("reconnect FAIL ") << millis() << endl;
        return false;
      }
    }
    else {
      Serial << F("connected") << endl;
    }
    return true;
  }

};
