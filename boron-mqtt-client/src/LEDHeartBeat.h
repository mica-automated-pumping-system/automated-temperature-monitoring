#pragma once
/*
  Singleton.

  Monitors the freezer's external-alarm-condition: a digital hi/low.
  Alarms for some configuration: n-secs of failure, etc.

*/
#include <Streaming.h>
#include <Every.h>

#include "array_size.h"
#include "Service.h"
#include "bright_leds_neopixel_dotstar.h"

class LEDHeartBeat : public ServiceInterface {
  public:
    static Every::Pattern *heartbeat;
    
    LEDHeartBeat() : ServiceInterface() {
    }
    const char * const description() const {
      return "LEDHeartBeat";
    }
    void begin() {
      status = ServiceInterface::Statuses::OK;
    }
    void run() {
      if ( (*heartbeat)() ) {
        //digitalWrite(LED_BUILTIN, !(LEDHeartBeat::heartbeat->state % 2) );
        LedBuiltinSet( !(LEDHeartBeat::heartbeat->state % 2) );
      }
    }
};
Every::Pattern *LEDHeartBeat::heartbeat = new Every::Pattern; // default heartbeat pattern
