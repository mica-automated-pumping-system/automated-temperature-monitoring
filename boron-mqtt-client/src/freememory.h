#pragma once

// freeMemory() should give bytes of heap that are free
//
#if defined(SPARK_PLATFORM)
  // already defined
#elif defined( __arm__ )
// should use uinstd.h to define sbrk but Due causes a conflict
extern "C" char* sbrk(int incr);
#elif defined(ESP32)
#include <Esp.h>
#else  // __ARM__
extern char *__brkval;
#endif  // __arm__

int freeMemory() {
#if defined(SPARK_PLATFORM)
  //static_assert(0,"SP");
  return System.freeMemory();
#elif defined(ARDUINO_ARCH_RP2040)
  // or TARGET_RP2040
  //static_assert(0,"RP2040");
  return rp2040.getFreeHeap();
#elif defined( __arm__ ) // has to go after some architectures
  //static_assert(0,"__arm__");
  char top;
  return &top - reinterpret_cast<char*>(sbrk(0));
#elif defined(ESP32)
  //static_assert(0,"ESP");
  return ESP.getFreeHeap();
#elif defined(CORE_TEENSY) || (ARDUINO > 103 && ARDUINO != 151)
  //static_assert(0,"Teensy/103+");
  char top;
  return &top - __brkval;
#else // ?
  static_assert(0, "What architecture is this, and how to figure free memory?");
  char top;
  return __brkval ? &top - __brkval : &top - __malloc_heap_start;
#endif
}
