# Use the particle boron as a mqtt client (with lte status): over uart

For the Particle Boron.

DO NOT USE ARDUINO IDE. Must use the particle dev system.

## Status LED (between buttons)

cyan (blue/green)       breathin        mobile connected & internet
cyan                    medium          connecting to cloud (to particle.io) 
magenta (red/blue)      medium          code loading (or firmware update)
magenta (red/blue)      breathing       "safe mode", connected to cloud (mobile+particle.io)
                                        both buttons, release RESET, wait for blink, relese MODE
green                   medium          looking for mobile (pre-code running)
                        breathing       mobile, but no particle.io
blue                    slow            "listen" for usb-serial. MODE button for three seconds, until blinking
white                   breathing       mobile off
yellow  (red/green)     medium          ready for code loading, "dfu mode"
                                        both buttons, release RESET, wait for YELLOW, release mode
red                     3/3             code crash. go to safe-mode, fix, reload
red                     SOS             hard fault
                        etc

Signal Strength
tap "mode", green flash count == bar count

## Setup this directory

Upon `git checkout` this directory contains only some files, like zServicesList.ino.
The base .ino and other files are in ../alarm-framework, and are linked into src/.
We link to them, so they are only in one place.
Note the src/.gitignore.

### alarm-framework shared files

For windows, manually create the shortcuts listed below, after "LINKS".
For linux, execute the lines after "LINKS", or run:
    (cd ..; ./make-from-alarm-framework --regen)

### Arduino libraries

Particle compilation will use all/every .ino and .h in the lib/ folder.
So, for Arduino libraries, we re-create the lib using links with just the .cpp and .h
(and especially not the examples).

See the LINKS.

## Setup Particle Dev

I needed to install the particle-cli first, to debug the usb connection
    bash <( curl -sL https://particle.io/install-cli )
    for me: rm ~.bash_profile, already had path
The clue is:
    particle usb list
        crashes with some NULL error

Download and install the udev rules
https://docs.particle.io/archives/installing-dfu-util/#installation-linux
sudo udevadm control --reload-rules && udevadm trigger
Now the particle usb list works, and the "connect" works in:

### particle cli update

        particle update-cli

### initial config (once)
    # ~/.particle/particle.config.json # should get setup somehow...
    particle tokens list # should be mostly empty
    particle token create --never-expires # copy the output!
    # edit into: ~/.particle/particle.config.json

    particle serial list # particle-identify doesn't seem to work

    # doesn't let me type at it (pyserial worked):
    particle serial monitor --port /dev/ttyACM0

## Activate the Boron

Go to the particle.io site and setup the device:

I had to
    particle update
    particle flash --local

And DON'T forget to attache the antenna, or it will never connect to the cell system.

    https://docs.particle.io/getting-started/setup/manual-setup/

I couldn't get web-based setup to work.

    Product "Minus80Tag" (general group of devices)
    Claim device
    Mark as development
    Name it as $product+$N

## local build with neopo
I could locally build with neopo:

    https://github.com/nrobinson2000/neopo

### Neopo virtual-environment install

The install directions do not work with python virtual-environment
(there's a sudo etc).

    * download the install script like:
            wget https://neopo.xyz/install
    * examine it, which was just another install script, so download that
            wget https://raw.githubusercontent.com/nrobinson2000/neopo/master/install.sh
    * Edit the script to use virtual-environment for the pip install
            - cd "$TEMPDIR/neopo"
            - $SUDOPY python3 -m pip install .
            -
            - # Run the neopo installer
            - .venv/bin/neopo install -s
            -
            - # Run Particle CLI so that it has a chance to pre-download its dependencies
            - .venv/bin/neopo particle usb configure
            + workdir=$(pwd)
            + cd "$TEMPDIR/neopo"
            + (. $workdir/.venv/bin/activate && pip3 install .)
            +
            + # Run the neopo installer
            + .venv/bin/neopo install
            +
            + # Run Particle CLI so that it has a chance to pre-download its dependencies
            + .venv/bin/neopo particle usb configure

    * run it (will ask for sudo)
            bash install.sh

### Testing setup

On development machine, in your virtual-environment:

    pip3.11 install --user pytest pyserial paho-mqtt

See the zerow-minus80/INSTALL for installing on the pi-zero

Use a usb-serial adapter for 3v microcontrollers, e.g. (Adafriut USB to TTL)[https://www.adafruit.com/product/954, to the Boron's UART (tx/rx + gnd). Do NOT connect the power! uart-revision: the Boron is 3v logic, so direct tx/rx/gnd to the pi's uart pins.

## Make

* I use `make`

    # cd to this directory, i.e. where `project.properties` is
    # try
    make help
    make where

    # build
    make compile # just compile
    make flash # compiles if needed, and flashes to boron
        make just-flash # just flash existing .dfu
        make boron_tests # to test if code is running (serial protocol test)

    # Use (copy) a file from ../alarm-framework
    make src/ln-s/$filename

* some tests are captured in make

    # Can run from a dev machine, pi zero not required
    # Must have /dev/ttyUSB0 as "serial-ttl" adapter to tx/rd of Boron
    # Boron's usb can be connected
    # Boron has to be powered
    # 2 statements, because 1 is round trip
    make boron_tests

### non-local build with workbench
Need the workbench to install the local build tools:
https://docs.particle.io/getting-started/setup/setup-options/

Install workbench:
https://docs.particle.io/quickstart/workbench/#linux

https://docs.particle.io/getting-started/developer-tools/workbench/#local-build-and-flash

## Links

INSTALLED Particle Libraries
particle library copy MQTT

LINKS to Arduino libraries
# Every, Alan Grover <awgrover@gmail.com>, https://github.com/awgrover/Every-for-arduino, 3.1.0
# make lib/every
ln -s /home/awgrover/Arduino/libraries/every/src lib/every/src
# Streaming, 6.1.1, Peter Polidoro <peter@polidoro.io>, https://github.com/janelia-arduino/Streaming
# make lib/Streaming
ln -s /home/awgrover/Arduino/libraries/Streaming/src lib/Streaming/src

LINKS to alarm-framework
ln -s ../../alarm-framework/alarm-framework.ino boron-mqtt-client/src/boron-mqtt-client.ino
ln -s ../../alarm-framework/bright_leds_neopixel_dotstar.h boron-mqtt-client/src/bright_leds_neopixel_dotstar.h
ln -s ../../alarm-framework/freememory.h boron-mqtt-client/src/freememory.h
ln -s ../../alarm-framework/LEDHeartBeat.h boron-mqtt-client/src/LEDHeartBeat.h
ln -s ../../alarm-framework/LinkList.h boron-mqtt-client/src/LinkList.h
ln -s ../../alarm-framework/Service.h boron-mqtt-client/src/Service.h
ln -s ../../alarm-framework/array_size.h boron-minus80/src/array_size.h
ln -s ../../alarm-framework/Serial1Service.h boron-minus80/src/Serial1Service.h
ln -s ../../alarm-framework/Parsing.h boron-minus80/src/Parsing.h
ln -s ../../alarm-framework/AwgLogger.h boron-minus80/src/AwgLogger.h
