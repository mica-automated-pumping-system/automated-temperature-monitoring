# Pi Image Maker

Configures os-images for Raspberry Pi, on a host Debian linux (i.e. before burning a sd-card or running the Pi).

This beta version can build on top-of any os-image, ??? builds Raspberry-Pi-OS (32bit) for the Pi Zero, Pi Zero 2. This is "beta" in the sense that it works fine so far, but not all of the flexibility is implemented.

Warning: this requires packer 1.10.* (see issue #4). If you already have newer packer installed, this won't work.

The advantages are:

* Full customization. You write the configuration recipe.
* High level interface. Doesn't require knowledge of Ansible/Packer for some common configuration. 
* Convenience. Build/configure the os on your regular (or even cloud) computer.
* Reproducibility. You write a recipe to do the build/configure, and you capture the os-image that was built.
* Updateable. You can re-run the recipe, for example updating packages, but not having to re-build earlier layers.

In summary:

* Can start with a standard Pi os-image.
* Uses Qemu and packer.io to build the images
* Uses Ansible to install/configure stuff.
* Caches the base os-image.
* Builds images step-by-step (sort of like Docker), so you don't have to rebuild base images, and can share layers across projects.
* You provide a recipe file (sort of like a Docker file) to specify the layers.
* Uses gnu-make to decide what layers to (re)make.

Yet another configurator? I couldn't find a sufficiently flexible solution, until I finished writing this one. Then I finally started finding other solutions.

Why doesn't this use Docker? The first version of this did use Docker ("gamma" version?). But, I changed to Packer particularly to avoid having to run a service daemon (like Docker does), and to enable `make` to use image files. For the behavior I wanted, I traded off Docker's "layers"/cache vs. make/Packer/Ansible features: so the complexity ended up about the same.

See below, _Other Image Makers_, where I list some other projects I found, some of which use Docker.

Will it work on _any_ os image? Maybe. The assumption it makes is a .xz image, and that we need to install the arm support for Qemu, so Packer will work with the Pi image. Ansible makes some assumptions ... "has python?". Tell me if you use this on other os images.

## Setup

Bootstrap to Ansible, and then uses Ansible to install the base. Sets up python3 in a local virtual-environment (`.venv`). You can examine what it does in: `build/install` and `build/host-setup.ansible.yml` (also has a `-n` to show-but-not-execute).

    build/install # asks for sudo access twice

## Usage

* Get a base image, e.g. Raspberry Pi os.
* Write a "recipe" to make a series of packer images. 
* Use some supplied build/*.ansible.yml files, or write your own ansible.yml files as needed for the recipe layers.
* Build image
    tools/packer-layers.py $yourrecipefile
    make -f ?????
* Burn image to sd-card
* Test the image

### Get Pi Base Image

You can use the script to get the latest:

    tools/get-raspbian --dir ~/os-images $weight $arch # creates $link-to-latest.img

e.g., 32 bit, headless for the Pi Zero (or Pi Zero W):

    tools/get-raspbian --dir ~/os-images armhf lite
    # fetched the latest (when I wrote this): 2024-03-15-raspios-bookworm-armhf-lite.img.xz

### Writing a recipe

The idea is to build a series of os-images, each one being a step and building on the previous, sort of like Docker.
The intent is to let you strategize the layers to allow re-use and reduce the amount of time it takes to build (again, like Docker).
This is particularly useful when you are developing the configuration layers, e.g. you don't have to 'apt upgrade' each time.

This framework provides some high-level functions (the DSL) for convenience in building each step. You don't need to know Ansible or Packer for some typical types of configuration, like enable-ssh, `apt install`, or `pip install`, etc. But, you can also write your own Ansible scripts.

    tools/packer-layers.py --help # lists the high-level, pre-imported functions, with some documentation

You can copy `build/example-make-pi-img` as a start. Recipe files are Python3 code, with some pre-imported functions (the DSL).

Also, look in `tools/packer-layers/` for anything `*.ansible.yml` for ansible scripts that are parameterized. Most of those are there to correspond to a DSL method. E.g. `tools/packer-layers/pip.ansible.yml` for `packer_pip(...)`

The general strategy is:

    # (the DSL functions are pre-imported, as is the `os` and `sys` modules)

    os_images = f"{os.environ['HOME']}/os-images" # stores images here, hard-coded, but to be fixed!

    # start with some image (e.g. a downloaded Pi OS)
    # (note the correspondance with the `tools/get-raspbian` above)
    baseimage( f"{os_images}/raspios/armhf/lite/2024-03-15-raspios-bookworm-armhf-lite.img.xz" )

    # create a series of layers (like Docker), aka os-images

    # DSL methods each create an os-image named after the arguments
    packer_ansible('tools/packer-layers/apt-upgrade.ansible.yml')

    # Or group them and name the layer
    # DSL methods in the block are grouped together to make the one image
    with layer_name('disable-bluetooth'):
        # do something similar and write your own ansible.yml's
        packer_ansible('tools/packer-layers/communications.ansible.yml',
            bluetooth=False
        )

    # keep doing that to build up to the final image. 

    # Your app specific configuration generally wants to be later/last
    with layer_name('python-venv'):
        # A good example of a high-level configuration:
        packer_pip(
            user="pi",
            requirements_file=f'/home/pi/{project}/capture-device/build/requirements.txt',
            venv=f'/home/pi/{project}/capture-device/.venv',
            python='python3', # happen to know it will be python3.11+
        )

Use the same name across projects to share the step, avoids rebuilding. However, watch out for name collisions across projects (I need to add the cache-per-project feature).

## Write your own ansible.yml files 

Put them anywhere, but I like to put them in a `$yourapp/build/` directory. You can parameterize the ansible files, see examples in `tools/packer-layers/*.yml`, and/or read the Ansible documentation.

    packer_ansible('build/yourcool.ansible.yml',
            # named args are passed as parameters to the ansible file, e.g.:
            noderedfile="build/coolnoderedflow"
        )

## Build image

Creates a bunch of files to build images, makefiles, var files for ansible. Then runs `make` to build them.
This tries to be idempotent, so doesn't change files unless it needs to.

    tools/packer-layers.py $yourrecipefile
    # creates a .img file

To just update the makefiles/var-files/etc (tried to be idempotent):

    tools/packer-layers.py --nomake $yourrecipefile
    # Note the line like: 
    # final: 'make' '-f' 'os_images/blahblah:yourlastlayer.makefile'

And then, to run `make`:

    make -f $thefilefromabove

## Re-Making A Layer

Delete the `.img`, and it will rebuild that layer and later ones:

    rm os_images/blahblah:somelayer.img
    make -f $thefilefromabove

If you edit your recipe (e.g $yourrecipefile above), it will only remake layers (and their descendants) that have a change:

    tools/packer-layers.py $yourrecipefile

## Debugging The Build

Try to find the error in the output. It can be tricky to find.

Some errors are about the host, e.g. can't find the *.ansible.yml file. Fix that locally.

Some errors are about the image, e.g. ansible trying to execute a command. 
You can "chroot" into the image and look around with a shell.

    # Re-build with KEEPFAIL
    env USEDEVBRANCH=1 KEEPFAIL=1 tools/packer-layers.py $yourrecipefile
    # chroot into the img that failed to finish
    tools/in-img os_images/blahblah:blah.img

    # explore with shell....
    
    # exit or ^d out of that shell when finished

    # rm the bad .img
    rm os_images/blahblah:blah.img

    # try again

## Burn Image to SD-Card

This guesses the name of the sd-card, but prompts you before burning (requires sudo):

    tools/sd-card-img out $path-to-last.img

You can then re-mount the sd-card (re-insert) and explore the file-system.

You can capture an existing sd-card with: `tools/sd-card-img in $path-to-save`

## Adding to the DSL for recipes

## Other Image Makers

Here's some other Pi image maker projects I found. This is just a starting point, I'm not keeping this up-to-date.

* (RPi-Distro/pi-gen)[https://github.com/RPi-Distro/pi-gen]. Build a Pi image from source, just like Raspberry Pi does.
* (Raspberry Pi Imager)[https://github.com/raspberrypi/rpi-imager]. Official tool to configure a Pi-os-image. Gui based tool.
* (cinderblock/RaspberryPi-Image-Edit)[https://github.com/cinderblock/RaspberryPi-Image-Edit]. An example of a common pattern with a preset list of configurations. Many other projects in the same spirit.
* (source4learn/raspberry-pi-os-image-builder)[https://github.com/source4learn/raspberry-pi-os-image-builder]. A Packer based solution. Only uses Packer scripts.
* (carlosperate/rpi-os-custom-image)[https://github.com/carlosperate/rpi-os-custom-image]. Docker based, interacts with raspi-config(I think). Multiple configuration examples.
* (kenfallon/fix-ssh-on-pi)[https://github.com/kenfallon/fix-ssh-on-pi]. Uses Ansible.
* (guysoft/CustomPiOS)[https://github.com/guysoft/CustomPiOS]. A Docker based solution, appears to be popular.

Other projects exist for making os-images from non-Raspberry-Pi-os bases. And, other projects have pre-built images for various uses (which could serve as base-images for this tool), e.g. machine-learning, etc.
