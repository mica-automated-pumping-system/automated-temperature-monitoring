variable "baseimage" {
  type    = string
}

source "arm" "pi" {
  image_build_method    = "reuse"
  #file_checksum_type    = "sha256"
  file_checksum_type    = "none"
  #file_checksum_url     = "https://downloads.raspberrypi.org/raspios_lite_armhf/images/raspios_lite_armhf-2021-11-08/2021-10-30-raspios-bullseye-armhf-lite.zip.sha256"
  #file_urls             = ["https://downloads.raspberrypi.org/raspios_lite_armhf/images/raspios_lite_armhf-2021-11-08/2021-10-30-raspios-bullseye-armhf-lite.zip"]
  # source
  file_urls             = ["${var.baseimage}"]
  file_target_extension = "img"
  #file_unarchive_cmd = ["xz", "--decompress", "$ARCHIVE_PATH"]
  file_unarchive_cmd = ["echo","$ARCHIVE_PATH"]
  #file_unarchive_cmd = ["cp", "$ARCHIVE_PATH", "$MOUNTPOINT"]
  image_chroot_env      = ["PATH=/usr/local/bin:/usr/local/sbin:/usr/bin:/usr/sbin:/bin:/sbin"]
  image_partitions {
    filesystem   = "vfat"
    mountpoint   = "/boot/firmware"
    name         = "boot"
    size         = "256M"
    start_sector = "8192"
    type         = "c"
  }
  image_partitions {
    filesystem   = "ext4"
    mountpoint   = "/"
    name         = "root"
    size         = "0"
    start_sector = "532480"
    type         = "83"
  }
  # result
  image_path                   = "/home/awgrover/os-images/raspios/armhf/lite/2024-03-15-raspios-bookworm-armhf-lite:apt-upgrade.img"
  # get current, add 1g?
  image_size                   = "3G"
  image_type                   = "dos"
  # corresponds to "ansible_host" below:
  image_mount_path             = "/tmp/rpi_mount"
  qemu_binary_destination_path = "/usr/bin/qemu-arm-static"
  qemu_binary_source_path      = "/usr/bin/qemu-arm-static"
}


build {
  sources = ["source.arm.pi"]

  provisioner "shell" {
    #inline = [ "ls -l /boot", "echo '###'", "ls -l /boot/firmware",  "mount", "cat /etc/mtab" ]
    inline = [ "df /", "df /boot/firmware", "echo ## READY" ]
  }
  provisioner "shell" {
    inline = [ "echo ## DONE" ]
  }
}
