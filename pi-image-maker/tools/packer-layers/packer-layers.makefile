MAKEFLAGS += --no-builtin-rules
.SUFFIXES:
SHELL:=/bin/bash

.PRECIOUS : %.img
%.img : %.img.xz
	## $@
	@# keeps the mod-date
	unxz --verbose --keep --force "$<"

# a specific `cp` rule will be supplied on when propagating a partition_table
.PRECIOUS : %.img.partition_table
%.img.partition_table : %.img
	## $@
	sfdisk -d "$<" > "$@"

.PRECIOUS : %.img.partition_table.json
%.img.partition_table.json : %.img.partition_table
	## $@
	echo '{ "partition" : ' > $@
	tools/sfdisk-d-to-pkr-vars "$<" >> "$@"
	echo '}' >> $@
