variable "baseimage" {
  type    = string
}
variable "nextimage" {
  type    = string
}
variable "partition" {
    # [ { start, size, type } ]
}
variable "ansible_file" {
    type = string
}
variable "ansible_var_file" {
    type = string
}

source "arm" "pi" {
  # same as 'reuse', but allows growth
  image_build_method    = "resize"

  #file_checksum_type    = "sha256"
  # FIXME: checksum
  file_checksum_type    = "none"
  #file_checksum_url     = "https://downloads.raspberrypi.org/raspios_lite_armhf/images/raspios_lite_armhf-2021-11-08/2021-10-30-raspios-bullseye-armhf-lite.zip.sha256"
  #file_urls             = ["https://downloads.raspberrypi.org/raspios_lite_armhf/images/raspios_lite_armhf-2021-11-08/2021-10-30-raspios-bullseye-armhf-lite.zip"]
  # source, ignored for "resize"
  # FIXME: drop this?
  file_urls             = ["${var.baseimage}"]
  file_target_extension = "img"
  #file_unarchive_cmd = ["xz", "--decompress", "$ARCHIVE_PATH"]
  # packer doesn't know how to cp an .img. we do that before
  file_unarchive_cmd = ["echo","$ARCHIVE_PATH"]
  # FIXME: check this. can we extract it?
  image_chroot_env      = ["PATH=/usr/local/bin:/usr/local/sbin:/usr/bin:/usr/sbin:/bin:/sbin"]
  image_partitions {
    filesystem   = "vfat"
    # documentation/tutorials have this wrong:
    mountpoint   = "/boot/firmware"
    name         = "boot"
    size         = "${var.partition[0].size}"
    start_sector = "${var.partition[0].start}"
    type         = "${var.partition[0].type}"
  }
  image_partitions {
    filesystem   = "ext4"
    mountpoint   = "/"
    name         = "root"
    # "0" for resizing
    size         = "0"
    start_sector = "${var.partition[1].start}"
    type         = "${var.partition[1].type}"
  }
  # result
  image_path                   = "${var.nextimage}"
  # get current, add 1g?
  image_size                   = "6G"
  image_type                   = "dos"
  # corresponds to "ansible_host" below:
  # FIXME: is there a var for this? then let it do the temp thing?
  # FIXME: this will break if you run more than 1 at a time...
  image_mount_path             = "/tmp/rpi_mount"
  qemu_binary_destination_path = "/usr/bin/qemu-arm-static"
  qemu_binary_source_path      = "/usr/bin/qemu-arm-static"
}

variable "ansible_pre" {
    type = string
    default = ""
}

build {
  sources = ["source.arm.pi"]

  provisioner "ansible" {
    command = "/root/.venv3/bin/ansible-playbook"
    extra_arguments = [
      "--connection=chroot",
      "-e", "ansible_host=/tmp/rpi_mount",
      "-e", "ansible_file=${ var.ansible_file }",
      "-e", "@${ var.ansible_var_file }",
      "-vvvv"
      ]
    playbook_file   = var.ansible_pre == "" ? var.ansible_file : var.ansible_pre
  }

  provisioner "shell" {
    inline = [ "echo 'PARTITION USED'", "df /" ]
  }
  provisioner "shell" {
    inline = [ "echo HOSTNAME", "ls -l /etc/hostname", "cat /etc/hostname" ]
  }

}
