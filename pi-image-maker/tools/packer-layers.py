#!/usr/bin/env -S sh -c 'exec "$(dirname $(realpath -s $0))/../.venv/bin/python3" "$0" "$@"'

"""Create images, layered in order.
    Creates an image for each layer,
    preserving the `sfdisk -d` partitioning
    using the `baseimage` name as a prefix

    I anticipate that base layers would be cached in something like `~/os-images`, since they are generic,
    and more app-specific layers would be in $projectdir (e.g. '.')

    All functions in this file, with names like `layer_xx` are part of the dsl available to your recipe.

    env DEBUG=0|1|2 # chooses logging level WARNING, INFO(default), DEBUG
    env USEDEVBRANCH=1 # in packer_git_checkout(), use the controller's current branch rather than `main`
    env KEEPFAIL=1 # does not `rm` the last .img on failure
"""

import os, sys
import argparse,subprocess, re, json, traceback, inspect
import tempfile, filecmp
from shlex import quote as shesc
from contextlib import contextmanager
from glob import glob
from textwrap import dedent

cli_args = None # so accessible to everybody, "exported" to your recipe

# rules
make_rules = {
    # this is Makefile stuff, escape the literal text here as you would for Makefile 
    #   (i.e. : -> \:, $ -> $$, shell, etc)
    # but it is for str.format, so escape { -> {{ and } -> }} if you need that literal
    # when you str.format(...), escape each argument-value as appropriate (rule escape for Makefile, recipe for shell)
    # name-of-rule-template : "str.format text...."

    # strategy is: next.img : previous.partition_table : previous.img

    'vars' :
        '#  to run the some.yml first, then the expected ansible yml via\n' +
        '#      packer_ansible() # in a recipe, via\n' +
        '#      tools/packer-layers.py --ansible-pre $some.yml ...\n' +
        '#      will add a makevar:\n' +
        '#      make ansible_pre=$some.yml ...\n' +
        '#      for ansible provisioner command \n' +
        '#  # for the first packer-ansible layer only\n' +
        'ansible_pre :=""# cli specified value\n' +
        'xansible_pre=$(ansible_pre)$(eval xansible_pre:=)# first time value, then ""',

    'partition_table' : 
        '{next_image}.partition_table : {previous_image}.partition_table\n' +
        '\t## $@' +
        '\t# we assume the same partitioning for the pi os\n'+
        '\tcp "$<" "$@"',

    'next_image' :
        '# next.img : previous.partition_table.json : previous.img\n' + 
        '#   we put `previous.img` first for convenience for $$< and only "make" escaping\n' +
        '{comment}\n' +
        '{next_image} : {previous_image} {previous_image}.partition_table.json {more_deps}\n' +
        '\t## $@\n' +
        '\t# chown because on previous failure it was root.root\n' +
        '\t[ -e "$@" ] && sudo chown $$USER:$$(groups $$USER | awk \'{{print $$3}}\') "$@" || true\n' +
        '\t# `cp` because packer won\'t "unpack" a "previous.img"\n' +
        '\tcp $< $@\n' +
        '\t# partition-table to make us is based on previous partition-table\n' +
        '\t# `sudo` for losetup etc.\n' +
        '\techo HOME $$HOME\n' +
        '\t@# with messy logic for ansible_pre\n' +

        "{packer_commands}" +
        #'\tPACKER_CONFIG_DIR=/root sudo env PACKER_LOG=1 packer build --var baseimage=$< --var nextimage=$@ --var-file="$<.partition_table.json" {more_packer_vars} {packer_file} || ( if [ "$$KEEPFAIL" == "" ]; then sudo rm "$@"; else sudo chown $$USER:$$(groups $$USER | awk \'{{print $$3}}\') $@; fi; false )\n' +
        #'\tPACKER_CONFIG_DIR=/root sudo env PACKER_LOG=1 packer build --var baseimage=$< --var nextimage=$@ --var-file="$<.partition_table.json" {more_packer_vars} { "$(if $(xansible_pre),--var ansible_pre=$(ansible_pre),,)" if more_packer_vars.contains("--var ansible_file=") else "" } {packer_file} || ( if [ "$$KEEPFAIL" == "" ]; then sudo rm "$@"; else sudo chown $$USER:$$(groups $$USER | awk \'{{print $$3}}\') $@; fi; false )\n' +
        '\t# chown because it was `sudo packer`\n' +

        '\tsudo chown $$USER:$$(groups $$USER | awk \'{{print $$3}}\') $@',
    'packer_command' : (
        # the actual packer command line
        # for use in each "next_image" make target, used by APackerCommand::make_command()
        '\tPACKER_CONFIG_DIR=/root sudo env PACKER_LOG=1 packer build --var baseimage=$< --var nextimage=$@ --var-file="$<.partition_table.json" {more_packer_vars} {packer_file} || ( if [ "$$KEEPFAIL" == "" ]; then sudo rm "$@"; else sudo chown $$USER:$$(groups $$USER | awk \'{{print $$3}}\') $@; fi; false )'
        ),
    'symlink' :
        '# Alias\n' +
        '{alias} : {previous_image}\n' +
        '\t## $@\n' +
        '\tln -f -s $< $@\n'
}

def our_asset(name):
    """Find dirname(us)/$name for ./ and dirname(realpath(us))/
        i.e. use our asset, or allow override by a local copy (e.g. "us" is linked to tools/packer-layers.py
    """
    #our = os.path.splitext(os.path.realpath(os.sys.argv[0]))[0] # realpath(us)/packer-layers
    our = os.path.dirname( os.path.dirname(os.path.realpath(os.sys.argv[0]))) # dirname( dirname(realpath(us))/... )
    #relative = os.path.join( os.path.basename( os.path.dirname( our )), os.path.basename( our )  )
    
    candidates = ( os.path.join(dir,name) for dir in ('.', our) )
    #print(f"# Cand: {candidates}")
    asset = next( (p for p in candidates if os.path.exists(p) ), None)
    if not asset:
        raise Exception(f"Couldn't find '{name}' in ., {our}")
    #print(f"# found {asset}")
    return asset

# we'll copy and append to this content
base_makefile = our_asset( "tools/packer-layers/packer-layers.makefile" ) # relative to $us
packer_ansible= our_asset( "tools/packer-layers/packer_ansible.pkr.hcl" ) # relative to $us

import logging
class RelativePathFilter:
    def filter(self,record):
        record.relativepath = os.path.relpath(record.pathname, os.path.dirname(sys.argv[0]))
        return True

try:
    logging_level=[ logging.WARNING, logging.INFO, logging.DEBUG ][ int(os.getenv('DEBUG',1)) ]
except IndexError:
   sys.stderr.write("Expected DEBUG=0|1|2\n")
   sys.exit(2)
logging.basicConfig(
    level=logging_level,
    format='[ %(asctime)s %(relativepath)s:%(lineno)s ] %(message)s', 
    datefmt='%Y-%m-%d %H:%M:%S'
)
logger = logging.getLogger( os.path.splitext(os.path.basename(__file__))[0] )
logger.addFilter(RelativePathFilter())
debug=logger.debug; info=logger.info; warning=logger.warning; error=logger.warning; critical=logger.critical

explicit_layer_name = None # see with_layer_name()
current_packer_layer = None # PackerCommands() # noop entry
seen_layer_names = {} # { cache_dir : Set( of names ), ... } # so we can prevent repeats
image_cache_dir = None

@contextmanager
def dynamic_var(name, value):
    """dynamic value for global"""
    
    was = globals()[name] if name in globals() else None
    try:
        globals()[name] = value
        yield
    finally:
        globals()[name] = was

# DSL:
@contextmanager
def layer_image_cache(dir):
    """Sets the global `image_cache_dir` for this with-block.
    The default is the dirname of the last `baseimage()`
    """
    
    if not os.path.exists(dir):
        info(f"Made {dir}")
        os.makedirs( dir, exist_ok=True)

    # we'll need the absolute path for the makefiles.
    if os.path.abspath(dir) != image_cache_dir:
        info(f"Now image cache: {image_cache_dir}")
    with dynamic_var('image_cache_dir', os.path.abspath(dir)):
        yield
        #debug(f"Exiting w/image_cache('{image_cache_dir}')")
    #debug(f"## -> '{image_cache_dir}'")

# DSL:
def layer_baseimage(file, use_previous_partition_table=False):
    """sets the `baseimage` for the next packer layer,
        unxz's
        propagates the previous partition
        (and sets the image_cache dir to dirname(file) cf `with image_cache(x)` )
    """
    global _baseimage,rules, image_cache_dir
    if '_baseimage' not in globals():
        _baseimage=None

    # don't remake
    # though you could remake if something is in-between (probably not good?)
    if file == _baseimage:
        image_cache_dir = os.path.dirname(file)
        return file

    previous_partition_table = None
    if use_previous_partition_table:
        previous_partition_table = f"{_baseimage}.partition_table"
                
    _baseimage=file

    info(f"baseimage {_baseimage}")

    if (parts:=os.path.splitext(file))[1] == '.xz':
        # there is an implicit rule for .img.xz -> .img
        # so, just continue as if .img
        _baseimage=parts[0]
        info(f"baseimage {_baseimage}")

    # the default rule is: 
    #   .img.partition_table : .img; sfdisk ...
    #   .img : .xz; unxz ...
    # but for propogating, we do cp on the explicit table
    if use_previous_partition_table:
        rules.append( 
            make_rules['partition_table'].format(
                next_image=_baseimage, 
                previous_image=previous_image
            )
        )

    image_cache_dir = os.path.dirname(file)
    return _baseimage

# DSL:
@contextmanager
def layer_layer_name(name):
    """`with layer_name($x):`
        all this stuff should apply to only 1 layer,
        e.g. 2 packer_ansible() commands
        if `name` is not null, use that as the explicit layer name,
            otherwise uses the first command for the name
        Note that nested `with layer_name()` only work properly if the `with` is in the tail position
        (a None `name` (`explicit_layer_name`), means "Don't group commands into a single layer, each is a layer")
    """    
    global current_packer_layer, seen_layer_names

    # gaurd against repeating a layer name
    if image_cache_dir not in seen_layer_names:
        seen_layer_names[image_cache_dir] = set()
    if name in seen_layer_names[image_cache_dir]:
        raise Exception(f"Duplicate layer name for `with image_cache('{image_cache_dir}'): {name}")
    seen_layer_names[image_cache_dir].add(name)

    with dynamic_var('explicit_layer_name', name):
        with dynamic_var('current_packer_layer', PackerCommands()):
            debug(f"INTO layer {name}")
            yield
            debug(f"EXIT layer {name}") #, packers {current_packer_layer}")
            debug(f"Packer commands\n" + "\n".join( x.make_command() for x in current_packer_layer.packers))
            generate_packer_make()
    # run the `post` layer op to actually create the layer

# DSL:
def layer_packer_packages( *packages ):
    """install packages"""
    layer_packer_ansible( 'tools/packer-layers/packages.ansible.yml', packages=packages )

def layer_packer_pip( **kwargs ):
    """install pip-venv modules
        user= default "pi",
        requirements_file= e.g. 'build/requirements.txt',
        venv= e.g.  f'/home/pi/{project}/.venv',
        python= default 'python3'
        modules= e.g. "ansible>0.9.0,passlib"
        update_cache= default = 'no', e.g. yes=apt update
    """
    allowed = ['user','venv','python','requirements_file','modules', 'update_cache']
    at_least = [ 'requirements_file','modules']
    for k in kwargs.keys():
        if k not in allowed:
            raise Exception(f"unexpected argument: {k}. allowed: {allowed}")
    if not next( ( x for x in kwargs.keys() if x in at_least ), None):
        raise Exception(f"need at least one of: {at_leaset}")
        
    layer_packer_ansible( 'tools/packer-layers/pip.ansible.yml', **kwargs )

def layer_packer_packages( *packages ):
    """install packages"""
    layer_packer_ansible( 'tools/packer-layers/packages.ansible.yml', packages=packages )

def layer_packer_enable_services( *services ):
    """disable services"""
    layer_packer_ansible( 'tools/packer-layers/services.ansible.yml', enable=services )

def layer_packer_disable_services( *services ):
    """disable services"""
    layer_packer_ansible( 'tools/packer-layers/services.ansible.yml', disable=services )

@contextmanager
def possible_update( filename ):
    """only update the file if it would change:
        yields with a file-handle for writing,
        which is may be a temp file,
        which is diffed with extant file,
        and only if different is the `filename` updated
    """
    
    if not os.path.exists(filename):
        with open(filename, 'w', encoding='utf-8') as fh:
            yield fh
        info(f"Made {filename}")

    else:
        # have to make tempfile, compare, and maybe replace the `filename`
        tempname = None
        with tempfile.NamedTemporaryFile(mode='w+t', encoding='utf-8', delete=False ) as fh:
            tempname = fh.name
            yield fh

        if filecmp.cmp( tempname, filename ):
            debug(f"No change: {filename}")
            os.remove( tempname )
        else:
            debug(f"Newer: {filename}")
            info(f"Made {filename}")
            os.remove( filename )
            os.rename( tempname, filename )

# DSL:
def layer_packer_ansible(ansible_file, **ansible_vars):
    """Make a packer-layer, using just ansible.
        .img name is ...:$basepartof($ansible_file).img
        `ansible_vars` become a .json file, and are available in the ansible.yml. should be { k:v, ... } at top level
    """
    #debug("##FIXME need unique ansible-vars across whole image_cache_dir (between makefiles!)")

    index = len(current_packer_layer.packers) # if multiple ansible's in a layer, need to distinquish
    debug(f"packer_ansible, ans {ansible_file}")
    ansible_var_file = replace_version_part( _baseimage, ansible_file) + f".ansible-var-{index}.json"
    debug(f"ansible_var_file {ansible_var_file}")
    with possible_update( ansible_var_file ) as fh:
        json.dump( ansible_vars, fh)

    # see `--ansible-pre` for xansible_pre, and 'vars' in `make_rules`
    layer_packer_layer( packer_ansible, our_asset(ansible_file), ansible_var_file, _name_as=ansible_file, ansible_file=our_asset(ansible_file), ansible_var_file=ansible_var_file, ansible_pre='$(xansible_pre)' )

def replace_version_part(image_name, file_name):
    """replace the version in .../.../blah:version.img with
    the file_name (without extensions)
    `with layer_name(x): ...` (`explicit_layer_name`) will override this.
    `with image_cache(x): ...` will replace the dirname
    """

    #debug(f"## replace version in: {image_name}")

    if explicit_layer_name:
        debug(f"## explicit layer: {explicit_layer_name}")
        file_name = explicit_layer_name

    else:
        # strip extensions
        debug(f"## `name` from {file_name}")
        while re.search(r'\.[^.]*$', file_name):
            file_name = os.path.splitext(os.path.basename(file_name))[0]
        #debug(f"## `name` stripped version {file_name}")

    new_image = f"{os.path.splitext(image_name)[0]}"
    #debug(f"## img w/o extension: {new_image}")
    #debug(f"## imag cache: {image_cache_dir}")
    if not image_cache_dir:
        raise Exception(f"Expected the DSL step to be in `with image_cache('some dir')`, saw: {image_cache_dir}")
    new_image = os.path.join( image_cache_dir, os.path.basename(new_image) ) # often a noop
    #debug(f"## splitext {new_image}")

    # remove the :... name I've added before
    while m:=re.search(r'^(.+):[^:]+$', new_image):
        new_image = m.group(1)
    new_image += f":{file_name}.img"
    #debug(f"## replaced {new_image}")
    return new_image

# DSL:
_packer_layer_ct=0 # actually executed
_packer_layer_i=0
_first_real_layer=True # need for FORCE

class PackerCommands:
    """A set of packer commands for one image layer"""

    class APackerCommand:
        def __init__(self, packer_file, packer_vars):
            self.packer_file = packer_file
            self.packer_vars = packer_vars
        def __str__(self):
            return f"packer ... --vars {self.packer_vars} {self.packer_file}"
        def make_command(self):
            """fully make-escaped, indented, possible multi-line (w/ \)"""
            # more_packer_vars packer_file
            # fixme: confusing: {v} probably needs to be shell-escaped, but I supply the literal "" for ansible_pre
            more_packer_vars = " ".join( [ ('--var '+f"{k}={v}") for k,v in self.packer_vars.items() ] )
            return make_rules['packer_command'].format(
               packer_file = shesc(our_asset(self.packer_file)),
               more_packer_vars = more_packer_vars
            )

    def __init__(self):
        self.packers = []
        self.deps = []
        self.name_as = None
        self.calling_info = None
        self.image_cache_dir = None
    def add(self, packer_file, deps, packer_vars, name_as=None ):
        """add one command to the layer"""
        global current_packer_layer # so we can generate NOW when not grouping commands into a layer (layer==None)

        # find where this layer_x was called, for error reporting during add
        # fixme: really should be closer to the with layer_name ?
        # fixme: and the PackerCommands should capture this layer_x call (for us in makefile comments, etc.)
        last = None
        for tb in traceback.extract_stack():
            if tb.name.startswith('layer_'): # and tb.filename != __file__:
                break # tb is the called, last is the callee
            last = tb # [ tb.name, tb.filename, tb.lineno ]

        self.calling_info = { 'calling' : tb.name, 'callee' : [ last.name, last.filename, last.lineno ] }
        debug(f"ADD to layer {explicit_layer_name}: {tb.name} AT {last}")

        if not self.name_as and name_as:
            self.name_as = name_as # only uses first one
        # deps are top-level
        self.deps.append( packer_file )
        self.deps.extend(deps)
        self.packers.append( self.APackerCommand( packer_file, packer_vars ) )
        self.image_cache_dir = image_cache_dir # because generate_packer_make() runs after a `with image_cache` exits
        debug(f"Packer Accum: {packer_file} :: {deps} AS { self.packers[-1] }")

        if not explicit_layer_name:
            debug(f"  Seperate layer, add now")
            generate_packer_make()
            current_packer_layer = PackerCommands()
            debug(f"  (ADD) reset current_packer_layer to {current_packer_layer}")

    def __str__(self):
        return (
            f"\n\t: {self.deps}\n\t\t" +
            "\n\t\t".join( [str(x) for x in self.packers] )
        )
        
def layer_packer_layer(packer_file, *deps, _name_as=None, **packer_vars):
    """Create the command to run packer with the appropriate args
        `deps` is a list of files we should depend on for make-logic
        `packer_vars` become packer-vars via cli --var k=v. so quoting can become an issue
        resulting .img is named from the previous .img, using the packer_file name (or the name_as file name)
    """
    debug(f"Packer-layer command {packer_file}")
    # the added info is resolved as a makefile rule/steps by generate_packer_make
    # in `with layer_layer_name()` or at `APackerCommand.add()`
    current_packer_layer.add(
       name_as = _name_as, # only first one per layer is allowed
       packer_file = shesc(our_asset(packer_file)),
       deps = deps,
       packer_vars = packer_vars
    )

def generate_packer_make():
    # using current_packer_layer
    # Actually generate the make rule/steps for a layer
    # Named layers can group (`with layer_layer_name()`), `None` layer makes a layer per layer_packer_layer() call

    if not current_packer_layer:
        error("Expected a current_packer_layer")
        return
    if len(current_packer_layer.packers) == 0:
        return # no packer layer

    global _packer_layer_ct, _packer_layer_i, _baseimage, _first_real_layer

    # the resulting image is named after the packer file
    packer_file = current_packer_layer.packers[0].packer_file
    name_as = current_packer_layer.name_as

    debug(f"## ------ GENERATING: {current_packer_layer}")
    with layer_image_cache( current_packer_layer.image_cache_dir ):
        if not image_cache_dir:
            at = f"{current_packer_layer.calling_info['calling']}() at {current_packer_layer.calling_info['callee']}" 
            raise Exception(f"Expected the DSL step to be in a `with image_cache('some dir')`, saw: {image_cache_dir}, for {at}")
        new_image = replace_version_part( _baseimage, name_as or packer_file )
        if shesc(new_image) != new_image:
            raise Exception(f"This doesn't handle anything in the layer-name that requires shell-escaping (e.g. spaces): {new_image}")

        # skip if START=somename, and we aren't "somename"
        #   or if START=n and we are <n
        if (
            (l:=cli_args.start_layer)
            and (_packer_layer_i < int(l) if re.match(r'\d+$',l) else not re.search(':' + re.escape(l) + r'\.img$', new_image) )
            ):
            warning(f"SKIPPED initial layer {_packer_layer_i}: {new_image}")
            _baseimage = new_image
            _packer_layer_i+=1
            return
        # convert a "somename" to n so we can START<=i to continue
        if ( (l:=cli_args.start_layer) and not re.match(r'\d+$',l) ):
            cli_args.start_layer = str(_packer_layer_i) # hack, so we continue from here

        # skip if at or past LAST_LAYER
        if (l:=cli_args.last_layer):
            if re.match(r'\d+$',l) and _packer_layer_ct >= int(l):
                # layer-ct: N
                warning(f"SKIPPED later layer {_packer_layer_i}: {new_image}")
                _packer_layer_i+=1
                return
            elif re.search(':' + re.escape(l) + r'\.img$', new_image):
                # layer-name
                # but, we want to do THIS layer, and start skipping after
                # so, change to LIMIT=N
                cli_args.last_layer = str(_packer_layer_ct + 1) # limit of actual layers, a count, not the abs index
                warning(f"  SKIP AT [{_packer_layer_i + 1}], AFTER {new_image}")

        (info if cli_args.last_layer or cli_args.start_layer else debug)(f"Layer [{_packer_layer_i}] < {new_image}")

        _packer_layer_i+=1
        _packer_layer_ct+=1

        debug(f"Make a layer {new_image}")
        debug(f"  on {_baseimage}")
        debug(f"  in {image_cache_dir}")
        debug(f"  packer {packer_file}")
        if os.path.exists(packer_file):
            debug(f"  accumulate...")
        else:
            error(f"Missing: {packer_file}")

        if _first_real_layer:
            _first_real_layer = False
            if cli_args.force_remake:
                if os.path.exists(new_image):
                    os.unlink(new_image)
                info(f"  FORCE remake, deleted {new_image}")

        packer_commands = "\n".join( [x.make_command() for x in current_packer_layer.packers] ) + "\n"
        # e.g. ansible layers have a dep of packer_ansible.pkr.hcl, so it would be repeated
        unique_deps = { k:None for k in current_packer_layer.deps } # preserves order
        more_deps = " ".join( [make_escape(x) for x in unique_deps.keys()] )
        rules.append(
            make_rules['next_image'].format(
               # FIXME: each packer_command should capture the location and add the comment
               comment = f"# {current_packer_layer.calling_info['calling']}() at {current_packer_layer.calling_info['callee']}",
               next_image = make_escape(new_image),
               previous_image = make_escape(_baseimage),
               more_deps = more_deps,
               packer_commands = packer_commands
            )
        )

        _baseimage = new_image

def make_escape(f):
    """for Makefiles, escaping targets/dependents, only : sofar"""
    rez = re.sub(':','\\:',f)
    return rez

def layer_file_contents(file=None, file_list=None):
    """for use in arguments: returns the contents of the file.
        converts from .json when that extension is ANYWHERE in the filename.
        If `file_list` = "some*glob" or [ a list of names ], will produce a [ list of contents ]
        Only one of `file` or `file_list`.
    """
    if file and file_list:
        raise Exception(f"Only one of `file` or `file_list`, saw file='{file}', file_list={file_list}")
    
    if file:
        file_list = [ file ] # will convert back to a scalar later
    if isinstance(file_list,str):
        # gave me a glob, not a list
        file_list = [ file_list ]
    debug(f"  file-contents from {file_list}")

    # expand globs
    expanded_list = []
    for a_file in file_list:
        if re.search(r'[^\\]\*', a_file):
            expanded_list.extend( [x for x in glob(a_file)] )
        else:
            expanded_list.append( a_file )
    debug(f"  expanded-file-contents from {expanded_list}")

    content = []
    for a_file in expanded_list:
        debug(f"    a-file-contents from {a_file}")
        if re.search(r'\.json(\.|$)', a_file):
            with open(a_file,'r') as fh:
                try:
                    content.append( json.load(fh) )
                    debug(f"    json -> {content[-1]}")
                except json.decoder.JSONDecodeError as e:
                    sys.stderr.write(f"IN {a_file}:\n")
                    raise e
        else:
            with open(a_file,'r') as fh:
                content.append( fh.read() )
                debug(f"    txt -> {content[-1]}")

    if file:
        return content[0]
    else:
        return content

def layer_packer_git_checkout(layer_prefix='app-git-', git_branch='main', user='pi',project=None):
    """git checkout (clone)
        To ~$user/$project
            `project` defaults to basename(remote url) w/o extension. 
            e.g. git@gitlab.com:labworx/pi-image-maker.git -> pi-image-maker
        Using the host current .git/config, remote.origin.url
        Shallow checkout (depth=1)
        From/as the `main` branch
            env USEDEVBRANCH=1 overrides branch: current branch name (i.e. "use this one")
        `layer_prefix` -> layer name {prefix}-{branch}

        Returns the checkout_dir path
    """
    # FIXME: factor to tools/packer-layers.py
    import subprocess,re

    # let the branch be overridable
    # FIXME: how would you use a .dev file here?
    if os.getenv('USEDEVBRANCH', None) != None:
        git_branch = subprocess.run(['git', 'branch', '--show-current'], stdout=subprocess.PIPE, check=True, encoding='utf-8').stdout.rstrip()

    # nb: will change to http: (aka read-only)
    git_origin = subprocess.run(['git', 'config', 'remote.origin.url'], stdout=subprocess.PIPE, check=True, encoding='utf-8').stdout.rstrip()
    # remote read-only
    # git@gitlab.com:labworx/shimadzu-spect-capture.git
    # https://gitlab.com/labworx/shimadzu-spect-capture.git
    git_origin = re.sub(r'^git@([^:]+):', 'https://\\g<1>/', git_origin )
    # the project ~= device.
    if project==None:
        project = os.path.splitext( os.path.basename( git_origin) )[0]

    checkout_dir = f"/home/{user}/{project}"

    # make it easy to keep dev-branch-layers around, and not stomp 'main' layer
    with layer_layer_name(f'{layer_prefix}{git_branch}'):
        # I used to do a git-shallow-clone on the host, copy to target machine, git-clone from there, then git-fetch
        layer_packer_ansible('tools/packer-layers/git.ansible.yml',
            # need a `user` for git ops on the device
            # FIXME: what is best practice?
            user = user,
            # FLAG:
            config_global = [ [ "user.email", user+"@{{ ansible_hostname }}" ], [ "user.name", project ] ],
            # FLAG:
            repo = git_origin,
            depth = 1, # from `pwd`, clones from there then pull-origin
            single_branch = True,
            version = git_branch,
            dest = checkout_dir
        )

    return checkout_dir
        
def layer_packer_raspi_config( **command_value ):
    """Use raspi-config to enable/disable/set things
        a list of [ "$cmd", "$value" ] for `raspi-config`
        e.g. [ ['do_ssh', '0' ], ... ]
        Inspect /usr/bin/raspi-config for possibilities and specific values.
            NB: enable/disable doesn't always map true:1, false:0
            (search for `case ..` and corresponding function, and/or do_x() functions)
        See `tools/packer-layers/communications.ansible.yml` for ssh, mdns, bluetooth, and serial_console enable
    """

    commands = [ ['raspi-config', 'nonint', c,v] for c,v in command_value.items() ]
    print("## Commands")
    layer_packer_ansible( 'tools/packer-layers/1command.ansible.yml', cmds=commands )

def layer_packer_raspi_config_delayed( **command_value ):
    """Use raspi-config to enable/disable/set things
        BUT, at first boot time (some config can't be done on the chroot .img)
        Arguments as per `packer_raspi_config()`
        e.g. do_i2c=0 # enable
    """
    global _packer_raspi_config_delayed_i
    if not '_packer_raspi_config_delayed_i' in globals():
        _packer_raspi_config_delayed_i = -1 
    _packer_raspi_config_delayed_i += 1

    commands = [ ['raspi-config', 'nonint', c,v] for c,v in command_value.items() ]
    layer_packer_boot_commands(commands, name=f'-raspi-config-{_packer_raspi_config_delayed_i}')

def layer_packer_boot_commands(cmds, name=None):
    """Runs commands on first boot.
        Makes a new `/etc/init.d/setup_once-$name.init` for each set of cmds
        (from template tools/packer-layers/setup_once.initd)
        `name` defaults to 0..n (auto-incrementing)
        `cmds` must be a list of:
            "cmd arg arg" # properly quoted bash line
            or
            [ cmd,arg,arg ] # words that will be shell-escaped to a single bash line
                e.g. " ".join( [shesc(w) for w $inthat] )
        If you want to reboot after your commands, add a command
            'REBOOT=true'
    """
    global _packer_boot_commands_i
    if not '_packer_boot_commands_i' in globals():
        _packer_boot_commands_i = -1 
    _packer_boot_commands_i += 1

    quoted = []
    for cmd in cmds:
        if isinstance(cmd,list):
            quoted.append( " ".join( [shesc(str(w)) for w in cmd] ) )
        else:
            quoted.append( cmd )

    layer_packer_ansible( 
        'tools/packer-layers/initd.ansible.yml', 
        src = our_asset('tools/packer-layers/setup_once.initd'),
        dest = f"setup_once{name or _packer_boot_commands}.initd",
        initd_enable = True,
        # For interpolation into the init.d template
        cmds = "\n".join( quoted ), 
    )
        
_aliases=[]
def layer_alias(alias):
    """Create a link to the last image as `alias`
    """

    link = os.path.dirname(_baseimage) + f"/{alias}.img"
    link = replace_version_part( _baseimage, f"/{alias}.img" )
    info(f"Alias: {link}")
    rules.append( 
        make_rules['symlink'].format(
            previous_image = make_escape(_baseimage), 
            alias = make_escape(link)
        )
    )
    _aliases.append( link )

def main():
    global rules,_baseimage,cli_args
    rules = []

    # collect some info before we parse, so --help can see it
    dsl_help = [ "# DSL for recipes", "# these are imported into your recipe","" ]
    environment = {
        'os' : os,
        'sys' : sys,
        'cli_args' : cli_args,
        'debug' : debug,
    }
    for m_name in globals():
        if (
            # nb: method:=
            # nb: fixme: better mechanism for non layer_x methods like 'debug'
            ( m_name.startswith('layer_') or m_name == 'debug')
            and callable(method:=globals()[m_name])
            ):
            if m:=re.match(r'layer_(.+)', m_name):
                # fixup for layer_x names -> x
                dsl_name = m.group(1)
                environment[dsl_name] = method
            else:
                dsl_name = m_name
            dsl_help.append( f"{dsl_name}{inspect.signature(method)}\n\t{method.__doc__}" )

    parser = argparse.ArgumentParser(
        description=__doc__, 
        formatter_class=argparse.RawTextHelpFormatter,
        epilog = "\n".join(dsl_help)
    )
    parser.add_argument('recipe', help="recipe file, a .py using the DSL below")
    parser.add_argument('baseimage', help="the initial $baseimage, default is none (commonly specified in the recipe)", nargs='?')
    parser.add_argument('--list', help="List the layers (same as: -start 999 ...)", action='store_true', )
    parser.add_argument('--nomake', help="stop after creating var & makefiles, don't actually make the img", action='store_true', )
    parser.add_argument('--config', help="a .py file, variables/fns will be visible in your recipe")
    parser.add_argument('--force-remake', '--force', help="forces re-make of the 1st layer-to-be-built by deleting it, which causes re-make of it and following layers", action='store_true')
    parser.add_argument('--ansible-pre', help=dedent("""\
        an ansible playbook.yml to run before the first `packer_ansible()`,
        like injecting a new `packer_ansible()` command.
        e.g. "dynamically" doing a git-pull: --ansible-pre pre-ansible-git-pull.yml
        does not modify the makefiles, etc., so does not re-make, and is not persistent.
        typically, you have to use --force to re-make using this pre-ansible playbook 
            (and thus probably want to use --start)
        """))
    parser.add_argument('--start-layer', '--start', help=dedent("""\
        N, or Name
        Skip till the indicated layer,
        Can be a number, starting from 0 (try --list to see numbering),
        or the name, 
            the arg to layer_name(...),
            or the implicit layer name, the portion ...:$layer-name.img
        Warning: this creates a new .makefile, with only the commands starting at START_LAYER
            the .makefile is run to make the .img's, so:
            * previous .img's must already exist
            * if this is the last layer, then you have to re-run w/o START_LAYER to re-create a full (non-skippling) final .makefile
        """))
    parser.add_argument('--last-layer', '--last', '--limit', help=dedent("""\
        Skip after the indicated layer,
        Can be a _count_, starting from 1,
        or the name, 
            the arg to layer_name(...),
            or the implicit layer name, the portion ...:$layer-name.img
            nb: skips _after_ the layer
        """))
    #parser.add_argument('--dir', help="cache directory for the images, default is '.'", default='.')

    cli_args = parser.parse_args()

    if cli_args.list:
        cli_args.start_layer = '999'
        cli_args.nomake = True

    if cli_args.baseimage:
        layer_baseimage( cli_args.baseimage )

    # get the config vars/methods
    if cli_args.config:
        with open(cli_args.config, mode='r') as fh:
            config = fh.read()
            compiled_config = compile( config, cli_args.recipe, mode='exec')
            config = None

        # executes the dsl, populates `rules`
        config_environment={}
        exec( compiled_config, config_environment )
        del config_environment['__builtins__']
        debug(f"# Config: {config_environment}")
        environment.update( config_environment )
        
    with open(cli_args.recipe, mode='r') as fh:
        code = fh.read()
        compiled_recipe = compile( code, cli_args.recipe, mode='exec')
        code = None

    # executes the dsl, populates `rules`
    # we always have to be in a layer_name()
    with layer_layer_name(None):
        exec( compiled_recipe, environment )

    makefile = f"{_baseimage}.makefile"
    debug(f"Makefile: {makefile}")

    with open(makefile,'w') as fh:

        # default target
        symlinks = ' '.join( [make_escape(x) for x in _aliases])
        fh.write( 
            f".PHONY : final_image\n" +
            f"final_image : {make_escape(_baseimage)} {make_escape(_baseimage)}.partition_table.json {symlinks}\n"+
            "\t## Final Image\n"+
            f"\t## $<\n" +
            "\n" 
        )

        fh.write( make_rules['vars'] + "\n\n" )

        # copy our basic stuff
        with open( base_makefile, 'r') as ih:
            for x in ih:
               fh.write( x )

        # add our explicit rules
        if rules:
            fh.write("\n# .img dependencies, and explicit propagate partition_tables\n\n")
        for r in rules:
            fh.write( r ); fh.write("\n")
            fh.write("\n")
    info(f"Made {makefile}")

    cmd = ['make', '-f', makefile ]

    if cli_args.ansible_pre:
        cmd.append(f"ansible_pre={cli_args.ansible_pre}")

    ( print if cli_args.nomake else debug)("final: "+" ".join( [shesc(x) for x in cmd] ))

    if not cli_args.nomake:
        info( " ".join( [shesc(x) for x in cmd] ) )
        subprocess.run(cmd, check=True)
main()
