#!/usr/bin/env perl
# make a .img from pi-sdcard
#   or make a sdcard from a .img
# --- out .img # will ask before making changes. suggests which /dev
# --- in [.img] # suggests which /dev. asks for .img if not provided
#   use /dev/stdout to stream output

use strict; use warnings; no warnings 'uninitialized'; use 5.012; no if ($^V ge v5.18.0), warnings => 'experimental::smartmatch';
use Carp; $SIG{__DIE__} = sub { Carp::confess @_ };

$|=1;

sub get_devices {
  my @devices;
  for my $line (`df -B1`) {
    chomp $line;

    # relevant /dev/*
    next if $line =~ /^\/dev\/loop/;
    next unless $line =~ /^\/dev\//;
    
    # remove obvioulsy wrong
    next if $line =~ / \/$/;

    push @devices, $line;
  }
  return @devices;
}

sub total_size {
  my $total = 0;
  for (@_) {
    # Filesystem        1B-blocks         Used   Available Use% Mounted  
    my ($dev, $blocks, $used, $free, $perc, $mount) = split /\s+/;
    $total += $used;
  }
  return $total;
}

sub main {
  while ( $ARGV[0] =~ /^-/ ) {
    if ( $ARGV[0] =~ /^(-h|--help)$/ ) {
      exec( 'awk', 'FNR==2,/^$/ {print}', $0 );
    }

    else {
      warn "Wat? ".$ARGV[0]."\n";
      exit 1;
      }
  }

  my $whichdir = shift @ARGV;
  die "Need in|out" if $whichdir !~ /^(in|out)$/;
  my $imgfile = shift @ARGV;

  if ($whichdir eq 'in') {
    system("grep VERSION_CODENAME /media/awgrover/rootfs/etc/os-release");
  }
  else {
    die "Need a .img file" if ! -e $imgfile;
    system('ls','-l', $imgfile);
  }

  # get device
  my @devices = get_devices();
  say join("\n",@devices);

  say "# Should be mounted...";
  print "Which block device (full, basename, optional `n` suffix): ";
  my $dev = <>;
  chomp $dev;
  $dev =~ s/\d$//;
  if ($dev !~ /\/dev\//) {
    $dev = "/dev/$dev";
  }
  say $dev;
  die "No $dev" if ! -e $dev;

  my @selected_devices = grep { /^$dev\d / } @devices;
  say "That's\n\t",join("\n\t",@selected_devices);

  my $bytes;
  if ( $whichdir eq 'in') {
    $bytes = total_size(@selected_devices);
  }
  else {
    $bytes = -s $imgfile;
  }
  printf "To copy $bytes %.2fM %.2fG\n",$bytes/(1024*1024),$bytes/(1024*1024*1024);

  my $count = $bytes/(1024.0*1024.0); # MB
  $count = int($count);
  #say "Round? ", ($bytes / (1024*1024)), " ? ", $count < ($bytes / (1024*1024));
  if ( $count < ($bytes / (1024*1024)) ) {
    $count += 1;
  }

  my ($dir, $infile, $outfile) = @{ { 
    in => [ "-> .img", $dev, $imgfile ],
    out => [ "-> sdcard $dev", $imgfile, $dev ],
  }->{$whichdir} };
  #say "$whichdir: dir '$dir', in '$infile', out '$outfile'";

  if (!$imgfile) {
    print "$dir (imgfile): ";
    $imgfile = <>;
    chomp $imgfile;
  }

  my @cmd;
  if ( $whichdir eq 'out') {
    push @cmd, ('sudo');
  }
  # `dd` time is dominated by the slowest device, which is the sd-card,
  # especially slow/older ones (20Mb/s!)
  # w/o `dsync`, dd caches (reads), leading to a confusing "stall" at the end while it fsyncs
  # all the writes to the slow channel,
  # which is about the same total length of time it would take w/o caching.
  # And I have "progress" turned on, so at least we get feedback.
  # Hoping that `nonblock` is slightly more efficient overlapping reads/writes
  push @cmd, ('time', 'dd', 'oflag=dsync,nonblock', "if=$infile", "of=$outfile", "status=progress");
  if ( $whichdir eq 'in') {
    push @cmd, ("count=$count", "bs=1M");
  }
  else {
    push @cmd, ("bs=4M");
  }

  say;
  say join(" ",@cmd);

  print "Execute $dir $count MB y|N: ";
  my $yn = <>;
  if ($yn !~ /^y/i) {
    exit 1;
  }

  if ($whichdir eq 'out') {
    system('sudo', '-p', 'sudo for dd password: ','echo','...') and die;
    for my $devline ( @selected_devices ) {
      my ($umountdev, @rest) = split(' ', $devline, 2);
      say "umount $umountdev";
      system('sudo', 'umount', $umountdev) and die;
    }
  }
  system(@cmd) and die;
  say "Copied, sync'ing...";
  system('sync');
  system('beep',$0);

  if ($whichdir eq 'out') {
    say "Eject and reinsert sd-card to check filesystem";
  }
}

main()
